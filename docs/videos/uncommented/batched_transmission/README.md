

# Videos

In this folder, two videos showing the batched transmission implementation are stored. 
Using the notation from the thesis,

- **Inorder 2021-07-12 16:31:47.mp4** shows SOU
- **Levelorder 2021-07-12 16:30:36.mp4** shows PLOU

