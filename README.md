

![](docs/images/europe_tiling.png "Europe Tiling")



# PATHFINDER-Visualization

This project uses a REST server (using the [pistache framework](https://github.com/oktal/pistache)) to allow accessing the PATHFINDER API (see below) via a interactive web-application written in JavaScript.

This code is an extension to the predecessor project  _PATHFINDER WEB_ which can be found [here](https://theogit.fmi.uni-stuttgart.de/ruppts/projektinf_pathfinder). For more information and a publicly available code repository, please scroll down.



#### Authors:

Lukas Baur
Tobias Rupp
Stefan Funke

 
University of Stuttgart
Formal Methods of Computer Science (**FMI**)





## Features

#### Redesigned Single Trajectory Plot

![](docs/images/saarland_plain.png "Plain trajectory plot")

#### Segment-Based Graph view

![](docs/images/saarland_segment_graph.png "Segment graph")

#### Heatmap

![](docs/images/saarland_heatmap.png "Heatmap")



#### Supports Trajectory-Tiling embeddings

![](docs/images/saarland_tiling.png "Tiling")



#### Pruned CH-Graph View

![](docs/images/germany_pruning.png "Pruning")

#### Edge-based retrial 

![](docs/images/saarland_ebpf.png "EBPF")





## Installation

There's an installation tutorial given at ``pathfinder_server/``. Take a look there.

The Frontend can be used without installation using modern web browser. It is located at ``pathfinder_client/index.html``. 



## Documentation

**Backend**

The main features are explained in ``docs/howTo.pdf``. There is also another readme-file in the corresponding code folder-



**Frontend**

Configuring the frontend is documented in `/pathfinder_client/docs.html`.



## Based on

#### PATHFINDERweb

An initial project developed at the University of Stuttgart for showing trajectories on a Web-Frontend, relying on a Pistache Server answering retrieval requests.

A repository close can be found [here](https://bitbucket.org/baurls/pathfinder-web/src/master/).



#### PATHFINDER

_(description taken from original authors)_

"PATHFINDER: Storage and Indexing of Massive Trajectory Sets" by Stefan Funke, Andre Nusser, Tobias Rupp, and Sabine Storandt. The paper will be presented at [SSTD 2019](http://sstd2019.org/) in Vienna.

It is a project to build a fast and flexible data structure for the storage and retrieval of paths in a road network graph. The core idea is that the paths are compressed using a Contraction Hierarchy (CH) and then retrieved using the R-tree like structure of the CH.
(https://gitlab.com/anusser/pathfinder)



#### Pistache

_(description taken from original authors)_

Pistache is a C++ REST framework written by Mathieu Stefani at Datacratic. It is written in pure C++11 with no external dependency and provides a low-level HTTP abstraction. (https://github.com/oktal/pistache)





## License

Apache License 2.0

