/**
 * PathfinderWeb functionalities
 *
 * 
 * Basic Code (Project Inf)
 * @author Shoma Kaiser | 3133314 | st142241@stud.uni-stuttgart.de
 * @author Patrick Lindemann | 3335811 | st155665@stud.uni-stuttgart.de
 * @author Enis Spahiu | 3129195 | st141173@stud.uni-stuttgart.de
 *
 * Extentions & Modifications (Master's Thesis)
 * @author Lukas Baur | 3131138 | st141998@stud.uni-stuttgart.de
 *
 * @version 2021/11/27
 * 
*/

const EDGE_PLOT_WEIGHT_TYPE__TRANSPARENCY = "transparency"
const EDGE_PLOT_WEIGHT_TYPE__THICKNESS = "thickness"

class Pathfinder {

    constructor(apiKey, options = {}) {

        /* Options container */

        this._o = defaults;
        this._batch_decoder;

        if (isEmpty(apiKey)) {
            alert('No Api key given! A mapbox access token is required to make the map work. Please set it in the scripts.js.');
            alert('For more information, visit https://docs.mapbox.com/help/how-mapbox-works/access-tokens/');
            return;
        } else {
            this._o.apiKey = apiKey;
        }


        // Set the defined options if they exist
        if (!isEmpty(options) && options !== {}) {
            for (let key in this._o) {
                if (!isUndefined(options[key]))
                    this._o[key] = options[key];
            }
        }

        this._calculated = false;
        this.heatmap_layer_visible = this._o.use_heatmap_layer;

        // Save the initial options for a possible reset
        this._initialOptions = $.extend(true, {}, this._o);

        /* Shape container */

        this._c = new ShapeContainer();

        /* Sidebar container and hooks */

        this._s = new SidebarContainer();

        this._addHook(this._s.button('rectangle', 'draw$'), 'click', e => {
            if (this._m.state() == 'drawing')
                return;
            else if (this._m.state() == 'created')
                this._m.reset('all');

            // Retrieve the values
            var min_lat = this._s.fieldVal('rectangle', 'min-lat$');
            var min_lon = this._s.fieldVal('rectangle', 'min-lon$');
            var max_lat = this._s.fieldVal('rectangle', 'max-lat$');
            var max_lon = this._s.fieldVal('rectangle', 'max-lon$');
            
            if (min_lat && min_lon && max_lat && max_lon) {
                var min = stringToNode(min_lat, min_lon);
                var max = stringToNode(max_lat, max_lon);

                this._c.node('rectangle', 'min', min);
                this._c.node('rectangle', 'max', max);
                this._m.draw('rectangle', { rectangle: { min, max } }, this._o);
                this._c.type('rectangle');
            } else {
                alert("One or more inputs are undefined!");
            }
        });


        this._addHook(this._s.field('exportType', 'export-traj$'), 'click', e => {
            this._reset_request_settings();
            this._o.exportType = 'trajectories';
        });
        this._addHook(this._s.field('exportType', 'export-segmentgraph$'), 'click', e => {
            this._reset_request_settings();
            this._o.exportType = 'segment_graph';
        });
        this._addHook(this._s.field('exportType', 'export-heatmap$'), 'click', e => {
            this._reset_request_settings();
            this._o.exportType = 'heatmap';
        });
        this._addHook(this._s.field('exportType', 'export-batch-plain-inorder$'), 'click', e => {
            this._reset_request_settings();
            this._o.exportType = 'batch_plain_inorder';
        });
        this._addHook(this._s.field('exportType', 'export-batch-plain-levelorder$'), 'click', e => {
            this._reset_request_settings();
            this._o.exportType = 'batch_plain_levelorder';
        });
        this._addHook(this._s.field('exportType', 'export-batch-edgewise-queue$'), 'click', e => {
            this._reset_request_settings();
            this._o.exportType = 'batch_edgewise_queue';
        });
        this._addHook(this._s.field('exportType', 'export-prunning$'), 'click', e => {
            this._reset_request_settings();
            this._o.exportType = 'prunning';
            this._o.prunning = true;
            // transfer the prunning level value
            if (!isNaN(Number(this._s.fieldVal('advPrunning', 'level$'))))
                this._o.cutoff_level = Number(this._s.fieldVal('advPrunning', 'level$'));
            else
                this._o.cutoff_level = -1;
        });
        this._addHook(this._s.field('exportType', 'export-edge-based-pathfinder$'), 'click', e => {
            this._reset_request_settings();
            this._o.exportType = 'edge-based-pathfinder';
        });
        this._addHook(this._s.field('exportType', 'export-graph-section$'), 'click', e => {
            this._reset_request_settings();
            this._o.exportType = 'graph_section';
        });
        this._addHook(this._s.field('exportType', 'export-graph-section-topology$'), 'click', e => {
            this._reset_request_settings();
            this._o.exportType = 'graph_section_topology';
        });

        this._addHook(this._s.field('advMinIntersections', 'count$'), 'change', e => {
            if (Number(this._s.fieldVal('advMinIntersections', 'count$')) > 0)
                this._o.min_intersection_filter = Number(this._s.fieldVal('advMinIntersections', 'count$'));
            else
                this._o.min_intersection_filter = 1;
        });

        this._addHook(this._s.field('advPacking', 'level$'), 'change', e => {
            if (!isNaN(Number(this._s.fieldVal('advPacking', 'level$'))))
                this._o.level = Number(this._s.fieldVal('advPacking', 'level$'))
            else
                this._o.level = 0;
        });

        this._addHook(this._s.button('advButtons', 'save$'), 'click', e => {
            this._initialOptions = $.extend(true, {}, this._o);
        });

        this._addHook(this._s.button('advButtons', 'reset$'), 'click', e => {
            this._reset('optionals');
        });

        this._addHook(this._s.button('result', 'calculate$'), 'click', e => {
            if (this._m.state() !== 'created')
                return;
            this._calculate_trajectories();
        });

        this._addHook(this._s.button('advButtons', 'calculate$'), 'click', e => {
            if (this._m.state() !== 'created')
                return;
            this._calculate_trajectories();
        });

        // Disable calculate button by default
        this._s.disableButton('result', 'calculate$');
        this._s.disableButton('advButtons', 'calculate$');

        /* Map container and hooks */

        this._m = new MapContainer('map', this._o.apiKey, { home: this._o.home });

        this._addHooks(this._m.getMap(), [
            // Map events
            'mousemove', 'touchmove', 'moveend', 'zoom', 'zoomend',
            // Draw Events
            'draw:drawstart', 'draw:drawstop', 'draw:created',
            // Edit events
            'draw:editstart', 'draw:editmove', 'draw:editresize', 'draw:edited', 'draw:editstop',
            // Delete events
            'draw:deletestart', 'draw:deleted', 'draw:deletestop'
        ], this._handle.bind(this));

        this._addHook(this._m.button('draw-rectangle$'), 'click', e => {
            this._reset('map');
            this._m.state('drawing');
            this._c.type('rectangle');
            $('#rect-submenu').collapse('show');
        });

        /* Document hooks */

        this._addHook($(document), 'keyup', this._handleKey.bind(this));

        /* General things */

        this._addHook($("#sidebar-toggle"), 'click', e => {
            e.preventDefault()
            $("#wrapper").toggleClass("toggled")
            // Resize the map properly
            setTimeout(() => {
                this._m.getMap().invalidateSize(true)
            }, 150)
        });

        // Datepicker initialization
        $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            startDate: '01/01/1970', // https://en.wikipedia.org/wiki/Unix_time
            endDate: '01/19/2038'
        });

        /* Misc */

        // Extend the L.LatLng object for quick coordinate array conversion like the node one.
        L.LatLng.prototype.toArray = function () {
            return [this.lat, this.lng];
        }

        // Extend the L.LatLng object for quick string conversion like the node one.
        L.LatLng.prototype.toString = function (decimals = -1) {
            if (decimals >= 0)
                return this.lat.toFixed(decimals) + " / " + this.lng.toFixed(decimals);
            return this.lat + " / " + this.lng;
        }

        // Component toggle functionalities
        $('.component-toggle').change(function () {
            // Find the real checkbox
            let checkbox = $(this).find('label input[type="checkbox"]')[0]
            // Enable or disable all sub-component inputs
            let subComponent$ = $(this).parent().parent()
            subComponent$.find('input').not(checkbox).prop('disabled', !checkbox.checked)
            subComponent$.find('button').prop('disabled', !checkbox.checked)
        }).trigger('change');

        // Transfer the packing & update options to the sidebar      
        this._transferOptions();

        this._update_heatmap(true);

        if(this._o.perform_evaluation){
            this._include_evaluation_file_dialog()
        }

    }

    _addHook(object$, event, callback, activation = 'on') {
        switch (activation) {
            case 'on': object$.on(event, e => { callback(e) }); break;
            case 'off': object$.off(event, e => { callback(e) }); break;
        }
    }

    _addHooks(object$, events, callback, activation = 'on') {
        events.forEach((event) => {
            this._addHook(object$, event, callback, activation);
        });
    }

    _removeHooks(object$) { }

    _handle(e, type = '') {
        type = isEmpty(type) ? e.type : type;
        var layer = e.layer,
            layers = e.layers;

        switch (type) {
            case 'mousemove':
            case 'touchmove':
                this._m.coordsField(e);
                if (!(this._m.state() === 'drawing' || this._m.state() === 'drawing-active'))
                    return;
                let node = new Node(e.latlng);
                if (this._m.state() == 'drawing') {
                    this._c.node('rectangle', 'min', node);
                    this._s.fieldVal('rectangle', 'min-lat$', node.getLat(7));
                    this._s.fieldVal('rectangle', 'min-lon$', node.getLng(7));
                } else if (this._m.state() == 'drawing-active') {
                    this._c.node('rectangle', 'max', node);
                    this._s.fieldVal('rectangle', 'max-lat$', node.getLat(7));
                    this._s.fieldVal('rectangle', 'max-lon$', node.getLng(7));
                }
                break;
            case 'moveend':
                this._update_heatmap();
                if (!this._o.updates || this._o.exportType  != 'prunning')
                    return;
                else if (this._m.state() != 'created' || !this._calculated)
                    return;

                this._update_trajetories();
                this._update_heatmap();
                break;

            case 'zoom':
                this._m.zoomField(this._m.getMap().getZoom());
                break;
            case 'zoomend':
                if (!this._o.updates 
                    || !this._o.packing 
                    || this._o.exportType == 'batch_plain_inorder'
                    || this._o.exportType == 'batch_plain_levelorder'
                    || this._o.exportType == 'batch_edgewise_queue'
                    )
                    return;
                else if (!this._m.state() == 'created' || !this._calculated)
                    return;

                let lastZoom = this._m.getMap().getZoom();
                setTimeout(() => {
                    if (lastZoom == this._m.getMap().getZoom()){
                        this._update_trajetories();
                        this._update_heatmap();
                    }
                }, this._o.updateRate);
                break;
            case 'draw:drawstart':
                this._m.getMap().on('touchstart', e => {
                    this._handle(e, 'mousemove');
                    if (this._m.state() == 'drawing')
                        this._m.state('drawing-active');
                    else if (this._m.state() == 'drawing-active') {
                        // Called if user starts dragging after double-click drawing
                        setTimeout(() => {
                            if (!this._m.state() == 'created') {
                                this._m.state('created');
                                this._handle(e, 'mousemove');
                                this._m.state('drawing-active');
                            }
                        }, 100);
                    }
                });
                break;
            case 'draw:drawstop':
                if (this._m.state() == 'drawing' || this._m.state() == 'drawing-active') {
                    this._reset('map');
                    this._m.resetState();
                }
                break;
            case 'draw:created':
                this._m.addLayer('draw', layer);
                this._m.state('created');
                this._s.enableButton('result', 'calculate$');
                this._s.enableButton('advButtons', 'calculate$');
                break;
            case 'draw:editstart':
                this._m.reset('trajectories edges');
                this._m.state('editing');
                this._s.disableButton('result', 'calculate$');
                this._s.disableButton('advButtons', 'calculate$');
                break;
            case 'draw:editmove':
            case 'draw:editresize':
                if (!this._m.state() == 'editing')
                    return;
                switch (this._c.type()) {
                    case 'rectangle':
                        this._s.fieldVal('rectangle', 'min-lat$', layer._latlngs[0][0]['lat']) // Default leaflet min node when editing
                        this._s.fieldVal('rectangle', 'min-lon$', layer._latlngs[0][0]['lng']) // Default leaflet min node when editing
                        this._s.fieldVal('rectangle', 'max-lat$', layer._latlngs[0][2]['lat']) // Default leaflet max node when editing
                        this._s.fieldVal('rectangle', 'max-lon$', layer._latlngs[0][2]['lng']) // Default leaflet max node when editing
                        break;
                }
                break;
            case 'draw:edited':
                if (!this._m.state() == 'editing' || !this._m.state() == 'created')
                    return;
                switch (this._c.type()) {
                    case 'rectangle':
                        layers.eachLayer(layer => {
                            if (layer instanceof L.Rectangle) {
                                this._c.node('rectangle', 'min', new Node(layer._latlngs[0][0]));
                                this._c.node('rectangle', 'max', new Node(layer._latlngs[0][2]));
                            }
                        });
                        break;
                }
                break;
            case 'draw:editstop':
                if (!this._m.state() == 'editing')
                    return
                switch (this._c.type()) {
                    case 'rectangle':
                        this._s.fieldVal('rectangle', 'min-lat$', this._c.node('rectangle', 'min').getLat(7));
                        this._s.fieldVal('rectangle', 'min-lon$', this._c.node('rectangle', 'min').getLng(7));
                        this._s.fieldVal('rectangle', 'max-lat$', this._c.node('rectangle', 'max').getLat(7));
                        this._s.fieldVal('rectangle', 'max-lon$', this._c.node('rectangle', 'max').getLng(7));
                        break
                }
                this._m.state('created')
                this._s.enableButton('result', 'calculate$');
                this._s.enableButton('advButtons', 'calculate$');
                break;
            case 'draw:deletestart':
                this._m.state('editing');
                this._s.disableButton('result', 'calculate$');
                this._s.disableButton('advButtons', 'calculate$');
                break;
            case 'draw:deleted':
                layers.eachLayer(layer => {
                    if (layer instanceof L.Rectangle) {
                        this._reset('map');
                        this._m.state('none');
                        this._c.type('none');
                        if(this._o.showTiles){
                            this._m.set_tiling(this._o.tilingUrl);
                        }
                    }
                });
                this.heatmap_layer_visible = this._o.use_heatmap_layer;
                break;
            case 'draw:deletestop':
                if (this._m.state() == 'editing') {
                    this._m.state('created');
                    this._s.enableButton('result', 'calculate$');
                    this._s.enableButton('advButtons', 'calculate$');
                }
                break;
            default:
        }
    }

    _reset_request_settings(){
        this._o.prunning = false;
        this._o.packing = false;
    }

    _handleKey(e, which = '') {
        which = isEmpty(which) ? e.which : which;

        switch (which) {
            case 27:
                // escape
                break;
        }

    }

    _calculate_trajectories(){
        this._update_trajetories(true)
    }

    _update_trajetories(is_first_request=false) {
        // break if map has no rectangle
        if (!this._m.state() == 'created')
            return;

        // Prepare the request
        var request = this._create_trajectory_request_object();
        if (isEmpty(request))
            return;

        if (is_first_request){
            // Prepare the map and overlays
            this._m.reset('trajectories edges');
            this._s.disableButton('result', 'calculate$');
            this._s.disableButton('advButtons', 'calculate$');
        }

        // Log the request if needed
        if (this._o.debug)
            if (is_first_request)
                console.log("Update Request: ", request);
            else
                console.log("Calculate Request: ", request);

        // Send the request
        this._send_trajectory_request(request, this._success_trajectory_response.bind(this), this._error_trajectory_response.bind(this));

    }

    _create_trajectory_request_object() {
        // create request
        var request = new CustomRequest();


        request.setShape(
            'rectangle',
            groupToCoords(this._c.transform('rectangle'))
        );

        // Add the map bounds and zoom to the request
        let map = this._m.getMap();
        request.setMapBounds(
            map.getBounds().getSouthWest().toArray(),
            map.getBounds().getNorthEast().toArray(),
            map.getZoom()
        );

        // Add a time interval to the request if enabled
        if (this._s.active('advInterval')) {
            // Retrieve the input values
            var start, end;

            // Retrieve the start input
            if (!isEmpty(this._s.fieldVal('advInterval', 'start$')))
                start = moment.utc(this._s.fieldVal('advInterval', 'start$'), 'MM-DD-YYYY').unix();

            // Retrieve the end input
            if (!isEmpty(this._s.fieldVal('advInterval', 'end$')))
                end = moment.utc(this._s.fieldVal('advInterval', 'end$'), 'MM-DD-YYYY').unix();

            // Check the values and put them into the interval if they are valid
            if (checkInterval(start, end))
                request.setTimeInterval(start, end);
            else
                return;
        }

        // Add the days to the request if needed
        if (this._s.active('advDays')) {
            // Retrieve the days
            var days = this._s.getIntervalDays();

            // Convert the selected days to a 7 bit string
            if (!isArray(days) || days == [])
                days = '1111111';
            else
                days = arrayToBitString(days);

            if (days == '0000000') {
                alert('Time slot error! No weekdays were selected, at least one is needed.')
                return;
            }
            request.setTimeSlots(days);
        }

        // Add packing and level settings to the request
        request.setOptions({ 
            packing: this._o.packing, 
            level: this._o.level, 
            exportType: this._o.exportType, 
            prunning: this._o.prunning,
            cutoff_level: this._o.cutoff_level,
            weightedEdges: this._o.weightedEdges,
            weightedEdgesBinning: this._o.weightedEdgesBinning,
            min_intersection_filter : this._o.min_intersection_filter,
            heatmapRadius : this._o.heatmapRadius,
            performLocalSearch : this._o.performLocalSearch,
            useBidirectionalEdges : this._o.useBidirectionalEdges,
            useNodeCounter : this._o.useNodeCounter,
            graph_level: this._o.graph_level,
            showAuxLines : this._o.include_aux_lines,
        });

        return request;
    }

    _send_trajectory_request(request, success, error) {
        // Log the request if needed
        if (this._o.debug)
            console.log("Calculate request:" , request);
        this._send_request('/calculate', request, success, error);
    }

    _send_heatmap_request(request, success, error) {
        this._send_request('/heatmap', request, success, error);
    }

    _send_request(request_path, request, success, error) {
        $.ajax({
            url: this._o.baseUrl + request_path,
            type: 'POST',
            dataType: 'json',
            data: request.toString(),
            contentType: 'application/json',
            mimeType: 'application/json',
            success: success,
            error: error
        });
    }

    _success_trajectory_response(result) {
        this.heatmap_layer_visible = false;
        this._m.reset('heatmap');

        type = get_draw_type(result);
        
        if (type == 'InitPlainInorderBatch'){
            this.batch_decoder = new PlainInorderBatchDecoder(this._m, this._o.baseUrl);
            this.batch_decoder.process_init_batch(result);
            this.batch_decoder.start_update_loop();
        }
        else if (type == 'InitPlainLevelorderBatch'){
            this.batch_decoder = new PlainLevelorderBatchDecoder(this._m, this._o.baseUrl);
            this.batch_decoder.process_init_batch(result);
            this.batch_decoder.start_update_loop();
        }
        else if (type =='InitLevelorderZoomedBatchDecoder'){
            this.batch_decoder = new LevelorderZoomedBatchDecoder(this._m, this._o.baseUrl);
            this.batch_decoder.process_init_batch(result);
            this.batch_decoder.start_update_loop();
        }
        else if (type =='InitEdgeWiseBatch'){
            this.batch_decoder = new EdgewiseLevelBatchDecoder(this._m, this._o.baseUrl);
            this.batch_decoder.process_init_batch(result);
            this.batch_decoder.start_update_loop();
        }
        else if(type == null)
        {
            console.log('Return result-type unknown');
        }
        else{
            this._m.draw(type, result, this._o);
        }
        this._s.setResults(result);
        this._s.enableButton('result', 'calculate$');
        this._s.enableButton('advButtons', 'calculate$');
        this._calculated = true;
    }

    _error_trajectory_response(xhr, status, error) {
        alert('Unable to retrieve results due to network problems. See console for details.')
        console.error('Error:', [xhr, status, error])
        this._s.enableButton('result', 'calculate$');
        this._s.enableButton('advButtons', 'calculate$');
        this._calculated = false;
    }


    _success_heatmap_response(result) {
        if ('heatmap' in result) {
            this._m.draw('heatmap', result, this._o);
        } else {
            console.log('Return result-type unknown');
        }
    }

    _error_heatmap_response(xhr, status, error) {
        alert('Unable to update heatmap due to network problems. See console for details.')
        console.error('Error:', [xhr, status, error])
    }


    _update_heatmap(is_first_request=false) {
        // break if option is deactivated
        if(this._o.use_heatmap_layer == false || this.heatmap_layer_visible == false)
            return;

        // Prepare the request
        var request = this._create_heatmap_request_object();
        if (isEmpty(request))
            return;

        if (is_first_request){
            // Prepare the map and overlays
            this._m.reset('heatmap');
        }

        // Log the request if needed
        if (this._o.debug)
            if (is_first_request)
                console.log("Update Heatmap-Request: ", request);
            else
                console.log("Calculate Heatmap-Request: ", request);

        this._send_heatmap_request(request, this._success_heatmap_response.bind(this), this._error_heatmap_response.bind(this));

    }


    _create_heatmap_request_object() {
        // create request
        var request = new HeatmapRequest();

        // Add the map bounds and zoom to the request
        let map = this._m.getMap();
        request.setMapBounds(
            map.getBounds().getSouthWest().toArray(),
            map.getBounds().getNorthEast().toArray(),
            map.getZoom()
        );
        return request;
    }



    _reset(type) {
        switch (type) {
            case 'map':
                this._m.reset('draw trajectories, edges');
                this._s.reset('shapes results');
                this._s.disableButton('result', 'calculate$');
                this._s.disableButton('advButtons', 'calculate$');
                this._calculated = false;
                break;
            case 'optionals':
                this._s.reset('advanced');
                this._o = $.extend(true, {}, this._initialOptions);
                this._transferOptions()
                this._calculated = false;
                break
            case 'all':
            default:
                this._m.reset('all')
                this._s.reset('all');
                this._o = $.extend(true, {}, this._initialOptions);
                this._transferOptions();
                this._s.disableButton('result', 'calculate$');
                this._s.disableButton('advButtons', 'calculate$');
                this._calculated = false;
        }
    }

    _transferOptions() {
        // time interval options
        this._s.field('advDays', 'days$').prop('checked', true);

        // graph section exporting
        this._s.field('advGraph', 'level$').change(() => {
            var numeric_value = Number(this._s.fieldVal('advGraph', 'level$'));
            if (!isNaN(numeric_value) && numeric_value > 0){
                this._o.graph_level = Number(this._s.fieldVal('advGraph', 'level$'));
            }else{
                this._o.graph_level = -1;
            }
        });
        this._s.field('advGraph', 'showAuxLines$').prop('checked', this._o.include_aux_lines);
        this._s.field('advGraph', 'showAuxLines$').change(() => {
            this._o.include_aux_lines = this._s.field('advGraph', 'showAuxLines$').prop('checked');
        });

        // prunning options
        this._s.field('advPrunning', 'level$').change(() => {
            if (this._s.fieldVal('advPrunning', 'level$') == "") {
                this._o.cutoff_level = 50; // apply placeholder
            }
            else if (!isNaN(Number(this._s.fieldVal('advPrunning', 'level$')))){
                this._o.cutoff_level = Number(this._s.fieldVal('advPrunning', 'level$'));
            }
            else{
                this._o.cutoff_level = -1; // set invalid
            }
        }).trigger('change');

        // tiling options adv-tiling
        this._s.active('advTiling', this._o.showTiles);
        this._s.checkbox('advTiling').change(() => {
            this._o.showTiles = this._s.active('advTiling')

            // setup or remove tiling connection
            if(this._o.showTiles == false){
                this._m.clear_tiling();
            }
            else{
                this._m.set_tiling(this._o.tilingUrl);
            }

        }).trigger('change');

        // retrieval options: local search
        this._s.active('advLocalSearch', this._o.performLocalSearch);
        this._s.checkbox('advLocalSearch').change(() => {
            this._o.performLocalSearch = this._s.active('advLocalSearch')
        }).trigger('change');

        // bidirectional edges
        this._s.active('advBidirectionalEdges', this._o.useBidirectionalEdges);
        this._s.checkbox('advBidirectionalEdges').change(() => {
            this._o.useBidirectionalEdges = this._s.active('advBidirectionalEdges')
        }).trigger('change');

        // use node count, too.
        this._s.active('advUseNodeCount', this._o.useNodeCounter);
        this._s.checkbox('advUseNodeCount').change(() => {
            this._o.useNodeCounter = this._s.active('advUseNodeCount')
        }).trigger('change');
                        
        // heatmap options
        this._s.active('advHeatmap', this._o.use_heatmap_layer);
        this._s.checkbox('advHeatmap').change(() => {
            this._o.use_heatmap_layer = this._s.active('advHeatmap');
        }).trigger('change');

        // edge-return settings
        this._s.active('advWeightedEdges', this._o.weightedEdges);
        this._s.checkbox('advWeightedEdges').change(() => {
            // check different checkboxes here.
            this._o.weightedEdges = this._s.active('advWeightedEdges');
        }).trigger('change');

        // ++ Edge-Weight options ++
        // sets the option tag to 'transparency' if it switched to active
        this._s.field('advWeightedEdges', 'type-transparency$').change(() => {
            this._o.edgePlottingType = [EDGE_PLOT_WEIGHT_TYPE__TRANSPARENCY];
        });
        // sets the option tag to 'thickness' if it switched to active
        this._s.field('advWeightedEdges', 'type-thickness$').change(() => {
            this._o.edgePlottingType = [EDGE_PLOT_WEIGHT_TYPE__THICKNESS];
        });
        // sets the option tag to 'both' if it switched to active
        this._s.field('advWeightedEdges', 'type-both$').change(() => {
            this._o.edgePlottingType = [EDGE_PLOT_WEIGHT_TYPE__THICKNESS, EDGE_PLOT_WEIGHT_TYPE__TRANSPARENCY];
        });

        // edge weighting binning settings
        // -- initial setup
        this._s.fieldVal('weightedEdgesBinning', 'edge-binning-number$', this._o.weightedEdgesBinning);
        this._s.field('weightedEdgesBinning', 'edge-binning-number$').prop('disabled', this._o.weightedEdgesBinning <= 0);

        // -- when interacting
        this._s.field('weightedEdgesBinning', 'edge-binning-number$').change(() => {
            var numeric_value = Number(this._s.fieldVal('weightedEdgesBinning', 'edge-binning-number$'));
            if (!isNaN(numeric_value) && numeric_value > 0)
                this._o.weightedEdgesBinning = Number(this._s.fieldVal('weightedEdgesBinning', 'edge-binning-number$'));
            else
                this._o.weightedEdgesBinning = -1;
        });
        this._s.checkbox('weightedEdgesBinning').change(() => {
            // transition on -> off: set to empty string, internally to -1
            // transition off -> check current entry, correct if nessesarry
            if(this._s.active('weightedEdgesBinning')){
                // keep value inside if already a valid number
                var numeric_value = Number(this._s.fieldVal('weightedEdgesBinning', 'edge-binning-number$')); 
                if (!isNaN(numeric_value) && numeric_value > 0)
                    this._o.weightedEdgesBinning = numeric_value;
                else
                    this._s.fieldVal('weightedEdgesBinning', 'edge-binning-number$', "5");
                    this._o.weightedEdgesBinning = 5;
            }
            else{
                this._s.fieldVal('weightedEdgesBinning', 'edge-binning-number$', "");
                this._o.weightedEdgesBinning = -1;
            }
        }).trigger('change');
        
        // packing options
        this._s.active('advPacking', this._o.packing);
        this._s.checkbox('advPacking').change(() => {
            this._o.packing = this._s.active('advPacking');
        }).trigger('change');

        this._s.field('advPacking', 'type-zoom$').change(() => {
            this._s.field('advPacking', 'level$').prop('disabled', true);
            this._o.level = -1;
        });
        this._s.field('advPacking', 'type-level$').change(() => {
            this._s.field('advPacking', 'level$').prop('disabled', false);
            if (!isNaN(Number(this._s.fieldVal('advPacking', 'level$'))))
                this._o.level = Number(this._s.fieldVal('advPacking', 'level$'));
            else
                this._o.level = 0;
        });

        if (this._o.level < 0) {
            this._s.field('advPacking', 'type-zoom$').prop('checked', true);
            this._s.field('advPacking', 'level$').prop('disabled', true);
        } else {
            this._s.field('advPacking', 'type-level$').prop('checked', true);
            this._s.fieldVal('advPacking', 'level$', this._o.level);
        }

        this._s.field('advPacking', 'updates$').prop('checked', this._o.updates);
        this._s.field('advPacking', 'updates$').change(() => {
            this._o.updates = this._s.field('advPacking', 'updates$').prop('checked');
        });

        // min intersection filter
        this._s.fieldVal('advMinIntersections', 'count$', this._o.min_intersection_filter);
    }

    _include_evaluation_file_dialog(){
        document.getElementById('evaluation_panel').style.display = "inline";
        parent = this;
        let onload_callback = function(loaded_text){
            console.log("Parsing json file..");
            var json = loaded_text;
            const parsed_obj = JSON.parse(json);
            console.log("Parsing completed successfully");
            parent._perform_evaluation(parsed_obj);
        };

        document.getElementById('file_input').onchange = function(){
            readExperimentJSON(this, onload_callback);
        }
    }

    _perform_evaluation(result){
        var plot_repetition_time = 5;
        var summary_object = null;
        var first_obj = Object.entries(result)[0][1][0];

        console.log(first_obj);

        if("trajectorySegmentGraph" in first_obj){
            summary_object =  this._perform_segment_graph_evaluation(result, plot_repetition_time);
        }
        else if ("heatmap" in first_obj){
            summary_object =  this._perform_pruned_graph_evaluation(result, plot_repetition_time);
        }
        else if ("weighted_binned_edges" in first_obj){
            summary_object =  this._perform_ebpf_graph_evaluation(result, plot_repetition_time);
        }
        else{
            console.error("Type not supported yet.")
        }
            
        console.log(summary_object);
        var summary_str = JSON.stringify(summary_object);
        alert(summary_str);
    }

    _perform_segment_graph_evaluation(result, plot_repetition_time){
        console.log("Performing SegmentGraph Evaluation..");
        var summary_object = {};
        for(const [zoom_level, zoom_grouped_data] of Object.entries(result)){
            console.log("Level: " + zoom_level);

            var total_plotted = 0;
            var start_time = new Date();
            for(let j in zoom_grouped_data){
                let plot_object = zoom_grouped_data[j];
                for (let i = 0; i < plot_repetition_time; i++) {
                    // plot object
                    let draw_type = get_draw_type(plot_object);
                    this._m.draw(draw_type, plot_object, null);
                    total_plotted += 1;
                }
            }
            var time_difference = new Date() - start_time; 
            var average = time_difference / total_plotted; 
            
            summary_object[zoom_level] = average / 1000;      
        }
        return summary_object;
    }

    _perform_pruned_graph_evaluation(result, plot_repetition_time){
        console.log("Performing Pruned Graph Evaluation..");
        var summary_object = {};
        for(const [cutoff_level, cutoff_grouped_data] of Object.entries(result)){
            console.log("Cutoff: " + cutoff_level);

            var total_plotted = 0;
            var start_time = new Date();
            for(let j in cutoff_grouped_data){
                let plot_object = cutoff_grouped_data[j];
                for (let i = 0; i < plot_repetition_time; i++) {
                    // plot object
                    let draw_type = get_draw_type(plot_object);
                    this._m.draw(draw_type, plot_object, {heatmapRadius: 80});
                    total_plotted += 1;
                }
            }
            var time_difference = new Date() - start_time; 
            var average = time_difference / total_plotted; 
            
            summary_object[cutoff_level] = average / 1000;      
        }
        return summary_object;
    }

    _perform_ebpf_graph_evaluation(result, plot_repetition_time){
        console.log("Performing EBPF Evaluation..");
        var summary_object = {};
        for(const [cutoff_level, cutoff_grouped_data] of Object.entries(result)){
            console.log("Cutoff: " + cutoff_level);

            var options =  {edgePlottingType: [EDGE_PLOT_WEIGHT_TYPE__TRANSPARENCY]};
            var total_plotted = 0;
            var start_time = new Date();
            for(let j in cutoff_grouped_data){
                let plot_object = cutoff_grouped_data[j];
                for (let i = 0; i < plot_repetition_time; i++) {
                    // plot object
                    let draw_type = get_draw_type(plot_object);
                    this._m.draw(draw_type, plot_object, options);
                    total_plotted += 1;
                }
            }
            var time_difference = new Date() - start_time; 
            var average = time_difference / total_plotted; 
            
            summary_object[cutoff_level] = average / 1000;      
        }
        return summary_object;
    }

   
}

var defaults = {

    /** 
     * Flag for debug options.
     * @type {boolean}
     */
    debug: false,

    /**
     * Flag for displaying the heatmap as an overlay.
     * @type {boolean}
     */
    use_heatmap_layer: true,

    /**
     * The shape settings. If a shape is set to false, its functionalities will not be used.
     */
    shapes: {
        rectangle: true
    },

    /**
     * The home point for the map. Can be set as coordinates or configured as an object to add a custom marker.
     */
    home: { 
        coords: [48.74519, 9.10664],
        marker: {
            heading: 'University of Stuttgart',
            text: 'Institute for Formal Methods of Computer Science (FMI)'
        } 
    },

    /**
     * The pathfinder server url.
     * @type {string}
     */
     baseUrl: "http://localhost:9080/pathfinder",


    /**
     * The tiling server url.
     * @type {string}
     */
     tilingUrl: "http://localhost:9081/tile_server/{z}/{x}/{y}",

    
    /**
     * Flag for trajectory packing on different zoom levels.
     * @type {boolean}
     */
     showTiles: true,


    /**
     * In which format the response should be send
     * @type {string}
     */
    exportType: "trajectories",


    /**
     * In which format the response should be send
     * @type {string}
     */
    edgePlottingType: [EDGE_PLOT_WEIGHT_TYPE__TRANSPARENCY],

    /**
     * Flag for trajectory packing on different zoom levels.
     * @type {boolean}
     */
     packing: true,


    /**
     * Flag for edge-based pathfinder for returning weight-vector, too.
     * @type {boolean}
     */
     weightedEdges: true,


    /**
     * Level of binning used for edge weighting. If set to -1, no binning is used.
     * @type {number}
     */
     weightedEdgesBinning: -1,

    
    /**
     * If set to a number greater or equal to 1 only edges are returned used that often.
     * @type {Number}
     */
     min_intersection_filter: 1,


     /**
      * If set to a number greater or equal to 0, the server will unpack the paths to this specific level.
      * @type {Number}
      */
     level: -1,


    /**
     * Flag for trajectory prunning takes place
     * @type {boolean}
     */
    prunning: false,

    /**
     * If set to a number greater or equal to 1, the server will skip returning edges smaller than this level.
     * @type {Number}
     */
    cutoff_level: -1,

    /**
     * Flag for client for trajectory updates on map interaction.
     * @type {boolean}
     */
    updates: true,

    /**
     * The update rate in miliseconds. If updates are enabled, they will only occur 
     * after the map is unchanged for this time interval.
     * @type {Number}
     */
    updateRate: 750,

    /**
     * Flag for requesting the server for experiment/evaluation object for mesurement.
     * @type {boolean}
     */
     perform_evaluation: false,

    /**
     * Parameter for tuning the influence of points depending on the grid size.
     * @type {Number}
     */
     heatmapRadius : 80,


    /**
     * Flag for edge-based pathfinder: Merge Back- and Foreward-Edges
     * @type {boolean}
     */
     useBidirectionalEdges: true,



    /**
     * Flag for edge-based pathfinder: Avoid global sweep
     * @type {boolean}
     */
     performLocalSearch: false, 

    /**


    /**
     * If set to a number greater or equal to 1, the server will skip returning edges smaller than this level.
     * This value is used when exporting graph sections
     * @type {Number}
     */
     graph_level: -1,


    /**
     * Include auxillary lines for exporting graph sections
     * @type {boolean}
     */
     include_aux_lines: true,
};

/**
 * The component class.
 */
class Component {

    /**
     * Creates a new Component instance.
     * @constructor 
     * @param {Object} groups A blueprint that maps names to key:value pairs and groups them.
     *                        If empty, the component will have no groups.
     */
    constructor(groups) {
        if (!isEmpty(groups)) {
            for (let group in groups)
                this[group] = groups[group];
        }
    }

    /**
     * Maps an item to a group with a key and value.
     * @param {string} group The group name. If it doesn't exist, the item will not be added.
     * @param {string} key The new item key.
     * @param {Object} value The new item value.
     */
    add(group, key, value) {
        if (this[group] !== undefined && !this[group].has(key))
            this[group].add(key, value);
    }

    /**
     * Removes an item from a group by key.
     * @param {string} group The group name. If it doesn't exist, no item will be removed.
     * @param {Object} key The item key, which can be a string or index.
     */
    remove(group, key) {
        if (this[group] !== undefined)
            this[group].remove(key, item);
    }

    /**
     * Sets the item value of an item 
     * @param {string} group The group name.
     * @param {Object} key The item key, which can be a string or index.
     * @param {Object} value The item value.
     */
    set(group, key, value) {
        if (this[group] !== undefined)
            this[group].set(key, value);
    }

    /**
     * Retrieves the item value by group and key.
     * @param {string} group The group name. If it doesn't exist, undefined will be returned.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {Object} The item value if the group and key exist. If not, undefined will be returned.
     */
    get(group, key) {
        return this[group] !== undefined ? this[group].get(key) : undefined;
    }

    /**
     * Checks if an item with a given key exists within a given group.
     * @param {string} group The group name.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {boolean} True if the group and key exist.
     */
    has(group, key) {
        return this[group] !== undefined && this[group].has(key);
    }

    /**
     * Retrieves the group item index by key.
     * @param {string} group The The group name.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {Object} The item value if the group and key exist. If not, -1 will be returned.
     */
    index(group, key) {
        return this[group].index(key);
    }

}

/**
 * A map / dictionary class for grouping items in key:value pairs. 
 * This class should only be used by components and therefore will not be exported.
 */
class Group {

    /**
     * Creates a new group from an array of item objects with key and value pairs.
     * @constructor
     * @param {Object[]} array The item array. If not an array, _items will be instantiated as [].
     */
    constructor(array) {
        this._items = isArray(array) ? array : [];
    }

    /**
     * Adds a key:value pair to the group.
     * @param {string} key The new item key.
     * @param {Object} value The new item value.
     */
    add(key, value) {
        this._items.push({ key, value });
    }

    /**
     * Removes an item from the group by key if it exists.
     * @param {Object} key The item key, which can be a string or index.
     */
    remove(key) {
        this._items.splice(this.index(key), 1);
    }

    /**
     * Sets the value of an item by key if it exists.
     * @param {Object} key The item key, which can be a string or index.
     * @param {Object} value The item value.
     */
    set(key, value) {
        this._items[this.index(key)].value = value;
    }

    /**
     * Retrieves the value of an item by key.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {Object} The item value if the key exists. If not, undefined will be returned.
     */
    get(key) {
        return this.has(key) ? this._items[this.index(key)].value : undefined;
    }

    /**
     * Checks if an item with a given key exists.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {boolean} True if the key exists.
     */
    has(key) {
        return this.index(key) >= 0;
    }

    /**
     * Retrieves the item index by key.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {Object} The item value if the key exists. If not, -1 will be returned.
     */
    index(key) {
        switch (typeOf(key)) {
            case 'number':
                if (key >= 0 && key < this._items.length)
                    return key;
                break;
            case 'string':
                for (let i = 0; i < this._items.length; i++) {
                    if (this._items[i].key === key)
                        return i;
                }
                break;
            default:
                return -1;
        }
    }

    /**
     * Iterates through the group items and executes a given function on the key:value pairs.
     * @param {function} func The function that is executed in each iteration.
     */
    forEach(func) {
        this._items.forEach((_item) => {
            func(_item.key, _item.value);
        });
    }

    /**
     * Returns the size of the group item array.
     */
    size() {
        return this._items.length;
    }

}

/**
 * A Node that holds the latitude and longitude.
 */
class Node {

    /**
     * Creates a new node instance.
     * @constructor
     * @param {*} arg0 Can be a Node, a L.LatLng object, an Array or else will be interpreted as the Node latitude.
     * @param {*} arg1 The Node longitude if arg0 is not a Node, L.LatLng or Array.
     */
    constructor(arg0, arg1) {
        if (arg0 instanceof Node || arg0 instanceof L.LatLng) {
            // Create a node directly from a node or latlng object
            this._lat = arg0.lat;
            this._lng = arg0.lng;
        } else if (isArray(arg1)) {
            // Create a node from array
            this._lat = arg0[0];
            this._lng = arg0[1];
        } else {
            this._lat = arg0;
            this._lng = arg1;
        }
    }

    /**
     * Sets the node latitude
     * @param {number} lat The new latitude
     */
    setLat(lat) {
        this._lat = lat;
    }

    /**
     * Sets the node longitude
     * @param {number} lng The new longitude
     */
    setLng(lng) {
        this._lng = lng;
    }

    /**
     * Returns the node latitude.
     * @param {number} decimals The number of decimals which will be used for rounding. If empty, the number won't be rounded.
     */
    getLat(decimals = -1) {
        return decimals >= 0 ? this._lat.toFixed(decimals) : this._lat;
    }

    /**
     * Returns the node longitude.
     * @param {number} decimals The number of decimals which will be used for rounding. If empty, the number won't be rounded.
     */
    getLng(decimals = -1) {
        return decimals >= 0 ? this._lng.toFixed(decimals) : this._lng;
    }

    /**
     * Converts the node to a leaflet latlng (L.LatLng) object.
     */
    toLatLng() {
        return new L.LatLng(this._lat, this._lng);
    }

    /**
     * Converts the node to a string.
     * @param {number} decimals The number of decimals which will be used for rounding. If empty, the numbers won't be rounded.
     */
    toString(decimals = -1) {
        return this.getLat(decimals) + " / " + this.getLng(decimals);
    }

    /**
     * Converts the node to an array. The result array will hold the lat at index 0, lng at index 1.
     */
    toArray() {
        return [this._lat, this._lng];
    }

}

/*
 * An abstract batch decoder handles the decoding of updates.
 */
class BatchDecoder{
  

    /**
     * Creates a new BatchDecoder instance.
     * @constructor
     * @param {Object} map_container pointer to the internal map object.
     * @param {String} base_url the server path.
     * @param {Object} path_layer list of all elements plotted onto the map 
     */
    constructor(map_container, base_url) {
        this.map_container = map_container;
        this.base_url = base_url;
        this.path_layer = null;
        this.REQUEST_PATH = '/batch_update';
    }

    process_init_batch(json_result){
        throw new TypeError('BatchDecoder: Called an abstract method'); 
    }

    _send_request(request_path, request, success, error) {
        $.ajax({
            url: this.base_url + request_path,
            type: 'POST',
            dataType: 'json',
            data: request.toString(),
            contentType: 'application/json',
            mimeType: 'application/json',
            success: success,
            error: error
        });
    }

    _error_batch_update_response(xhr, status, error) {
        alert('Unable to update the current view due to network problems. See console for details.')
        console.error('Error:', [xhr, status, error])
    }

    _apply_inorder_update_to_path(path_obj, new_points){
        let old_points = path_obj.getLatLngs();
        let merged_points = old_points.slice(0, -1).concat(new_points).concat(old_points.slice(-1));
        path_obj.setLatLngs(merged_points);
    }
      
}


class PlainInorderBatchDecoder extends BatchDecoder{
    /**
     * Creates a new PlainInorderBatchDecoder instance.
     * @constructor
     * @param {Object} map_container pointer to the internal map object.
     * @param {String} base_url the server path.
    */
     constructor(map_container, base_url) {
        super(map_container, base_url);
        this.trajectory_layer_mapping = null;
        this.next_index_mapping= null;
        this.batch_size = 50;
    }

    /**
     * Creates the initial styles for each response trajectory
     * @constructor
     * @param {object} json_result The json response 
     */
    process_init_batch(json_result){
        // draw the inital shapes with appropriate style 
        this.path_layer = this.map_container.draw('InitPlainInorderBatch', json_result, null);
       
        // link the trajectoriy-path-ids with the layers
        this.trajectory_layer_mapping = {}
        this.next_index_mapping = {}
        let i = 0;
        for(var path_id in json_result.InitPlainInorderBatch){
            this.trajectory_layer_mapping[path_id] = i;
            this.next_index_mapping[path_id] = 1;
            i ++;
        }
    }

    start_update_loop(){
        this._send_update_request();
    }

    _send_update_request(){
        let request = new PlainInorderUpdateRequest(this.next_index_mapping, this.batch_size);

        this._send_request(
            this.REQUEST_PATH, 
            request, 
            this._success_batch_update_response.bind(this), 
            this._error_batch_update_response.bind(this)
            );
    }

    _success_batch_update_response(result){
        for(var path_id in result.update){
            let current_traj_object = this.path_layer[this.trajectory_layer_mapping[path_id]];
            let new_points = result.update[path_id];
            this._apply_inorder_update_to_path(current_traj_object, new_points);
            if(new_points.length == this.batch_size){
                this.next_index_mapping[path_id] += this.batch_size; 
            }
            else{
                delete this.next_index_mapping[path_id];
            }
        } 

        // request next batch, if an entry is left
        for(var key in this.next_index_mapping){
            this._send_update_request();
            break;
        }
    }

}

class PathRequest{
    /**
     * Creates a new Path object instance.
     * @constructor
     * @param {number} next_level the next level to request
     * @param {number} n the total number of elements of the trajectory
     * @param {number} layer_id the index reference to the map-layer.
    */
    constructor(next_level, n, layer_id){
        this.layer_id = layer_id;
        this.n = n;
        this.next_level = next_level;
        this.max_level = Math.floor(Math.log2(n-2+1)) + 1;
        this.next_index = this._get_start_index();
        this.next_offset = this._get_skip_index();
    }
 
    increment_level(){
        this.next_level += 1;
        this.next_index = this._get_start_index();
        this.next_offset = this._get_skip_index();
    }
    
    _get_start_index(){
        // lvl 1 == most central point (= 1 point)
        // lvl 2 == left and right  (= at most 2 points)
        // lvl 2 == left, left and right, right  (= at most 4 points)
        return Math.pow(2, this.max_level - this.next_level);
    }
    
    _get_skip_index(){
        return Math.pow(2, this.max_level - this.next_level + 1);
    }

}

class PlainLevelorderBatchDecoder extends BatchDecoder{
    /**
     * Creates a new PlainLevelorderBatchDecoder instance.
     * @constructor
     * @param {Object} map_container pointer to the internal map object.
     * @param {String} base_url the server path.
    */
     constructor(map_container, base_url) {
        super(map_container, base_url);
        this.paths = null;
        this.path_layer = null;
        this.update_order = null;
        this.max_upload_size = 100;
        this.batch_size = 50;  //  not in use right now
    }

    /**
     * Creates the initial styles for each response trajectory
     * @constructor
     * @param {object} json_result The json response 
     */
    process_init_batch(json_result){
        // draw the inital shapes with appropriate style 
        this.path_layer = this.map_container.draw('InitPlainLevelorderBatch', json_result, null);
       
        // link the trajectoriy-path-ids with the layers
        this.paths = {};
        this.update_order = [];
        let i = 0;
        for(var path_id in json_result.InitPlainLevelorderBatch){
            let n = parseInt(json_result.InitPlainLevelorderBatch[path_id].n);
            
            this.paths[path_id] = new PathRequest(1, n, i);
            this.update_order.push(path_id);
            i ++;
        }
    }

    start_update_loop(){
        this._send_update_request();
    }

    _send_update_request(){
        // pop elements from 'todo'-queue
        let relevant_values = this.update_order.slice(0, this.max_upload_size);
        // remove elements from 'todo' queue
        if (this.update_order.length > this.max_upload_size){
            this.update_order = this.update_order.slice(this.max_upload_size);
        }
        else{
            this.update_order = [];
        }

        var next_index_and_offset_mapping = {};
        for(var path_id of relevant_values){
            let path = this.paths[path_id];
            next_index_and_offset_mapping[path_id] = [path.next_index, path.next_offset];
        }
        let request = new PlainLevelorderUpdateRequest(next_index_and_offset_mapping, this.batch_size);

        this._send_request(
            this.REQUEST_PATH, 
            request, 
            this._success_batch_update_response.bind(this), 
            this._error_batch_update_response.bind(this)
            );
    }

    _success_batch_update_response(result){
        for(var path_id in result.update){
            let current_traj_object = this.path_layer[this.paths[path_id].layer_id];
            let new_points = result.update[path_id][0];
            let next_index = result.update[path_id][1];
            let offset = 1;
            let path = this.paths[path_id];

            this._apply_levelorder_update_to_path(current_traj_object, new_points, offset);
            
            // prepare for next round
            path.increment_level();
            
            if(path.next_level > path.max_level){
                // remove because we're finished here.
                delete this.paths[path_id];
            }
            else{
                this.update_order.push(path_id);
            }
        } 

        // request next batch, if an entry is left
        for(var path_id in this.update_order){
            this._send_update_request();
            break;
        }
    }

    _apply_levelorder_update_to_path(path_obj, new_points, offset){
        let old_points = path_obj.getLatLngs();
        var merged_points = [];
        for(var i=0; i < old_points.length; i++){
            if(i < new_points.length){
                merged_points.push(old_points[i]);
                merged_points.push(new_points[i]);
            }
            else{
                // "new points"-list is empty. 
                // Push rest of old points.
                merged_points = merged_points.concat(old_points.slice(i, -1)).concat(old_points.slice(-1));
                break;
            }
        }
        path_obj.setLatLngs(merged_points);
    }

}

class LevelorderZoomedBatchDecoder extends BatchDecoder{

}

class EdgewiseLevelBatchDecoder extends BatchDecoder{
    /**
     * Creates a new EdgewiseLevelBatchDecoder instance.
     * @constructor
     * @param {Object} map_container pointer to the internal map object.
     * @param {String} base_url the server path.
    */
     constructor(map_container, base_url) {
        super(map_container, base_url);
        this.trajectory_layer_mapping = null;
        this.paths = []
        this.max_edges_requested = 250;
        this.path_edge_map = {}
        this.resolved_edges = {}
    }

    /**
     * Creates the initial styles for each response trajectory (only uv-paths)
     * @constructor
     * @param {object} json_result The json response 
     */
    process_init_batch(json_result){
        // draw the inital shapes with appropriate style 
        this.path_layer = this.map_container.draw('InitEdgeWiseBatch', json_result, null);
       
        // link the trajectoriy-path-ids with the layers
        this.trajectory_layer_mapping = {}
        let i = 0;
        for(var path_id_str in json_result.InitEdgeWiseBatch){
            this.trajectory_layer_mapping[path_id_str] = i;
            this.paths.push(parseInt(path_id_str))
            i ++;
        }
    }

    start_update_loop(){
        this._send_root_edge_request();
    }


    _send_root_edge_request(){
        let request = new GetRootNodesRequest(this.paths);

        this._send_request(
            this.REQUEST_PATH, 
            request, 
            this._success_root_edge_request_response.bind(this), 
            this._error_batch_update_response.bind(this)
            );
    }

    _success_root_edge_request_response(result){
        
        var open_edges = {};
        for(var path_id in result.edgemap){
            let current_nodes = result.edgemap[path_id][1];
            let current_edges = result.edgemap[path_id][0];
            let current_traj_object = this.path_layer[this.trajectory_layer_mapping[path_id]];
            this.path_edge_map[path_id] = current_edges;
            this._apply_inorder_update_to_path(current_traj_object, current_nodes);
            for(var edge in open_edges){
                if(edge in open_edges){
                    open_edges[edge].push(path_id);
                } 
                else{
                    open_edges[edge] = [path_id];
                }
            }
        }
    }

}

/**
 * An abstract request class
 */
class AbstractRequest{
    /**
         * Stringifies the request and returns it
         */
    toString() {
        return JSON.stringify(this);
    }
}

class PlainInorderUpdateRequest extends AbstractRequest{
    /**
     * Creates a new PlainInorderUpdateRequest instance.
     * @constructor
     */
    constructor(next_requested_index_map, batch_size) {
        super();
        this.batch_type = 'PlainInorder';
        this.next_requested_index_map = next_requested_index_map;
        this.batch_size = batch_size;
    }
}

class PlainLevelorderUpdateRequest extends AbstractRequest{
    /**
     * Creates a new PlainInorderUpdateRequest instance.
     * @constructor
     */
    constructor(next_offset_and_skip_index, batch_size) {
        super();
        this.batch_type = 'PlainLevelorder';
        this.next_offset_and_skip_index = next_offset_and_skip_index;
        this.batch_size = batch_size;
    }
}

class GetRootNodesRequest extends AbstractRequest{
    /**
     * Creates a new PlainInorderUpdateRequest instance.
     * @constructor
     */
    constructor(paths) {
        super();
        this.batch_type = 'GetRootNodes';
        this.paths = paths;
    }


}

/**
 * A request object for updating the heatmap.
 * @var {Object} bounds The map bounds. Holds the bounds as 2-dimensional coordinate array (0: southwest corner, 1: northeast corner) and the zoom level.
 */
class HeatmapRequest extends AbstractRequest{

    /**
     * Creates a new request instance.
     * @constructor
     * @param {object} args The request arguments.
     */
    constructor(args = {}) {
        super();
        this.bounds = !isEmpty(args.bounds) ? args.bounds : {};
    }

    /**
     * Sets the map bounding box and zoom for this request.
     * @param {Number[]} southWest The south west (bottom-left) corner as coordinate array.
     * @param {Number[]} northEast The north east (top-right) corner as coordinate array.
     * @param {Number} zoom The map zoom level.
     */
    setMapBounds(southWest, northEast, zoom) {
        this.bounds = { coords: [southWest, northEast], zoom };
    }
}


/**
 * A custom Request object for server interaction.
 * @var {Object} shape The shape. Holds only type 'rectangle' and the coordinates as 2-dimensional array.
 * @var {Object} bounds The map bounds. Holds the bounds as 2-dimensional coordinate array (0: southwest corner, 1: northeast corner) and the zoom level.
 * @var {Object} [timeInterval={}] The time interval. If set, only trajectories with a timestamp in the interval will be retrieved.
 * @var {Object} [options={}] Possible options: packing - If true, the server will compress the trajectories according to the map zoom level.
 *                                              level - If packing is enabled, the server will compress the trajectories to this level. 
 */
class CustomRequest extends AbstractRequest{

    /**
     * Creates a new request instance.
     * @constructor
     * @param {object} args The request arguments.
     */
    constructor(args = {}) {
        super();
        this.shape = !isEmpty(args.shape) ? args.shape : {};
        this.bounds = !isEmpty(args.bounds) ? args.bounds : {};
        this.timeInterval = !isEmpty(args.timeInterval) ? args.timeInterval : {};
        this.timeSlots = !isEmpty(args.timeSlots) ? args.timeSlots : '';
        this.options = !isEmpty(args.options) ? args.packing : {};
    }

    /**
     * Sets the shape for this request.
     * @param {} type The shape type. Can only be 'rectangle' however. 
     * @param {*} coords The shape coordinates as 2-dimensional array.
     */
    setShape(type, coords) {
        this.shape = { type, coords };
    }

    /**
     * Sets the map bounding box and zoom for this request.
     * @param {Number[]} southWest The south west (bottom-left) corner as coordinate array.
     * @param {Number[]} northEast The north east (top-right) corner as coordinate array.
     * @param {Number} zoom The map zoom level.
     */
    setMapBounds(southWest, northEast, zoom) {
        this.bounds = { coords: [southWest, northEast], zoom };
    }

    /**
     * Sets the time interval for this request.
     * @param {Number} start The start date in unix time format.
     * @param {Number} end The end date in unix time format.
     */
    setTimeInterval(start, end) {
        // Save the time intervals
        this.timeInterval = { start, end };
    }

    /**
     * Sets the time slots for this request
     * @param {String} days The weekdays as converted bitstring
     */
    setTimeSlots(days = []) {
        this.timeSlots = days;
    }

    /**
    * Sets the packing options for this request.
    * @param {Object} options The options.
    */
    setOptions(options) {
        console.log(options);

        this.options = {
            packing: !isUndefined(options.packing) ? options.packing : false,
            level: !isUndefined(options.level) ? options.level : -1,
            exportType: !isUndefined(options.exportType) ? options.exportType : "",
            prunning: !isUndefined(options.prunning) ? options.prunning : false,
            cutoff_level: !isUndefined(options.cutoff_level) ? options.cutoff_level : -1,
            min_intersection_filter: !isUndefined(options.min_intersection_filter) ? options.min_intersection_filter : 1,
            weightedEdges: !isUndefined(options.weightedEdges) ? options.weightedEdges : false,
            weightedEdgesBinning: !isUndefined(options.weightedEdgesBinning) ? options.weightedEdgesBinning : -1,
            performLocalSearch: !isUndefined(options.performLocalSearch) ? options.performLocalSearch : false,
            useBidirectionalEdges: !isUndefined(options.useBidirectionalEdges) ? options.useBidirectionalEdges : false,
            graph_level: !isUndefined(options.graph_level) ? options.graph_level : -1,
            useNodeCounter: !isUndefined(options.useNodeCounter) ? options.useNodeCounter : false,
            showAuxLines : !isUndefined(options.showAuxLines) ? options.showAuxLines : false,
        };
    }

}

/**
 * The ShapeContainer class.
 */
class ShapeContainer extends Component {

    /**
     * ShapeContainer
     */
    constructor() {

        // Create the groups.
        super({
            rectangle: new Component({
                _nodes: new Group([
                    { key: 'min', value: undefined },
                    { key: 'max', value: undefined },
                ])
            })
        });

        // Create the supported shape types.
        this._shapeTypes = [
            'none',
            'rectangle'
        ];

        this._shapeType = 'none';
    }

    reset(types) {
        var words = types.split(' ');
        words.forEach(type => {
            switch (type) {
                case 'rectangle':
                    this._clearNodes('rectangle');
                    break;
                case 'all':
                    this._clearNodes('rectangle');
            }
        });
    }

    /* Type functions */

    type(shapeType = '') {
        if (isEmpty(shapeType))
            return this._shapeType;
        this._shapeType = this.hasType(shapeType) ? shapeType : undefined;
    }

    hasType(shapeType) {
        return this._shapeTypes.includes(shapeType);
    }

    resetType() {
        this._shapeType = 'none';
    }

    /* Node functions */

    addNode(component, key, node) {
        this[component].add('_nodes', key, node);
    }

    removeNode(component, key) {
        this[component].remove('_nodes', key);
    }

    node(component, key, node = undefined) {
        if (isUndefined(key))
            return this[component]._nodes;
        else if (isUndefined(node))
            return this[component].get('_nodes', key);
        this[component].set('_nodes', key, node);
    }

    hasNode(component, key) {
        return this[component].has('_nodes', key);
    }

    _clearNodes(component) {
        this[component]._nodes.forEach((key, _node) => {
            this[component]._nodes[key] = undefined;
        });
    }

    /**
     * Converts a shape to the correct representation of geometric values
     */
    transform(component) {

        switch (component) {
            case 'rectangle':
                // Check if both coordiantes are set
                if (isEmpty(this.node('rectangle', 'min')) || isEmpty(this.node('rectangle', 'max')))
                    return;

                let min = this.node('rectangle', 'min');
                let max = this.node('rectangle', 'max');

                // 4 "normal" possible rectangle cases
                if (min._lat < max._lat && min._lng < max._lng) {
                    // Rectangle correct

                } else if (min._lat < max._lat && min._lng > max._lng) {
                    // Max is right from min, switch lngs
                    let tmpLng = min._lng;
                    min._lng = max._lng;
                    max._lng = tmpLng;

                } else if (min._lat > max._lat && min._lng < max._lng) {
                    // Min is on top of max, switch lats
                    let tmpLat = min._lat;
                    min._lat = max._lat;
                    max._lat = tmpLat;

                } else if (min._lat > max._lat && min._lng > max._lng) {
                    // Rectangle is correct, but min and max are switched
                    let tmp = min;
                    min = max;
                    max = tmp;

                } else {
                    // Rectangle is a point or line
                }

                // Replace the transformed values
                this.node('rectangle', 'min', min);
                this.node('rectangle', 'max', max);

            default:

        }
        return this[component]._nodes;
    }
}

/**
 * The MapContainer class.
 */
class MapContainer extends Component {

    /**
     * MapContainer
     * @constructor
     * @param {*} domId 
     * @param {*} apiKey 
     * @param {*} options 
     */
    constructor(domId, apiKey, options) {

        // Create the groups
        super({
            _layerGroups: new Group(),
            _controls: new Group(),
            _fields: new Group(),
            _buttons: new Group()
        });

        // Create the supported shape types.
        this._mapStates = [
            'none',
            'drawing',
            'drawing-active',
            'created',
            'editing',
        ];

        // Save the options and the apiKey
        this._options = {};
        this._options.apiKey = apiKey;
        if (isArray(options.home)) {
            this._options.home = options.home;
        } else {
            if (isUndefined(options.home.coords) || !isArray(options.home.coords)) {
                console.error('Invalid home point format! The MapContainer can\'t be created.');
                return;
            }
            this._options.home = options.home.coords;
            this._options.marker = options.home.marker;
        }

        // Hard-coded max zoom
        this._options.maxZoom = 18;

        // Create the map
        this._map = L.map(
            domId,
            {
                center: this._options.home,
                zoom: this._options.maxZoom
            }
        );

        // Initialize the state
        this._mapState = 'none';

        // Initialize the groups
        this._init();

    }

    _init() {

        /* Layers */

        this.add(
            '_layerGroups',
            'tiles',
            L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=" + this._options.apiKey, {
                attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
                tileSize: 512,
                maxZoom: this._options.maxZoom,
                zoomOffset: -1,
                id: "mapbox/light-v10"
            }).addTo(this._map)
        );

        this.add('_layerGroups', 'tiling_overlay', new L.FeatureGroup().addTo(this._map));

        this.add('_layerGroups', 'draw', new L.FeatureGroup().addTo(this._map));

        this.add('_layerGroups', 'trajectories', new L.FeatureGroup().addTo(this._map));

        this.add('_layerGroups', 'edges', new L.FeatureGroup().addTo(this._map));

        this.add('_layerGroups', 'passing_trajectories', new L.FeatureGroup().addTo(this._map));

        this.add('_layerGroups', 'heatmap', new L.FeatureGroup().addTo(this._map));

        this.add('_layerGroups', 'markers', new L.FeatureGroup().addTo(this._map));

        // Create a home marker if set in options
        if (this._options.marker !== false) {
            this._createHomeMarker();
        }

        /* Controls */

        let baseColor = '#b82c16';

        this.add(
            '_controls',
            'draw',
            new L.Control.Draw({
                position: 'topleft',
                draw: {
                    polygon: false,
                    polyline: false, // Turn off line drawing
                    rectangle: {
                        shapeOptions: {
                            color: baseColor,
                        },
                        metric: true,
                    },
                    circle: false,
                    marker: false, // Turn off marker placing
                    circlemarker: false, // Turn off circlemarker placing
                },
                edit: {
                    featureGroup: this.layerGroup('draw'),
                    remove: true
                }
            })
        );
        this._map.addControl(this.get('_controls', 'draw'));

        L.Control.Home = L.Control.extend({
            options: {
                position: 'topright'
            },
            onAdd: (map) => {
                var container = L.DomUtil.create('div', 'leaflet-control-btn leaflet-bar leaflet-control');
                container.innerHTML = '<i class="fas fa-home"></i>'
                container.title = 'Go home';
                container.style.paddingLeft = '6px'
                container.onclick = () => {
                    map.flyTo(this._options.home, this._options.maxZoom);
                }
                L.DomEvent.disableClickPropagation(container);
                return container;
            }
        });
        this.add('_controls', 'home', new L.Control.Home());
        this._map.addControl(this.get('_controls', 'home'));

        /* Fields */

        $('.leaflet-bottom.leaflet-left').html(
            '<div id="map-coords" class="leaflet-control-attribution leaflet-control">'
            + '<a href="https://en.wikipedia.org/wiki/Latitude">Latitude</a>: <span id="map-lat">' + this._options.home[0].toFixed(5) + '</span> | '
            + '<a href="https://en.wikipedia.org/wiki/Longitude">Longitude</a>: <span id="map-lng">' + this._options.home[1].toFixed(5) + '</span> | '
            + 'Zoom: <span id="map-zoom">' + this._map.getZoom() + '</span>'
            + '</div>'
        );

        this.add('_fields', 'coords-lat$', $('#map-coords > #map-lat'));

        this.add('_fields', 'coords-lng$', $('#map-coords > #map-lng'));

        this.add('_fields', 'coords-zoom$', $('#map-coords > #map-zoom'));

        /* Buttons */

        this.add(
            '_buttons',
            'draw-rectangle$',
            $('.leaflet-draw-draw-rectangle')
        )

        this.add(
            '_buttons',
            'edit$',
            $('.leaflet-draw-edit-edit')
        )

        this.add(
            '_buttons',
            'remove$',
            $('.leaflet-draw-edit-remove')
        )

    }

    /**
     * Multiple type support, splitted on space
     * @param {string} types 
     */
    reset(types) {
        var words = types.split(' ');
        words.forEach(type => {
            switch (type) {
                case 'tiles':
                    return;
                case 'tiling_overlay':
                    this._clearLayerGroup('tiling_overlay');
                    break;
                case 'draw':
                    this._clearLayerGroup('draw');
                    break;
                case 'trajectories':
                    this._clearLayerGroup('trajectories');
                    break;
                case 'edges':
                    this._clearLayerGroup('edges');
                    break;
                case 'passing_trajectories':
                    this._clearLayerGroup('passing_trajectories');
                    break;
                case 'heatmap':
                    this._clearLayerGroup('heatmap');
                    break;
                case 'all':
                default:
                    this._clearLayerGroup('draw');
                    this._clearLayerGroup('trajectories');
                    this._clearLayerGroup('edges');
                    this._clearLayerGroup('passing_trajectories');
                    this._clearLayerGroup('heatmap');
            }
        });
    }

    /* Map functions */

    getMap() {
        return this._map;
    }

    /* State functions */

    state(mapState = '') {
        if (isEmpty(mapState))
            return this._mapState;
        this._mapState = this.hasState(mapState) ? mapState : undefined;
    }

    hasState(mapState) {
        return this._mapStates.includes(mapState);
    }

    resetState() {
        this._mapState = 'none';
    }

    _pointlist_to_polyline(pointlist){
        var line_list = [];
        for(var i = 1; i <= pointlist.length; i+=2) {
            var lat = pointlist[i-1];
            var lon = pointlist[i];
            line_list.push([lat, lon]);
        }
        return line_list;
    }

    clear_tiling(){
        console.log("Clearing tiling layer.")
        this.reset("tiling_overlay")
        this._map.removeLayer(this.layerGroup('tiling_overlay'));
    }

    set_tiling(link){
        console.log("Setting up tiling: " + link);
        this._map.removeLayer(this.layerGroup('tiling_overlay'));
        var tiling_overlay = new L.FeatureGroup().addTo(this._map);
        var tiling_layer = L.tileLayer.wms(link, 
        {
            format: 'image/png',
            transparent: true,
            crs: L.CRS.Simple
        });
        tiling_overlay.addLayer(tiling_layer);
        this.set('_layerGroups', 'tiling_overlay', tiling_overlay);
    }

    /* Draw functions */

    draw(type, item, options) {
        if (type != 'rectangle'){
            this.clear_tiling();
        }

        switch (type) {
            case 'rectangle':
                var rect = item.rectangle;
                var leafletRect = L.rectangle(
                    [rect.min.toArray(), rect.max.toArray()],
                    {
                        color: 'gray'
                    }
                );
                this.addLayer('draw', leafletRect);
                this.state('created');
                break;

            case 'trajectories':
                var start = new Date(); 
                var newPathLayer = new L.FeatureGroup().addTo(this._map);

                // y = min(1,exp(-0.05x)+0.1
                var trajectories = item.trajectories;
                var opacity = Math.min(1, Math.exp(-0.05 * trajectories.length)+0.1);
                var style = {
                    color: '#dc3545',
                    opacity: opacity,
                    weight: 3,
                    clickable: true
                }
                trajectories.forEach((trajectory, index) => {
                    // Create the polyline
                    var polyLine = L.polyline(trajectory.coords, style);

                    // Create a popup for further information
                    var popupHTML = `<div class="content-trajectory">`
                        + `<h6>Trajectory</h6>`
                        + `<table>`
                        + `<tr><th>Id:</th><td>${index + 1}</td></tr>`
                        + `<tr><th>Length:</th><td>${distanceToString(trajectory.length)}</td></tr>`;
                    if (!isUndefined(trajectory.timestamp)) {
                        var start = trajectory.timestamp.start,
                            end = trajectory.timestamp.end;
                        if (start == -1 || end == -1) {
                            popupHTML += `<tr><th>Timestamp:</th><td><i>Invalid</i></td></tr>`;
                        } else if (start === end) {
                            popupHTML += `<tr><th>Timestamp:</th><td>${unixToDate(start)}</td></tr>`;
                        } else {
                            popupHTML += `<tr><th>Timestamp:</th><td>${unixToDate(start)} -<br/>${unixToDate(end)}</td></tr>`;
                        }
                    }
                    popupHTML += `</table></div>`;
                    polyLine.bindPopup(popupHTML);

                    // Polyline actions
                    var clicked = false;
                    polyLine.on({
                        'mouseover': e => {
                            var layer = e.target;
                            if (!clicked)
                                layer.setStyle({
                                    color: '#007bff',
                                    opacity: 1,
                                    weight: 3
                                });
                            layer.bringToFront();
                        },
                        'mouseout': e => {
                            var layer = e.target;
                            if (!clicked)
                                layer.setStyle(style);
                        },
                        'popupopen': e => {
                            var layer = e.target;
                            layer.setStyle({
                                color: '#0063cc',
                                opacity: 1,
                                weight: 4
                            });
                            clicked = true;
                        },
                        'popupclose': e => {
                            var layer = e.target;
                            layer.setStyle(style);
                            clicked = false;
                        }
                    });

                    // Add the trajectory to the new path layer
                    polyLine.addTo(newPathLayer);
                });
                this._map.removeLayer(this.layerGroup('trajectories'));
                this.set('_layerGroups', 'trajectories', newPathLayer);
                var time_difference = new Date() - start; 
                if (options.debug){
                    console.log(time_difference + " needed for plotting.");
                }
                break;
            

            case 'weighted_edges':
                var start = new Date(); 
                var newEdgesLayer = new L.FeatureGroup().addTo(this._map);
                var zoom = this._map._zoom;
                
                var edges = item.weighted_edges[0];
                var weights = item.weights;
                
                edges.forEach((edgeset, index) => {
                var opacity = 1.0;
                var weight = 3;
                if(options.edgePlottingType.includes(EDGE_PLOT_WEIGHT_TYPE__TRANSPARENCY)){
                    opacity = lineOpacity(weights[index], zoom);
                } 
                if(options.edgePlottingType.includes(EDGE_PLOT_WEIGHT_TYPE__THICKNESS)){
                    weight = lineThickness(weights[index], zoom);
                }

                    var style = {
                        color: '#540cc9',
                        opacity: opacity,
                        weight: weight,
                        clickable: false
                    }
                    // Create the polyline
                    var polyLine = L.polyline(edgeset, style);
                    
                    // Add the trajectory to the new path layer
                    polyLine.addTo(newEdgesLayer);
                });
                this._map.removeLayer(this.layerGroup('edges'));
                this.set('_layerGroups', 'edges', newEdgesLayer);

                // print debug details
                var time_difference = new Date() - start; 
                if (options.debug){
                    console.log(time_difference + " needed for plotting.");
                }
                break;
            

            case 'weighted_binned_edges':
                var start = new Date(); 
                var newEdgesLayer = new L.FeatureGroup().addTo(this._map);
                var zoom = this._map._zoom;
                                
                item.weighted_binned_edges.forEach((group, group_index) => {
    
                    var group_edges = group['edges'];
                    var edges_weight = group['weight'];
                
                    var opacity = 1.0;
                    var weight = 3;
                    if(options.edgePlottingType.includes(EDGE_PLOT_WEIGHT_TYPE__TRANSPARENCY)){
                        opacity = lineOpacity(edges_weight, zoom);
                    } 
                    if(options.edgePlottingType.includes(EDGE_PLOT_WEIGHT_TYPE__THICKNESS)){
                        weight = lineThickness(edges_weight, zoom);
                    }

                        var style = {
                            color: '#540cc9',
                            opacity: opacity,
                            weight: weight,
                            clickable: false
                        }
                        // Create the polyline
                        var polyLine = L.polyline(group_edges, style);
                        
                        // Add the trajectory to the new path layer
                        polyLine.addTo(newEdgesLayer);
                    });
                this._map.removeLayer(this.layerGroup('edges'));
                this.set('_layerGroups', 'edges', newEdgesLayer);
                
                // print debug details
                var time_difference = new Date() - start; 
                if (options.debug){
                    console.log(time_difference + " needed for plotting.")
                }
                break;
            
            
            case 'edges':
                var start = new Date(); 
                var newPathLayer = new L.FeatureGroup().addTo(this._map);

                // -- add edges ------
                var edges = item.edges;
                var style = {
                    color: '#222222',
                    opacity: 1,
                    weight: 3,
                    clickable: false
                }
                var polyLine = L.polyline(edges, style);
                polyLine.addTo(newPathLayer);

                // -- add heatmap ------
                if('heatmap' in item){
                    var heatmap_data = item.heatmap;
                    var heatmap = L.heatLayer(heatmap_data, {radius: options.heatmapRadius});
                    heatmap.addTo(newPathLayer);
                }

                this._map.removeLayer(this.layerGroup('edges'));
                this.set('_layerGroups', 'edges', newPathLayer);
                var time_difference = new Date() - start; 
                if (options.debug){
                    console.log(time_difference + " needed for plotting.");
                }
                break;

            case 'colorcoded_edges':
                var start = new Date(); 
                var newPathLayer = new L.FeatureGroup().addTo(this._map);

                var show_arrows = true;

                
                var edge_data = item.colorcoded_edges;
                if (show_arrows){
                    convert_to_arrow_list(edge_data.shortcuts);
                    convert_to_arrow_list(edge_data.decrease_plain);
                    convert_to_arrow_list(edge_data.increase_plain);
                    convert_to_arrow_list(edge_data.aux_shortcuts);
                }
                // -- add edges ------
                var shortcuts = edge_data.shortcuts;
                var decrease_plain = edge_data.decrease_plain;
                var increase_plain = edge_data.increase_plain;
                var auxiliary_shortcuts = edge_data.aux_shortcuts;
                
                var shortcuts_style = {
                    color: '#008800',
                    opacity: 1,
                    weight: 3,
                    dashArray: "4",
                    clickable: false
                };
                var auxiliary_shortcuts_style = {
                    color: '#ff0990 ',
                    opacity: .8,
                    weight: 2,
                    dashArray: "2 4",
                    clickable: false
                };
                var increase_plain_style = {
                    color: '#bb0000',
                    opacity: 0.7,
                    weight: 4,
                    clickable: false
                } ;
                var decrease_plain_style = {
                    color: '#0000bb',
                    opacity: 0.7,
                    weight: 4,
                //    dashArray: "2",
                    clickable: false
                };

                L.polyline(auxiliary_shortcuts, auxiliary_shortcuts_style).addTo(newPathLayer);
                L.polyline(shortcuts, shortcuts_style).addTo(newPathLayer);
                L.polyline(increase_plain, increase_plain_style).addTo(newPathLayer);
                L.polyline(decrease_plain, decrease_plain_style).addTo(newPathLayer);


                this._map.removeLayer(this.layerGroup('edges'));
                this.set('_layerGroups', 'edges', newPathLayer);
                var time_difference = new Date() - start; 
                if (options.debug){
                    console.log(time_difference + " needed for plotting.");
                }
                break;


            case 'trajectorySegmentGraph':
                // code for plotting the traj graph

                var newPathLayer = new L.FeatureGroup().addTo(this._map);

                var trajectory_graph = item.trajectorySegmentGraph;
                var segments = trajectory_graph.segments;

                for (var seg_id in segments){
                    let segment = segments[seg_id];

                    // Create the polyline
                    let style = {
                        opacity: 1,
                        weight: 3,
                        clickable: true,
                        color : segment.c
                    };
                    var polyLine = L.polyline(this._pointlist_to_polyline(segment.n), style);

                    // Create a popup for further information
                    var popupHTML = `<div class="content-trajectory">`
                        + `<h6>Trajectory</h6>`
                        + `<table>`
                        + `<tr><th>Id:</th><td>${seg_id}</td></tr>`
                        + `<tr><th>Super-Imposition-ID:</th><td>${segment.sid}</td></tr>`
                        + `<tr><th>Trajectories:</th><td>${segment.t}</td></tr>`
                        + `<tr><th>u:</th><td>${segment.u}</td></tr>`
                        + `<tr><th>v:</th><td>${segment.v}</td></tr>`;
                    popupHTML += `</table></div>`;
                    polyLine.bindPopup(popupHTML);

                    // Polyline actions
                    var clicked = false;
                    polyLine.on({
                        'mouseover': e => {
                            var layer = e.target;
                            if (!clicked)
                                layer.setStyle({
                                    color: '#ADD8E6',
                                    opacity: 1,
                                    weight: 6
                                });
                            layer.bringToFront();
                        },
                        'mouseout': e => {
                            var layer = e.target;
                            if (!clicked)
                                layer.setStyle(style);
                        },
                        'popupopen': e => {
                            var layer = e.target;
                            layer.setStyle({
                                color: '#0063cc',
                                opacity: 1,
                                weight: 4
                            });
                            clicked = true;

                            // show all trajectories which pass through the current segment.
                            let passing_style = {
                                opacity: 1,
                                weight: 4,
                                clickable: false,
                                color : '#007bff'
                            };


                            function non_empty_intersection(a, b)
                            {
                            let idx_a = 0
                            let idx_b = 0
                            while( a.length > idx_a && b.length > idx_b )
                            {  
                                if      (a[idx_a] < b[idx_b]) { idx_a += 1 }
                                else if (a[idx_a] > b[idx_b]) { idx_b += 1 }
                                else { return true }
                            }
                            return false;
                            }

                            let passingPathLayer = new L.FeatureGroup().addTo(this._map);
                            for (let other_seg_id in segments){
                                let other_segment = segments[other_seg_id];
                                if(non_empty_intersection(segment.t, other_segment.t)){
                                    var segment_polyLine = L.polyline(this._pointlist_to_polyline(other_segment.n), passing_style);
                                    segment_polyLine.addTo(passingPathLayer);
                                 } 
                            }
                            this.set('_layerGroups', 'passing_trajectories', passingPathLayer);

                        },
                        'popupclose': e => {
                            var layer = e.target;
                            layer.setStyle(style);
                            clicked = false;
                            this._map.removeLayer(this.layerGroup('passing_trajectories'));
                        }
                    });

                    // Add the trajectory to the new path layer
                    polyLine.addTo(newPathLayer);
                }
                this._map.removeLayer(this.layerGroup('trajectories'));
                this.set('_layerGroups', 'trajectories', newPathLayer);
                break;


            case 'trajectoryHeatmap':
                // code for plotting the request heatmap

                var newHeatLayer = new L.FeatureGroup().addTo(this._map);

                var heatmap_data = item.trajectoryHeatmap;
                var heatmap = L.heatLayer(heatmap_data, {radius: 25});
                heatmap.addTo(newHeatLayer);
                this._map.removeLayer(this.layerGroup('trajectories'));
                this.set('_layerGroups', 'trajectories', newHeatLayer);
                break;


            case 'heatmap':
                // code for plotting the heatmap
                var newHeatLayer = new L.FeatureGroup().addTo(this._map);

                var heatmap_data = item.heatmap;
                var heatmap = L.heatLayer(heatmap_data, {radius: 25});
                heatmap.addTo(newHeatLayer);
                this._map.removeLayer(this.layerGroup('heatmap'));
                this.set('_layerGroups', 'heatmap', newHeatLayer);
                break;


            case 'InitPlainInorderBatch':
                // Code for plotting the initial batch results (u and v only)
                var newPathLayer = new L.FeatureGroup().addTo(this._map);

                var trajectories = item.InitPlainInorderBatch;
                var style = {
                    color: '#8000FF',
                    opacity: 1,
                    weight: 3,
                    clickable: true
                }
                
                var polylines = []
                for (var path_id in trajectories){
                    
                    let path = trajectories[path_id];

                    var polyLine = L.polyline(path.uv, style);
                    polylines.push(polyLine)

                    // Create a popup for further information
                    var popupHTML = `<div class="content-trajectory">`
                        + `<h6>Trajectory</h6>`
                        + `<table>`
                        + `<tr><th>Global-ID:</th><td>${path_id}</td></tr>`
                        + `<tr><th>Length:</th><td>${distanceToString(path.length)}</td></tr>`;
                    if (!isUndefined(path.timestamp)) {
                        var start = path.timestamp.start,
                            end = path.timestamp.end;
                        if (start == -1 || end == -1) {
                            popupHTML += `<tr><th>Timestamp:</th><td><i>Invalid</i></td></tr>`;
                        } else if (start === end) {
                            popupHTML += `<tr><th>Timestamp:</th><td>${unixToDate(start)}</td></tr>`;
                        } else {
                            popupHTML += `<tr><th>Timestamp:</th><td>${unixToDate(start)} -<br/>${unixToDate(end)}</td></tr>`;
                        }
                    }
                    popupHTML += `</table></div>`;
                    polyLine.bindPopup(popupHTML);

                    // Polyline actions
                    var clicked = false;
                    polyLine.on({
                        'mouseover': e => {
                            var layer = e.target;
                            if (!clicked)
                                layer.setStyle({
                                    color: '#007bff',
                                    opacity: 1,
                                    weight: 3
                                });
                            layer.bringToFront();
                        },
                        'mouseout': e => {
                            var layer = e.target;
                            if (!clicked)
                                layer.setStyle(style);
                        },
                        'popupopen': e => {
                            var layer = e.target;
                            layer.setStyle({
                                color: '#0063cc',
                                opacity: 1,
                                weight: 4
                            });
                            clicked = true;
                        },
                        'popupclose': e => {
                            var layer = e.target;
                            layer.setStyle(style);
                            clicked = false;
                        }
                    });

                    // Add the trajectory to the new path layer
                    polyLine.addTo(newPathLayer);
                }
                this._map.removeLayer(this.layerGroup('trajectories'));
                this.set('_layerGroups', 'trajectories', newPathLayer);
                return polylines;
    
                    
            case 'InitPlainLevelorderBatch':
                // Code for plotting the initial batch results (u and v only)
                var newPathLayer = new L.FeatureGroup().addTo(this._map);

                var trajectories = item.InitPlainLevelorderBatch;
                var style = {
                    color: '#003600',
                    opacity: 1,
                    weight: 3,
                    clickable: true
                }
                
                var polylines = []
                for (var path_id in trajectories){
                    
                    let path = trajectories[path_id];

                    var polyLine = L.polyline(path.uv, style);
                    polylines.push(polyLine)

                    // Create a popup for further information
                    var popupHTML = `<div class="content-trajectory">`
                        + `<h6>Trajectory</h6>`
                        + `<table>`
                        + `<tr><th>Global-ID:</th><td>${path_id}</td></tr>`
                        + `<tr><th>Length:</th><td>${distanceToString(path.length)}</td></tr>`;
                    if (!isUndefined(path.timestamp)) {
                        var start = path.timestamp.start,
                            end = path.timestamp.end;
                        if (start == -1 || end == -1) {
                            popupHTML += `<tr><th>Timestamp:</th><td><i>Invalid</i></td></tr>`;
                        } else if (start === end) {
                            popupHTML += `<tr><th>Timestamp:</th><td>${unixToDate(start)}</td></tr>`;
                        } else {
                            popupHTML += `<tr><th>Timestamp:</th><td>${unixToDate(start)} -<br/>${unixToDate(end)}</td></tr>`;
                        }
                    }
                    popupHTML += `</table></div>`;
                    polyLine.bindPopup(popupHTML);

                    // Polyline actions
                    var clicked = false;
                    polyLine.on({
                        'mouseover': e => {
                            var layer = e.target;
                            if (!clicked)
                                layer.setStyle({
                                    color: '#007bff',
                                    opacity: 1,
                                    weight: 3
                                });
                            layer.bringToFront();
                        },
                        'mouseout': e => {
                            var layer = e.target;
                            if (!clicked)
                                layer.setStyle(style);
                        },
                        'popupopen': e => {
                            var layer = e.target;
                            layer.setStyle({
                                color: '#0063cc',
                                opacity: 1,
                                weight: 4
                            });
                            clicked = true;
                        },
                        'popupclose': e => {
                            var layer = e.target;
                            layer.setStyle(style);
                            clicked = false;
                        }
                    });

                    // Add the trajectory to the new path layer
                    polyLine.addTo(newPathLayer);
                }
                this._map.removeLayer(this.layerGroup('trajectories'));
                this.set('_layerGroups', 'trajectories', newPathLayer);
                return polylines;

    
            case 'InitEdgeWiseBatch':
                // Code for plotting the initial batch results (u and v only)
                var newPathLayer = new L.FeatureGroup().addTo(this._map);

                var trajectories = item.InitEdgeWiseBatch;
                var style = {
                    color: '#FF0080',
                    opacity: 1,
                    weight: 3,
                    clickable: true
                }
                
                var polylines = []
                for (var path_id in trajectories){
                    
                    let path = trajectories[path_id];

                    var polyLine = L.polyline(path.uv, style);
                    polylines.push(polyLine)

                    // Create a popup for further information
                    var popupHTML = `<div class="content-trajectory">`
                        + `<h6>Trajectory</h6>`
                        + `<table>`
                        + `<tr><th>Global-ID:</th><td>${path_id}</td></tr>`
                        + `<tr><th>Length:</th><td>${distanceToString(path.length)}</td></tr>`;
                    if (!isUndefined(path.timestamp)) {
                        var start = path.timestamp.start,
                            end = path.timestamp.end;
                        if (start == -1 || end == -1) {
                            popupHTML += `<tr><th>Timestamp:</th><td><i>Invalid</i></td></tr>`;
                        } else if (start === end) {
                            popupHTML += `<tr><th>Timestamp:</th><td>${unixToDate(start)}</td></tr>`;
                        } else {
                            popupHTML += `<tr><th>Timestamp:</th><td>${unixToDate(start)} -<br/>${unixToDate(end)}</td></tr>`;
                        }
                    }
                    popupHTML += `</table></div>`;
                    polyLine.bindPopup(popupHTML);

                    // Polyline actions
                    var clicked = false;
                    polyLine.on({
                        'mouseover': e => {
                            var layer = e.target;
                            if (!clicked)
                                layer.setStyle({
                                    color: '#007bff',
                                    opacity: 1,
                                    weight: 3
                                });
                            layer.bringToFront();
                        },
                        'mouseout': e => {
                            var layer = e.target;
                            if (!clicked)
                                layer.setStyle(style);
                        },
                        'popupopen': e => {
                            var layer = e.target;
                            layer.setStyle({
                                color: '#0063cc',
                                opacity: 1,
                                weight: 4
                            });
                            clicked = true;
                        },
                        'popupclose': e => {
                            var layer = e.target;
                            layer.setStyle(style);
                            clicked = false;
                        }
                    });

                    // Add the trajectory to the new path layer
                    polyLine.addTo(newPathLayer);
                }
                this._map.removeLayer(this.layerGroup('trajectories'));
                this.set('_layerGroups', 'trajectories', newPathLayer);
                return polylines;
            
        }
    }

    /* Layer group functions */

    layerGroup(key, layerGroup = undefined) {
        if (isUndefined(key))
            return this._layerGroups;
        else if (isUndefined(layerGroup))
            return this.get('_layerGroups', key);
        this.set('_layerGroups', key, layerGroup);
    }

    hasLayerGroup(key) {
        return this.has('_layerGroups', key);
    }

    _clearLayerGroup(key) {
        let layers = this.layerGroup(key)._layers;
        for (let layer in layers)
            this.layerGroup(key).removeLayer(layer);
    }

    /* Layer functions */

    addLayer(group, layer) {
        this.layerGroup(group).addLayer(layer);
    }

    /* Button functions */

    button(key, button = undefined) {
        if (isUndefined(key))
            return this._buttons;
        else if (isUndefined(button))
            return this.get('_buttons', key);
        this.set('_buttons', key, button);
    }

    /* Coordinate functions */

    coordsField(e = undefined) {
        if (isUndefined(e))
            return [this.get('_fields', 'coords-lat$'), this.get('_fields', 'coords-lng$')];
        this.get('_fields', 'coords-lat$').text(e.latlng.lat.toFixed(5));
        this.get('_fields', 'coords-lng$').text(e.latlng.lng.toFixed(5));
    }

    zoomField(zoom = undefined) {
        if (isUndefined(zoom))
            return this.get('_fields', 'coords-zoom$');
        zoom = zoom % 1 == 0 ? zoom : zoom.toFixed(2);
        this.get('_fields', 'coords-zoom$').text(zoom);
    }

    /* Marker functions */

    _createHomeMarker() {
        var homeMarker = L.marker(this._options.home);
        var popupHTML = '';

        // Create a popup for marker information if set in options
        if (hasProperty(this._options.marker, 'heading'))
            popupHTML += `<h6>${this._options.marker.heading}</h6>`
        else
            popupHTML += `<h6>Home</h6>`

        if (hasProperty(this._options.marker, 'text')) {
            popupHTML += `<p>${this._options.marker.text}</p>`;
        }

        homeMarker.bindPopup(popupHTML);
        this.addLayer('markers', homeMarker);
    }

}

/**
 * The SidebarContainer class.
 */
class SidebarContainer extends Component {

    /**
     * Creates a new SidebarContainer instance.
     * @constructor
     */
    constructor() {
        super({
            rectangle: new Component({
                _fields: new Group([
                    { key: 'min-lat$', value: $('#rect-min-lat input') },
                    { key: 'min-lon$', value: $('#rect-min-lon input') },
                    { key: 'max-lat$', value: $('#rect-max-lat input') },
                    { key: 'max-lon$', value: $('#rect-max-lon input') },
                ]),
                _buttons: new Group([
                    { key: 'draw$', value: $('#rect-draw') },
                ])
            }),
            exportType: new Component({
                _fields: new Group([
                    { key: 'export-traj$', value: $('#export-type a').eq(0) },
                    { key: 'export-segmentgraph$', value: $('#export-type a').eq(1) },
                    { key: 'export-heatmap$', value: $('#export-type a').eq(2) },
                    { key: 'export-batch$', value: $('#export-type a').eq(3) },
                    { key: 'export-batch-plain-inorder$', value: $('#method_batch input').eq(0) },
                    { key: 'export-batch-plain-levelorder$', value: $('#method_batch input').eq(1) },
                    { key: 'export-batch-edgewise-queue$', value: $('#method_batch input').eq(2) },
                    { key: 'export-prunning$', value: $('#export-type a').eq(4) },
                    { key: 'export-edge-based-pathfinder$', value: $('#export-type a').eq(5) },
                    { key: 'export-graph-section$', value: $('#export-type a').eq(6) },
                    { key: 'export-graph-section-topology$', value: $('#export-type a').eq(7) }
                ])
            }),
            advInterval: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-interval .component-toggle input').first() }
                ]),
                _fields: new Group([
                    { key: 'start$', value: $('#adv-interval #interval-start input') },
                    { key: 'end$', value: $('#adv-interval #interval-end input') }
                ])
            }),
            advDays: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-days .component-toggle input').first() }
                ]),
                _fields: new Group([
                    { key: 'days$', value: $('#adv-days #days-boxes input') }
                ])
            }),
            advHeatmap: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-heatmap .component-toggle input').first() }
                ])
            }),
            advTiling: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-tiling .component-toggle input').first() }
                ])
            }),
            advLocalSearch: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-local-search .component-toggle input').first() }
                ])
            }),
            advBidirectionalEdges: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-bidirectional-edges .component-toggle input').first() }
                ])
            }),
            advUseNodeCount: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-use-nodecount .component-toggle input').first() }
                ])
            }),
            advPacking: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-packing .component-toggle input').first() }
                ]),
                _fields: new Group([
                    { key: 'type-zoom$', value: $('#adv-packing #packing-type input').eq(0) },
                    { key: 'type-level$', value: $('#adv-packing #packing-type input').eq(1) },
                    { key: 'level$', value: $('#adv-packing #packing-level input') },
                    { key: 'updates$', value: $('#adv-packing #packing-updates input') }
                ])
            }),
            advPrunning: new Component({
                _fields: new Group([
                    { key: 'level$', value: $('#adv-prunning #prunning-level input') }
                ])
            }),
            advMinIntersections: new Component({
                _fields: new Group([
                    { key: 'count$', value: $('#adv-minintersection #minintersection input') }
                ])
            }),
            advWeightedEdges: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-edgebased .component-toggle input').first() }
                ]),
                _fields: new Group([
                    { key: 'type-transparency$', value: $('#adv-edgebased #weight-type input').eq(0) },
                    { key: 'type-thickness$', value: $('#adv-edgebased #weight-type input').eq(1) },
                    { key: 'type-both$', value: $('#adv-edgebased #weight-type input').eq(2) }
                ])
            }),
            weightedEdgesBinning: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-binning .component-toggle input').first() }
                ]),
                _fields: new Group([
                    { key: 'edge-binning-number$', value: $('#adv-binning #binning-size input').first() }
                ])
            }),
            advButtons: new Component({
                _buttons: new Group([
                    { key: 'calculate$', value: $('#adv-buttons #button-calculate') },
                    { key: 'save$', value: $('#adv-buttons #button-save') },
                    { key: 'reset$', value: $('#adv-buttons #button-reset') }
                ])
            }),
            result: new Component({
                _fields: new Group([
                    { key: 'found$', value: $('#result-found') },
                    { key: 'total$', value: $('#result-total') },
                    { key: 'length$', value: $('#result-length') }
                ]),
                _buttons: new Group([
                    { key: 'calculate$', value: $('#calculate') },
                ])
            }),
            advGraph: new Component({
                _fields: new Group([
                    { key: 'level$', value: $('#adv-graph #graph-level input') },
                    { key: 'showAuxLines$', value: $('#adv-graph #graph-aux-shortcuts input') }
                ])
            }),
        });
    }

    /**
     * Multiple type support, splitted on space
     * @param {*} types 
     */
    reset(types) {
        var words = types.split(' ');
        words.forEach(type => {
            switch (type) {
                case 'shapes':
                    this._clearFields('rectangle');
                    break;
                case 'rectangle':
                    this._clearFields('rectangle');
                    break;
                case 'advanced':
                    this._clearFields('advInterval');
                    this._clearFields('advDays');
                    this._clearFields('advPacking');
                    this._clearFields('advWeightedEdges');
                    break;
                case 'results':
                    this._clearFields('result');
                    break;
                case 'all':
                    this._clearFields('rectangle');
                    this._clearFields('advInterval');
                    this._clearFields('result');
            }
        });
    }

    /* Field functions */

    addField(component, key, field) {
        this[component].add('_fields', key, field);
    }

    removeField(component, key) {
        this[component].remove('_fields', key);
    }

    field(component, key, field = undefined) {
        if (isUndefined(key))
            return this[component]._fields;
        else if (isUndefined(field))
            return this[component].get('_fields', key);
        this[component].set('_fields', key, field);
    }

    fieldVal(component, key, value = undefined) {
        if (isUndefined(value))
            return this[component].get('_fields', key).val();
        this[component].get('_fields', key).val(value);
    }

    fieldText(component, key, text = undefined) {
        if (isUndefined(text))
            return this[component].get('_fields', key).text();
        this[component].get('_fields', key).text(text);
    }

    hasField(component, key) {
        return this[component].has('_fields', key);
    }

    _clearFields(component) {
        this[component]._fields.forEach((key, _field) => {
            this[component].get('_fields', key).val('');
            this[component].get('_fields', key).text('');
        });
    }

    /* Checkbox functions */
    checkbox(component, checkbox = undefined) {
        if (isUndefined(checkbox))
            return this[component].get('_checkbox', 'active$');
        this[component].set('_buttons', 'active$', checkbox);
    }

    active(component, bool = undefined) {
        if (isUndefined(bool))
            return this[component].get('_checkbox', 'active$').prop('checked');
        this[component].get('_checkbox', 'active$').prop('checked', bool);
    }

    /* Button functions */

    button(component, key, button = undefined) {
        if (isUndefined(key))
            return this[component]._buttons;
        else if (isUndefined(button))
            return this[component].get('_buttons', key);
        this[component].set('_buttons', key, button);
    }

    hasButton(component, key) {
        return this[component].has('_buttons', key);
    }

    enableButton(component, key) {
        this[component].get('_buttons', key).prop('disabled', false);
    }

    disableButton(component, key) {
        this[component].get('_buttons', key).prop('disabled', true);
    }

    /* Other functions */

    getIntervalDays() {
        // Retrieve the interval weekday checkboxes
        let checkboxes$ = this.field('advDays', 'days$').find('input').end();
        // Save their checked values in an array
        let boolArray = []
        $.each(checkboxes$, function (index, checkbox) {
            boolArray.push(checkbox.checked);
        })
        // Return the checked value array
        return boolArray;
    }

    /**
     * Validate if values are set and put their values in the result fields
     * @param {*} item 
     */
    setResults(item) {
        // Validate and set found
        var found = !isUndefined(item.found) ? item.found : '-';
        this.fieldText('result', 'found$', found);

        // Validate and set total
        var total = !isUndefined(item.total) ? item.total : '-';
        this.fieldText('result', 'total$', total);

        // Validate and set total length 
        var length = !isUndefined(item.length) && item.length > 0 ? distanceToString(item.length) : '-';
        this.fieldText('result', 'length$', length);
    }

}

/**
 * Some util functions
 */
function hasProperty(object, key) {
    return object ? hasOwnProperty.call(object, key) : false;
}

function typeOf(obj) {
    return toString.call(obj).slice(8, -1).toLowerCase();
}

function isUndefined(value) {
    return typeof value === 'undefined'
}

function isNull(value) {
    return typeof value === 'null'
}

function isEmpty(value) {
    return isUndefined(value) || isNull(value) || (isString(value) && value == '')
}

function isString(value) {
    return typeof value === 'string'
}

function isNaN(value) {
    return Number.isNaN(value);
}

function isNumber(value) {
    return typeof value === 'number' && !isNaN(value)
}

function isArray(value) {
    return typeOf(value) === 'array' && Array.isArray(value);
}

function stringToNode(lat, lon) {
    return new Node(Number(lat), Number(lon))
}

function groupToCoords(group) {
    let coords = [];
    group.forEach((key, _node) => {
        coords.push([_node.getLat(), _node.getLng()]);
    });
    return coords;
}

function checkInterval(start, end) {
    if (isEmpty(start) && !isEmpty(end)) {
        alert("Invalid time interval input! .")
        return new IllegalArgumentException('No start time was set.')
    } else if (!this.isEmpty(start) && this.isEmpty(end)) {
        alert("Invalid time interval input! No end time was set.")
        return false
    } else if (!this.isEmpty(start) && !this.isEmpty(end)) {
        if (start === NaN) {
            alert("Invalid time interval input! The start date is invalid.")
            return false
        } else if (end === NaN) {
            alert("Invalid time interval input! The end date is invalid.")
            return false
        } else if (start < 0 || end < 0) {
            alert("Invalid time interval input! The start or end date is to small.")
            return false
        } else if (start > 2147483647 || end > 2147483647) {
            alert("Invalid time interval input! The start or end date is too big.")
            return false
        } else if (start > end) {
            alert("Invalid time interval input! The start date is greater than the end date.")
            return false
        }
        return true;
    }
    alert("Invalid time interval input! No start and end times were set.")
    return false;
}

function arrayToBitString(array) {
    if (!Array.isArray(array))
        return ""

    let bitString = ""
    array.forEach((val) => {
        bitString += val ? "1" : "0"
    })
    return bitString
}

function distanceToString(dist, metric = true) {
    if (this.isEmpty(dist) || !isNumber(dist))
        return 0;
    else if (dist < 0)
        return NaN;

    var result = "";

    if (metric) {
        // Trajectory lengths are in dm
        dist = dist / 10;
        result = (dist.toString().indexOf(".") < 0 ? dist : dist.toFixed(2)) + " m";
        if (dist > 1000) {
            dist = dist / 1000;
            result = (dist.toString().indexOf(".") < 0 ? dist : dist.toFixed(2)) + " km";
        }
    } else {
        units = ['in', 'ft', 'yd', 'mi']
    }
    return result;
}

function init_reader(){
     // init reader
     var reader;
     if (window.File && window.FileReader && window.FileList && window.Blob) {
         reader = new FileReader();
     } else {
         alert('The File APIs are not fully supported by your browser. Fallback required.');
         return null;
     }
     return reader
}


function readExperimentJSON(filePath, callback) {
    // this code is similar to the example from Stackoverflow (see https://stackoverflow.com/questions/13709482/how-to-read-text-file-in-javascript) 
        
    // read the file using the reader just created
    if(filePath.files && filePath.files[0]) { // if html 5 supported
        let fileReader = init_reader();
        fileReader.addEventListener("load", () =>{
            callback(fileReader.result);
        });
        fileReader.readAsText(filePath.files[0]);
    }
    else { //this is where you could fallback to Java Applet, Flash or similar
        return null;
    }       
} 

function get_draw_type(result){
    let keywords = ['trajectories', 'edges', 'colorcoded_edges', 'weighted_edges', 'weighted_binned_edges', 'trajectorySegmentGraph', 'trajectoryHeatmap', 'InitPlainInorderBatch', 'InitPlainLevelorderBatch', 'InitLevelorderZoomedBatchDecoder', 'InitEdgeWiseBatch'];   
    for(let i in keywords){
        let keyword = keywords[i];
        if (keyword in result){
            return keyword;
        }   
    }
    console.log('Return result-type unknown');
    return null;
}


function unixToDate(unix) {
    return moment.unix(unix).format("MM/DD/YYYY");
}

function lineThickness(edgeweight, zoom){
    console.log(zoom);
    return 0.5 + (0.15 * Math.log10(edgeweight)) * (0.8+ Math.exp(0.42*zoom -8));
}
function lineOpacity(edgeweight, zoom){
    return 0.2 + (0.1 * Math.log10(edgeweight)) * (0.8+ Math.exp(0.42*zoom -8));
}

function convert_to_arrow_list(edge_list){
    console.log(edge_list);
    console.log(typeof(edge_list));
    console.log("Converting into arrows");
    arrow_list = [];
    for(var edge in edge_list){
        arrow_list.push(get_arrow_lines_from_points(edge_list[edge]))
    }
    edge_list.push(...arrow_list);
}

function get_arrow_lines_from_points(point_pair, alpha=0.166, beta=.0555){

    let a = point_pair[0];
    let b = point_pair[1];
    
    console.log(a);
    console.log(b);
    
    let x1 = a[0];
    let y1 = a[1];
    let x2 = b[0];
    let y2 = b[1];
    
    let v1_x = x2-x1;
    let v1_y = y2-y1;

    let v2_x =  v1_y;
    let v2_y = - v1_x;
    
    let p1_x = x1 + (1.0-alpha)*v1_x;
    let p1_y = y1 + (1.0-alpha)*v1_y; 
    
    let p2_x = p1_x + beta*v2_x;
    let p2_y = p1_y + beta*v2_y; 

    let p3_x = p1_x - beta*v2_x;
    let p3_y = p1_y - beta*v2_y; 

    let p2 = [p2_x, p2_y];
    let p3 = [p3_x, p3_y];
    
    console.log([[x2,y2], [p2_x, p2_y], [p3_x, p3_y], [x2, y2]]);
    
    return [b, p2, p3, b];
}