/* Default Mapbox API-Key, you can change it here if needed */
const API_KEY = 'pk.eyJ1IjoiZm1pLWJhdXJscyIsImEiOiJja21ldTluNm8waHJmMm9wanlleWNoNXBkIn0.A6kjG_G6dj1VbpLLNDvpBg';

// Load the modules first
load(modules, VENDOR_PATH, function() {

    /* Pathfinder initialization */
    if (typeof Pathfinder !== 'undefined') {
        pf = new Pathfinder(API_KEY, {
            debug: true,
            
            home: { 
                coords: [49.35104, 7.18544],
                marker: {
                    heading: 'Saarland',
                    text: ''
                } 
            },

            // set radius according to grid size:
            //   12x12   30
            //   10x10   50
            //   8 x 8   80
            heatmapRadius: 30,

            use_heatmap_layer: false,

            perform_evaluation: false,

            // uncomment this to use a PATHFINDER-Server different from localhost
            // baseUrl: "http://threadripper.informatik.uni-stuttgart.de:9080/pathfinder",

            // uncomment this to use a tiling-server different from localhost
            //tilingUrl: "http://threadripper.informatik.uni-stuttgart.de:9081/tile_server/{z}/{x}/{y}",
            
        });
    }

});

