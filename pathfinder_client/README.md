
# PATHFINDER-Vis Frontend

The most important files are:

- index.html
  - entry-point for the frontend 
  - main functions
- assets/js/scripts.js
  - options are configured, e.g., start coordinates, tiling server host addresses, debug mode...
- assets/js/pathfinder-1.0.0/pathfinder.js
  - main frontend code
  - all options are defined here.





Lukas Baur

