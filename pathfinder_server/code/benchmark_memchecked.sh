cd build

gitDir=~/git/
out="saarland_foundPaths.sg"

export GLIBCXX_FORCE_NEW
nof_queries=100
nof_gen_paths=100000
graph="../../data/saarland-180101-time-all-ch.ftxt"


out="saarland_foundPaths.sg"
valgrind --num-callers=20 --main-stacksize=10000000 --leak-resolution=high --leak-check=yes --log-file="logfile.txt" --track-origins=yes --read-var-info=yes ./performance_test -i $graph -a $nof_gen_paths -w $nof_queries -t 4
# --leak-check=full --show-leak-kinds=all
#--track-origins=yes
