#include "experiments.h"

#include "ch_traversal.h"
#include "defs.h"
#include "dijkstra.h"
#include "exporter.h"
#include "flags.h"
#include "geometry.h"
#include "graph.h"
#include "grid_hierarchy.h"
#include "index/indices.h"
#include "interval_tree.h"
#include "io/folder_util.h"
#include "merge_sorted_unique.h"
#include "naive_ch_pathfinder.h"
#include "naive_pathfinder.h"
#include "parser.h"
#include "partitioner_util.h"
#include "path_generator.h"
#include "path_importer.h"
#include "path_parser.h"
#include "pathfinder.h"
#include "pathfinder_ds.h"
#include "paths.h"
#include "paths_to_ds.h"
#include "random.h"
#include "set_util.h"
#include "stxxl_init.h"
#include <algorithm>
#include <list>
#include <memory>
#include <string>
#include <unordered_set>

#include <fstream>
#include <iostream>
#include <stxxl/vector>

// to get local time
#include <ctime>
#include <iomanip>

namespace
{

void printExpHeader(std::string const& exp_name)
{
	std::string line(8 + exp_name.length(), '-');

	Print("Start Experiment: " << exp_name);
	Print(line);
}

void printExpFooter(std::string const& exp_name)
{
	std::string line(16 + exp_name.length(), '=');

	Print("");
	Print(line);
	Print(exp_name << " test successful");
	Print(line);
	Print("");
}

} // namespace

//
// Tests
//

namespace pf
{

static Random random;

static const std::string exp_folder = "experiment_results/";

std::string time_str()
{
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);
	std::ostringstream oss;
	oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
	return oss.str() + ": ";
}

pf::BoundingBox _get_random_box_on_level(int power, const PathfinderDS& p_ds)
{
	// get base bounding box
	const Graph& graph = p_ds.getGraph();
	BoundingBox graph_bb(graph);

	if (power == 0) {
		return graph_bb;
	}

	// set zoom
	double factor = std::pow(2, -power);
	auto width = graph_bb.getWidth() * factor;
	auto height = graph_bb.getHeight() * factor;

	// get box
	auto random_box = random.getBox(graph, width, height);
	return random_box;
}

pf::BoundingBox _get_random_box_of_relative_size(
    double relative_size, const PathfinderDS& p_ds)
{
	assert(relative_size <= 1.0);
	assert(relative_size > 0);

	const Graph& graph = p_ds.getGraph();
	BoundingBox graph_bb(graph);
	if (relative_size == 1.0) {
		return graph_bb;
	}

	double side_length_factor = std::sqrt(relative_size);
	auto random_box =
	    random.getBox(graph, graph_bb.getWidth() * side_length_factor,
	        graph_bb.getHeight() * side_length_factor);

	return random_box;
}

void experiments::init_graph_and_paths(Graph& graph, PathsDS& paths_ds,
    std::string graph_filename, std::string paths_filename,
    bool parse_binary_paths)
{
	// init graph
	Print("Read graph...");
	graph.init(graph_filename);
	Print("Graph loaded successfully");

	// get base bounding box
	BoundingBox graph_bb(graph);

	Print("Read paths...");
	// load paths
	if (parse_binary_paths) {
		// (a) binary
		PathImporter(graph, paths_ds).importPathsBin(paths_filename);
	} else {
		// (b) plain
		PathParser path_parser(graph, paths_ds);
		path_parser.parse(paths_filename);

		// compress paths
		paths_ds = partitioner_util::partitionPaths(graph, paths_ds);
	}
	Print("Paths loaded successfully");
}

void _print_graph_statistics(const Graph& graph)
{
	Print("Some graph statics:");
	Print("#nodes graph : " << graph.getAllNodes().size());
	Print("#edges graph : " << graph.getAllEdges().size());
	Print("max level    : " << graph.getMaxLevel());
}

void _create_empty_file(std::string& filename)
{
	std::ofstream file;
	file.open(filename);
	file << "";
	file.close();
}

void measure_segment_graph_trajectory_exporting(PathfinderDS& p_ds,
    Pathfinder& pathfinder, const Exporter& exporter, int zoom_level,
    std::ofstream& log_file, std::ofstream& result_file,
    std::ofstream& json_file, size_t repeat_times,
    const std::initializer_list<int>& bounding_box_size_powers)
{
	Print("  Start testing..");
	log_file << time_str() << "  Start testing..";

	json_file << "{";
	result_file << "power,time,size,earlyStop" << std::endl;
	bool first_run = true;
	TimeTracker time_tracker;
	for (size_t power : bounding_box_size_powers) {
		if (first_run) {
			first_run = false;
		} else {
			json_file << ",";
		}
		json_file << "\"" << power << "\":[";
		log_file << time_str() << "New Power: " << power << std::endl;
		double time_needed = 0;
		double size_needed = 0;
		bool didStopEarly = false;

		size_t number_of_points = 0;
		for (size_t i = 0; i < repeat_times; i++) {
			BoundingBox request_bb = _get_random_box_on_level(power, p_ds);
			PathIDs path_ids = pathfinder.run(Query(request_bb));

			time_tracker.start();

			Options options;
			options.setPacking(true);
			options.setZoom(zoom_level);

			LevelExtractor level_extractor(1.8713, 17.);

			ExportData export_data;
			export_data.found_paths = path_ids;
			UnpackingUtil::unpack_and_copy_paths(
			    export_data.paths_ds, path_ids, options, p_ds, level_extractor);

			const auto& exported_json =
			    exporter.exportSegmentGraph(export_data);

			time_tracker.stop();
			double last_time_delta = time_tracker.getLastMeasurment();
			time_needed += last_time_delta;
			size_needed += exported_json.size();

			if (i > 0) {
				json_file << ",";
			}
			json_file << exported_json;

			// find value of file-sizes (skipped since size() returns the sizein
			// bytes already)
			/*
			std::string size_file_name = "transmission_sizes/" +
			std::to_string(exported_json.size()) + ".txt";
			_create_empty_file(size_file_name);
			std::ofstream file_size_file;
			file_size_file.open(size_file_name);
			file_size_file << exported_json;
			file_size_file.close();
			*/

			log_file << i << "   : " << last_time_delta << " seconds"
			         << std::endl;
			number_of_points += 1;
			if (last_time_delta > 5) {
				log_file
				    << "took longer than 5 seconds. Skip other plottings, too"
				    << std::endl;
				didStopEarly = true;
				break;
			}
		}
		double average_time = time_needed / number_of_points;
		double average_size = size_needed / number_of_points;
		result_file << power << "," << average_time << "," << average_size
		            << "," << didStopEarly << std::endl;
		json_file << "]";
	}

	json_file << "}";
}

void measure_ebpf_trajectory_exporting(PathfinderDS& p_ds,
    Pathfinder& pathfinder, const Exporter& exporter, int zoom_level,
    std::ofstream& log_file, std::ofstream& result_file,
    std::ofstream& json_file, size_t repeat_times,
    const std::initializer_list<int>& bounding_box_size_powers)
{
	// run tests for a fixed EBPF setting
	//   binning:       yes
	//   bins:          25
	//   repetitions:   20
	//   timeout:       5s
	//   unpack level:  w.r.t. zoom level

	Print("  Start testing..");
	log_file << time_str() << "  Start testing..";

	json_file << "{";
	result_file << "power,time,size,earlyStop" << std::endl;
	bool first_run = true;
	TimeTracker time_tracker;
	for (size_t power : bounding_box_size_powers) {
		if (first_run) {
			first_run = false;
		} else {
			json_file << ",";
		}
		json_file << "\"" << power << "\":[";
		log_file << time_str() << "New Power: " << power << std::endl;
		double time_needed = 0;
		double size_needed = 0;
		bool didStopEarly = false;

		size_t number_of_points = 0;
		for (size_t i = 0; i < repeat_times; i++) {
			BoundingBox request_bb = _get_random_box_on_level(power, p_ds);
			Counter counts;
			EdgeIDs edge_ids = pathfinder.run_edgesearch_only_with_counts(
			    Query(request_bb), counts);

			time_tracker.start();

			Options options;
			options.setPacking(true);
			options.setZoom(zoom_level);
			options.setWeightedEdgeBinning(25);
			LevelExtractor level_extractor(1.8713, 17.);

			ExportData export_data;
			export_data.found_edges = edge_ids;
			export_data.edge_weights = counts;
			const auto& exported_json = exporter.export_weighted_edgeset(
			    export_data, options, p_ds, level_extractor);

			time_tracker.stop();
			double last_time_delta = time_tracker.getLastMeasurment();
			time_needed += last_time_delta;
			size_needed += exported_json.size();

			if (i > 0) {
				json_file << ",";
			}
			json_file << exported_json;

			log_file << i << "   : " << last_time_delta << " seconds"
			         << std::endl;
			number_of_points += 1;
			if (last_time_delta > 5) {
				log_file
				    << "took longer than 5 seconds. Skip other plottings, too"
				    << std::endl;
				didStopEarly = true;
				break;
			}
		}
		double average_time = time_needed / number_of_points;
		double average_size = size_needed / number_of_points;
		result_file << power << "," << average_time << "," << average_size
		            << "," << didStopEarly << std::endl;
		json_file << "]";
	}

	json_file << "}";
}

bool _run_EBPF_exporttype(const std::vector<ExportData>& export_datasets,
    const Exporter& exporter, const Options& options, double& itertations,
    double& time_needed, double& size_needed, double& no_of_edges,
    PathfinderDS& p_ds, const LevelExtractor& level_extractor,
    const bool& is_local, std::ofstream& log_file, uint timeout_limit = 5, bool use_bidirectional_edges = false, bool use_node_count = false)
{
	bool didStopEarly = false;

	TimeTracker time_tracker;
	for (auto& export_data : export_datasets) {
		Print("iteration #" << itertations);
		itertations += 1;
		no_of_edges += export_data.found_edges.size();

		time_tracker.start();
		const auto& exported_json = exporter.export_weighted_edgeset(
		    export_data, options, p_ds, level_extractor, is_local, use_bidirectional_edges, use_node_count);
		time_tracker.stop();
		double last_time_delta = time_tracker.getLastMeasurment();

		time_needed += last_time_delta;
		size_needed += exported_json.size();

		auto type = "global";
		if (is_local) {
			type = "local";
		}
		log_file << itertations << "  " << type << " : " << last_time_delta
		         << " seconds" << std::endl;
		if (last_time_delta > timeout_limit) {
			log_file << "took longer than " << timeout_limit
			         << " seconds. Skip other plottings, too" << std::endl;
			didStopEarly = true;
			break;
		}
	}
	time_needed /= itertations;
	size_needed /= itertations;
	no_of_edges /= itertations;
	return didStopEarly;
}

void measure_ebpf_comparison_local_global(PathfinderDS& p_ds,
    Pathfinder& pathfinder, const Exporter& exporter, int zoom_level,
    std::ofstream& log_file, std::ofstream& result_file, size_t repeat_times,
    const std::initializer_list<int>& bounding_box_size_powers)
{
	// run tests for a fixed EBPF setting
	//   binning:       yes
	//   bins:          25
	//   repetitions:   20
	//   timeout:       5s
	//   unpack level:  w.r.t. zoom level

	Print("  Start testing..");
	log_file << time_str() << "  Start testing..";
	result_file << "power,globalAvgTime,globalAvgSize,globalAvgEdges,"
	               "globalDidEarlyFinish,localAvgTime,localAvgSize,"
	               "localAvgEdges,localDidEarlyFinish"
	            << std::endl;
	TimeTracker time_tracker;
	for (size_t power : bounding_box_size_powers) {
		log_file << time_str() << "New Power: " << power << std::endl;

		// define options and vectors
		Options options;
		options.setPacking(true);
		options.setZoom(zoom_level);
		options.setWeightedEdgeBinning(25);
		LevelExtractor level_extractor(1.8713, 17.);

		std::vector<ExportData> export_datasets;
		// create all requests
		for (size_t i = 0; i < repeat_times; i++) {
			BoundingBox request_bb = _get_random_box_on_level(power, p_ds);

			Counter counts;
			EdgeIDs edge_ids = pathfinder.run_edgesearch_only_with_counts(
			    Query(request_bb), counts);

			ExportData export_data;
			export_data.found_edges = edge_ids;
			export_data.edge_weights = counts;
			export_datasets.push_back(export_data);
		}

		// local
		double local_itertations = 0;
		double local_avg_time = 0;
		double local_avg_size = 0;
		double local_avg_edges = 0;
		bool local_did_early_finish =
		    _run_EBPF_exporttype(export_datasets, exporter, options,
		        local_itertations, local_avg_time, local_avg_size,
		        local_avg_edges, p_ds, level_extractor, true, log_file);

		// global
		double global_itertations = 0;
		double global_avg_time = 0;
		double global_avg_size = 0;
		double global_avg_edges = 0;
		bool global_did_early_finish =
		    _run_EBPF_exporttype(export_datasets, exporter, options,
		        global_itertations, global_avg_time, global_avg_size,
		        global_avg_edges, p_ds, level_extractor, false, log_file);

		result_file << power << "," << global_avg_time << "," << global_avg_size
		            << "," << global_avg_edges << "," << global_did_early_finish
		            << "," << local_avg_time << "," << local_avg_size << ","
		            << local_avg_edges << "," << local_did_early_finish
		            << std::endl;
	}
}

void measure_ebpf_comparison_local_global_scale(
    experiments::ExperimentEnvironment& exp_env, int zoom_level,
    std::ofstream& log_file, std::ofstream& result_file, size_t repeat_times,
    const std::vector<double>& scale_factors, double timeout_limit, 
	bool use_bidirectional_edges, bool use_node_count)
{
	// run tests for a fixed EBPF setting
	//   binning:       yes
	//   bins:          25
	//   repetitions:   20
	//   timeout:       5s
	//   unpack level:  w.r.t. zoom level

	Print("  Start testing..");
	log_file << time_str() << "  Start testing..";
	result_file << "scale,globalAvgTime,globalAvgSize,globalAvgEdges,"
	               "globalDidEarlyFinish,localAvgTime,localAvgSize,"
	               "localAvgEdges,localDidEarlyFinish"
	            << std::endl;

	TimeTracker time_tracker;
	for (double scale : scale_factors) {
		log_file << time_str() << "New Scale: " << scale << std::endl;

		Print("iteration with scale #" << scale);

		// define options and vectors
		Options options;
		options.setPacking(true);
		options.setZoom(zoom_level);
		options.setWeightedEdgeBinning(25);
		LevelExtractor level_extractor(1.8713, 17.);

		std::vector<ExportData> export_datasets;
		// create all requests
		for (size_t i = 0; i < repeat_times; i++) {
			BoundingBox request_bb =
			    _get_random_box_of_relative_size(scale, exp_env.p_ds);

			Counter counts;
			EdgeIDs edge_ids =
			    exp_env.pathfinder.run_edgesearch_only_with_counts(
			        Query(request_bb), counts);

			ExportData export_data;
			export_data.found_edges = edge_ids;
			export_data.edge_weights = counts;
			export_datasets.push_back(export_data);
		}

		// local
		Print("Starting timing-loop: local");
		double local_itertations = 0;
		double local_avg_time = 0;
		double local_avg_size = 0;
		double local_avg_edges = 0;
		bool local_did_early_finish = _run_EBPF_exporttype(export_datasets,
		    exp_env.exporter, options, local_itertations, local_avg_time,
		    local_avg_size, local_avg_edges, exp_env.p_ds, level_extractor,
		    true, log_file, timeout_limit, use_bidirectional_edges, use_node_count);
		Print("Ending timing-loop: local");

		// global
		Print("Starting timing-loop: global");
		double global_itertations = 0;
		double global_avg_time = 0;
		double global_avg_size = 0;
		double global_avg_edges = 0;
		bool global_did_early_finish = _run_EBPF_exporttype(export_datasets,
		    exp_env.exporter, options, global_itertations, global_avg_time,
		    global_avg_size, global_avg_edges, exp_env.p_ds, level_extractor,
		    false, log_file, timeout_limit, use_bidirectional_edges, use_node_count);

		result_file << scale << "," << global_avg_time << "," << global_avg_size
		            << "," << global_avg_edges << "," << global_did_early_finish
		            << "," << local_avg_time << "," << local_avg_size << ","
		            << local_avg_edges << "," << local_did_early_finish
		            << std::endl;
		Print("Ending timing-loop: global");
	}
}

void measure_pruned_trajectory_exporting(const PathfinderDS& p_ds,
    Pathfinder& pathfinder, const Exporter& exporter, int zoom_level,
    std::ofstream& log_file, std::ofstream& result_file,
    std::ofstream& json_file, size_t repeat_times,
    const std::initializer_list<int>& cutoff_values, const int& power,
    const GridStack& grid_stack)
{
	Print("  Start testing..");
	log_file << time_str() << "  Start testing..";

	json_file << "{";
	result_file << "cutoff,time,size,earlyStop" << std::endl;
	bool first_run = true;
	TimeTracker time_tracker;
	for (size_t cutoff : cutoff_values) {
		if (first_run) {
			first_run = false;
		} else {
			json_file << ",";
		}
		json_file << "\"" << cutoff << "\":[";
		log_file << time_str() << "New cutoff: " << cutoff << std::endl;
		double time_needed = 0;
		double size_needed = 0;
		bool didStopEarly = false;

		size_t number_of_points = 0;
		for (size_t i = 0; i < repeat_times; i++) {
			BoundingBox request_bb = _get_random_box_on_level(power, p_ds);
			PathIDs path_ids = pathfinder.run(Query(request_bb));

			time_tracker.start();

			Options options;
			options.setPacking(true);
			options.setZoom(zoom_level);
			options.setCutoffLevel(cutoff);
			LevelExtractor level_extractor(1.8713, 17.);

			ExportData export_data;
			export_data.found_paths = path_ids;
			export_data.boxes = {request_bb, request_bb};
			const auto& exported_json = exporter.export_prunned_segments(
			    export_data, options, p_ds, grid_stack, level_extractor);

			time_tracker.stop();
			double last_time_delta = time_tracker.getLastMeasurment();
			time_needed += last_time_delta;
			size_needed += exported_json.size();

			if (i > 0) {
				json_file << ",";
			}
			json_file << exported_json;

			log_file << i << "   : " << last_time_delta << " seconds"
			         << std::endl;
			number_of_points += 1;
			if (last_time_delta > 5) {
				log_file
				    << "took longer than 5 seconds. Skip other plottings, too"
				    << std::endl;
				didStopEarly = true;
				break;
			}
		}
		double average_time = time_needed / number_of_points;
		double average_size = size_needed / number_of_points;
		result_file << cutoff << "," << average_time << "," << average_size
		            << "," << didStopEarly << std::endl;
		json_file << "]";
	}

	json_file << "}";
}

void experiments::runAll(unsigned int seed)
{
	random.seed(seed);
	runAll();
}

void experiments::runAll()
{

	Print("Not supported any longer.");
	Print("Chose a single experiment to run!");
	run(-1);
}

void experiments::run(unsigned int exp_number)
{

	auto ebpf_no_of_threads = EBPF_DEFAULT_NO_OF_THREADS;
	auto ebpf_repeat_times = EBPF_DEFAULT_REPEAT_TIMES;
	auto ebpf_seed = EBPF_DEFAULT_SEED;

	FolderUtil::create_folder(exp_folder);
	switch (exp_number) {
	case 0:
		run_edge_share_ratio_exp_saarland();
		break;
	case 1:
		run_edge_share_ratio_exp_germany();
		break;
	case 2:
		run_edge_share_ratio_exp_cuba();
		break;
	case 3:
		measure_segment_graph_trajectory_exporting_saarland();
		break;
	case 4:
		measure_segment_graph_trajectory_exporting_germany();
		break;
	case 5:
		measure_ebpf_trajectory_exporting_saarland();
		break;
	case 6:
		measure_ebpf_trajectory_exporting_germany();
		break;
	case 7:
		measure_pruning_exporting_saarland();
		break;
	case 8:
		measure_pruning_exporting_germany();
		break;
	case 9:
		measure_pruning_exporting_saarland_100k();
		break;
	case 10:
		measure_pruning_exporting_europe_500k();
		break;
	case 11:
		measure_ebpf_trajectory_exporting_saarland_100k();
		break;
	case 12:
		measure_pruning_exporting_increase_of_size();
		break;
	case 13:
		measure_ebpf_trajectory_exporting_saarland_1M();
		break;
	case 14:
		measure_ebpf_trajectory_exporting_saarland_10M();
		break;
	case 15:
		measure_ebpf_exporting_local_vs_global_saarland();
		break;
	case 16:
		measure_ebpf_exporting_local_vs_global_saarland_100k();
		break;
	case 17:
		measure_ebpf_exporting_local_vs_global_germany();
		break;

	// requesting different graphs
	// --- local sweeps
	case 18:
		measure_ebpf_requesting_saarland_for_log_evaluation(
		    true, ebpf_no_of_threads, ebpf_repeat_times, ebpf_seed);
		break;
	case 19:
		measure_ebpf_requesting_saarland100K_for_log_evaluation(
		    true, ebpf_no_of_threads, ebpf_repeat_times, ebpf_seed);
		break;
	case 20:
		measure_ebpf_requesting_germany_for_log_evaluation(
		    true, ebpf_no_of_threads, ebpf_repeat_times, ebpf_seed);
		break;
	// --- global sweeps
	case 21:
		measure_ebpf_requesting_saarland_for_log_evaluation(
		    false, ebpf_no_of_threads, ebpf_repeat_times, ebpf_seed);
		break;
	case 22:
		measure_ebpf_requesting_saarland100K_for_log_evaluation(
		    false, ebpf_no_of_threads, ebpf_repeat_times, ebpf_seed);
		break;
	case 23:
		measure_ebpf_requesting_germany_for_log_evaluation(
		    false, ebpf_no_of_threads, ebpf_repeat_times, ebpf_seed);
		break;
	case 24:
		measure_ebpf_exporting_local_global_intersection_saarland();
		break;
	case 25:
		measure_ebpf_exporting_local_global_intersection_saarland100k();
		break;
	case 26:
		measure_ebpf_exporting_local_global_intersection_germany();
		break;
	case 27:
		measure_ebpf_exporting_local_global_intersection_nodeCount_saarland();
		break;
	case 28:
		measure_ebpf_exporting_local_global_intersection_nodeCount_saarland100k();
		break;
	case 29:
		measure_ebpf_exporting_local_global_intersection_nodeCount_germany();
		break;

	default:
		Print("Experiment does not exist. Choose from:");
		Print("==== Shared edges ==============");
		Print("[0]  share_ratio_exp_saarland");
		Print("[1]  share_ratio_exp_germany");
		Print("[2]  share_ratio_exp_cuba");
		Print("[3]  exporting segment graph: Saarland");
		Print("[4]  exporting segment graph: Germany");
		Print("[5]  exporting EBPF: Saarland");
		Print("[11] exporting EBPF: Saarland 100k");
		Print("[13] exporting EBPF: Saarland 1M");
		Print("[14] exporting EBPF: Saarland 10M");
		Print("[6]  exporting EBPF: Germany");
		Print("[7]  exporting pruning: Saarland");
		Print("[8]  exporting pruning: Germany");
		Print("[9]  exporting pruning: Saarland 100k");
		Print("[10] exporting pruning: Europe 500k");
		Print("[12] exporting pruning: Saarland: Increase in size");
		Print("[15] exporting EBPF - global vs. local: Saarland");
		Print("[16] exporting EBPF - global vs. local: Saarland100k");
		Print("[17] exporting EBPF - global vs. local: Germany");
		Print("[18] exporting EBPF: requesting Saarland for log evaluation     "
		      "[local]");
		Print("[19] exporting EBPF: requesting Saarland100K for log evaluation "
		      "[local]");
		Print("[20] exporting EBPF: requesting Germany for log evaluation      "
		      "[local]");
		Print("[21] exporting EBPF: requesting Saarland for log evaluation     "
		      "[global]");
		Print("[22] exporting EBPF: requesting Saarland100K for log evaluation "
		      "[global]");
		Print("[23] exporting EBPF: requesting Germany for log evaluation      "
		      "[global]");
		Print("[24] exporting EBPF: Find global/local intersection: Saarland");
		Print("[25] exporting EBPF: Find global/local intersection: "
		      "Saarland100k");
		Print("[26] exporting EBPF: Find global/local intersection: Germany");
		Print("[27] exporting EBPF: Find global/local intersection: Saarland     - with Node Count");
		Print("[28] exporting EBPF: Find global/local intersection: Saarland100k - with Node Count");
		Print("[29] exporting EBPF: Find global/local intersection: Germany      - with Node Count");

		Print("");
	}
	Print("== EXPERIMENT FINISHED ==");
}

void experiments::run(unsigned int exp_number, unsigned int seed)
{
	random.seed(seed);
	run(exp_number);
}

double _analyze_sharing_degree(const PathIDs& path_ids, PathsDS paths_ds,
    const Graph& graph, bool log_details, bool unpack)
{
	EdgeIDs all_edges;
	for (PathID path_id : path_ids) {
		auto path = paths_ds.getPath(path_id);
		if (unpack) {
			path = path.unpack(graph);
		}
		for (auto edge : path.getEdges()) {
			all_edges.push_back(edge);
		}
	}
	auto total_edge_numbers = all_edges.size();

	std::sort(all_edges.begin(), all_edges.end());
	all_edges.erase(
	    std::unique(all_edges.begin(), all_edges.end()), all_edges.end());

	auto unique_edge_numbers = static_cast<double>(all_edges.size());
	auto unique_ratio = unique_edge_numbers / total_edge_numbers;

	auto coverage = unique_edge_numbers / graph.getAllEdges().size();

	if (log_details) {
		Print("++ All edges (graph): " << graph.getAllEdges().size());
		Print("++ All paths:         " << path_ids.size());
		Print("++ All path-edges:    " << total_edge_numbers);
		Print("++ Unique path-edges: " << unique_edge_numbers);
		Print("++ Coverage:          " << coverage);
		Print("++ Uniqueness ratio:  " << unique_ratio);
	}

	return unique_ratio;
}

void _run_edge_share_ratio_exp_for(const std::string& graph_filename,
    const std::string& paths_filename, const std::string& name)
{
	Graph graph;
	PathsDS paths_ds;

	graph.init(graph_filename);

	// load paths
	PathParser path_parser(graph, paths_ds);
	path_parser.parse(paths_filename);

	// compress paths
	paths_ds = partitioner_util::partitionPaths(graph, paths_ds);

	PathIDs all_paths;
	for (PathID i = 0; i < paths_ds.getNrOfPaths(); i++) {
		all_paths.push_back(i);
	}
	Print("--Realdaten " << name << "(fully unpacked) --- ");
	_analyze_sharing_degree(all_paths, paths_ds, graph, true, true);
	Print("--Realdaten " << name << "(root-edges only) --- ");
	_analyze_sharing_degree(all_paths, paths_ds, graph, true, false);
}

void experiments::run_edge_share_ratio_exp_saarland()
{
	_run_edge_share_ratio_exp_for(
	    realgraph_saarland_filename, realpaths_saarland_filename, "Saarland");
}
void experiments::run_edge_share_ratio_exp_germany()
{
	_run_edge_share_ratio_exp_for(
	    realgraph_germany_filename, realpaths_germany_filename, "Germany");
}
void experiments::run_edge_share_ratio_exp_cuba()
{
	_run_edge_share_ratio_exp_for(
	    realgraph_cuba_filename, realpaths_cuba_filename, "Cuba");
}

void experiments::run_edge_share_ratio_exp_example()
{
	const std::string EXP_NAME = "Edge Sharing Ratio";
	const uint nr_of_threads_to_test = 4;
	const auto number_of_paths_vec = {
	    10, 100, 320, 1000, 3200, 10000, 32000, 100000};

	double power = 3;
	const uint max_path_distance = 400;
	printExpHeader(EXP_NAME);

	// init graph
	Graph graph;
	graph.init(realgraph_saarland_filename);

	// get base bounding box
	BoundingBox graph_bb(graph);

	// set zoom
	double factor = std::pow(2, -power);
	auto width = graph_bb.getWidth() * factor;
	auto height = graph_bb.getHeight() * factor;

	for (uint number_of_paths : number_of_paths_vec) {
		Print("---------------------------------");
		Print("Number of paths: " << number_of_paths);
		PathGenerator path_generator(graph, random, nr_of_threads_to_test);
		PathsDS paths_ds = path_generator.getRandomShortestPaths(
		    number_of_paths, max_path_distance);

		Print("Init pathfinder data structures");
		PathfinderDS pathfinder_ds(
		    graph, nr_of_threads_to_test, std::move(paths_ds));
		Pathfinder pathfinder(pathfinder_ds, nr_of_threads_to_test);

		// analyze full path
		Print("Full Graph:");
		auto all_path_ids = pathfinder.run(Query(graph_bb));
		_analyze_sharing_degree(all_path_ids, paths_ds, graph, true, true);

		_print_graph_statistics(graph);
		Exporter exporter(graph);
		double average_degree_plain = 0;
		double average_degree_indexed = 0;

		for (std::size_t i = 0; i < number_of_queries; ++i) {
			auto random_box = random.getBox(graph, width, height);
			auto path_ids = pathfinder.run(Query(random_box));
			if (path_ids.size() < 1) {
				i--;
				continue;
			}
			PathsDS found_paths_ds;
			for (PathID path_id : path_ids) {
				Path unpackedPath = paths_ds.getPath(path_id).unpack(graph);
				found_paths_ds.addPath(unpackedPath);
			}
			ExportData data;
			Options options;
			data.total_paths = paths_ds.getNrOfPaths();
			data.intersection_paths = path_ids.size();
			data.paths_ds = found_paths_ds;
			auto message_len_plain = exporter.exportTrajectories(data, options);
			auto message_len_indexed =
			    exporter.exportTrajectoriesEdgeIndexed(data, options);

			average_degree_plain += message_len_plain.size();
			average_degree_indexed += message_len_indexed.size();
		}
		average_degree_plain /= number_of_queries;
		average_degree_indexed /= number_of_queries;

		Print("++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++");
		Print("++ Max Distance     " << max_path_distance);
		Print("++ #Queries:        " << number_of_queries);
		Print("++ Factor:          " << factor << " (power:" << power << ")");
		Print("++ #Paths:          " << number_of_paths);
		Print("++ Average plain:   " << average_degree_plain);
		Print("++ Average indexed: " << average_degree_indexed);
		Print("++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++");
	}
	printExpFooter(EXP_NAME);
}

void experiments::run_edge_share_ratio_exp()
{
	const std::string EXP_NAME = "Edge Sharing Ratio";

	printExpHeader(EXP_NAME);
	run_edge_share_ratio_exp_example();

	printExpFooter(EXP_NAME);
}

void experiments::measure_segment_graph_trajectory_exporting_saarland()
{
	const std::string EXP_NAME =
	    "Measuring Segment Graph Trajectory Exporting: Saarland";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 20;

	std::string experiment_folder = exp_folder + "SegmentGraphExporting/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "03_Saarland/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    realpaths_saarland_filename);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	for (int zoom_level = 1; zoom_level <= 18; zoom_level++) {
		std::string zoom_level_string = zoom_level > 9
		                                    ? std::to_string(zoom_level)
		                                    : "0" + std::to_string(zoom_level);
		std::string log_output_filename = experiment_folder + "saarland_zoom_" +
		                                  zoom_level_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "saarland_zoom_" +
		                                     zoom_level_string + "_results.txt";
		std::string json_output_filename = experiment_folder +
		                                   "saarland_zoom_" +
		                                   zoom_level_string + "_requests.json";

		Print("NEW ZOOM LEVEL : " << zoom_level);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		auto bounding_box_size_powers = {5, 4, 3, 2, 1}; // 1/32, ..., 1/2

		measure_segment_graph_trajectory_exporting(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, json_file, repeat_times,
		    bounding_box_size_powers);

		result_file.close();
		log_file.close();
		json_file.close();
	}
}

void experiments::measure_segment_graph_trajectory_exporting_germany()
{
	const std::string EXP_NAME =
	    "Measuring Segment Graph Trajectory Exporting: Germany";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 20;

	std::string experiment_folder = exp_folder + "SegmentGraphExporting/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "04_Germany/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_germany_filename,
	    realpaths_germany_filename);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	for (int zoom_level = 1; zoom_level <= 18; zoom_level++) {
		std::string zoom_level_string = zoom_level > 9
		                                    ? std::to_string(zoom_level)
		                                    : "0" + std::to_string(zoom_level);
		std::string log_output_filename = experiment_folder + "germany_zoom_" +
		                                  zoom_level_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "germany_zoom_" +
		                                     zoom_level_string + "_results.txt";
		std::string json_output_filename = experiment_folder + "germany_zoom_" +
		                                   zoom_level_string + "_requests.json";

		Print("NEW ZOOM LEVEL : " << zoom_level);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		auto bounding_box_size_powers = {5, 4, 3, 2, 1}; // 1/32, ..., 1/2

		measure_segment_graph_trajectory_exporting(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, json_file, repeat_times,
		    bounding_box_size_powers);

		result_file.close();
		log_file.close();
		json_file.close();
	}
}

void experiments::measure_ebpf_trajectory_exporting_saarland()
{
	const std::string EXP_NAME =
	    "Measuring EBPF Trajectory Exporting: Saarland";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 20;

	std::string experiment_folder = exp_folder + "EBPF/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Saarland/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    realpaths_saarland_filename);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	for (int zoom_level = 2; zoom_level <= 18; zoom_level += 2) {
		std::string zoom_level_string = zoom_level > 9
		                                    ? std::to_string(zoom_level)
		                                    : "0" + std::to_string(zoom_level);
		std::string log_output_filename = experiment_folder + "saarland_zoom_" +
		                                  zoom_level_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "saarland_zoom_" +
		                                     zoom_level_string + "_results.txt";
		std::string json_output_filename = experiment_folder +
		                                   "saarland_zoom_" +
		                                   zoom_level_string + "_requests.json";

		Print("NEW ZOOM LEVEL : " << zoom_level);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		auto bounding_box_size_powers = {5, 4, 3, 2, 1}; // 1/32, ..., 1/2

		// measure_segment_graph_trajectory_exporting(paths_ds, p_ds,
		// pathfinder, exporter, zoom_level, log_file, result_file, json_file,
		// repeat_times, bounding_box_size_powers);
		measure_ebpf_trajectory_exporting(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, json_file, repeat_times,
		    bounding_box_size_powers);

		result_file.close();
		log_file.close();
		json_file.close();
	}
}

void experiments::measure_ebpf_trajectory_exporting_saarland_100k()
{
	const std::string EXP_NAME =
	    "Measuring EBPF Trajectory Exporting: Saarland 1ook";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 20;

	std::string experiment_folder = exp_folder + "EBPF/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Saarland_100K/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    saarland_generated_100k_filename, true);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	for (int zoom_level = 2; zoom_level <= 18; zoom_level += 2) {
		std::string zoom_level_string = zoom_level > 9
		                                    ? std::to_string(zoom_level)
		                                    : "0" + std::to_string(zoom_level);
		std::string log_output_filename = experiment_folder +
		                                  "saarland100k_zoom_" +
		                                  zoom_level_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "saarland100k_zoom_" +
		                                     zoom_level_string + "_results.txt";
		std::string json_output_filename = experiment_folder +
		                                   "saarland100k_zoom_" +
		                                   zoom_level_string + "_requests.json";

		Print("NEW ZOOM LEVEL : " << zoom_level);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		auto bounding_box_size_powers = {5, 4, 3, 2, 1}; // 1/32, ..., 1/2

		measure_ebpf_trajectory_exporting(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, json_file, repeat_times,
		    bounding_box_size_powers);

		result_file.close();
		log_file.close();
		json_file.close();
	}
}

void experiments::measure_ebpf_trajectory_exporting_saarland_1M()
{
	const std::string EXP_NAME =
	    "Measuring EBPF Trajectory Exporting: Saarland 1M";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 20;

	std::string experiment_folder = exp_folder + "EBPF/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Saarland_1M/";
	FolderUtil::create_folder(experiment_folder);
	const std::string resquest_folder = experiment_folder + "requests/";
	FolderUtil::create_folder(resquest_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    saarland_generated_1M_filename, true);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	for (int zoom_level = 2; zoom_level <= 18; zoom_level += 2) {
		std::string zoom_level_string = zoom_level > 9
		                                    ? std::to_string(zoom_level)
		                                    : "0" + std::to_string(zoom_level);
		std::string log_output_filename = experiment_folder +
		                                  "saarland1M_zoom_" +
		                                  zoom_level_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "saarland1M_zoom_" +
		                                     zoom_level_string + "_results.txt";
		std::string json_output_filename = resquest_folder +
		                                   "saarland1M_zoom_" +
		                                   zoom_level_string + "_requests.json";

		Print("NEW ZOOM LEVEL : " << zoom_level);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		auto bounding_box_size_powers = {5, 4, 3, 2, 1}; // 1/32, ..., 1/2

		measure_ebpf_trajectory_exporting(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, json_file, repeat_times,
		    bounding_box_size_powers);

		result_file.close();
		log_file.close();
		json_file.close();
	}
}

void experiments::measure_ebpf_trajectory_exporting_saarland_10M()
{
	const std::string EXP_NAME =
	    "Measuring EBPF Trajectory Exporting: Saarland 10M";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 20;

	std::string experiment_folder = exp_folder + "EBPF/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Saarland_10M/";
	FolderUtil::create_folder(experiment_folder);
	const std::string resquest_folder = experiment_folder + "requests/";
	FolderUtil::create_folder(resquest_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    saarland_generated_10M_filename, true);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	for (int zoom_level = 2; zoom_level <= 18; zoom_level += 2) {
		std::string zoom_level_string = zoom_level > 9
		                                    ? std::to_string(zoom_level)
		                                    : "0" + std::to_string(zoom_level);
		std::string log_output_filename = experiment_folder +
		                                  "saarland10M_zoom_" +
		                                  zoom_level_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "saarland10M_zoom_" +
		                                     zoom_level_string + "_results.txt";
		std::string json_output_filename = resquest_folder +
		                                   "saarland10M_zoom_" +
		                                   zoom_level_string + "_requests.json";

		Print("NEW ZOOM LEVEL : " << zoom_level);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		auto bounding_box_size_powers = {5, 4, 3, 2, 1}; // 1/32, ..., 1/2

		measure_ebpf_trajectory_exporting(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, json_file, repeat_times,
		    bounding_box_size_powers);

		result_file.close();
		log_file.close();
		json_file.close();
	}
}

void experiments::measure_ebpf_trajectory_exporting_germany()
{
	const std::string EXP_NAME = "Measuring EBPF Trajectory Exporting: Germany";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 20;

	std::string experiment_folder = exp_folder + "EBPF/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Germany/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_germany_filename,
	    realpaths_germany_filename);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	for (int zoom_level = 2; zoom_level <= 18; zoom_level += 2) {
		std::string zoom_level_string = zoom_level > 9
		                                    ? std::to_string(zoom_level)
		                                    : "0" + std::to_string(zoom_level);
		std::string log_output_filename = experiment_folder + "germany_zoom_" +
		                                  zoom_level_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "germany_zoom_" +
		                                     zoom_level_string + "_results.txt";
		std::string json_output_filename = experiment_folder + "germany_zoom_" +
		                                   zoom_level_string + "_requests.json";

		Print("NEW ZOOM LEVEL : " << zoom_level);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		auto bounding_box_size_powers = {5, 4, 3, 2, 1}; // 1/32, ..., 1/2

		// measure_segment_graph_trajectory_exporting(paths_ds, p_ds,
		// pathfinder, exporter, zoom_level, log_file, result_file, json_file,
		// repeat_times, bounding_box_size_powers);
		measure_ebpf_trajectory_exporting(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, json_file, repeat_times,
		    bounding_box_size_powers);

		result_file.close();
		log_file.close();
		json_file.close();
	}
}

void experiments::measure_ebpf_exporting_local_vs_global_saarland()
{
	const std::string EXP_NAME =
	    "EBPF Exporting Comparison (local vs. global): Saarland";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 200;

	std::string experiment_folder = exp_folder + "EBPF_local_vs_global/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Saarland/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    realpaths_saarland_filename, false);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	for (int zoom_level = 2; zoom_level <= 18; zoom_level += 2) {
		std::string zoom_level_string = zoom_level > 9
		                                    ? std::to_string(zoom_level)
		                                    : "0" + std::to_string(zoom_level);
		std::string log_output_filename = experiment_folder + "saarland_zoom_" +
		                                  zoom_level_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "saarland_zoom_" +
		                                     zoom_level_string + "_results.txt";

		Print("NEW ZOOM LEVEL : " << zoom_level);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		auto bounding_box_size_powers = {5, 4, 3, 2, 1, 0}; // 1/32, ..., 1/2, 1

		measure_ebpf_comparison_local_global(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, repeat_times,
		    bounding_box_size_powers);

		result_file.close();
		log_file.close();
	}
}

std::vector<double> _generate_decreasing_range(const double scale_range_start,
    const double scale_range_end, const double scale_range_delta)
{

	assert(scale_range_start >= scale_range_end);
	assert(scale_range_delta > 0);

	std::vector<double> scale_range;
	auto current_scale = scale_range_start;
	while (current_scale >= scale_range_end) {
		scale_range.push_back(current_scale);
		current_scale -= scale_range_delta;
	}
	return scale_range;
}

void _measure_ebpf_exporting_local_global_intersection(
    experiments::ExperimentEnvironment& exp_env,
    const std::vector<double>& scale_range, const size_t repeat_times = 100,
    const double timeout_limit = 30.0, const int start_zoom_lvl = 1,
    const int end_zoom_lvl = 1,
	const bool use_bidirectional_edges = false, 
	const bool use_node_count = false
	)
{
	const std::string EXP_NAME =
	    "EBPF local vs. global Intersection: " + exp_env.graph_name;
	printExpHeader(EXP_NAME);
	random.seed(564344);

	std::string experiment_folder =
	    exp_folder + "EBPF_local_global_intersection/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + exp_env.graph_name + "/";
	FolderUtil::create_folder(experiment_folder);

	for (int zoom_level = start_zoom_lvl; zoom_level <= end_zoom_lvl;
	     zoom_level += 2) {
		std::string zoom_level_string = zoom_level > 9
		                                    ? std::to_string(zoom_level)
		                                    : "0" + std::to_string(zoom_level);
		std::string log_output_filename = experiment_folder + "saarland_zoom_" +
		                                  zoom_level_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "saarland_zoom_" +
		                                     zoom_level_string + "_results.txt";

		Print("NEW ZOOM LEVEL : " << zoom_level);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		measure_ebpf_comparison_local_global_scale(exp_env, zoom_level,
		    log_file, result_file, repeat_times, scale_range, timeout_limit, use_bidirectional_edges, use_node_count);

		result_file.close();
		log_file.close();
	}
}

// EXP 24
void experiments::measure_ebpf_exporting_local_global_intersection_saarland()
{
	// Declare instance
	SaarlandSetup setup;

	// create datastructures
	uint no_of_threads = 32;
	auto p_ds =
	    PathfinderDS(setup.graph, no_of_threads, std::move(setup.paths_ds));
	auto pathfinder = Pathfinder(p_ds, no_of_threads);

	// merge to one object
	ExperimentEnvironment exp_env(setup, p_ds, pathfinder);

	// define resolution
	auto scale_range = _generate_decreasing_range(1.0, 0.01, 0.01);

	// run experiment
	_measure_ebpf_exporting_local_global_intersection(
	    exp_env, scale_range, 200);
}


// EXP 25
void experiments::
    measure_ebpf_exporting_local_global_intersection_saarland100k()
{
	// Declare instance
	Saarland100kSetup setup;

	// create datastructures
	uint no_of_threads = 32;
	auto p_ds =
	    PathfinderDS(setup.graph, no_of_threads, std::move(setup.paths_ds));
	auto pathfinder = Pathfinder(p_ds, no_of_threads);

	// merge to one object
	ExperimentEnvironment exp_env(setup, p_ds, pathfinder);

	// define resolution
	auto scale_range = _generate_decreasing_range(1.0, 0.05, 0.05);

	// run experiment
	_measure_ebpf_exporting_local_global_intersection(exp_env, scale_range);
}


// EXP 26
void experiments::measure_ebpf_exporting_local_global_intersection_germany()
{
	// Declare instance
	GermanySetup setup;

	// create datastructures
	uint no_of_threads = 32;
	auto p_ds =
	    PathfinderDS(setup.graph, no_of_threads, std::move(setup.paths_ds));
	auto pathfinder = Pathfinder(p_ds, no_of_threads);

	// merge to one object
	ExperimentEnvironment exp_env(setup, p_ds, pathfinder);

	// define resolution
	auto scale_range = _generate_decreasing_range(0.20, 0.005, 0.005);

	// run experiment
	_measure_ebpf_exporting_local_global_intersection(exp_env, scale_range);
}


// EXP 27
void experiments::measure_ebpf_exporting_local_global_intersection_nodeCount_saarland()
{
	// Declare instance
	SaarlandSetup setup;

	// create datastructures
	uint no_of_threads = 32;
	auto p_ds =
	    PathfinderDS(setup.graph, no_of_threads, std::move(setup.paths_ds));
	auto pathfinder = Pathfinder(p_ds, no_of_threads);

	// merge to one object
	ExperimentEnvironment exp_env(setup, p_ds, pathfinder);

	size_t repeats_times = 100; 
	double resolution = 0.01; 

	// define resolution
	auto scale_range = _generate_decreasing_range(1.0, resolution, resolution);  


	bool use_bidirectional_edges = false; 
	bool use_node_count = true;

	// run experiment
	_measure_ebpf_exporting_local_global_intersection(
	    exp_env, scale_range, repeats_times, 30.0, 1, 1, use_bidirectional_edges, use_node_count);
}


// EXP 28
void experiments::measure_ebpf_exporting_local_global_intersection_nodeCount_saarland100k()
{
	// Declare instance
	Saarland100kSetup setup;

	// create datastructures
	uint no_of_threads = 32;
	auto p_ds =
	    PathfinderDS(setup.graph, no_of_threads, std::move(setup.paths_ds));
	auto pathfinder = Pathfinder(p_ds, no_of_threads);

	// merge to one object
	ExperimentEnvironment exp_env(setup, p_ds, pathfinder);

	size_t repeats_times = 100; 
	double resolution = 0.05; 

	bool use_bidirectional_edges = false; 
	bool use_node_count = true;

	// define resolution
	auto scale_range = _generate_decreasing_range(1.0, resolution, resolution);  

	// run experiment
	_measure_ebpf_exporting_local_global_intersection(
	    exp_env, scale_range, repeats_times, 30.0, 1, 1, use_bidirectional_edges, use_node_count);
}


// EXP 29
void experiments::measure_ebpf_exporting_local_global_intersection_nodeCount_germany()
{
	// Declare instance
	GermanySetup setup;

	// create datastructures
	uint no_of_threads = 32;
	auto p_ds =
	    PathfinderDS(setup.graph, no_of_threads, std::move(setup.paths_ds));
	auto pathfinder = Pathfinder(p_ds, no_of_threads);

	// merge to one object
	ExperimentEnvironment exp_env(setup, p_ds, pathfinder);

	size_t repeats_times = 100; 
	double resolution = 0.005; 

	bool use_bidirectional_edges = false; 
	bool use_node_count = true;

	// define resolution
	auto scale_range = _generate_decreasing_range(0.20, resolution, resolution);

	// run experiment
	_measure_ebpf_exporting_local_global_intersection(
	    exp_env, scale_range, repeats_times, 30.0, 1, 1, use_bidirectional_edges, use_node_count);
}


void experiments::measure_ebpf_exporting_local_vs_global_saarland_100k()
{
	const std::string EXP_NAME =
	    "EBPF Exporting Comparison (local vs. global): Saarland100k";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 50;

	std::string experiment_folder = exp_folder + "EBPF_local_vs_global/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Saarland100k/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    saarland_generated_100k_filename, true);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	for (int zoom_level = 2; zoom_level <= 18; zoom_level += 2) {
		std::string zoom_level_string = zoom_level > 9
		                                    ? std::to_string(zoom_level)
		                                    : "0" + std::to_string(zoom_level);
		std::string log_output_filename = experiment_folder +
		                                  "saarland100k_zoom_" +
		                                  zoom_level_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "saarland100k_zoom_" +
		                                     zoom_level_string + "_results.txt";

		Print("NEW ZOOM LEVEL : " << zoom_level);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		auto bounding_box_size_powers = {5, 4, 3, 2, 1, 0}; // 1/32, ..., 1/2, 1

		measure_ebpf_comparison_local_global(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, repeat_times,
		    bounding_box_size_powers);

		result_file.close();
		log_file.close();
	}
}

void experiments::measure_ebpf_exporting_local_vs_global_germany()
{
	const std::string EXP_NAME =
	    "EBPF Exporting Comparison (local vs. global): Germany";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 200;

	std::string experiment_folder = exp_folder + "EBPF_local_vs_global/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Germany/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_germany_filename,
	    realpaths_germany_filename, false);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	for (int zoom_level = 2; zoom_level <= 18; zoom_level += 2) {
		std::string zoom_level_string = zoom_level > 9
		                                    ? std::to_string(zoom_level)
		                                    : "0" + std::to_string(zoom_level);
		std::string log_output_filename = experiment_folder + "germany_zoom_" +
		                                  zoom_level_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "germany_zoom_" +
		                                     zoom_level_string + "_results.txt";

		Print("NEW ZOOM LEVEL : " << zoom_level);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		auto bounding_box_size_powers = {
		    7, 6, 5, 4, 3, 2}; // 1/128, 1/64, 1/32, ..., 1/4

		measure_ebpf_comparison_local_global(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, repeat_times,
		    bounding_box_size_powers);

		result_file.close();
		log_file.close();
	}
}

void measure_ebpf_requesting_for_log_evaluation(Graph& graph, PathsDS& paths_ds,
    bool& use_local_search, const uint no_of_threads, size_t repeat_times,
    int seed)
{
	random.seed(seed);

	std::cout << std::fixed << std::setprecision(6);

	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	auto bounding_box_size_powers = {5, 4, 3, 2, 1, 0}; // 1/32, ..., 1/2, 1

	for (auto& box_size_factor : bounding_box_size_powers) {
		Print("~~ REQUESTING FLAG (ö?0&$): NEW FACTOR: [" << box_size_factor
		                                                  << "] ~~");

		for (size_t i = 0; i < repeat_times; i++) {
			Print("iteration no. #" << i);
			BoundingBox request_bb =
			    _get_random_box_on_level(box_size_factor, p_ds);
			Counter counts;
			EdgeIDs edge_ids = pathfinder.run_edgesearch_only_with_counts(
			    Query(request_bb), counts);

			Options options;
			options.setPacking(true);
			options.setZoom(2);
			options.setWeightedEdgeBinning(25);
			LevelExtractor level_extractor(1.8713, 17.);

			ExportData export_data;
			export_data.found_edges = edge_ids;
			export_data.edge_weights = counts;
			const auto& exported_json = exporter.export_weighted_edgeset(
			    export_data, options, p_ds, level_extractor, use_local_search);
			Print("~json_size: |" << exported_json.size() << "|");
		}
	}

	std::cout << std::setprecision(6) << std::defaultfloat;
}

void experiments::measure_ebpf_requesting_saarland_for_log_evaluation(
    bool use_local_search, const uint no_of_threads, size_t repeat_times,
    int seed)
{
	const std::string EXP_NAME = "(18/21) EBPF Exporting: Requesting Saarland";
	printExpHeader(EXP_NAME);

	// create graph
	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    realpaths_saarland_filename, false);

	// create requests
	measure_ebpf_requesting_for_log_evaluation(
	    graph, paths_ds, use_local_search, no_of_threads, repeat_times, seed);
}

void experiments::measure_ebpf_requesting_saarland100K_for_log_evaluation(
    bool use_local_search, const uint no_of_threads, size_t repeat_times,
    int seed)
{
	const std::string EXP_NAME =
	    "(19/22) EBPF Exporting: Requesting Saarland100K";
	printExpHeader(EXP_NAME);

	// create graph
	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    saarland_generated_100k_filename, true);

	// create requests
	measure_ebpf_requesting_for_log_evaluation(
	    graph, paths_ds, use_local_search, no_of_threads, repeat_times, seed);
}

void experiments::measure_ebpf_requesting_germany_for_log_evaluation(
    bool use_local_search, const uint no_of_threads, size_t repeat_times,
    int seed)
{
	const std::string EXP_NAME = "(20/23) EBPF Exporting: Requesting Germany";
	printExpHeader(EXP_NAME);

	// create graph
	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_germany_filename,
	    realpaths_germany_filename, false);

	// create requests
	measure_ebpf_requesting_for_log_evaluation(
	    graph, paths_ds, use_local_search, no_of_threads, repeat_times, seed);
}

void experiments::measure_pruning_exporting_saarland()
{
	const std::string EXP_NAME =
	    "Measuring Pruning Trajectory Exporting: Saarland";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 20;

	std::string experiment_folder = exp_folder + "Pruning/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Saarland/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    realpaths_saarland_filename);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	const GridIndex p = 6;
	const GridIndex q = 6;
	const GridIndex level = 4;

	std::cout << time_str() << "Create grid: " << p << " x " << q << " with "
	          << level << " levels." << std::endl;
	auto grid_hierarchy_assigner =
	    GridHierarchyAssigner(p, q, level, paths_ds, graph);
	auto level_0_grid = grid_hierarchy_assigner.get_level0_grid();
	const GridStack grid_stack = GridStack(level_0_grid, level);

	// create hierarchy unsing the object
	grid_hierarchy_assigner.create_hierarchy();
	std::cout << time_str() << "  done." << std::endl;

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	const std::vector<int> bounding_box_size_powers = {
	    5, 4, 3, 2, 1}; // 1/32, ..., 1/2
	const std::vector<int> zoom_levels = {15, 14, 13, 12, 11};

	const auto cutoff_levels = {128, 64, 32, 16, 8, 4};
	for (size_t i = 0; i < bounding_box_size_powers.size(); i++) {
		auto& power = bounding_box_size_powers[i];
		auto& zoom_level = zoom_levels[i];

		std::string power_string =
		    power > 9 ? std::to_string(power) : "0" + std::to_string(power);
		std::string log_output_filename =
		    experiment_folder + "saarland_power_" + power_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "saarland_power_" + power_string +
		                                     "_results.txt";
		std::string json_output_filename = experiment_folder +
		                                   "saarland_power_" + power_string +
		                                   "_requests.json";

		Print("NEW POWER LEVEL : " << power);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		measure_pruned_trajectory_exporting(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, json_file, repeat_times,
		    cutoff_levels, power, grid_stack);

		result_file.close();
		log_file.close();
		json_file.close();
	}
}

void experiments::measure_pruning_exporting_saarland_100k()
{
	const std::string EXP_NAME =
	    "Measuring Pruning Trajectory Exporting: Saarland 100k";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 20;

	std::string experiment_folder = exp_folder + "Pruning/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Saarland_100k/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    saarland_generated_100k_filename, true);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	const GridIndex p = 6;
	const GridIndex q = 6;
	const GridIndex level = 4;

	std::cout << time_str() << "Create grid: " << p << " x " << q << " with "
	          << level << " levels." << std::endl;
	auto grid_hierarchy_assigner =
	    GridHierarchyAssigner(p, q, level, paths_ds, graph);
	auto level_0_grid = grid_hierarchy_assigner.get_level0_grid();
	const GridStack grid_stack = GridStack(level_0_grid, level);

	// create hierarchy unsing the object
	grid_hierarchy_assigner.create_hierarchy();
	std::cout << time_str() << "  done." << std::endl;

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	const std::vector<int> bounding_box_size_powers = {
	    5, 4, 3, 2, 1}; // 1/32, ..., 1/2
	const std::vector<int> zoom_levels = {15, 14, 13, 12, 11};

	const auto cutoff_levels = {128, 64, 32, 16, 8, 4};
	for (size_t i = 0; i < bounding_box_size_powers.size(); i++) {
		auto& power = bounding_box_size_powers[i];
		auto& zoom_level = zoom_levels[i];

		std::string power_string =
		    power > 9 ? std::to_string(power) : "0" + std::to_string(power);
		std::string log_output_filename =
		    experiment_folder + "saarland_power_" + power_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "saarland_power_" + power_string +
		                                     "_results.txt";
		std::string json_output_filename = experiment_folder +
		                                   "saarland_power_" + power_string +
		                                   "_requests.json";

		Print("NEW POWER LEVEL : " << power);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		measure_pruned_trajectory_exporting(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, json_file, repeat_times,
		    cutoff_levels, power, grid_stack);

		result_file.close();
		log_file.close();
		json_file.close();
	}
}

void experiments::measure_pruning_exporting_europe_500k()
{
	const std::string EXP_NAME =
	    "Measuring Pruning Trajectory Exporting: Europe 500k";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 20;

	std::string experiment_folder = exp_folder + "Pruning/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Europe_500k/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_europe_filename,
	    europe_generated_500k_filename, true);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	const GridIndex p = 7;
	const GridIndex q = 7;
	const GridIndex level = 4;

	std::cout << time_str() << "Create grid: " << p << " x " << q << " with "
	          << level << " levels." << std::endl;
	auto grid_hierarchy_assigner =
	    GridHierarchyAssigner(p, q, level, paths_ds, graph);
	auto level_0_grid = grid_hierarchy_assigner.get_level0_grid();
	const GridStack grid_stack = GridStack(level_0_grid, level);

	// create hierarchy unsing the object
	grid_hierarchy_assigner.create_hierarchy();
	std::cout << time_str() << "  done." << std::endl;

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	const std::vector<int> bounding_box_size_powers = {
	    6, 5, 4, 3, 2}; // 1/64, 1/32,..., 1/8, 1/4
	const std::vector<int> zoom_levels = {9, 8, 7, 6, 5};

	const auto cutoff_levels = {512, 256, 128, 64};
	for (size_t i = 0; i < bounding_box_size_powers.size(); i++) {
		auto& power = bounding_box_size_powers[i];
		auto& zoom_level = zoom_levels[i];

		std::string power_string =
		    power > 9 ? std::to_string(power) : "0" + std::to_string(power);
		std::string log_output_filename =
		    experiment_folder + "europe_power_" + power_string + "_log.txt";
		std::string result_output_filename =
		    experiment_folder + "europe_power_" + power_string + "_results.txt";
		std::string json_output_filename = experiment_folder + "europe_power_" +
		                                   power_string + "_requests.json";

		Print("NEW POWER LEVEL : " << power);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		measure_pruned_trajectory_exporting(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, json_file, repeat_times,
		    cutoff_levels, power, grid_stack);

		result_file.close();
		log_file.close();
		json_file.close();
	}
}

void experiments::measure_pruning_exporting_germany()
{
	const std::string EXP_NAME =
	    "Measuring Pruning Trajectory Exporting: Germany";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;
	size_t repeat_times = 20;

	std::string experiment_folder = exp_folder + "Pruning/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Germany/";
	FolderUtil::create_folder(experiment_folder);

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_germany_filename,
	    realpaths_germany_filename);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	const GridIndex p = 6;
	const GridIndex q = 6;
	const GridIndex level = 4;

	std::cout << time_str() << "Create grid: " << p << " x " << q << " with "
	          << level << " levels." << std::endl;
	auto grid_hierarchy_assigner =
	    GridHierarchyAssigner(p, q, level, paths_ds, graph);
	auto level_0_grid = grid_hierarchy_assigner.get_level0_grid();
	const GridStack grid_stack = GridStack(level_0_grid, level);

	// create hierarchy unsing the object
	grid_hierarchy_assigner.create_hierarchy();
	std::cout << time_str() << "  done." << std::endl;

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	const std::vector<int> bounding_box_size_powers = {
	    5, 4, 3, 2, 1}; // 1/32, ..., 1/2
	const std::vector<int> zoom_levels = {
	    11, 10, 9, 8, 7}; // ranging from 1 to 18

	const auto cutoff_levels = {512, 256, 128, 64, 32};
	for (size_t i = 0; i < bounding_box_size_powers.size(); i++) {
		auto& power = bounding_box_size_powers[i];
		auto& zoom_level = zoom_levels[i];

		std::string power_string =
		    power > 9 ? std::to_string(power) : "0" + std::to_string(power);
		std::string log_output_filename =
		    experiment_folder + "germany_power_" + power_string + "_log.txt";
		std::string result_output_filename = experiment_folder +
		                                     "germany_power_" + power_string +
		                                     "_results.txt";
		std::string json_output_filename = experiment_folder +
		                                   "germany_power_" + power_string +
		                                   "_requests.json";

		Print("NEW POWER LEVEL : " << power);
		_create_empty_file(log_output_filename);
		std::ofstream log_file;
		log_file.open(log_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(result_output_filename);
		std::ofstream result_file;
		result_file.open(result_output_filename,
		    std::ios::out | std::ios::app); // append to file

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		measure_pruned_trajectory_exporting(p_ds, pathfinder, exporter,
		    zoom_level, log_file, result_file, json_file, repeat_times,
		    cutoff_levels, power, grid_stack);

		result_file.close();
		log_file.close();
		json_file.close();
	}
}

void experiments::measure_pruning_exporting_increase_of_size()
{
	const std::string EXP_NAME =
	    "Measuring Pruning Trajectory Exporting: Saarland (increase in sizes)";
	printExpHeader(EXP_NAME);
	const uint no_of_threads = 32;

	std::string experiment_folder = exp_folder + "Pruning/";
	FolderUtil::create_folder(experiment_folder);
	experiment_folder = experiment_folder + "Saarland Size Over Cutoff/";
	FolderUtil::create_folder(experiment_folder);

	const std::string request_folder = experiment_folder + "json responses/";
	FolderUtil::create_folder(request_folder);

	const int power = 0; // whole saarland
	const int zoom_level = 11;

	// create oputput files
	std::string power_string =
	    power > 9 ? std::to_string(power) : "0" + std::to_string(power);
	std::string log_output_filename =
	    experiment_folder + "saarland_over_sizes_log.txt";
	std::string result_output_filename =
	    experiment_folder + "saarland_power_over_sizes_result.txt";

	_create_empty_file(log_output_filename);
	std::ofstream log_file;
	log_file.open(
	    log_output_filename, std::ios::out | std::ios::app); // append to file

	_create_empty_file(result_output_filename);
	std::ofstream result_file;
	result_file.open(result_output_filename,
	    std::ios::out | std::ios::app); // append to file

	random.seed(564344);

	Graph graph;
	PathsDS paths_ds;
	init_graph_and_paths(graph, paths_ds, realgraph_saarland_filename,
	    realpaths_saarland_filename);
	PathfinderDS p_ds = PathfinderDS(graph, no_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(p_ds, no_of_threads);

	const GridIndex p = 11;
	const GridIndex q = 11;
	const GridIndex level = 4;

	std::cout << time_str() << "Create grid: " << p << " x " << q << " with "
	          << level << " levels." << std::endl;
	result_file << time_str() << "Create grid: " << p << " x " << q << " with "
	            << level << " levels." << std::endl;
	auto grid_hierarchy_assigner =
	    GridHierarchyAssigner(p, q, level, paths_ds, graph);
	auto level_0_grid = grid_hierarchy_assigner.get_level0_grid();
	const GridStack grid_stack = GridStack(level_0_grid, level);

	// create hierarchy unsing the object
	grid_hierarchy_assigner.create_hierarchy();
	std::cout << time_str() << "  done." << std::endl;
	result_file << time_str() << "  done." << std::endl;

	_print_graph_statistics(graph);
	Exporter exporter(graph);

	result_file << "cutoff,time,totalSize" << std::endl;

	for (size_t cutoff_levels = 1; cutoff_levels <= 153; cutoff_levels++) {
		Print("NEW CUTOFF LEVEL : " << cutoff_levels);
		log_file << "NEW CUTOFF LEVEL : " << cutoff_levels << std::endl;

		std::string str_filename = "";
		if (cutoff_levels < 10) {
			str_filename = "00" + std::to_string(cutoff_levels);
		} else if (cutoff_levels < 100) {
			str_filename = "0" + std::to_string(cutoff_levels);
		} else {
			str_filename = std::to_string(cutoff_levels);
		}

		std::string json_output_filename = request_folder +
		                                   "power_over_sizes_result_cutoff_" +
		                                   str_filename + ".json";

		_create_empty_file(json_output_filename);
		std::ofstream json_file;
		json_file.open(json_output_filename,
		    std::ios::out | std::ios::app); // append to file

		TimeTracker time_tracker;

		BoundingBox request_bb = _get_random_box_on_level(power, p_ds);
		PathIDs path_ids = pathfinder.run(Query(request_bb));

		time_tracker.start();

		Options options;
		options.setPacking(true);
		options.setZoom(zoom_level);
		options.setCutoffLevel(cutoff_levels);
		LevelExtractor level_extractor(1.8713, 17.);

		ExportData export_data;
		export_data.found_paths = path_ids;
		export_data.boxes = {request_bb, request_bb};
		const auto& exported_json = exporter.export_prunned_segments(
		    export_data, options, p_ds, grid_stack, level_extractor);
		json_file << exported_json;

		time_tracker.stop();
		double time_needed = time_tracker.getLastMeasurment();

		result_file << cutoff_levels << "," << time_needed << ","
		            << exported_json.size() << std::endl;
		json_file.close();
	}

	result_file.close();
	log_file.close();
}

} // namespace pf

// just in case anyone does anything stupid with this file...
#undef Test
