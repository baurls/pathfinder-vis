#include "evaluation.h"
#include "exporter.h"
#include "paths_to_ds.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, Master Thesis 2021.    #
    #                                                 #
    #   Supervisor: T. Rupp, Prof. S. Funke           #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

Evaluator::Evaluator(PathfinderDS& p_ds, Pathfinder& p)
    : p_ds(p_ds), exporter(Exporter(p_ds.getGraph())), pathfinder(p)
{
	// todo
}

std::string Evaluator::evaluate_experiment(int number) const
{
	std::string objects;
	switch (number) {
	case 1:
		objects = experiment_no_1();
		break;
	case 3:
		objects = experiment_export_segment_graph();
		break;

	default:
		std::cout << "Object ID is not known: Make sure to set a valid one."
		          << std::endl;
		break;
	}

	return "{\"experiment_no\": " + std::to_string(number) +
	       ", \"data\" : " + objects + "}";
}

std::string Evaluator::experiment_no_1() const
{
	auto paths = _get_random_path_result_on_level(3);
	return "[{\"key\" : \"some_data here\"}]";
}

std::string Evaluator::experiment_export_segment_graph() const
{
	random.seed(564344);
	std::string export_folder = "";
	Exporter exporter(p_ds.getGraph());
	size_t repeat_times = 2;
	auto min_level = 1;
	auto max_level = 8;

	Print("Calculate test set for frontend");
	auto bounding_box_size_powers = {5, 4, 3, 2, 1}; // 1/32, ..., 1/2

	std::string data_str = "{";
	for (int zoom_level = min_level; zoom_level <= max_level;
	     zoom_level++) { // go over all zoom levels
		if (zoom_level > min_level) {
			data_str += ",";
		}
		data_str += "\"" + std::to_string(zoom_level) + "\":{";
		auto first_round = true;
		for (auto box_size : bounding_box_size_powers) {
			if (first_round) {
				first_round = false;
			} else {
				data_str += ",";
			}
			data_str += "\"" + std::to_string(box_size) + "\":[";
			for (size_t i = 0; i < repeat_times;
			     i++) { // get some samples for each level
				if (i > 0) {
					data_str += ",";
				}
				auto path_ids = _get_random_path_result_on_level(box_size);

				Options options;
				options.setPacking(true);
				options.setZoom(zoom_level);

				LevelExtractor level_extractor(1.8713, 17.);

				ExportData export_data;
				export_data.found_paths = path_ids;
				UnpackingUtil::unpack_and_copy_paths(export_data.paths_ds,
				    path_ids, options, p_ds, level_extractor);

				auto exported_json = exporter.exportSegmentGraph(export_data);
				data_str += exported_json;
			}
			data_str += "]";
		}
		data_str += "}";
	}
	data_str += "}";

	return "{\"plot_type\" : \"segment_graph\", \"data\" :" + data_str + "}";
}

// util functions
PathIDs Evaluator::_get_random_path_result_on_level(int power) const
{
	// get base bounding box
	const Graph& graph = p_ds.getGraph();
	BoundingBox graph_bb(graph);

	// set zoom
	double factor = std::pow(2, -power);
	auto width = graph_bb.getWidth() * factor;
	auto height = graph_bb.getHeight() * factor;

	// get box
	auto random_box = random.getBox(graph, width, height);

	// run query and return result
	auto path_ids = pathfinder.run(Query(random_box));
	return path_ids;
}

} // namespace pf