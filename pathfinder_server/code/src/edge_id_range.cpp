#include "edge_id_range.h"

namespace pf
{

ReverseEdgeIDRange EdgeIDRange::reverse() const
{
	return ReverseEdgeIDRange(begin_it, end_it);
}

EdgeIDRange ReverseEdgeIDRange::reverse() const
{
	return EdgeIDRange(end_it.base(), begin_it.base());
}

} // namespace pf
