#pragma once

#include "paths_ds.h"
#include "shortest_path_partitioner.h"
#include "time_tracker.h"

#include <memory>

namespace pf
{

namespace set_util
{

template <typename T>
bool static checkEquality(std::vector<T> const& to_check,
    std::vector<T> const& baseline, bool verbose = true)
{
	std::vector<T> missing;
	std::set_difference(baseline.begin(), baseline.end(), to_check.begin(),
	    to_check.end(), std::inserter(missing, missing.begin()));
	std::vector<T> additional;
	std::set_difference(to_check.begin(), to_check.end(), baseline.begin(),
	    baseline.end(), std::inserter(additional, additional.begin()));

	if (!missing.empty() || !additional.empty()) {
		if (verbose) {
			std::cout << "Non-matching vectors found!\n";
			std::cout << "Missing elements: ";
			for (auto m : missing) {
				std::cout << m << " ";
			}
			std::cout << "\n";
			std::cout << "Additional elements: ";
			for (auto a : additional) {
				std::cout << a << " ";
			}
			std::cout << "\n";
		}
		return false;
	}

	return true;
}

template <typename T>
bool static checkSubset(std::vector<T> const& to_check,
    std::vector<T> const& baseline, bool verbose = true)
{
	std::vector<T> additional;
	std::set_difference(to_check.begin(), to_check.end(), baseline.begin(),
	    baseline.end(), std::inserter(additional, additional.begin()));

	if (!additional.empty()) {
		if (verbose) {
			std::cout << "\n";
			std::cout << "Additional elements: ";
			for (auto a : additional) {
				std::cout << a << " ";
			}
			std::cout << "\n";
		}
		return false;
	}

	return true;
}

} // namespace set_util

} // namespace pf
