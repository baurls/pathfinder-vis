#pragma once
#pragma once

#include "graph.h"
#include "level_extractor.h"
#include "options.h"
#include "pathfinder_ds.h"
#include "paths.h"
#include "paths_ds.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, Master Thesis 2021.    #
    #                                                 #
    #   Supervisor: T. Rupp, Prof. S. Funke           #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

class UnpackingUtil
{
public:
	static void unpack_and_copy_paths(PathsDS& export_data_paths_ds,
	    PathIDs& found_paths, Options const& query_options,
	    const PathfinderDS& p_ds, const LevelExtractor& level_extractor);
};

} // namespace pf