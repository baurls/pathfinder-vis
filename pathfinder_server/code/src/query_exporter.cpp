#include "query_exporter.h"

#include "defs.h"
#include "timeSlots.h"
#include "time_tracker.h"

#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>

namespace pf
{

QueryExporter::QueryExporter()
{
}

void QueryExporter::exportQueriesForPerformanceTest(const Queries& queries,
    std::string graph_filename, double factorForBBSize) const
{
	std::string directory_name =
	    graph_filename + "_" + std::to_string(queries.size()) + "_queries";
	mkdir(directory_name.c_str(), 0774);
	std::string file_name =
	    directory_name + "/" + std::to_string(factorForBBSize);
	exportQueries(queries, file_name);
}

void QueryExporter::exportQueries(
    const Queries& queries, std::string file_name) const
{
	std::ofstream f(file_name);
	if (!f.is_open()) {
		throw Exception("The file " + file_name + " could not be opened.");
	}

	TimeTracker time_tracker;

	Print("Export queries ...");
	time_tracker.start();
	writeHeader(queries, f);
	writeQueries(queries, f);
	time_tracker.stop();
	Print("Took " << time_tracker.getLastMeasurment() << " seconds");
}

void QueryExporter::writeHeader(Queries const& queries, std::ofstream& f) const
{
	f << queries.size() << "\n";
	f << "\n";
}

void QueryExporter::writeQueries(Queries const& queries, std::ofstream& f) const
{
	for (Query const& query : queries) {
		// enhance precision for reproducible results
		f << std::fixed << std::setprecision(8);
		f << query.bounding_box.min_coordinate.lat << " "
		  << query.bounding_box.min_coordinate.lon << " "
		  << query.bounding_box.max_coordinate.lat << " "
		  << query.bounding_box.max_coordinate.lon << " "
		  << query.time_intervall.min_time << " "
		  << query.time_intervall.max_time << " ";

		TimeSlots const& slotMask = query.time_slots;
		f << slotMask.slots;
		f << "\n";
	}
}

} // namespace pf
