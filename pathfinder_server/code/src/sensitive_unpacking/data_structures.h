#pragma once

#include "../basic_types.h"
#include "../defs.h"
#include "../pairwise_sort.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, 2022.				  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace sensitive_unpacking
{

struct WeightedEdgeset {

	EdgeIDs edges;
	Counter weights;

	WeightedEdgeset()
	{
	}

	WeightedEdgeset(EdgeIDs edges, Counter weights)
	    : edges(edges), weights(weights)
	{
		assert(edges.size() == weights.size());
	}

	// weights are 1 initially.
	WeightedEdgeset(EdgeIDs edges) : edges(edges), weights(edges.size(), 1)
	{
		assert(edges.size() == weights.size());
	}

	void addEdge(EdgeID edge_id, uint edge_weight)
	{
		edges.push_back(edge_id);
		weights.push_back(edge_weight);
	}

	std::size_t size() const
	{
		return edges.size();
	}

	// sort increasingly
	void sortByWeights()
	{
		sort_pairwise(edges, weights);
	}

	// sort increasingly
	void sortByEdgeIDs()
	{
		sort_pairwise(weights, edges);
	}
};

class EdgeCountDS
{

public:
	EdgeCountDS();
	void increment_count(const EdgeID& edge_id, const IntCount& weight);
	bool edgecount_exists(const EdgeID& edge_id) const;
	bool has_positive_count(const EdgeID& edge_id) const;
	void accumulate_weights(const EdgeID& target, const EdgeID& accumulant);
	void clear_count(const EdgeID& edge_id);
	bool have_equal_counts(
	    const EdgeID& e1, const EdgeID& e2, const EdgeID& e3) const;
	IntCount get_count(const EdgeID& edge_id) const;

private:
	std::map<EdgeID, IntCount> c;
};

struct CountAndActive {
	IntCount count;
	bool active;

	CountAndActive() : count(c::INVALID_COUNT), active(false)
	{
	}
};

class EdgeCountAndActiveDS
{

public:
	EdgeCountAndActiveDS(const Graph& graph);

	void increment_count(const EdgeID edge_id, const IntCount weight);
	bool edgecount_exists(const EdgeID edge_id) const;
	bool has_positive_count(const EdgeID edge_id) const;
	void accumulate_weights(const EdgeID target, const EdgeID accumulant);
	void clear_count(const EdgeID edge_id);
	bool have_equal_counts(
	    const EdgeID e1, const EdgeID e2, const EdgeID e3) const;
	IntCount get_count(const EdgeID edge_id) const;

	bool isActive(const EdgeID edge_id) const;
	void setActive(const EdgeID edge_id, bool active);
	const EdgeIDs& getToReset() const;

	void reset();

private:
	std::vector<CountAndActive> caa;
	EdgeIDs to_reset;
};

class NodeCountDS
{

public:
	NodeCountDS(const Graph& graph);
	void increment_count(const NodeID node_id, const IntCount weight);
	bool count_exists(const NodeID node_id) const;
	IntCount get_count(const NodeID node_id) const;
	void reset();

private:
	Counter counter;
	NodeIDs to_reset;
};

class MergeInfoTracker
{

public:
	MergeInfoTracker();
	void increment(EdgeID edge_id, IntCount weight);
	bool isOverflowed();
	bool canMerge(IntCount node_count);
	void reset();

private:
	std::pair<EdgeID, IntCount> slot1;
	std::pair<EdgeID, IntCount> slot2;

	bool overflowed;
};

} // namespace sensitive_unpacking

} // namespace pf
