#pragma once

#include "../basic_types.h"
#include "../defs.h"
#include "../graph.h"
#include "../pathfinder_ds.h"
#include "data_structures.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, Master Thesis 2021.    #
    #                                                 #
    #   Supervisor: T. Rupp, Prof. S. Funke           #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace sensitive_unpacking
{

namespace global
{

// == GLOBAL FUNCTIONS ==

// unweighted case
EdgeIDs unpack_edgeset(const EdgeIDs& E, const size_t& min_intersection,
    PathfinderDS& p_ds, const bool use_bidirectional_edges = false,
    bool use_node_count = false);
// weighted case
void unpack_weighted_edgeset(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& min_intersection, PathfinderDS& p_ds,
    const bool use_bidirectional_edges = false, bool use_node_count = false);

// == PRIVATE FUNCTIONS ===

EdgeIDs _initial_double_sweep(
    const EdgeIDs& E, const Level& cutoff, const Graph& graph);
EdgeIDs _flooding_triple_sweep(const EdgeIDs& E, const size_t& min_intersection,
    PathfinderDS& p_ds, const bool use_bidirectional_edges);
void _flooding_triple_sweep_weighted(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& min_intersection, PathfinderDS& p_ds,
    const bool use_bidirectional_edges);
void _flooding_triple_sweep_weightedWithNodeCounters(
    const WeightedEdgeset& E_in, WeightedEdgeset& E_out,
    const size_t& min_intersection, PathfinderDS& p_ds);
/*
void _flooding_triple_sweep_weightedWithNodeCounters(const WeightedEdgeset&
E_in, WeightedEdgeset& E_out, const size_t& min_intersection, PathfinderDS&
p_ds);
 * */
void _initial_double_sweep_weighted(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& min_intersection, PathfinderDS& p_ds);

} // namespace global

} // namespace sensitive_unpacking

} // namespace pf
