
#include "data_structures.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, 2022.				  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace sensitive_unpacking
{

EdgeCountDS::EdgeCountDS()
{
}

bool EdgeCountDS::edgecount_exists(const EdgeID& edge_id) const
{
	return c.find(edge_id) != c.end();
}

void EdgeCountDS::increment_count(const EdgeID& edge_id, const IntCount& weight)
{
	if (edgecount_exists(edge_id)) {
		c[edge_id] += weight;
	} else {
		c[edge_id] = weight;
	}
}

// increments the first parameter's count by the second.
void EdgeCountDS::accumulate_weights(
    const EdgeID& target, const EdgeID& accumulant)
{
	increment_count(target, c[accumulant]);
}

// increments the first parameter's count by the second.
void EdgeCountDS::clear_count(const EdgeID& edge_id)
{
	c[edge_id] = 0;
}

bool EdgeCountDS::have_equal_counts(
    const EdgeID& e1, const EdgeID& e2, const EdgeID& e3) const
{
	return (c.at(e1) == c.at(e2)) and (c.at(e2) == c.at(e3));
}

bool EdgeCountDS::has_positive_count(const EdgeID& edge_id) const
{
	return c.at(edge_id) > 0;
}

IntCount EdgeCountDS::get_count(const EdgeID& edge_id) const
{
	return c.at(edge_id);
}

EdgeCountAndActiveDS::EdgeCountAndActiveDS(const Graph& graph)
    : caa(graph.getAllEdges().size())
{
}

bool EdgeCountAndActiveDS::edgecount_exists(const EdgeID edge_id) const
{
	return caa[edge_id].count != c::INVALID_COUNT;
}

void EdgeCountAndActiveDS::increment_count(
    const EdgeID edge_id, const IntCount weight)
{
	if (edgecount_exists(edge_id)) {
		caa[edge_id].count += weight;
	} else {
		caa[edge_id].count = weight;
		to_reset.push_back(edge_id);
	}
}

// increments the first parameter's count by the second.
void EdgeCountAndActiveDS::accumulate_weights(
    const EdgeID target, const EdgeID accumulant)
{
	increment_count(target, caa[accumulant].count);
}

void EdgeCountAndActiveDS::clear_count(const EdgeID edge_id)
{
	caa[edge_id].count = 0;
}

bool EdgeCountAndActiveDS::have_equal_counts(
    const EdgeID e1, const EdgeID e2, const EdgeID e3) const
{
	return (caa.at(e1).count == caa.at(e2).count) and
	       (caa.at(e2).count == caa.at(e3).count);
}

bool EdgeCountAndActiveDS::has_positive_count(const EdgeID edge_id) const
{
	return caa.at(edge_id).count > 0;
}

IntCount EdgeCountAndActiveDS::get_count(const EdgeID edge_id) const
{
	return caa.at(edge_id).count;
}

bool EdgeCountAndActiveDS::isActive(const EdgeID edge_id) const
{
	return caa[edge_id].active;
}

void EdgeCountAndActiveDS::setActive(const EdgeID edge_id, bool active)
{
	caa[edge_id].active = active;
	assert(caa[edge_id].count !=
	       c::INVALID_COUNT); // This means that this edge_id has been scheduled
	                          // for resetting already
}

const EdgeIDs& EdgeCountAndActiveDS::getToReset() const
{
	return to_reset;
}

void EdgeCountAndActiveDS::reset()
{
	for (auto edge_id : to_reset) {
		caa[edge_id].count = c::INVALID_COUNT;
		caa[edge_id].active = false;
	}
	to_reset.clear();
}

NodeCountDS::NodeCountDS(const Graph& graph)
    : counter(graph.getAllNodes().size(), c::INVALID_COUNT)
{
}

bool NodeCountDS::count_exists(const NodeID node_id) const
{
	return counter[node_id] != c::INVALID_COUNT;
}

void NodeCountDS::increment_count(const NodeID node_id, const IntCount weight)
{
	if (count_exists(node_id)) {
		counter[node_id] += weight;
	} else {
		counter[node_id] = weight;
		to_reset.push_back(node_id);
	}
}

IntCount NodeCountDS::get_count(const NodeID node_id) const
{
	return counter.at(node_id);
}

void NodeCountDS::reset()
{
	for (auto node_id : to_reset) {
		counter[node_id] = c::INVALID_COUNT;
	}
	to_reset.clear();
}

MergeInfoTracker::MergeInfoTracker()
{
	reset();
}

void MergeInfoTracker::increment(EdgeID edge_id, IntCount weight)
{
	if (slot1.first == c::NO_EID) {
		slot1.first = edge_id;
		slot1.second = weight;
	} else if (slot1.first == edge_id) {
		slot1.second += weight;
	} else if (slot2.first == c::NO_EID) {
		slot2.first = edge_id;
		slot2.second = weight;
	} else if (slot2.first == edge_id) {
		slot2.second += weight;
	} else {
		overflowed = true;
	}
}

bool MergeInfoTracker::isOverflowed()
{
	return overflowed;
}

bool MergeInfoTracker::canMerge(IntCount node_count)
{
	// wenn die 2 gemappten counts beide gleich dem knotencount sind wollen wir
	// mergen
	bool exactlyTwoEdges =
	    slot1.first != c::NO_EID && slot2.first != c::NO_EID && !overflowed;
	bool all_equal_counts =
	    slot1.second == node_count && slot2.second == node_count;
	return exactlyTwoEdges && all_equal_counts;
}

void MergeInfoTracker::reset()
{
	slot1.first = c::NO_EID;
	slot1.second = c::INVALID_COUNT;
	slot2.first = c::NO_EID;
	slot2.second = c::INVALID_COUNT;
	overflowed = false;
}

} // namespace sensitive_unpacking

} // namespace pf
