#pragma once

#include "../basic_types.h"
#include "../defs.h"
#include "../graph.h"
#include "../pathfinder_ds.h"
#include "data_structures.h"
#include "shared.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, 2022.				  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace sensitive_unpacking
{

namespace local
{

// == GLOBAL FUNCTIONS ==

// unweighted case
EdgeIDs unpack_edgeset(const EdgeIDs& E, const size_t& min_intersection,
    PathfinderDS& p_ds, const bool use_bidirectional_edges = false,
    bool use_node_count = false);
// weighted case
void unpack_weighted_edgeset(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& min_intersection, PathfinderDS& p_ds,
    const bool use_bidirectional_edges = false, bool use_node_count = false);

// == PRIVATE FUNCTIONS ===

void _unpack_weighted_locally(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& min_intersection, PathfinderDS& p_ds,
    const bool use_bidirectional_edges);
void _unpack_weighted_locallyWithNodeCounters(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& min_intersection, PathfinderDS& p_ds);

} // namespace local

} // namespace sensitive_unpacking

namespace unit_tests
{
void testSensitiveUnpacking();
void testSensitiveUnpackingWeighted();
void testSensitiveUnpackingWeightedTiming();
} // namespace unit_tests

} // namespace pf
