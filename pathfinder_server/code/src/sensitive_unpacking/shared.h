#pragma once

#include "../basic_types.h"
#include "../defs.h"
#include "../graph.h"

#include <cmath>

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, 2022.				  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace sensitive_unpacking
{

float _to_percent(uint portion, uint total);

namespace shared
{
// converts all edes to their increasing-order equivalent
EdgeIDs make_edges_bidirectional(const EdgeIDs& E, const Graph& graph);

EdgeIDs getUpwardEdgesOfNode(NodeID node_id, const Graph& graph);

} // namespace shared

} // namespace sensitive_unpacking

} // namespace pf
