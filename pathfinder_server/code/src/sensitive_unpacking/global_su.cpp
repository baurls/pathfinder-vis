
#include "global_su.h"
#include "../time_tracker.h"
#include "shared.h"

#include <bits/stdc++.h>

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, Master Thesis 2021.    #
    #                                                 #
    #   Supervisor: T. Rupp, Prof. S. Funke           #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace sensitive_unpacking
{

namespace global
{

EdgeIDs unpack_edgeset(const EdgeIDs& E, const size_t& min_intersection,
    PathfinderDS& p_ds, const bool use_bidirectional_edges, bool use_node_count)
{
	const WeightedEdgeset E_in(E);
	WeightedEdgeset E_out;
	unpack_weighted_edgeset(E_in, E_out, min_intersection, p_ds,
	    use_bidirectional_edges, use_node_count);

	return E_out.edges;
}

void unpack_weighted_edgeset(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& min_intersection, PathfinderDS& p_ds,
    const bool use_bidirectional_edges, bool use_node_count)
{
	if (use_node_count) {
		return _flooding_triple_sweep_weightedWithNodeCounters(
		    E_in, E_out, min_intersection, p_ds);
	} else {
		return _flooding_triple_sweep_weighted(
		    E_in, E_out, min_intersection, p_ds, use_bidirectional_edges);
	}
}

void unpack_weighted_edgesetWithNodeCounters(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& min_intersection, PathfinderDS& p_ds)
{
	return _flooding_triple_sweep_weightedWithNodeCounters(
	    E_in, E_out, min_intersection, p_ds);
}

void _flooding_triple_sweep_weighted(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& min_intersection, PathfinderDS& p_ds,
    const bool use_bidirectional_edges)
{
	const Graph& graph = p_ds.getGraph();
	SweepEdges& sweep_edges = p_ds.getSweepEdges();

	Print("| Unpacking sensitively: sweep-based, global ("
	      << E_in.size() << " start edges) [Flooding]");

	if (E_in.size() == 0) {
		Print("| | Skip calculation due to empty input list");
		Print("| Sensitive unpacking finished successfully");
		return;
	}

	TimeTracker time_tracker;
	time_tracker.start();

	// flip edges in case of bidirectional only
	const EdgeIDs E =
	    (use_bidirectional_edges)
	        ? sensitive_unpacking::shared::make_edges_bidirectional(
	              E_in.edges, graph)
	        : E_in.edges;

	// initially mark all incoming edges
	for (size_t i = 0; i < E.size(); i++) {
		const EdgeID edge_id = E.at(i);
		EdgeID sweep_edge_id = graph.getLevelsortedEdgeIndexMap().at(edge_id);
		sweep_edges.at(sweep_edge_id).count += E_in.weights.at(i);
	}

	time_tracker.stop();
	Print("| | Init data structures: " << time_tracker.getLastMeasurment()
	                                   << " seconds");

	time_tracker.start();

	// First sweep (top down): Push markings downwards
	const auto& min_index = graph.get_first_shortcut_edge_index();
	for (size_t i = sweep_edges.size(); i > min_index; i--) {
		const SweepEdge& parent = sweep_edges.at(i - 1);
		SweepEdge& child_1 = sweep_edges.at(parent.child_edge1);
		SweepEdge& child_2 = sweep_edges.at(parent.child_edge2);
		// propagate weighting downwards
		if (parent.count > 0) {
			child_1.count += parent.count;
			child_2.count += parent.count;
		}
	}
	time_tracker.stop();
	Print("| | First sweep (top down flooding): "
	      << time_tracker.getLastMeasurment() << " seconds");

	time_tracker.start();
	// Second Sweep (bootom-up)
	for (size_t i = graph.get_first_shortcut_edge_index();
	     i < sweep_edges.size(); i++) {
		SweepEdge& parent = sweep_edges.at(i);
		if (parent.count == 0) {
			continue;
		}
		SweepEdge& child_1 = sweep_edges.at(parent.child_edge1);
		SweepEdge& child_2 = sweep_edges.at(parent.child_edge2);

		if (child_1.count == child_2.count and child_2.count == parent.count) {
			// merge e_1 and e_2 by replacing with parent
			child_1.count = 0;
			child_2.count = 0;
		} else {
			// propagate "perform split upwards"
			parent.count = 0;
		}
	}
	time_tracker.stop();
	Print("| | Merge upwards: " << time_tracker.getLastMeasurment()
	                            << " seconds");

	time_tracker.start();
	// Third Sweep: Collect matches, filter for min intersections, reset
	for (size_t i = 0; i < sweep_edges.size(); i++) {
		SweepEdge& sweep_edge = sweep_edges.at(i);
		if (sweep_edge.count >= min_intersection) {
			E_out.addEdge(sweep_edge.id, sweep_edge.count);
		}
		sweep_edge.count = 0; // TODO: maybe fill() instead?
	}
	time_tracker.stop();
	Print("| | Collect (filter for intersections >="
	      << min_intersection << "): " << time_tracker.getLastMeasurment()
	      << " seconds");
	Print("| Sensitive unpacking finished successfully");
}

void _flooding_triple_sweep_weightedWithNodeCounters(
    const WeightedEdgeset& E_in, WeightedEdgeset& E_out,
    const size_t& min_intersection, PathfinderDS& p_ds)
{
	const Graph& graph = p_ds.getGraph();
	SweepEdges& sweep_edges = p_ds.getSweepEdges();
	Counter& node_counter = p_ds.getNodeCounter();

	Print("| Unpacking sensitively: sweep-based, global ("
	      << E_in.size() << " start edges) [Flooding]");

	if (E_in.size() == 0) {
		Print("| | Skip calculation due to empty input list");
		Print("| Sensitive unpacking finished successfully");
		return;
	}

	TimeTracker time_tracker;
	time_tracker.start();

	for (const SweepEdge& sweep_edge : sweep_edges) {
		assert(sweep_edge.count == 0);
	}

	std::fill(node_counter.begin(), node_counter.end(), 0);

	// flip edges in case of bidirectional only
	const EdgeIDs& E = E_in.edges;

	// initially mark all incoming edges
	for (size_t i = 0; i < E.size(); i++) {
		const EdgeID edge_id = E.at(i);

		EdgeID sweep_edge_id = graph.getLevelsortedEdgeIndexMap().at(edge_id);
		SweepEdge& sweep_edge = sweep_edges.at(sweep_edge_id);
		sweep_edge.count += E_in.weights.at(i);
		node_counter.at(sweep_edge.source) += E_in.weights.at(i);
		node_counter.at(sweep_edge.target) += E_in.weights.at(i);
	}

	time_tracker.stop();
	Print("| | Init data structures: " << time_tracker.getLastMeasurment()
	                                   << " seconds");

	time_tracker.start();

	// First sweep (top down): Push markings downwards
	const auto& min_index = graph.get_first_shortcut_edge_index();
	for (size_t i = sweep_edges.size(); i > min_index; i--) {
		const SweepEdge& parent = sweep_edges.at(i - 1);
		SweepEdge& child_1 = sweep_edges.at(parent.child_edge1);
		SweepEdge& child_2 = sweep_edges.at(parent.child_edge2);
		// propagate weighting downwards
		if (parent.count > 0) {
			child_1.count += parent.count;
			child_2.count += parent.count;
			node_counter.at(parent.shortcut_node) += parent.count;
		}
	}
	time_tracker.stop();
	Print("| | First sweep (top down flooding): "
	      << time_tracker.getLastMeasurment() << " seconds");

	size_t merge_counter = 0;
	size_t propagate_counter = 0;

	time_tracker.start();
	// Second Sweep (bootom-up)
	MergeInfoTracker mergeInfoTracker;

	for (NodeID sweep_node_id = 0;
	     sweep_node_id < (NodeID)graph.getNumberOfNodes(); sweep_node_id++) {

		if (p_ds.getSweepOffset(sweep_node_id) ==
		    p_ds.getSweepOffset(sweep_node_id + 1)) {
			continue;
		}

		// jetzt die counts auf die eindeutigen ungerichteten Kanten mappen
		mergeInfoTracker.reset();

		for (EdgeID child_sweep_edge_id : p_ds.getUpIndicesOf(sweep_node_id)) {
			const SweepEdge& child = sweep_edges.at(child_sweep_edge_id);
			IntCount weight = child.count;
			if (weight > 0) {
				EdgeID unique_sweep_edge_id =
				    p_ds.getSweepToUnidirectionalSweep(child_sweep_edge_id);

				mergeInfoTracker.increment(unique_sweep_edge_id, weight);
				if (mergeInfoTracker.isOverflowed()) {
					break;
				}
			}
		}

		if (mergeInfoTracker.canMerge(node_counter.at(sweep_node_id))) {
			for (EdgeID sweep_edge_id = p_ds.getSweepOffset(sweep_node_id);
			     sweep_edge_id < (EdgeID)p_ds.getSweepOffset(sweep_node_id + 1);
			     sweep_edge_id++) {

				const SweepEdge& parent = sweep_edges.at(sweep_edge_id);
				SweepEdge& child_1 = sweep_edges.at(parent.child_edge1);
				SweepEdge& child_2 = sweep_edges.at(parent.child_edge2);

				child_1.count = 0;
				child_2.count = 0;

				assert(parent.shortcut_node == sweep_node_id);

				merge_counter++;
			}

		} else {
			for (EdgeID sweep_edge_id = p_ds.getSweepOffset(sweep_node_id);
			     sweep_edge_id < (EdgeID)p_ds.getSweepOffset(sweep_node_id + 1);
			     sweep_edge_id++) {

				SweepEdge& parent = sweep_edges.at(sweep_edge_id);
				parent.count = 0;

				assert(parent.shortcut_node == sweep_node_id);

				propagate_counter++;
			}
		}
	}
	time_tracker.stop();
	Print("| | Merge upwards: " << time_tracker.getLastMeasurment()
	                            << " seconds");
	assert(merge_counter + propagate_counter ==
	       sweep_edges.size() - graph.get_first_shortcut_edge_index());

	time_tracker.start();
	// Third Sweep: Collect matches, filter for min intersections, reset
	for (size_t i = 0; i < sweep_edges.size(); i++) {
		SweepEdge& sweep_edge = sweep_edges.at(i);
		if (sweep_edge.count >= min_intersection) {
			E_out.addEdge(sweep_edge.id, sweep_edge.count);
		}
		sweep_edge.count = 0;
	}
	time_tracker.stop();
	Print("| | Collect (filter for intersections >="
	      << min_intersection << "): " << time_tracker.getLastMeasurment()
	      << " seconds");

	Print("| Sensitive unpacking finished successfully");
}

EdgeIDs _initial_double_sweep(
    const EdgeIDs& E, const Level& cutoff, PathfinderDS& p_ds)
{
	const Graph& graph = p_ds.getGraph();

	TimeTracker time_tracker;
	time_tracker.start();
	Print("| Unpacking sensitively (" << E.size() << " start edges)");
	// Init flag vector
	std::vector<bool> hasChildren(graph.getNumberOfEdges(), false);
	std::vector<bool> activeFlag(graph.getNumberOfEdges(), false);

	// initially mark all incoming edges
	for (const auto& edge_id : E) {
		const Edge& edge = graph.getEdge(edge_id);
		if (graph.getShortcutLevel(edge) >= cutoff) {
			hasChildren[edge_id] = true;
			activeFlag[edge_id] = true;
			Print("initial: " << edge_id);
		}
	}
	time_tracker.stop();
	Print("| | Init data structures: " << time_tracker.getLastMeasurment()
	                                   << " seconds");

	time_tracker.start();
	const Edges& level_sorted_edges = graph.getLevelorderedEdges();

	// First sweep (bottom up): Push markings upwards
	for (size_t i = graph.get_first_shortcut_edge_index();
	     i < level_sorted_edges.size(); i++) {
		const Edge& parent = level_sorted_edges[i];
		const auto& child_1 = parent.child_edge1;
		const auto& child_2 = parent.child_edge2;
		if (hasChildren[child_1] or hasChildren[child_2]) {
			hasChildren[parent.id] = true;
			Print("parent.id: " << parent.id << " lvl: "
			                    << graph.getShortcutLevel(parent.id));
			Print("child_1: " << child_1 << ": " << hasChildren[child_1]
			                  << " lvl: " << graph.getShortcutLevel(child_1));
			Print("child_2: " << child_2 << ": " << hasChildren[child_2]
			                  << " lvl: " << graph.getShortcutLevel(child_2));
		}
	}
	time_tracker.stop();
	Print(
	    "| | Mark upwards: " << time_tracker.getLastMeasurment() << " seconds");

	time_tracker.start();
	EdgeIDs E_ex;
	// Second Sweep (top-down): Export w.r.t. child existing markings
	for (size_t i = level_sorted_edges.size(); i > 0; i--) {
		const Edge& current =
		    level_sorted_edges[i - 1]; // index shift by one bc no -1 for
		                               // unsigned int possible

		// make sure to export edges only which are above the threshold
		if (graph.getShortcutLevel(current) < cutoff) {
			break;
		}
		// ignore non-relevant edges
		if (!hasChildren[current.id] or !activeFlag[current.id]) {
			continue;
		}
		if (!current.isShortcut()) {
			E_ex.push_back(current.id); // only the case if cutoff < 1
			continue;
		}
		const EdgeID& child_1 = current.child_edge1;
		const EdgeID& child_2 = current.child_edge2;
		if (!hasChildren[child_1] and !hasChildren[child_2]) {
			E_ex.push_back(current.id);
		} else if (hasChildren[child_1] and hasChildren[child_2]) {
			activeFlag[child_1] = true;
			activeFlag[child_2] = true;
		} else if (hasChildren[child_1]) {
			if (graph.getShortcutLevel(child_2) >= cutoff) {
				E_ex.push_back(child_2);
			}
			activeFlag[child_1] = true;
		} else {
			if (graph.getShortcutLevel(child_1) >= cutoff) {
				E_ex.push_back(child_1);
			}
			activeFlag[child_2] = true;
		}
	}
	time_tracker.stop();
	Print("| | Export downwards: " << time_tracker.getLastMeasurment()
	                               << " seconds");

	return E_ex;
}

void _initial_double_sweep_weighted(const EdgeIDs& E,
    const Counter& initial_weights, EdgeIDs& E_ex, Counter& weights,
    const Level& cutoff, PathfinderDS& p_ds)
{
	const Graph& graph = p_ds.getGraph();

	TimeTracker time_tracker;
	time_tracker.start();
	Print("| Unpacking sensitively and weighted (" << E.size()
	                                               << " start edges)");
	// Init flag vector
	std::vector<bool> hasChildren(graph.getNumberOfEdges(), false);
	Counter count(graph.getNumberOfEdges(), 0);

	// initially mark all incoming edges
	for (const auto& edge_id : E) {
		const Edge& edge = graph.getEdge(edge_id);
		if (graph.getShortcutLevel(edge) >= cutoff) {
			hasChildren[edge_id] = true;
			count[edge_id] = initial_weights[edge_id];
		}
	}
	time_tracker.stop();
	Print("| | Init data structures: " << time_tracker.getLastMeasurment()
	                                   << " seconds");

	time_tracker.start();
	const Edges& level_sorted_edges = graph.getLevelorderedEdges();

	// First sweep (bottom up): Push markings upwards
	for (size_t i = graph.get_first_shortcut_edge_index();
	     i < level_sorted_edges.size(); i++) {
		const Edge& parent = level_sorted_edges[i];
		const auto& child_1 = parent.child_edge1;
		const auto& child_2 = parent.child_edge2;
		if (hasChildren[child_1] or hasChildren[child_2]) {
			hasChildren[parent.id] = true;
		}
	}
	time_tracker.stop();
	Print(
	    "| | Mark upwards: " << time_tracker.getLastMeasurment() << " seconds");

	time_tracker.start();
	// Second Sweep (top-down): Export w.r.t. child existing markings
	for (size_t i = level_sorted_edges.size(); i > 0; i--) {
		const Edge& current =
		    level_sorted_edges[i - 1]; // index shift by one bc no -1 for
		                               // unsigned int possible
		if (!hasChildren[current.id]) {
			continue;
		}
		if (!current.isShortcut()) {
			E_ex.push_back(current.id);
			weights.push_back(count[current.id]);
			continue;
		}
		const EdgeID& child_1 = current.child_edge1;
		const EdgeID& child_2 = current.child_edge2;
		if (!hasChildren[child_1] and !hasChildren[child_2]) {
			E_ex.push_back(current.id);
			weights.push_back(count[current.id]);
		} else if (hasChildren[child_1] and hasChildren[child_2]) {
			count[child_1] += count[current.id];
			count[child_2] += count[current.id];
		} else if (hasChildren[child_1]) {
			E_ex.push_back(child_2);
			weights.push_back(count[current.id]);
			count[child_1] += count[current.id];
		} else {
			E_ex.push_back(child_1);
			weights.push_back(count[current.id]);
			count[child_2] += count[current.id];
		}
	}
	time_tracker.stop();
	Print("| | Export downwards: " << time_tracker.getLastMeasurment()
	                               << " seconds");
}

} // namespace global

} // namespace sensitive_unpacking

} // namespace pf
