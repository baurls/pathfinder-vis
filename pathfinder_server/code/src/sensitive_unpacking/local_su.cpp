
#include "local_su.h"
#include "../time_tracker.h"
#include "data_structures.h"

#include <bits/stdc++.h>

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, 2022.				  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace sensitive_unpacking
{

namespace local
{

// ##########################################

// ##########      util code     ############

// ##########################################

// filters E and w wrt. a threshold zeta (minimum intersection)
void _filter_results_wrt_zeta(WeightedEdgeset& E,
    const EdgeCountAndActiveDS& ecaa_ds, const IntCount& zeta)
{
	for (const auto& e : ecaa_ds.getToReset()) {
		if (ecaa_ds.get_count(e) >= zeta &&
		    ecaa_ds.isActive(e)) { // TODO:impliziert count > 1 active?
			E.addEdge(e, ecaa_ds.get_count(e));
		}
	}
}

// copies E (which is a set) and w to final vectors
void _copy_results(WeightedEdgeset& E, const EdgeCountAndActiveDS& ecaa_ds)
{
	for (const auto& e : ecaa_ds.getToReset()) {
		if (ecaa_ds.isActive(e)) {
			E.addEdge(e, ecaa_ds.get_count(e));
		}
	}
}

void _filter_results_wrt_zeta_sweep(WeightedEdgeset& E,
    const EdgeCountAndActiveDS& ecaa_ds, PathfinderDS& p_ds,
    const IntCount& zeta)
{
	for (const auto& e : ecaa_ds.getToReset()) {
		if (ecaa_ds.get_count(e) >= zeta &&
		    ecaa_ds.isActive(e)) { // TODO:impliziert count > 1 active?
			SweepEdge const& sweep_edge = p_ds.getSweepEdges().at(e);
			E.addEdge(sweep_edge.id, ecaa_ds.get_count(e));
		}
	}
}

// copies E (which is a set) and w to final vectors
void _copy_results_sweep(
    WeightedEdgeset& E, const EdgeCountAndActiveDS& ecaa_ds, PathfinderDS& p_ds)
{
	for (const auto& e : ecaa_ds.getToReset()) {
		if (ecaa_ds.isActive(e)) {
			SweepEdge const& sweep_edge = p_ds.getSweepEdges().at(e);
			E.addEdge(sweep_edge.id, ecaa_ds.get_count(e));
		}
	}
}

// ##########################################

// ##########      main code     ############

// ##########################################

// Given Edges 'E', a new edgeset 'E_ex' is calculated wrt. sensitive
// conditions.
EdgeIDs unpack_edgeset(const EdgeIDs& E, const size_t& zeta, PathfinderDS& p_ds,
    const bool use_bidirectional_edges, bool use_node_count)
{
	// unweighted calculation is just a special case of weighted where all
	// weights are 1 initially
	const WeightedEdgeset E_in(E);
	WeightedEdgeset E_out;
	unpack_weighted_edgeset(
	    E_in, E_out, zeta, p_ds, use_bidirectional_edges, use_node_count);

	return E_out.edges;
}

// Given a weighted Edgeset 'E', a new edgeset 'E_ex'
// is calculated wrt. sensitive conditions.
void unpack_weighted_edgeset(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& zeta, PathfinderDS& p_ds,
    const bool use_bidirectional_edges, bool use_node_count)
{
	if (use_node_count) {
		_unpack_weighted_locallyWithNodeCounters(E_in, E_out, zeta, p_ds);
	} else {
		_unpack_weighted_locally(
		    E_in, E_out, zeta, p_ds, use_bidirectional_edges);
	}
}

// Given a weighted Edgeset 'E', a new edgeset 'E_ex'
// is calculated wrt. sensitive conditions.
void _unpack_weighted_locally(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& zeta, PathfinderDS& p_ds,
    const bool use_bidirectional_edges)
{
	Print("| Unpacking sensitively: local (" << E_in.size()
	                                         << " start edges) [Flooding]");

	// break in case of no input edge
	if (E_in.size() == 0) {
		Print("| | Skip calculation due to empty input list");
		Print("| Sensitive unpacking finished successfully");
		return;
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	0. Initialization & inital marking
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	TimeTracker time_tracker;
	time_tracker.start();

	const Graph& graph = p_ds.getGraph();
	EdgeCountAndActiveDS& ecaa_ds = p_ds.getEdgeCountActiveDS();

	// max heap with (key, value)
	std::priority_queue<std::pair<pf::Level, pf::EdgeID>> prioQ;

	// flip edges in case of bidirectional only
	const EdgeIDs E =
	    (use_bidirectional_edges)
	        ? sensitive_unpacking::shared::make_edges_bidirectional(
	              E_in.edges, graph)
	        : E_in.edges;

	// initially mark all incoming edges
	for (size_t i = 0; i < E.size(); i++) {
		const auto& edge_id = E[i];
		ecaa_ds.increment_count(edge_id, E_in.weights[i]);
		prioQ.push(std::make_pair(graph.getShortcutLevel(edge_id), edge_id));
	}
	time_tracker.stop();
	Print("| | Init data structures: " << time_tracker.getLastMeasurment()
	                                   << " seconds");

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	1. Downward-Propagation
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	time_tracker.start();
	std::stack<EdgeID> sorted_parents_stack;

	// just for statistics
	uint downward_visits = 0;
	uint unique_downward_visits = 0;
	uint no_parents_found;
	uint no_plain_edges_found;
	uint merge_count = 0;
	uint propagate_count = 0;

	while (not prioQ.empty()) {
		// extract node with highest level
		const auto& parent_kv_pair = prioQ.top();
		const auto parent_id = parent_kv_pair.second;
		prioQ.pop();

		assert(ecaa_ds.edgecount_exists(parent_id));
		downward_visits++;

		// make sure to not visit the same edge twice
		if (ecaa_ds.isActive(parent_id)) {
			continue;
		}
		const Edge& parent = graph.getEdge(parent_id);

		ecaa_ds.setActive(parent_id, true);
		unique_downward_visits++;

		if (parent.isShortcut()) {
			// push parent to stack (for reverse sweep)
			sorted_parents_stack.push(parent_id);

			// propagate weighting downwards
			const auto& child_1 = parent.child_edge1;
			const auto& child_2 = parent.child_edge2;
			ecaa_ds.accumulate_weights(child_1, parent_id);
			ecaa_ds.accumulate_weights(child_2, parent_id);

			// append childs to queue
			prioQ.push(
			    std::make_pair(graph.getShortcutLevel(child_1), child_1));
			prioQ.push(
			    std::make_pair(graph.getShortcutLevel(child_2), child_2));
		}
	}
	time_tracker.stop();
	Print("| | First sweep (top down flooding): "
	      << time_tracker.getLastMeasurment() << " seconds");

	no_parents_found = sorted_parents_stack.size();
	no_plain_edges_found = unique_downward_visits - no_parents_found;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	2. Upwards-Propagation
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	time_tracker.start();
	while (!sorted_parents_stack.empty()) {
		const EdgeID parent_id = sorted_parents_stack.top();
		sorted_parents_stack.pop();
		const Edge& parent = graph.getEdge(parent_id);

		assert(ecaa_ds.has_positive_count(parent.id));

		const EdgeID& child_1 = parent.child_edge1;
		const EdgeID& child_2 = parent.child_edge2;

		if (ecaa_ds.have_equal_counts(child_1, child_2, parent.id)) {
			// merge e_1 and e_2 by replacing with parent
			ecaa_ds.clear_count(child_1);
			ecaa_ds.clear_count(child_2);
			ecaa_ds.setActive(child_1, false);
			ecaa_ds.setActive(child_2, false);
			merge_count += 1;
		} else {
			// propagate "perform split" upwards
			ecaa_ds.clear_count(parent.id);
			ecaa_ds.setActive(parent.id, false);
			propagate_count += 1;
		}
	}
	time_tracker.stop();
	Print("| | Merge upwards: " << time_tracker.getLastMeasurment()
	                            << " seconds");

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	3. Collect Matches
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	time_tracker.start();
	// Third Sweep: Collect matches, filter for min intersections
	if (zeta > 1) {
		_filter_results_wrt_zeta(E_out, ecaa_ds, zeta);
	} else {
		_copy_results(E_out, ecaa_ds);
	}
	time_tracker.stop();

	Print("| | Collect (filter for intersections >="
	      << zeta << "): " << time_tracker.getLastMeasurment() << " seconds");

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	4. Reset DS
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	time_tracker.start();
	ecaa_ds.reset();
	time_tracker.stop();

	Print("| | Reset DS: " << time_tracker.getLastMeasurment() << " seconds");

	Print("| Sensitive unpacking finished successfully");

	Print("| Merging Statistics:");
	Print("| | #visits downwards:        " << downward_visits);
	Print("| | #unique visits downwards: "
	      << unique_downward_visits << " ("
	      << _to_percent(unique_downward_visits, downward_visits) << "%)");
	Print("| | | #plain edes:            "
	      << no_plain_edges_found << " ("
	      << _to_percent(no_plain_edges_found, unique_downward_visits) << "%)");
	Print("| | | #shortcuts:             "
	      << no_parents_found << " ("
	      << _to_percent(no_parents_found, unique_downward_visits) << "%)");
	Print("| | | | #merges:              "
	      << merge_count << " (" << _to_percent(merge_count, no_parents_found)
	      << "%)");
	Print("| | | | #propagations:        "
	      << propagate_count << " ("
	      << _to_percent(propagate_count, no_parents_found) << "%)");
}

void _unpack_weighted_locallyWithNodeCounters(const WeightedEdgeset& E_in,
    WeightedEdgeset& E_out, const size_t& zeta, PathfinderDS& p_ds)
{
	Print("| Unpacking sensitively: local with Node-Counters ("
	      << E_in.size() << " start edges) [Flooding]");

	// break in case of no input edge
	if (E_in.size() == 0) {
		Print("| | Skip calculation due to empty input list");
		Print("| Sensitive unpacking finished successfully");
		return;
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	0. Initialization & inital marking
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	TimeTracker time_tracker;
	time_tracker.start();

	const Graph& graph = p_ds.getGraph();
	EdgeCountAndActiveDS& ecaa_ds = p_ds.getEdgeCountActiveDS();
	NodeCountDS& nc_ds = p_ds.getNodeCountDS();

	for (uint i = 0; i < graph.getNumberOfEdges(); i++) {
		assert(ecaa_ds.get_count(i) == c::INVALID_COUNT);
		assert(ecaa_ds.isActive(i) == false);
	}

	for (uint i = 0; i < graph.getNumberOfNodes(); i++) {
		assert(nc_ds.get_count(i) == c::INVALID_COUNT);
	}

	// max heap with (key, value) where key is (level, shortcutNode) //TODO: as
	// nodes should be sorted with respect to level, sorting w.r.t. nodes should
	// be sufficient
	using PQKey = std::pair<pf::Level, NodeID>;
	using PQElement = std::pair<PQKey, pf::EdgeID>;

	auto comparator = [](PQElement lhs, PQElement rhs) {
		const PQKey& lhs_key = lhs.first;
		const PQKey& rhs_key = rhs.first;
		return lhs_key.first < rhs_key.first ||
		       (lhs_key.first == rhs_key.first &&
		           lhs_key.second < rhs_key.second);
	};

	std::priority_queue<PQElement, std::vector<PQElement>, decltype(comparator)>
	    prioQ(comparator);

	const EdgeIDs& E = E_in.edges;

	// initially mark all incoming edges
	for (size_t i = 0; i < E.size(); i++) {
		EdgeID edge_id = E[i];
		const EdgeID sweep_edge_id =
		    graph.getLevelsortedEdgeIndexMap().at(edge_id);
		const SweepEdge& sweep_edge = p_ds.getSweepEdges().at(sweep_edge_id);
		assert(sweep_edge.id == edge_id);
		ecaa_ds.increment_count(sweep_edge_id, E_in.weights[i]);
		nc_ds.increment_count(sweep_edge.source, E_in.weights[i]);
		nc_ds.increment_count(sweep_edge.target, E_in.weights[i]);
		prioQ.push(
		    std::make_pair(std::make_pair(graph.getShortcutLevel(edge_id),
		                       sweep_edge.shortcut_node),
		        sweep_edge_id));
	}
	time_tracker.stop();
	Print("| | Init data structures: " << time_tracker.getLastMeasurment()
	                                   << " seconds");

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	1. Downward-Propagation
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	time_tracker.start();
	std::stack<EdgeID> sorted_parents_stack;

	// just for statistics
	uint downward_visits = 0;
	uint unique_downward_visits = 0;
	uint no_parents_found;
	uint no_plain_edges_found;
	uint merge_count = 0;
	uint propagate_count = 0;

	while (not prioQ.empty()) {
		// extract node with highest level
		const auto& parent_kv_pair = prioQ.top();
		const auto parent_id = parent_kv_pair.second;
		prioQ.pop();

		assert(ecaa_ds.edgecount_exists(parent_id));
		downward_visits++;

		// make sure to not visit the same edge twice
		if (ecaa_ds.isActive(parent_id)) {
			continue;
		}
		const SweepEdge& parent = p_ds.getSweepEdges().at(parent_id);

		ecaa_ds.setActive(parent_id, true);
		unique_downward_visits++;

		if (parent.isShortcut()) {
			// push parent to stack (for reverse sweep)
			sorted_parents_stack.push(parent_id);

			// propagate weighting downwards
			const EdgeID child_id_1 = parent.child_edge1;
			const EdgeID child_id_2 = parent.child_edge2;
			const SweepEdge& child_1 = p_ds.getSweepEdges().at(child_id_1);
			const SweepEdge& child_2 = p_ds.getSweepEdges().at(child_id_2);
			ecaa_ds.accumulate_weights(child_id_1, parent_id);
			ecaa_ds.accumulate_weights(child_id_2, parent_id);
			nc_ds.increment_count(
			    parent.shortcut_node, ecaa_ds.get_count(parent_id));

			// append childs to queue
			prioQ.push(
			    std::make_pair(std::make_pair(p_ds.getShortcutLevel(child_id_1),
			                       child_1.shortcut_node),
			        child_id_1));
			prioQ.push(
			    std::make_pair(std::make_pair(p_ds.getShortcutLevel(child_id_2),
			                       child_2.shortcut_node),
			        child_id_2));
		}
	}
	time_tracker.stop();
	Print("| | First sweep (top down flooding): "
	      << time_tracker.getLastMeasurment() << " seconds");

	no_parents_found = sorted_parents_stack.size();
	no_plain_edges_found = unique_downward_visits - no_parents_found;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	2. Upwards-Propagation
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	time_tracker.start();
	MergeInfoTracker mergeInfoTracker;

	while (!sorted_parents_stack
	            .empty()) { // TODO: build the stack/prioqueue differently, as
		                    // we extract all nodes with the same shortcut Node
		                    // in on go here

		// erstmal alle node mit der gleichen shortcutNodeID poppen, wir wissen
		// ja die kommen hintereinander, da der stack reverse zur prioqueue
		// aufgebaut wurde und die danach geordnet war
		EdgeIDs edgesWithSameShortcutNode;
		bool proceed;
		do {
			edgesWithSameShortcutNode.push_back(sorted_parents_stack.top());
			sorted_parents_stack.pop();

			if (sorted_parents_stack.empty()) {
				proceed = false;
			} else {
				NodeID shortcut_node_of_list =
				    p_ds.getSweepEdges()
				        .at(edgesWithSameShortcutNode.back())
				        .shortcut_node;
				NodeID shortcut_node_of_next_element =
				    p_ds.getSweepEdges()
				        .at(sorted_parents_stack.top())
				        .shortcut_node;
				proceed =
				    (shortcut_node_of_list == shortcut_node_of_next_element);
			}

		} while (proceed);

		// jetzt die counts auf die eindeutigen ungerichteten Kanten mappen
		mergeInfoTracker.reset();

		NodeID shortcut_node_of_list = p_ds.getSweepEdges()
		                                   .at(edgesWithSameShortcutNode.back())
		                                   .shortcut_node;

		for (EdgeID child_edge_id :
		    p_ds.getUpIndicesOf(shortcut_node_of_list)) {
			IntCount weight = ecaa_ds.get_count(child_edge_id);
			EdgeID unique_edge_id =
			    p_ds.getSweepToUnidirectionalSweep(child_edge_id);

			if (weight > 0) {
				mergeInfoTracker.increment(unique_edge_id, weight);
				if (mergeInfoTracker.isOverflowed()) {
					break;
				}
			}
		}

		if (mergeInfoTracker.canMerge(nc_ds.get_count(shortcut_node_of_list))) {
			for (EdgeID sweep_edge_id : edgesWithSameShortcutNode) {

				const SweepEdge& parent =
				    p_ds.getSweepEdges().at(sweep_edge_id);
				const EdgeID& child_1 = parent.child_edge1;
				const EdgeID& child_2 = parent.child_edge2;

				ecaa_ds.clear_count(child_1);
				ecaa_ds.clear_count(child_2);
				ecaa_ds.setActive(child_1, false);
				ecaa_ds.setActive(child_2, false);
				merge_count += 1;
			}

		} else {
			for (EdgeID sweep_edge_id : edgesWithSameShortcutNode) {
				ecaa_ds.clear_count(sweep_edge_id);
				ecaa_ds.setActive(sweep_edge_id, false);
				propagate_count += 1;
			}
		}
	}
	time_tracker.stop();
	Print("| | Merge upwards: " << time_tracker.getLastMeasurment()
	                            << " seconds");

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	3. Collect Matches
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	time_tracker.start();
	// Third Sweep: Collect matches, filter for min intersections
	if (zeta > 1) {
		_filter_results_wrt_zeta_sweep(E_out, ecaa_ds, p_ds, zeta);
	} else {
		_copy_results_sweep(E_out, ecaa_ds, p_ds);
	}
	time_tracker.stop();

	Print("| | Collect (filter for intersections >="
	      << zeta << "): " << time_tracker.getLastMeasurment() << " seconds");

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	4. Reset DS
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	time_tracker.start();
	ecaa_ds.reset();
	nc_ds.reset();
	time_tracker.stop();

	Print("| | Reset DS: " << time_tracker.getLastMeasurment() << " seconds");

	Print("| Sensitive unpacking finished successfully");

	Print("| Merging Statistics:");
	Print("| | #visits downwards:        " << downward_visits);
	Print("| | #unique visits downwards: "
	      << unique_downward_visits << " ("
	      << _to_percent(unique_downward_visits, downward_visits) << "%)");
	Print("| | | #plain edes:            "
	      << no_plain_edges_found << " ("
	      << _to_percent(no_plain_edges_found, unique_downward_visits) << "%)");
	Print("| | | #shortcuts:             "
	      << no_parents_found << " ("
	      << _to_percent(no_parents_found, unique_downward_visits) << "%)");
	Print("| | | | #merges:              "
	      << merge_count << " (" << _to_percent(merge_count, no_parents_found)
	      << "%)");
	Print("| | | | #propagations:        "
	      << propagate_count << " ("
	      << _to_percent(propagate_count, no_parents_found) << "%)");
}

} // namespace local

} // namespace sensitive_unpacking

} // namespace pf
