
#include "shared.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, 2022.				  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace sensitive_unpacking
{

float _to_percent(uint portion, uint total)
{
	int digits = 2;
	float accuracy = std::pow(10.0f, (float)digits);
	float percent =
	    std::round(accuracy * 100 * ((float)portion / (float)total));
	return percent / accuracy;
}

namespace shared
{

EdgeIDs make_edges_bidirectional(const EdgeIDs& E, const Graph& graph)
{
	EdgeIDs E_out;
	size_t flipped_edges = 0;
	size_t unflippable_edges = 0;
	size_t kept_edges;

	// check each edge for possible flip.
	for (size_t i = 0; i < E.size(); i++) {
		const EdgeID edge_id = E[i];
		if (not graph.edgeHasIncreasingOrder(edge_id)) {
			EdgeID flipped_edge = graph.getFlippedEdge(edge_id);
			if (flipped_edge == c::NO_EID) {
				flipped_edge = edge_id;
				unflippable_edges += 1;
				E_out.push_back(edge_id);
			} else {
				flipped_edges += 1;
				E_out.push_back(flipped_edge);
			}
		} else {
			E_out.push_back(edge_id);
		}
	}

	// print overview
	kept_edges = E.size() - flipped_edges - unflippable_edges;
	Print("| | Bidirectinal Edge Flipping Summary:");
	Print("| | | #kept:     " << kept_edges << " ("
	                          << _to_percent(kept_edges, E.size()) << "%)");
	Print("| | | #flipped:     " << flipped_edges << " ("
	                             << _to_percent(flipped_edges, E.size())
	                             << "%)");
	Print("| | | #unflippable: " << unflippable_edges << " ("
	                             << _to_percent(unflippable_edges, E.size())
	                             << "%)");

	return E_out;
}

EdgeIDs getUpwardEdgesOfNode(NodeID node_id, const Graph& graph)
{
	EdgeIDs upwardEdgesOfNode;
	for (const Edge& edge : graph.getUpEdgesOf(node_id, Direction::Forward)) {
		upwardEdgesOfNode.push_back(edge.id);
	}
	for (const Edge& edge : graph.getUpEdgesOf(node_id, Direction::Backward)) {
		upwardEdgesOfNode.push_back(edge.id);
	}
	return upwardEdgesOfNode;
}

} // namespace shared

} // namespace sensitive_unpacking

} // namespace pf
