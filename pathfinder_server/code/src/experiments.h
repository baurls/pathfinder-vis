#pragma once

#include "basic_types.h"
#include "defs.h"
#include "exporter.h"
#include "pathfinder.h"
#include "paths_ds.h"

namespace pf
{

namespace experiments
{
void runAll(unsigned int seed);
void run(unsigned int exp_number);
void run(unsigned int exp_number, unsigned int seed);
void runAll();
void run_edge_share_ratio_exp();
void run_edge_share_ratio_exp_saarland();
void run_edge_share_ratio_exp_example();
void run_edge_share_ratio_exp_germany();
void run_edge_share_ratio_exp_cuba();

void measure_segment_graph_trajectory_exporting_saarland();
void measure_segment_graph_trajectory_exporting_germany();

void measure_ebpf_trajectory_exporting_saarland();
void measure_ebpf_trajectory_exporting_saarland_100k();
void measure_ebpf_trajectory_exporting_saarland_1M();
void measure_ebpf_trajectory_exporting_saarland_10M();
void measure_ebpf_trajectory_exporting_germany();

void measure_ebpf_exporting_local_vs_global_saarland();
void measure_ebpf_exporting_local_vs_global_saarland_100k();
void measure_ebpf_exporting_local_vs_global_germany();

// EXP 24, 25, 26
void measure_ebpf_exporting_local_global_intersection_saarland();
void measure_ebpf_exporting_local_global_intersection_saarland100k();
void measure_ebpf_exporting_local_global_intersection_germany();


// EXP 27, 28, 29
void measure_ebpf_exporting_local_global_intersection_nodeCount_saarland();
void measure_ebpf_exporting_local_global_intersection_nodeCount_saarland100k();
void measure_ebpf_exporting_local_global_intersection_nodeCount_germany();


void measure_ebpf_requesting_saarland_for_log_evaluation(bool use_local_serach,
    const uint no_of_threads, size_t repeat_times, int seed);
void measure_ebpf_requesting_saarland100K_for_log_evaluation(
    bool use_local_serach, const uint no_of_threads, size_t repeat_times,
    int seed);
void measure_ebpf_requesting_germany_for_log_evaluation(bool use_local_serach,
    const uint no_of_threads, size_t repeat_times, int seed);

void measure_pruning_exporting_saarland();
void measure_pruning_exporting_germany();
void measure_pruning_exporting_saarland_100k();
void measure_pruning_exporting_europe_500k();
void measure_pruning_exporting_increase_of_size();

#if defined(PATHS_ON_DISK)
const uint nr_of_threads = 1;
#else
const uint nr_of_threads = 2;
#endif

const double max_path_distance = 50;

const std::size_t number_of_queries = 100;

const std::size_t max_number_of_queries_to_find_example = 100000;

const std::size_t number_of_simple_tests = 100;

// Setting for querying EBPF-instance with SU
const uint EBPF_DEFAULT_NO_OF_THREADS = 32;
const size_t EBPF_DEFAULT_REPEAT_TIMES = 500;
const int EBPF_DEFAULT_SEED = 564344;

// --- small example
static const std::string testgraph_filename =
    "../../data/test_graph/graph.graph";
static const std::string testpaths_filename =
    "../../data/test_graph/paths.json";

// artificially generated paths
static const std::string saarland_generated_1M_filename =
    "../../data/generated/saarland_len50_size1M1000000_gen.bin";
static const std::string saarland_generated_10M_filename =
    "../../data/generated/saarland_len50_size10M10000000_gen.bin";
static const std::string saarland_generated_100k_filename =
    "../../data/generated/saarland_len500_size100k100000_gen.bin";
static const std::string europe_generated_500k_filename =
    "../../data/generated/europe500000_gen.bin";

// --- real graphs-/paths- data ---

static const std::string realgraph_saarland_filename =
    "../../data/saarland/graph.graph";
static const std::string realpaths_saarland_filename =
    "../../data/saarland/paths.json";

static const std::string realgraph_cuba_filename =
    "../../data/cuba/graph.graph";
static const std::string realpaths_cuba_filename = "../../data/cuba/paths.json";

static const std::string realgraph_germany_filename =
    "/home/rupp/osm/data/germany/"
    "germany-180101.maxSpeed.all.distanceDM.ch.gbin";
static const std::string realpaths_germany_filename =
    "/home/rupp/osm/data/germany/paths.ges.json";

static const std::string realgraph_europe_filename =
    "/home/rupp/osm/data/europe/europe-210101-maxspeed-even_faster_car-ch.ftxt";

void init_graph_and_paths(Graph& graph, PathsDS& paths_ds,
    std::string graph_filename, std::string paths_filename,
    bool parse_binary_paths = false);

// Abstract struct of Graph-Pathfile-Setups
struct GraphPathsSetup {
	PathsDS paths_ds;
	Graph graph;
	std::string graph_name;

	GraphPathsSetup() = delete;
	GraphPathsSetup(std::string graph_filename, std::string paths_filename,
	    std::string graph_name, bool parse_binary_paths = false)
	    : graph_name(graph_name)
	{
		init_graph_and_paths(graph, paths_ds, graph_filename, paths_filename,
		    parse_binary_paths);
	};
};

// Concrete Setup Types
struct SaarlandSetup : GraphPathsSetup {
	SaarlandSetup()
	    : GraphPathsSetup(realgraph_saarland_filename,
	          realpaths_saarland_filename, "Saarland", false){};
};

struct Saarland100kSetup : GraphPathsSetup {
	Saarland100kSetup()
	    : GraphPathsSetup(realgraph_saarland_filename,
	          saarland_generated_100k_filename, "Saarland100k", true){};
};

struct GermanySetup : GraphPathsSetup {
	GermanySetup()
	    : GraphPathsSetup(realgraph_germany_filename,
	          realpaths_germany_filename, "Germany", false){};
};

// Combines all experiment-related codes
struct ExperimentEnvironment {
	std::string graph_name;
	Pathfinder pathfinder;
	PathsDS paths_ds;
	PathfinderDS& p_ds;
	Graph graph;
	Exporter exporter;

	ExperimentEnvironment(GraphPathsSetup& g_p_setup,
	    PathfinderDS& pathfinder_ds, Pathfinder& pfinder)
	    : graph(g_p_setup.graph)
	    , paths_ds(g_p_setup.paths_ds)
	    , graph_name(g_p_setup.graph_name)
	    , exporter(Exporter(g_p_setup.graph))
	    , pathfinder(pfinder)
	    , p_ds(pathfinder_ds){};
	ExperimentEnvironment() = delete;
};

} // namespace experiments

} // namespace pf
