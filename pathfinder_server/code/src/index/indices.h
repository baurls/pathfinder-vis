#pragma once

#include "IIndex.h"

#include <memory>

namespace pf
{

enum class InvertedIndicesVariant {
	IntervallTree,
	Vector,
	VectorOnDisk,
	WithoutTimeConstraint
};

std::unique_ptr<IIndex> createIndex(
    InvertedIndicesVariant inverted_indices_variant);

std::string invertedIndexVariantToString(
    InvertedIndicesVariant inverted_indices_variant);

} // namespace pf
