#pragma once

#include "IIndex.h"

namespace pf
{

// Inverse mapping from edge to path ids via an offset array
class VectorIndex : public IIndex
{
private:
	std::vector<PathIntervall> edge_to_paths_array;
	std::vector<std::size_t> edge_to_paths_offsets;

public:
	bool isContainedInPath(EdgeID edge_id) const
	{
		auto begin_index = edge_to_paths_offsets[edge_id];
		auto end_index = edge_to_paths_offsets[edge_id + 1];
		return begin_index != end_index;
	}

	TimeIntervall getTimeIntervall(EdgeID edge_id) const
	{
		auto begin_index = edge_to_paths_offsets[edge_id];
		auto end_index = edge_to_paths_offsets[edge_id + 1];

		TimeIntervall time_intervall;
		for (auto index = begin_index; index < end_index; ++index) {
			time_intervall.expand(edge_to_paths_array[index].time_intervall);
		}
		return time_intervall;
	}

	PathIntervalls getPathIntervalls(EdgeID edge_id) const
	{
		auto begin_index = edge_to_paths_offsets[edge_id];
		auto end_index = edge_to_paths_offsets[edge_id + 1];
		auto begin = &edge_to_paths_array[begin_index];
		auto end = &edge_to_paths_array[end_index];

		return PathIntervalls(begin, end);
	}

	void getPathIDsOnlySpatial(EdgeID edge_id, PathIDs& path_ids,
	    NodeIDs& search_stack, std::vector<char>& collected) const
	{
		Unused(search_stack);
		auto begin_index = edge_to_paths_offsets[edge_id];
		auto end_index = edge_to_paths_offsets[edge_id + 1];

		for (auto index = begin_index; index < end_index; ++index) {

			PathIntervall path_intervall = edge_to_paths_array[index];

			pushIfNotYetCollected(path_ids, path_intervall.path_id, collected);
		}
	}
	
	size_t getPathIDsCountOnlySpatial(EdgeID edge_id) const
	{
		return edge_to_paths_offsets[edge_id + 1] - edge_to_paths_offsets[edge_id];
	}

	void getPathIDs(EdgeID edge_id, Query const& query, PathIDs& path_ids,
	    NodeIDs& search_stack, std::vector<char>& collected) const
	{
		Unused(search_stack);
		auto begin_index = edge_to_paths_offsets[edge_id];
		auto end_index = edge_to_paths_offsets[edge_id + 1];

		for (auto index = begin_index; index < end_index; ++index) {

			PathIntervall path_intervall = edge_to_paths_array[index];
			TimeIntervall time_intervall = path_intervall.time_intervall;
			time_intervall.intersect(query.time_intervall);
			TimeSlots time_slots(path_intervall.time_intervall);
			time_slots.intersect(query.time_slots);

			if (time_intervall.isValid() && time_slots.isValid()) {
				pushIfNotYetCollected(
				    path_ids, path_intervall.path_id, collected);
			}
		}
	}

	size_t getPathIDsCount(EdgeID edge_id, Query const& query, NodeIDs& search_stack) const
	{
		Unused(search_stack);
		auto begin_index = edge_to_paths_offsets[edge_id];
		auto end_index = edge_to_paths_offsets[edge_id + 1];
		size_t counter = 0;
		for (auto index = begin_index; index < end_index; ++index) {

			PathIntervall path_intervall = edge_to_paths_array[index];
			TimeIntervall time_intervall = path_intervall.time_intervall;
			time_intervall.intersect(query.time_intervall);
			TimeSlots time_slots(path_intervall.time_intervall);
			time_slots.intersect(query.time_slots);

			if (time_intervall.isValid() && time_slots.isValid()) {
				counter++;
			}
		}
		return counter;
	}

	void init(uint numberOfEdges, uint numberOfPathEdges)
	{
		edge_to_paths_offsets.reserve(numberOfEdges + 1);
		edge_to_paths_array.reserve(numberOfPathEdges);
		edge_to_paths_offsets.push_back(0);
	}

	void preAddEdge()
	{
	}

	void addPathIntervall(PathIntervall path_intervall)
	{
		edge_to_paths_array.push_back(path_intervall);
	}

	void postAddEdge(uint offset)
	{
		edge_to_paths_offsets.push_back(offset);
	}

	void finish()
	{
	}

	void clearCaches() const
	{
	}
};

} // namespace pf
