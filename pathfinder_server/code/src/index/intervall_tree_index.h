#pragma once

#include "IIndex.h"

#include "../interval_tree.h"

namespace pf
{
// Inverse mapping from edge to path ids via intervalTrees
class IntervallTreeIndex : public IIndex
{
private:
	// Probably very inefficient in space, TODO: use one large vector for
	// the data of all interval trees
	using PathIntervallTree = IntervalTree<TimeType, PathID>;
	using PathIntervallTrees = std::vector<PathIntervallTree>;
	PathIntervallTrees edge_to_path_intervall_tree;

public:
	IntervallTreeIndex()
	{
	}

	bool isContainedInPath(EdgeID edge_id) const
	{
		NodeIDs search_stack;
		auto& intervall_tree = edge_to_path_intervall_tree[edge_id];
		auto intervals = intervall_tree.search(c::FULL_INTERVALL.min_time,
		    c::FULL_INTERVALL.max_time, search_stack);
		return !intervals.empty();
	}

	TimeIntervall getTimeIntervall(EdgeID edge_id) const
	{
		NodeIDs search_stack;
		auto& intervall_tree = edge_to_path_intervall_tree[edge_id];
		auto intervals = intervall_tree.search(c::FULL_INTERVALL.min_time,
		    c::FULL_INTERVALL.max_time, search_stack);

		TimeIntervall time_intervall;
		for (auto const& intervall : intervals) {
			TimeIntervall retrieved_time_intervall(
			    intervall.begin, intervall.end);
			time_intervall.expand(retrieved_time_intervall);
		}
		return time_intervall;
	}

	PathIntervalls getPathIntervalls(EdgeID edge_id) const
	{
		NodeIDs search_stack;
		auto& intervall_tree = edge_to_path_intervall_tree[edge_id];
		auto intervals = intervall_tree.search(c::FULL_INTERVALL.min_time,
		    c::FULL_INTERVALL.max_time, search_stack);

		PathIntervalls path_intervalls;
		for (auto const& intervall : intervals) {
			TimeIntervall retrieved_time_intervall(
			    intervall.begin, intervall.end);
			PathIntervall path_intervall(
			    intervall.value, retrieved_time_intervall);
			path_intervalls.push_back(path_intervall);
		}
		return path_intervalls;
	}

	void getPathIDsOnlySpatial(EdgeID edge_id, PathIDs& path_ids,
	    NodeIDs& search_stack, std::vector<char>& collected) const
	{
		PathIntervallTree const& path_intervall_tree =
		    edge_to_path_intervall_tree[edge_id];

		auto path_intervalls_retrieved =
		    path_intervall_tree.search(c::FULL_INTERVALL.min_time,
		        c::FULL_INTERVALL.max_time, search_stack);

		for (auto path_intervall_retrieved : path_intervalls_retrieved) {
			PathID path_id = path_intervall_retrieved.value;
			pushIfNotYetCollected(path_ids, path_id, collected);
		}
	}

	size_t getPathIDsCountOnlySpatial(EdgeID edge_id) const
	{
		auto search_stack = NodeIDs();
		
		PathIntervallTree const& path_intervall_tree =
		    edge_to_path_intervall_tree[edge_id];
		auto path_intervalls_retrieved =
		    path_intervall_tree.search(c::FULL_INTERVALL.min_time,
		        c::FULL_INTERVALL.max_time, search_stack);
		return path_intervalls_retrieved.size();
	}

	void getPathIDs(EdgeID edge_id, Query const& query, PathIDs& path_ids,
	    NodeIDs& search_stack, std::vector<char>& collected) const
	{
		PathIntervallTree const& path_intervall_tree =
		    edge_to_path_intervall_tree[edge_id];

		TimeIntervall const& query_intervall = query.time_intervall;
		auto path_intervalls_retrieved = path_intervall_tree.search(
		    query_intervall.min_time, query_intervall.max_time, search_stack);

		for (auto path_intervall_retrieved : path_intervalls_retrieved) {
			TimeIntervall time_intervall(
			    path_intervall_retrieved.begin, path_intervall_retrieved.end);

			PathID path_id = path_intervall_retrieved.value;

			TimeSlots time_slots(time_intervall);
			time_slots.intersect(query.time_slots);
			if (time_slots.isValid()) {
				pushIfNotYetCollected(path_ids, path_id, collected);
			}
		}
	}

	size_t getPathIDsCount(EdgeID edge_id, Query const& query,
	    NodeIDs& search_stack) const
	{
		PathIntervallTree const& path_intervall_tree =
		    edge_to_path_intervall_tree[edge_id];

		TimeIntervall const& query_intervall = query.time_intervall;
		auto path_intervalls_retrieved = path_intervall_tree.search(
		    query_intervall.min_time, query_intervall.max_time, search_stack);

		size_t counter = 0;
		for (auto path_intervall_retrieved : path_intervalls_retrieved) {
			TimeIntervall time_intervall(
			    path_intervall_retrieved.begin, path_intervall_retrieved.end);

			TimeSlots time_slots(time_intervall);
			time_slots.intersect(query.time_slots);
			if (time_slots.isValid()) {
				counter++;
			}
		}
		return counter;
	}

	void init(uint numberOfEdges, uint numberOfPathEdges)
	{
		Unused(numberOfPathEdges);
		edge_to_path_intervall_tree.reserve(numberOfEdges);
	}

	void preAddEdge()
	{
		edge_to_path_intervall_tree.push_back(IntervalTree<TimeType, PathID>());
	}

	void addPathIntervall(PathIntervall path_intervall)
	{
		auto& intervall_tree = edge_to_path_intervall_tree.back();
		intervall_tree.emplace(path_intervall.time_intervall.min_time,
		    path_intervall.time_intervall.max_time, path_intervall.path_id);
	}

	void postAddEdge(uint offset)
	{
		Unused(offset);
		auto& intervall_tree = edge_to_path_intervall_tree.back();
		intervall_tree.build();
	}

	void finish()
	{
	}

	void clearCaches() const
	{
	}
};

} // namespace pf
