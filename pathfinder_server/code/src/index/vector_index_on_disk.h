#pragma once

#include "IIndex.h"

#include <stxxl/vector>

namespace pf
{

// Inverse mapping from edge to path ids via an offset array
class VectorIndexOnDisk : public IIndex
{
private:
	stxxl::VECTOR_GENERATOR<PathIntervall>::result edge_to_paths_array;
	stxxl::VECTOR_GENERATOR<size_t>::result edge_to_paths_offsets;

	stxxl::stats_data stats_begin;

public:
	virtual ~VectorIndexOnDisk()
	{
		edge_to_paths_array.clear();
		edge_to_paths_offsets.clear();
	}

	bool isContainedInPath(EdgeID edge_id) const
	{
		auto const& const_edge_to_paths_offsets = edge_to_paths_offsets;
		auto begin_index = const_edge_to_paths_offsets[edge_id];
		auto end_index = const_edge_to_paths_offsets[edge_id + 1];
		return begin_index != end_index;
	}

	TimeIntervall getTimeIntervall(EdgeID edge_id) const
	{
		auto const& const_edge_to_paths_offsets = edge_to_paths_offsets;
		auto begin_index = const_edge_to_paths_offsets[edge_id];
		auto end_index = const_edge_to_paths_offsets[edge_id + 1];

		auto const& const_edge_to_paths_array = edge_to_paths_array;
		TimeIntervall time_intervall;
		for (auto index = begin_index; index < end_index; ++index) {
			time_intervall.expand(
			    const_edge_to_paths_array[index].time_intervall);
		}
		return time_intervall;
	}

	PathIntervalls getPathIntervalls(EdgeID edge_id) const
	{
		auto const& const_edge_to_paths_offsets = edge_to_paths_offsets;
		auto begin_index = const_edge_to_paths_offsets[edge_id];
		auto end_index = const_edge_to_paths_offsets[edge_id + 1];

		auto const& const_edge_to_paths_array = edge_to_paths_array;
		auto begin = &const_edge_to_paths_array[begin_index];
		auto end = &const_edge_to_paths_array[end_index];

		return PathIntervalls(begin, end);
	}

	size_t getPathIDsCountOnlySpatial(EdgeID edge_id) const
	{
		return edge_to_paths_offsets[edge_id + 1] - edge_to_paths_offsets[edge_id];
	}

	void getPathIDsOnlySpatial(EdgeID edge_id, PathIDs& path_ids,
	    NodeIDs& search_stack, std::vector<char>& collected) const
	{
		Unused(search_stack);
		auto const& const_edge_to_paths_offsets = edge_to_paths_offsets;
		auto begin_index = const_edge_to_paths_offsets[edge_id];
		auto end_index = const_edge_to_paths_offsets[edge_id + 1];

		auto const& const_edge_to_paths_array = edge_to_paths_array;

		for (auto index = begin_index; index < end_index; ++index) {

			PathIntervall const path_intervall =
			    const_edge_to_paths_array[index];

			pushIfNotYetCollected(path_ids, path_intervall.path_id, collected);
		}
	}

	void getPathIDs(EdgeID edge_id, Query const& query, PathIDs& path_ids,
	    NodeIDs& search_stack, std::vector<char>& collected) const
	{
		Unused(search_stack);
		auto const& const_edge_to_paths_offsets = edge_to_paths_offsets;
		auto begin_index = const_edge_to_paths_offsets[edge_id];
		auto end_index = const_edge_to_paths_offsets[edge_id + 1];

		auto const& const_edge_to_paths_array = edge_to_paths_array;

		for (auto index = begin_index; index < end_index; ++index) {

			PathIntervall const path_intervall =
			    const_edge_to_paths_array[index];
			TimeIntervall time_intervall = path_intervall.time_intervall;
			time_intervall.intersect(query.time_intervall);
			TimeSlots time_slots(path_intervall.time_intervall);
			time_slots.intersect(query.time_slots);

			if (time_intervall.isValid() && time_slots.isValid()) {
				pushIfNotYetCollected(
				    path_ids, path_intervall.path_id, collected);
			}
		}
	}

	size_t getPathIDsCount(EdgeID edge_id, Query const& query, NodeIDs& search_stack) const
	{
		Unused(search_stack);
		auto const& const_edge_to_paths_offsets = edge_to_paths_offsets;
		auto begin_index = const_edge_to_paths_offsets[edge_id];
		auto end_index = const_edge_to_paths_offsets[edge_id + 1];

		auto const& const_edge_to_paths_array = edge_to_paths_array;
		size_t counter = 0;
		for (auto index = begin_index; index < end_index; ++index) {

			PathIntervall const path_intervall =
			    const_edge_to_paths_array[index];
			TimeIntervall time_intervall = path_intervall.time_intervall;
			time_intervall.intersect(query.time_intervall);
			TimeSlots time_slots(path_intervall.time_intervall);
			time_slots.intersect(query.time_slots);

			if (time_intervall.isValid() && time_slots.isValid()) {
				counter++;
			}
		}
		return counter;
	}

	void init(uint numberOfEdges, uint numberOfPathEdges)
	{
		edge_to_paths_offsets.reserve(numberOfEdges + 1);
		edge_to_paths_array.reserve(numberOfPathEdges);
		edge_to_paths_offsets.push_back(0);

		stats_begin = stxxl::stats_data(*stxxl::stats::get_instance());
	}

	void preAddEdge()
	{
	}

	void addPathIntervall(PathIntervall path_intervall)
	{
		edge_to_paths_array.push_back(path_intervall);
	}

	void postAddEdge(uint offset)
	{
		edge_to_paths_offsets.push_back(offset);
	}

	void finish()
	{
#if defined(PATHS_ON_DISK_STATS)
		// for testing purposes to monitor write operations more cleanly
		// unfortunately these lines causes valgrind to moan about
		// uninitialised bytes
		edge_to_paths_offsets.flush();
		edge_to_paths_array.flush();

		stxxl::stats_data stats_end(*stxxl::stats::get_instance());
		Print(stats_end - stats_begin);
#endif
	}

	// to measure reads more cleanly
	void clearCaches() const
	{
		edge_to_paths_offsets.deallocate_page_cache();
		edge_to_paths_array.deallocate_page_cache();

		edge_to_paths_offsets.allocate_page_cache();
		edge_to_paths_array.allocate_page_cache();
	}
};

} // namespace pf
