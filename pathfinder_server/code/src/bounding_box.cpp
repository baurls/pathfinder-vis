#include "bounding_box.h"
#include <assert.h>

namespace pf
{

BoundingBox::BoundingBox()
{
}

BoundingBox::BoundingBox(Coordinate coordinate)
    : min_coordinate(coordinate), max_coordinate(coordinate)
{
}

BoundingBox::BoundingBox(Coordinate min_coordinate, Coordinate max_coordinate)
    : min_coordinate(min_coordinate), max_coordinate(max_coordinate)
{
}

BoundingBox::BoundingBox(Node const& node1, Node const& node2)
    : BoundingBox(node1.coordinate)
{
	expand(node2.coordinate);
}

// only for uncompressed paths. Otherwise, vertices might be skipped when
// shortcuts
BoundingBox::BoundingBox(Graph const& graph, Path const& path)
{
	for (EdgeID edge_id : path.getEdges()) {
		auto const& edge = graph.getEdge(edge_id);
		auto const& source = graph.getNode(edge.source);
		auto const& target = graph.getNode(edge.target);

		expand(BoundingBox(source, target));
	}
}

// returns a minimal bounding box containing all points of the graph
BoundingBox::BoundingBox(Graph const& graph)
{
	// can't calculate size if there are no points given.
	const Nodes& nodes = graph.getAllNodes();
	assert(nodes.size() > 0);

	// init boundaries to one point
	auto min_lat = nodes[0].coordinate.lat;
	auto min_lon = nodes[0].coordinate.lon;
	auto max_lat = min_lat;
	auto max_lon = min_lon;

	// loop over all points, update the boundary
	for (const auto node : nodes) {
		// update lon's
		min_lon = std::min(min_lon, node.coordinate.lon);
		max_lon = std::max(max_lon, node.coordinate.lon);

		// update lat's
		min_lat = std::min(min_lat, node.coordinate.lat);
		max_lat = std::max(max_lat, node.coordinate.lat);
	}
	auto min_coord = Coordinate(min_lat, min_lon);
	auto max_coord = Coordinate(max_lat, max_lon);

	expand(BoundingBox(min_coord, max_coord));
}

bool BoundingBox::operator==(BoundingBox const& box) const
{
	return min_coordinate == box.min_coordinate &&
	       max_coordinate == box.max_coordinate;
}

bool BoundingBox::isValid() const
{
	return min_coordinate.isValid() && max_coordinate.isValid();
}

void BoundingBox::expand(BoundingBox const& box)
{
	if (!box.isValid()) {
		return;
	}
	if (!isValid()) {
		*this = box;
		return;
	}

	min_coordinate.minMerge(box.min_coordinate);
	max_coordinate.maxMerge(box.max_coordinate);
}

void BoundingBox::intersect(BoundingBox const& box)
{
	min_coordinate.maxMerge(box.min_coordinate);
	max_coordinate.minMerge(box.max_coordinate);

	if (min_coordinate.lon > max_coordinate.lon ||
	    min_coordinate.lat > max_coordinate.lat) {

		min_coordinate.setInvalid();
		max_coordinate.setInvalid();
	}
}

Coordinate BoundingBox::calcCenter() const
{
	auto lat_center = (min_coordinate.lat + max_coordinate.lat) / 2;
	auto lon_center = (min_coordinate.lon + max_coordinate.lon) / 2;
	return {lat_center, lon_center};
}

bool BoundingBox::contains(BoundingBox const& box) const
{
	return min_coordinate.lat <= box.min_coordinate.lat &&
	       min_coordinate.lon <= box.min_coordinate.lon &&
	       max_coordinate.lat >= box.max_coordinate.lat &&
	       max_coordinate.lon >= box.max_coordinate.lon;
}

bool BoundingBox::contains(Coordinate const& coordinate) const
{
	return min_coordinate.lat <= coordinate.lat &&
	       min_coordinate.lon <= coordinate.lon &&
	       max_coordinate.lat >= coordinate.lat &&
	       max_coordinate.lon >= coordinate.lon;
}

bool BoundingBox::overlaps_with(BoundingBox const& box) const
{
	return box.min_coordinate.lon <= max_coordinate.lon &&
	       box.min_coordinate.lat <= max_coordinate.lat &&
	       box.max_coordinate.lon >= min_coordinate.lon &&
	       box.max_coordinate.lat >= min_coordinate.lat;
}

double BoundingBox::getHeight() const
{
	return max_coordinate.lat - min_coordinate.lat;
}

double BoundingBox::getWidth() const
{
	return max_coordinate.lon - min_coordinate.lon;
}

std::ostream& operator<<(std::ostream& stream, BoundingBox const& box)
{
	stream << "min coordinate: " << box.min_coordinate
	       << ", max coordinate: " << box.max_coordinate;

	return stream;
}

} // namespace pf
