#include "parser.h"
#include "time_tracker.h"

#include "defs.h"

#include <cassert>
#include <fstream>
#include <iostream>
#include <string>

namespace pf
{

Parser::Parser(Nodes& nodes, Edges& edges) : nodes(nodes), edges(edges)
{
}

void Parser::readHeader(std::ifstream& f)
{
	std::string line;
	bool is_header = true;

	while (is_header) {

		std::getline(f, line);

		if (line.empty()) {
			is_header = false;
		} else if (line[0] != '#') {
			throw Exception("Unexpected read while parsing the header in the "
			                "following line:\n" +
			                line);
		}
	}
}

void Parser::readNumberOfNodesAndEdges(std::ifstream& f)
{
	f >> number_of_nodes >> number_of_edges;
}

void Parser::readNumberOfNodesAndEdgesBin(std::ifstream& f)
{
	number_of_nodes = readBinary<int>(f);
	Print("number_of_nodes:");
	Print(number_of_nodes);

	number_of_edges = readBinary<int>(f);
	Print("number_of_edges:");
	Print(number_of_edges);
}

template <typename T>
T Parser::readBinary(std::ifstream& f)
{
	char* buff = new char[sizeof(T)];
	f.read(buff, sizeof(T));
	T* values = (T*)buff; // reinterpret as ints
	T value = values[0];
	SwapEnd(value); // endian conversion TODO: make it make safe + unit test
	//	Print(value);
	return value;
}

template <typename T>
void Parser::SwapEnd(T& var)
{
	char* varArray = reinterpret_cast<char*>(&var);
	for (long i = 0; i < static_cast<long>(sizeof(var) / 2); i++)
		std::swap(varArray[sizeof(var) - 1 - i], varArray[i]);
}

void Parser::printInt(char* charArray)
{
	const int CHAR_BIT = 8;
	int output[CHAR_BIT];

	for (int i = 0; i < 4; i++) {
		char c = charArray[i];

		for (int j = 0; j < CHAR_BIT; ++j) {
			output[j] = (c >> j) & 1;
		}

		for (int k = 0; k < CHAR_BIT; ++k) {
			std::cout << output[k];
		}
		std::cout << " ";
	}
	std::cout << std::endl;
}

void Parser::skipRead(std::ifstream& f)
{
	for (int i = 0; i < number_of_nodes + 1; i++) {
		readBinary<int>(f);
	}
	for (int i = 0; i < number_of_nodes + 1; i++) {
		readBinary<int>(f);
	}

	// read-out edge data
	for (int j = 0; j < number_of_edges; j++) {
		readBinary<int>(f);
	}
	for (int j = 0; j < number_of_edges; j++) {
		readBinary<int>(f);
	}
}

void Parser::readNodesBin(std::ifstream& f)
{
	Print("read Nodes");

	for (NodeID i = 0; i < number_of_nodes; ++i) {
		nodes.emplace_back();
		Node& node = nodes.back();

		node.id = i;
		double lon = readBinary<double>(f);
		double lat = readBinary<double>(f);
		Coordinate coordinate = Coordinate(lat, lon);
		node.coordinate = coordinate;
	}

	for (NodeID i = 0; i < number_of_nodes; ++i) {
		Node& node = nodes[i];
		node.level = readBinary<int>(f);
	}

	for (NodeID i = 0; i < number_of_nodes; ++i) {
		Node& node = nodes[i];
		node.osm_id = readBinary<long>(f);
	}

	Print("read Nodes done");
}

void Parser::readNodes(std::ifstream& f)
{
	for (NodeID i = 0; i < number_of_nodes; ++i) {
		nodes.emplace_back();
		auto& node = nodes.back();

		f >> node.id >> node.osm_id >> node.coordinate.lat >>
		    node.coordinate.lon >> node.elevation >> node.level;
	}
}

void Parser::readEdgesBin(std::ifstream& f)
{
	Print("read Edges");

	for (EdgeID i = 0; i < number_of_edges; ++i) {
		edges.emplace_back();
		Edge& edge = edges.back();
		edge.id = i;
		edge.source = readBinary<int>(f);
		edge.target = readBinary<int>(f);
		edge.length = readBinary<int>(f);
	}

	for (EdgeID i = 0; i < number_of_edges; ++i) {
		Edge& edge = edges[i];
		edge.child_edge1 = readBinary<int>(f);
		edge.child_edge2 = readBinary<int>(f);
	}

	Print("read Edges done");
}

void Parser::readEdges(std::ifstream& f)
{
	for (EdgeID i = 0; i < number_of_edges; ++i) {
		edges.emplace_back();
		auto& edge = edges.back();

		edge.id = i;
		f >> edge.source >> edge.target >> edge.length >> edge.type >>
		    edge.speed >> edge.child_edge1 >> edge.child_edge2;
	}
}

bool Parser::parse(const std::string& filename)
{
	assert(!already_parsed);
	already_parsed = true;

	if ((filename.compare(filename.size() - 5, 5, ".gbin") == 0)) {
		Print("| Parsing binary file..");
		return parseBin(filename);
	} else {
		Print("| Parsing non-binary file..");
		return parseNormal(filename);
	}
}

bool Parser::parseNormal(const std::string& filename)
{
	bool success = true;
	std::ifstream f(filename.c_str(), std::ios::in | std::ios::binary);

	TimeTracker time_tracker;

	if (f.is_open()) {
		try {
			time_tracker.start();
			Print("| | Reading Header..");
			readHeader(f);
			time_tracker.stop();
			Print("| | ↳ Took " << time_tracker.getLastMeasurment()
			                    << " seconds");

			time_tracker.start();
			Print("| | Reading #Edges and #Nodes..");
			readNumberOfNodesAndEdges(f);
			time_tracker.stop();
			Print("| | ↳ Took " << time_tracker.getLastMeasurment()
			                    << " seconds");

			time_tracker.start();
			Print("| | Reading Nodes..");
			readNodes(f);
			time_tracker.stop();
			Print("| | ↳ Took " << time_tracker.getLastMeasurment()
			                    << " seconds");

			time_tracker.start();
			Print("| | Reading Edges..");
			readEdges(f);
			time_tracker.stop();
			Print("| | ↳ Took " << time_tracker.getLastMeasurment()
			                    << " seconds");

		} catch (Exception e) {
			throw e;
			success = false;
		}

		f.close();
	} else {
		success = false;
	}

	return success;
}

bool Parser::parseBin(const std::string& filename)
{
	bool success = true;
	std::ifstream f(filename.c_str(), std::ios::in | std::ios::binary);

	TimeTracker time_tracker;

	if (f.is_open()) {
		try {

			time_tracker.start();
			Print("| | Reading #Edges and #Nodes..");
			readNumberOfNodesAndEdgesBin(f);
			time_tracker.stop();
			Print("| | ↳ Took " << time_tracker.getLastMeasurment()
			                    << " seconds");

			time_tracker.start();
			Print("| | Reading Nodes..");
			readNodesBin(f);
			time_tracker.stop();
			Print("| | ↳ Took " << time_tracker.getLastMeasurment()
			                    << " seconds");

			time_tracker.start();
			Print("| | Skip Read..");
			skipRead(f);
			time_tracker.stop();
			Print("| | ↳ Took " << time_tracker.getLastMeasurment()
			                    << " seconds");

			time_tracker.start();
			Print("| | Reading Edges..");
			readEdgesBin(f);
			time_tracker.stop();
			Print("| | ↳ Took " << time_tracker.getLastMeasurment()
			                    << " seconds");

		} catch (Exception e) {
			throw e;
			success = false;
		}

		f.close();
	} else {
		success = false;
	}

	return success;
}

} // namespace pf
