#pragma once

#include "basic_types.h"
#include "defs.h"
#include "graph.h"
#include "paths.h"

#include <array>
#include <queue>

namespace pf
{

namespace unit_tests
{
void testDijkstra();
} // namespace unit_tests

struct DijkstraResult {
	bool path_found;

	Path path;
	Length path_length;

	DijkstraResult()
	{
		clear();
	};

	void clear()
	{
		path_found = false;
		path.clear();
		path_length = c::NO_LENGTH;
	};
};

class Dijkstra
{
public:
	Dijkstra(Graph const& graph);

	void run(NodeID source, NodeID target);

	DijkstraResult const& getResult() const;
	DijkstraResult stealResult();

private:
	struct PQElement;
	typedef std::priority_queue<PQElement, std::vector<PQElement>,
	    std::greater<PQElement>>
	    PQ;

	Graph const& graph;

	EdgeIDs found_by;
	Lengths distances;
	NodeIDs reset_distances;

	DijkstraResult result;

	void reset();
	void relaxAllEdges(PQ& pq, PQElement const& top);
	void buildPath(NodeID source, NodeID target, PQ const& pq);
};

struct Dijkstra::PQElement {
	NodeID node;
	EdgeID found_by;
	Length distance;

	PQElement(NodeID node, EdgeID found_by, Length distance)
	    : node(node), found_by(found_by), distance(distance)
	{
	}

	bool operator>(PQElement const& other) const
	{
		return distance > other.distance;
	}
};

class CHDijkstra
{
public:
	CHDijkstra(Graph const& graph);

	void run(NodeID source, NodeID target);

	DijkstraResult const& getResult() const;
	DijkstraResult stealResult();

private:
	struct PQElement;
	typedef std::priority_queue<PQElement, std::vector<PQElement>,
	    std::greater<PQElement>>
	    PQ;

	Graph const& graph;

	class DirectionInfos
	{
	private:
		struct DirectionInfo {
			EdgeIDs found_by;
			Lengths distances;
			NodeIDs reset_distances;
		};

		std::array<DirectionInfo, 2> direction_info;
		using iterator = decltype(direction_info)::iterator;

	public:
		DirectionInfos() = default;

		DirectionInfo& operator[](Direction direction)
		{
			return direction_info[(std::size_t)direction];
		}

		iterator begin()
		{
			return direction_info.begin();
		}

		iterator end()
		{
			return direction_info.end();
		}
	};

	DirectionInfos dir;
	DijkstraResult result;

	void reset();
	void relaxAllEdges(PQ& pq, PQElement const& top);
	void buildPath(NodeID source, NodeID center_node, NodeID target);
};

struct CHDijkstra::PQElement {
	NodeID node;
	EdgeID found_by;
	Direction direction;
	Length distance;

	PQElement(
	    NodeID node, EdgeID found_by, Direction direction, Length distance)
	    : node(node)
	    , found_by(found_by)
	    , direction(direction)
	    , distance(distance)
	{
	}

	bool operator>(PQElement const& other) const
	{
		return distance > other.distance;
	}
};

} // namespace pf
