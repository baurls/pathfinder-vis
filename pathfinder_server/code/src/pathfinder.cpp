#include "pathfinder.h"
#include <algorithm>

namespace pf
{

Pathfinder::Pathfinder(PathfinderDS& ds, int nr_of_threads)
    : ds(ds)
    , edge_candidate_finder(ds, nr_of_threads)
    , refiner(ds, nr_of_threads)
{
	initTimeTrackers();
}

void Pathfinder::calculate_candidates(Query const& query)
{
	substepTimeTrackers.at(Substep::Get_top_nodes).start();
	NodeIDs top_nodes = ds.getTopNodes(query.bounding_box);
	substepTimeTrackers.at(Substep::Get_top_nodes).stop();

	substepTimeTrackers.at(Substep::Find_edge_candidates).start();
	edge_candidate_finder.findEdgeCandidates(
	    query, top_nodes, edge_candidates_total);
	substepTimeTrackers.at(Substep::Find_edge_candidates).stop();

	substepTimeTrackers.at(Substep::Refine_candidates).start();
	refiner.refineCandidates(edge_candidates_total, query.bounding_box);
	substepTimeTrackers.at(Substep::Refine_candidates).stop();
}

auto Pathfinder::run(Query const& query) -> PathIDs
{
	// the candidates are stored into 'edge_candidates_total'
	calculate_candidates(query);

#if defined(PATHS_ON_DISK)
	substepTimeTrackers.at(Substep::Sort_edges).start();
	// This greatly enhances the speed of getPathID by a factor of 50
	// because the path data for close edge ids are close to each other
	// in the path data vector. Therefore pages do not need to be switched
	// so often
	std::sort(edge_candidates_total.begin(), edge_candidates_total.end());
	substepTimeTrackers.at(Substep::Sort_edges).stop();
#endif

#if defined(PATHS_ON_DISK_STATS)
	stxxl_tracker.start();
#endif

	substepTimeTrackers.at(Substep::Get_path_ids).start();
	ds.getPathIDs(edge_candidates_total, query);
	substepTimeTrackers.at(Substep::Get_path_ids).stop();

#if defined(PATHS_ON_DISK_STATS)
	stxxl_tracker.stop();
#endif

	substepTimeTrackers.at(Substep::Remove_duplicate_path_ids).start();
	auto path_ids = ds.removeDuplicatePathIDs();
	substepTimeTrackers.at(Substep::Remove_duplicate_path_ids).stop();

	substepTimeTrackers.at(Substep::Reset_Collected).start();
	ds.resetCollected();
	substepTimeTrackers.at(Substep::Reset_Collected).stop();

	return path_ids;
}

auto Pathfinder::run_edgesearch_only(Query const& query) -> EdgeIDs
{
	// the candidates are stored into 'edge_candidates_total'
	calculate_candidates(query);

	return edge_candidates_total;
}

EdgeIDs Pathfinder::run_edgesearch_only_with_counts(
    Query const& query, Counter& edge_counts)
{
	// the candidates are stored into 'edge_candidates_total'
	calculate_candidates(query);

#if defined(PATHS_ON_DISK)
	substepTimeTrackers.at(Substep::Sort_edges).start();
	// This greatly enhances the speed of getPathID by a factor of 50
	// because the path data for close edge ids are close to each other
	// in the path data vector. Therefore pages do not need to be switched
	// so often
	std::sort(edge_candidates_total.begin(), edge_candidates_total.end());
	substepTimeTrackers.at(Substep::Sort_edges).stop();
#endif

	substepTimeTrackers.at(Substep::Get_path_ids).start();
	Counter edge_candidates_count =
	    ds.getPathIDsCount(edge_candidates_total, query);
	substepTimeTrackers.at(Substep::Get_path_ids).stop();

	// prepare return
	edge_counts = edge_candidates_count;
	return edge_candidates_total;
}

void Pathfinder::printTimes()
{
	for (const auto& entry : substepTimeTrackers) {
		Substep substep = entry.first;
		// get non-const reference
		TimeTracker& time_tracker = substepTimeTrackers.at(entry.first);
		Print("-------------------------");
		Print(getTextForSubstep(substep) + " times:");
		Print("-------------------------");
		time_tracker.printSummary();

		time_tracker.clear();
	}
}

std::map<Substep, TimeTracker> Pathfinder::getSubstepTimeTrackers()
{
	return substepTimeTrackers;
}

StxxlTracker* Pathfinder::getStxxlTracker()
{
	return &stxxl_tracker;
}

void Pathfinder::initTimeTrackers()
{
	for (Substep const& substep : allSubsteps) {
		substepTimeTrackers.emplace(substep, TimeTracker());
	}
}

} // namespace pf
