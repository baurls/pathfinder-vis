#include "path_exporter.h"

#include "defs.h"
#include "paths_ds.h"
#include "time_tracker.h"

#include <fstream>
#include <ios>
#include <sys/stat.h>
#include <sys/types.h>

namespace pf
{

PathExporter::PathExporter()
{
}

void PathExporter::exportPathsForPerformanceTest(
    const PathsDS& paths_ds, std::string graph_filename, uint distance) const
{
	std::string directory_name = graph_filename + "_resorted_" +
	                             std::to_string(paths_ds.getNrOfPaths()) +
	                             "_paths";
	mkdir(directory_name.c_str(), 0774);
	std::string file_name = directory_name + "/" + std::to_string(distance);
	exportPaths(paths_ds, file_name);
}

void PathExporter::exportPaths(
    const PathsDS& paths_ds, std::string file_name) const
{
	std::ofstream f(file_name);
	if (!f.is_open()) {
		throw Exception("The file " + file_name + " could not be opened.");
	}

	TimeTracker time_tracker;

	Print("| Export paths ...");
	time_tracker.start();
	writeHeader(paths_ds, f);
	writePaths(paths_ds, f);
	time_tracker.stop();
	Print("| ↳ Took " << time_tracker.getLastMeasurment() << " seconds");
}

void PathExporter::writeHeader(PathsDS const& paths_ds, std::ofstream& f) const
{
	f << paths_ds.getNrOfPaths() << "\n";
	f << "\n";
}

void PathExporter::writePaths(PathsDS const& paths_ds, std::ofstream& f) const
{
	for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); path_id++) {
		Path path = paths_ds.getPath(path_id);
		f << path.getStartTime();
		f << "\n";
		for (EdgeIndex edge_index = 0; edge_index < path.getEdges().size();
		     edge_index++) {
			f << path.getEdge(edge_index);
			f << "\n";
			f << path.getTimeIntervallForEdge(edge_index).max_time;
			f << "\n";
		}
		f << "\n";
	}
}

void PathExporter::exportPathsBin(
    const PathsDS& paths_ds, std::string file_name) const
{
	std::ofstream f;
	f.open(file_name, std::ios::out | std::ios::binary);

	if (!f.is_open()) {
		throw Exception("The file " + file_name + " could not be opened.");
	}

	TimeTracker time_tracker;

	Print("| Export paths ...");
	time_tracker.start();
	writeBinHeader(paths_ds, f);
	writeBinPaths(paths_ds, f);
	time_tracker.stop();
	Print("| ↳ Took " << time_tracker.getLastMeasurment() << " seconds");
}

void PathExporter::writeBinHeader(
    PathsDS const& paths_ds, std::ofstream& f) const
{
	long nof_paths = paths_ds.getNrOfPaths();
	f.write((char*)&nof_paths, sizeof(long));
}

void PathExporter::writeBinPaths(
    PathsDS const& paths_ds, std::ofstream& f) const
{
	for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); path_id++) {
		Path path = paths_ds.getPath(path_id);
		long path_size = path.getEdges().size();
		f.write((char*)&path_size, sizeof(long));
		TimeType start_time = path.getStartTime();
		f.write((char*)&start_time, sizeof(TimeType));

		for (EdgeIndex edge_index = 0; edge_index < path.getEdges().size();
		     edge_index++) {
			EdgeID edge_id = path.getEdge(edge_index);
			f.write((char*)&edge_id, sizeof(EdgeID));
			TimeType maxTime =
			    path.getTimeIntervallForEdge(edge_index).max_time;
			f.write((char*)&maxTime, sizeof(TimeType));
		}
	}
}

} // namespace pf
