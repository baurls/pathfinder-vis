#pragma once

#include "geometry.h"
#include "io/export_types.h"

#include <cstdint>
#include <string>
#include <vector>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{
	class GridHeatmapExporter
	{
		public:
			static void append_as_json(std::string* json, const CountGrid& count_grid, const float cutoff_value);
	};

} // namespace pf

