#pragma once

#include "basic_types.h"
#include "bounding_box.h"
#include "defs.h"
#include "graph.h"
#include "paths.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, Master Thesis 2021.    #
    #                                                 #
    #   Supervisor: T. Rupp, Prof. S. Funke           #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

// types
using Count = unsigned long int;

namespace unit_tests
{
void testCountGrid();
} // namespace unit_tests

//
// Member functions of CountGrid
//
//            j
//		_____________n
//     |              |
//     |              |
//   i |              |
//     |              |
//    m|______________|
//

class Grid
{
public:
	// constructor
	Grid(const GridIndex m, const GridIndex n, const BoundingBox& point_bb);
	Grid(const GridIndex m, const GridIndex n, const Coordinate& min_coordinate,
	    const Coordinate& max_coordinate);

	// creating an object using an existing instance
	Grid clone() const;
	Grid get_increased_level_copy() const;

	// attributes
	const GridIndex m;
	const GridIndex n;
	const GridIndex no_of_cells;
	const Coordinate p0;
	const Coordinate p1;
	const Coordinate center_offset;

	// navigating: cells
	void get_cell(const double x, const double y, GridIndex& i_out,
	    GridIndex& j_out) const;
	void get_cell(
	    const Coordinate point, GridIndex& i_out, GridIndex& j_out) const;
	GlobalGridIndex get_cell(const Coordinate point) const;

	// navigating: index conversion
	GlobalGridIndex index_for(const GridIndex i, const GridIndex j) const;
	void index_for(
	    const GlobalGridIndex index, GridIndex& i, GridIndex& j) const;

	// navigating: cell center
	Coordinate get_cell_center(const GlobalGridIndex& k) const;
	Coordinate get_cell_center(const GridIndex& i, const GridIndex& j) const;

	// get corners of a cell
	inline Coordinate get_upper_left_corner(
	    const GridIndex i, const GridIndex j) const
	{
		return Coordinate(p0.lat + i * h_y, p0.lon + j * h_x);
	};
	inline Coordinate get_upper_right_corner(
	    const GridIndex i, const GridIndex j) const
	{
		return Coordinate(p0.lat + i * h_y, p0.lon + (j + 1) * h_x);
	};
	inline Coordinate get_lower_left_corner(
	    const GridIndex i, const GridIndex j) const
	{
		return Coordinate(p0.lat + (i + 1) * h_y, p0.lon + j * h_x);
	};
	inline Coordinate get_lower_right_corner(
	    const GridIndex i, const GridIndex j) const
	{
		return Coordinate(p0.lat + (i + 1) * h_y, p0.lon + (j + 1) * h_x);
	};

protected:
	const double span_x;
	const double span_y;
	const double h_x;
	const double h_y;

private:
	Grid();
};

class CountGrid : public Grid
{
public:
	// constructor
	CountGrid(const GridIndex m, const GridIndex n, const BoundingBox& point_bb)
	    : Grid(m, n, point_bb)
	{
		count_vector = std::vector<Count>(m * n, 0);
	}
	CountGrid(const Grid grid) : Grid(grid.m, grid.n, grid.p0, grid.p1)
	{
		count_vector = std::vector<Count>(m * n, 0);
	}

	// methods
	void add_point(const Coordinate& point);
	void add_point(const double x, const double y);
	inline Count count_at(const GridIndex i, const GridIndex j) const;
	void process_covering(const CellCovering& covering);
	std::vector<Count> count_vector;

private:
	CountGrid();

	inline Count increment_count(const GridIndex i, const GridIndex j)
	{
		return ++count_vector[index_for(i, j)];
	}
};

} // namespace pf
