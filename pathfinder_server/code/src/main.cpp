
#include "defs.h"
#include "evaluation.h"
#include "exporter.h"
#include "flags.h"
#include "graph.h"
#include "grid.h"
#include "grid_hierarchy.h"
#include "grid_stack.h"
#include "heatmap_builder.h"
#include "intersection_checker.h"
#include "level_extractor.h"
#include "naive_ch_pathfinder.h"
#include "naive_pathfinder.h"
#include "options.h"
#include "pairwise_sort.h"
#include "path_exporter.h"
#include "path_generator.h"
#include "path_importer.h"
#include "path_parser.h"
#include "pathfinder.h"
#include "pathfinder_ds.h"
#include "paths_to_ds.h"
#include "random.h"
#include "stxxl_init.h"
#include "time_tracker.h"

#include <fstream>
#include <sstream>
#include <string>

#include <pistache/description.h>
#include <pistache/endpoint.h>
#include <pistache/http.h>
#include <pistache/http_header.h>

#include <pistache/serializer/rapidjson.h>

#include <rapidjson/document.h>
#include <rapidjson/rapidjson.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

using namespace std;

pf::Graph graph;
pf::PathsDS paths_ds;
pf::PathsDS raw_paths_ds;
pf::GridStack grid_stack;

pf::LevelExtractor level_extractor;

using namespace Pistache;

namespace Generic
{

void handleReady(const Rest::Request&, Http::ResponseWriter response)
{
	response.send(Http::Code::Ok, "1");
}
} // namespace Generic

class PathFinderService
{

public:
	// Initialization of Pathfinder
	pf::PathfinderDS p_ds;
	pf::Pathfinder p;
	TimeTracker time_tracker;
	pf::BoundingBox query_box;
	pf::BoundingBox view_box;
	pf::Options query_options;

	// Heatmap
	pf::HeatmapBuilder heatmap_builder = pf::HeatmapBuilder(graph, paths_ds);
	pf::RelCounterVector rel_count_counter =
	    heatmap_builder.get_truncated_rel_nodes_counter(8);

	// Cache variables
	long no_of_found_paths = 0;
	pf::BoundingBox cached_query_box;
	pf::Options cached_query_options;
	pf::PathIDs cached_results;
	pf::EdgeIDs cached_edgeset;
	pf::Counter cached_edge_weights;

	// for batched, inorder update
	PathFinderService(
	    Address addr, uint nr_of_pathfinder_threads, uint nr_of_service_threads)
	    : httpEndpoint(std::make_shared<Http::Endpoint>(addr))
	    , desc("Pathfinder API", "0.1")
	    , nr_of_pathfinder_threads(nr_of_pathfinder_threads)
	    , nr_of_service_threads(nr_of_service_threads)
	    , p_ds(pf::PathfinderDS(
	          graph, nr_of_pathfinder_threads, std::move(paths_ds)))
	    , p(pf::Pathfinder(p_ds, nr_of_pathfinder_threads))
	{
	}

	void init()
	{
		auto opts = Http::Endpoint::options().threads(nr_of_service_threads);
		httpEndpoint->init(opts);
		createDescription();
	}

	void start()
	{
		router.initFromDescription(desc);

		Rest::Swagger swagger(desc);
		swagger.uiPath("/doc")
		    .uiDirectory("/home/octal/code/web/swagger-ui-2.1.4/dist")
		    .apiPath("/PathFinder-api.json")
		    .serializer(&Rest::Serializer::rapidJson)
		    .install(router);

		httpEndpoint->setHandler(router.handler());
		httpEndpoint->serve();
	}

	void stop()
	{

		httpEndpoint->setHandler(router.handler());
		httpEndpoint->shutdown();
	}

private:
	std::shared_ptr<Http::Endpoint> httpEndpoint;
	Rest::Description desc;
	Rest::Router router;
	const size_t nr_of_pathfinder_threads;
	const size_t nr_of_service_threads;

	// Creates the server description and maps the paths
	void createDescription()
	{

		desc.info().license(
		    "Apache", "http://www.apache.org/licenses/LICENSE-2.0");

		auto backendErrorResponse =
		    desc.response(Http::Code::Internal_Server_Error,
		        "An error occured with the backend");

		desc.schemes(Rest::Scheme::Http)
		    .basePath("/pathfinder")
		    .produces(MIME(Application, Json))
		    .consumes(MIME(Application, Json));

		desc.route(desc.get("/ready"))
		    .bind(&Generic::handleReady)
		    .response(Http::Code::Ok, "Response to the /ready call")
		    .hide();

		auto pathfinderPath = desc.path("/pathfinder");

		// handler of normal pathfinder requests
		pathfinderPath.route(desc.options("/calculate"), "Json-Response")
		    .bind(&PathFinderService::optionsRecieved, this);

		pathfinderPath.route(desc.post("/calculate"), "Json-Response")
		    .bind(&PathFinderService::calculate, this)
		    .produces(MIME(Application, Json))
		    .consumes(MIME(Application, Json));

		// heatmap handler
		pathfinderPath.route(desc.options("/heatmap"), "Json-Response")
		    .bind(&PathFinderService::optionsRecieved, this);

		pathfinderPath.route(desc.post("/heatmap"), "Json-Response")
		    .bind(&PathFinderService::heatmap_update, this)
		    .produces(MIME(Application, Json))
		    .consumes(MIME(Application, Json));

		// batch handler
		pathfinderPath.route(desc.options("/batch_update"), "Json-Response")
		    .bind(&PathFinderService::optionsRecieved, this);

		pathfinderPath.route(desc.post("/batch_update"), "Json-Response")
		    .bind(&PathFinderService::batch_update, this)
		    .produces(MIME(Application, Json))
		    .consumes(MIME(Application, Json));

		// evaluation handler
		pathfinderPath.route(desc.options("/evaluation"), "Json-Response")
		    .bind(&PathFinderService::optionsRecieved, this);

		pathfinderPath.route(desc.post("/evaluation"), "Json-Response")
		    .bind(&PathFinderService::evaluate, this)
		    .produces(MIME(Application, Json))
		    .consumes(MIME(Application, Json));
	}

	void optionsRecieved(const Rest::Request&, Http::ResponseWriter response)
	{
		// Allow all methods for access control, send the response
		response.headers().add<Http::Header::AccessControlAllowOrigin>("*");
		response.headers().add<Http::Header::AccessControlAllowHeaders>(
		    "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		response.headers().add<Http::Header::AccessControlAllowMethods>(
		    "GET, PUT, POST, DELETE, PATCH, OPTIONS");
		response.send(Http::Code::Ok);
	}

	void serveIndex(const Rest::Request& request, Http::ResponseWriter response)
	{
		std::ifstream ifs("./webfiles/index.html");
		std::string content((std::istreambuf_iterator<char>(ifs)),
		    (std::istreambuf_iterator<char>()));
		response.headers().add<Http::Header::ContentType>(
		    MIME(Application, Html));
		request.method();
		response.send(Http::Code::Ok, content);
	}

	void serveAbout(const Rest::Request& request, Http::ResponseWriter response)
	{
		response.headers().add<Http::Header::ContentType>(
		    MIME(Application, Html));
		request.method();
		Http::serveFile(response, "./webfiles/about.html");
		response.send(Http::Code::Ok);
	}

	void serveDocs(const Rest::Request& request, Http::ResponseWriter response)
	{
		response.headers().add<Http::Header::ContentType>(
		    MIME(Application, Html));
		request.method();
		Http::serveFile(response, "./webfiles/docs.html");
		response.send(Http::Code::Ok);
	}

	void add_response_headers(Http::ResponseWriter& response)
	{
		// Allow all methods for access control
		response.headers().add<Http::Header::AccessControlAllowOrigin>("*");
		response.headers().add<Http::Header::AccessControlAllowHeaders>(
		    "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		response.headers().add<Http::Header::AccessControlAllowMethods>(
		    "GET, PUT, POST, DELETE, PATCH, OPTIONS");
	}

	rapidjson::Document parse_request_body(const string& body)
	{
		// Save the request information globally
		const char* c = &(body[0]);

		rapidjson::Document d;
		// Parse the request content, then check if it is a JSON
		d.Parse(c);
		if (!d.IsObject()) {
			throw std::invalid_argument(
			    "The request is not a valid JSON file.");
		}
		return d;
	}

	void batch_update(
	    const Rest::Request& request, Http::ResponseWriter response)
	{
		try {
			_batch_update(request, response);
		} catch (const invalid_argument& e) {
			response.send(Http::Code::Bad_Request, e.what());
		}
	}

	void _batch_update(
	    const Rest::Request& request, Http::ResponseWriter& response)
	{
		add_response_headers(response);

		// parse json request
		auto d = parse_request_body(request.body());

		// forward to correct baching method:
		if (!d.HasMember("batch_type")) {
			throw std::invalid_argument(
			    "The required field 'batch_type' was not set.");
		}

		std::string batch_type = d["batch_type"].GetString();

		if (batch_type == "PlainInorder") {
			_batch_update_plain_inorder(d, response);
		}
		if (batch_type == "PlainLevelorder") {
			_batch_update_plain_levelorder(d, response);
		}
		if (batch_type == "GetRootNodes") {
			_batch_update_get_root_edges(d, response);
		}
	}

	void _batch_update_plain_inorder(
	    rapidjson::Document& d, Http::ResponseWriter& response)
	{
		if (!d.HasMember("next_requested_index_map")) {
			throw std::invalid_argument(
			    "The required field 'next_requested_index_map' was not set.");
		}
		if (!d.HasMember("batch_size")) {
			throw std::invalid_argument(
			    "The required field 'batch_size' was not set.");
		}
		rapidjson::Value& request_index_map =
		    *get_attribute(d, "next_requested_index_map");

		int batch_size = d["batch_size"].GetUint64();

		std::string result = "{\"update\" : {";
		auto is_first_round = true;

		for (auto& item : request_index_map.GetObject()) {
			size_t path_id = std::stoi(item.name.GetString());
			size_t index = item.value.GetInt64();

			auto current_path = raw_paths_ds.getPath(path_id);
			auto current_path_len = current_path.getNoOfRootEdges() + 1;

			// add ',' if 2nd round or later
			if (is_first_round) {
				is_first_round = false;
				result += "\"" + to_string(path_id) + "\":[";
			} else {
				result += ",\"" + to_string(path_id) + "\":[";
			}

			auto is_first_node = true;
			auto lim = min(index + batch_size, current_path_len);
			size_t i = index;
			for (; i < lim; i++) {
				auto node_id = current_path.getNode(graph, i);
				auto node = graph.getNode(node_id);
				if (is_first_node) {
					is_first_node = false;
					result += _coordinates_to_string(node.coordinate);
				} else {
					result += "," + _coordinates_to_string(node.coordinate);
				}
			}
			result += "]";
		}

		result += "}}";
		response.send(Http::Code::Ok, result);
	}

	void _batch_update_plain_levelorder(
	    rapidjson::Document& d, Http::ResponseWriter& response)
	{
		if (!d.HasMember("next_offset_and_skip_index")) {
			throw std::invalid_argument(
			    "The required field 'next_offset_and_skip_index' was not set.");
		}
		if (!d.HasMember("batch_size")) {
			throw std::invalid_argument(
			    "The required field 'batch_size' was not set.");
		}
		rapidjson::Value& request_offset_and_skip_index_map =
		    *get_attribute(d, "next_offset_and_skip_index");

		// int batch_size = d["batch_size"].GetUint64();

		std::string result = "{\"update\" : {";
		auto is_first_round = true;

		for (auto& item : request_offset_and_skip_index_map.GetObject()) {
			size_t path_id = std::stoi(item.name.GetString());
			size_t start_index = item.value[0].GetInt64();
			size_t skipping = item.value[1].GetInt64();

			auto current_path = raw_paths_ds.getPath(path_id);
			auto current_path_len = current_path.getNoOfRootEdges() + 1;

			// add ',' if 2nd round or later
			if (is_first_round) {
				is_first_round = false;
				result += "\"" + to_string(path_id) + "\":[[";
			} else {
				result += ",\"" + to_string(path_id) + "\":[[";
			}

			auto is_first_node = true;
			// auto lim = min(start_index + skipping*batch_size,
			// current_path_len); batch size ignored for now
			auto lim = current_path_len;

			size_t i = start_index;
			for (; i < lim; i += skipping) {
				auto node_id = current_path.getNode(graph, i);
				auto node = graph.getNode(node_id);
				if (is_first_node) {
					is_first_node = false;
					result += _coordinates_to_string(node.coordinate);
				} else {
					result += "," + _coordinates_to_string(node.coordinate);
				}
			}
			result += "],";
			result += to_string(i);
			result += "]";
		}

		result += "}}";
		response.send(Http::Code::Ok, result);
	}

	inline std::string _coordinates_to_string(pf::Coordinate& coords) const
	{
		return "[" + to_string(coords.lat) + "," + to_string(coords.lon) + "]";
	}

	void _batch_update_get_root_edges(
	    rapidjson::Document& d, Http::ResponseWriter& response)
	{
		if (!d.HasMember("paths")) {
			throw std::invalid_argument(
			    "The required field 'paths' was not set.");
		}
		rapidjson::Value& requested_paths = d["paths"];

		std::string result = "{\"edgemap\" : {";
		auto is_first_round = true;

		for (unsigned int i = 0; i < requested_paths.Size(); i++) {
			size_t path_id = requested_paths[i].GetInt64();

			// add ',' if 2nd round or later
			if (is_first_round) {
				is_first_round = false;
				result += "\"" + to_string(path_id) + "\":[";
			} else {
				result += ",\"" + to_string(path_id) + "\":[";
			}

			auto path = p_ds.getPathsDS().getPath(path_id);

			// copy all edge-ids
			auto path_edges = path.getEdges();
			auto is_first_node = true;
			result += "[";
			for (auto edge_id : path_edges) {
				if (is_first_node) {
					is_first_node = false;
					result += to_string(edge_id);
				} else {
					result += "," + to_string(edge_id);
				}
			}
			result += "],[";

			// copy the corresponding nodes, too:
			auto counter = 0;
			for (auto edge_id : path_edges) {
				counter += 1;
				if (counter == 1) {
					continue;
				} else if (counter > 2) {
					result += ",";
				}
				auto edge = graph.getEdge(edge_id);
				auto node = graph.getNode(edge.source);
				result += _coordinates_to_string(node.coordinate);
			}
			result += "]]";
		}

		result += "}}";
		response.send(Http::Code::Ok, result);
	}

	void heatmap_update(
	    const Rest::Request& request, Http::ResponseWriter response)
	{
		try {
			_heatmap_update(request, response);
		} catch (const invalid_argument& e) {
			response.send(Http::Code::Bad_Request, e.what());
		}
	}

	string create_result_string(pf::BoundingBox bb)
	{
		//    ____________max
		//    |           |
		//    |        x  |
		//    |           |
		//    |___________|
		//	 min

		auto n = graph.getAllNodes().size();
		vector<bool> matching_vec(n, false);

		string return_str = "{\"heatmap\": [";

		//[22.06, -79.42, 0.5], [22.07, -79.42, 0.8], [22.08, -79.42, 1.0] ]}";

		size_t i = 0;
		for (; i < n; i++) {
			auto coordinate = graph.getNode(i).coordinate;
			if (bb.contains(coordinate) && rel_count_counter[i] > 0.0f) {
				auto lat = coordinate.lat;
				auto lon = coordinate.lon;
				return_str = return_str + "[" + to_string(lat) + "," +
				             to_string(lon) + "," +
				             to_string(rel_count_counter[i]) + "]";
				break;
			}
		}
		for (; i < n; i++) {
			auto coordinate = graph.getNode(i).coordinate;
			if (bb.contains(coordinate) && rel_count_counter[i] > 0.0f) {
				auto lat = coordinate.lat;
				auto lon = coordinate.lon;
				return_str = return_str + ",[" + to_string(lat) + "," +
				             to_string(lon) + "," +
				             to_string(rel_count_counter[i]) + "]";
			}
		}
		return_str = return_str + "]}";
		return return_str;
	}

	void _heatmap_update(
	    const Rest::Request& request, Http::ResponseWriter& response)
	{
		add_response_headers(response);

		// parse json request
		auto d = parse_request_body(request.body());

		// load coordinates and zoom
		rapidjson::Value& bounds = *get_attribute(d, "bounds");
		rapidjson::Value& coords = *get_attribute(bounds, "coords");
		rapidjson::Value& zoom_val = *get_attribute(bounds, "zoom");
		int zoom = zoom_val.GetUint64();

		// extract bounds and store as bounding box
		pf::Coordinate min =
		    pf::Coordinate(coords[0][0].GetDouble(), coords[0][1].GetDouble());
		pf::Coordinate max =
		    pf::Coordinate(coords[1][0].GetDouble(), coords[1][1].GetDouble());
		pf::BoundingBox window_view_box = pf::BoundingBox(min, max);

		cout << "zoom " << zoom << " box: " << window_view_box << endl;

		// return result json
		string result = create_result_string(window_view_box);
		response.send(Http::Code::Ok, result);
	}

	rapidjson::Value* get_attribute(rapidjson::Value& v, string attribute_name)
	{
		if (!v.HasMember(attribute_name.c_str())) {
			throw std::invalid_argument(
			    "Can't extract the value '" + attribute_name + "'.");
		}
		rapidjson::Value* value = &v[attribute_name.c_str()];
		return value;
	}

	rapidjson::Value* get_attribute(
	    rapidjson::Document& d, string attribute_name)
	{
		if (!d.HasMember(attribute_name.c_str())) {
			throw std::invalid_argument(
			    "The request does not have a '" + attribute_name + "' field.");
		}
		rapidjson::Value* value = &d[attribute_name.c_str()];

		// Check if the shape is not empty and valid
		if (value->ObjectEmpty()) {
			throw std::invalid_argument(
			    "The request " + attribute_name + " object is empty.");
		}
		return value;
	}

	void evaluate(const Rest::Request& request, Http::ResponseWriter response)
	{
		try {
			_evaluate(request, response);
		} catch (const invalid_argument& e) {
			response.send(Http::Code::Bad_Request, e.what());
		}
	}

	void _evaluate(const Rest::Request& request, Http::ResponseWriter& response)
	{
		Print("----------- Request retrieved ------------");
		TimeTracker time_tracker;
		time_tracker.start();

		add_response_headers(response);
		// lukas
		pf::Evaluator frontend_evaluator(p_ds, p);

		auto d = parse_request_body(request.body());

		// load coordinates and zoom
		rapidjson::Value& raw_expID = d["experimentID"];
		int experimentID = raw_expID.GetUint64();

		string result = frontend_evaluator.evaluate_experiment(experimentID);

		time_tracker.stop();
		Print("Generating evaluation data finished. Took "
		      << time_tracker.getLastMeasurment() << " seconds");

		/* Send response */
		response.send(
		    Http::Code::Ok, "Generation was successfull. Total file sizes: " +
		                        to_string(result.size()));
		Print("----------- Response send ------------");
	}

	void calculate(const Rest::Request& request, Http::ResponseWriter response)
	{
		try {
			_calculate(request, response);
		} catch (const invalid_argument& e) {
			response.send(Http::Code::Bad_Request, e.what());
		}
	}

	void _calculate(
	    const Rest::Request& request, Http::ResponseWriter& response)
	{
		Print("----------- Request retrieved ------------");
		TimeTracker time_tracker;

		add_response_headers(response);

		_parse_options_from_query(request);

		/* Pathfinder calculation */
		pf::PathIDs found_paths;
		pf::EdgeIDs found_edgeset;
		pf::Counter edge_weights;

		/* Check for cached values */
		// If the request information didn't change, no new calculation is
		// needed.

		Print("Checking for cached results.");
		if (compareCachedRequest(query_box, query_options)) {
			// Same shape, time interval and options set, use cached
			// intersection_paths.
			Print("| Cached results for this configuration found.");
			found_paths = cached_results;
			found_edgeset = cached_edgeset;
			edge_weights = cached_edge_weights;

		} else {
			Print("| No cached results found.");

			time_tracker.start();
			Print("| Starting pathfinder calculation...");

			// Prepare the query
			pf::Query query(query_box);
			query.time_intervall = query_options.getInterval();
			query.time_slots = query_options.getSlots();

			// Run the pathfinder
			if (query_options.getExportType() ==
			    pf::ExportType::EDGE_BASED_PATHFINDER) {
				if (query_options.getReturnWeightedEdges()) {
					Print(
					    "| | Run 'edgeset only'-PATHFINDER (with weights)...");
					found_edgeset =
					    p.run_edgesearch_only_with_counts(query, edge_weights);
					cached_edge_weights = edge_weights;
				} else {
					Print("| | Run 'edgeset only'-PATHFINDER (without "
					      "weights)...");
					// run query to get the edges
					found_edgeset = p.run_edgesearch_only(query);
				}

				// Cache the results
				cached_edgeset = found_edgeset;

			} else if (query_options.getExportType() ==
			           pf::ExportType::GRAPH_SECTION) {
				Print("| | Skipt PATHFINDER requesting (graph section)...");
			} else {
				Print("| | Run normal PATHFINDER...");
				// run query to geh the paths
				found_paths = p.run(query);

				// Cache the results
				cached_results = found_paths;
			}

			// Cache the results
			cached_query_box = query_box;
			cached_query_options = query_options;

			time_tracker.stop();
			Print("| Calculation finished. Took "
			      << time_tracker.getLastMeasurment() << " seconds");
		}

		no_of_found_paths = found_paths.size();
		Print("Retrieved " + std::to_string(no_of_found_paths) + " paths \n");

		/* Result actions */
		pf::ExportData export_data;

		if (query_options.getExportType() ==
		        pf::ExportType::BATCHED_PLAIN_INORDER ||
		    query_options.getExportType() == pf::ExportType::BATCHED_EDGEWISE ||
		    query_options.getExportType() ==
		        pf::ExportType::BATCHED_PLAIN_LEVELORDER) {
			Print("Skipping unpacking paths: Batching");
			export_data.paths_ds = raw_paths_ds;
		} else if (query_options.getExportType() ==
		           pf::ExportType::EDGE_BASED_PATHFINDER) {
			Print("Skip paths unpacking (using Pathfinder-Edge-only mode)");
			// don't copy and don't unpack paths
		} else if (query_options.getExportType() ==
		               pf::ExportType::GRAPH_SECTION or
		           query_options.getExportType() ==
		               pf::ExportType::GRAPH_SECTION_TOPOLOGY) {
			// don't do anything: Exporting takes place only on graph-structure
			Print("No PF-compressed trajectory access required - only "
			      "returning graph data");
		} else if (query_options.getPrunning()) {
			Print("Skip paths unpacking (since prunning is activated)");
			// simply copy all compressed paths
			export_data.found_paths = found_paths;
		} else {
			// Unpack the intersecting paths (acoording to query_options) into
			// the paths_ds
			Print("Unpacking intersecting paths into new paths_ds...");
			pf::UnpackingUtil::unpack_and_copy_paths(export_data.paths_ds,
			    found_paths, query_options, p_ds, level_extractor);
			Print("Unpacking finished.");
		}

		/* Export preparation */
		export_data.boxes = {query_box, view_box};
		export_data.intersection_paths = no_of_found_paths;
		export_data.total_paths = p_ds.getPathsDS().getNrOfPaths();
		export_data.found_edges = found_edgeset;
		export_data.edge_weights = edge_weights;

		time_tracker.start();
		pf::Exporter exporter(p_ds.getGraph());

		string result;
		if (query_options.getExportType() == pf::ExportType::TRAJECORIES) {
			Print("Export the results as normal segments...");
			result = exporter.exportTrajectories(export_data, query_options);
		} else if (query_options.getExportType() ==
		           pf::ExportType::SEGMENT_GRAPH) {
			Print("Exporting the results as SegmentGraph..");
			result = exporter.exportSegmentGraph(export_data);
		} else if (query_options.getExportType() == pf::ExportType::HEATMAP) {
			Print("Exporting the results as Heatmap..");
			result = exporter.exportHeatmap(export_data);
		} else if (query_options.getExportType() ==
		           pf::ExportType::BATCHED_PLAIN_INORDER) {
			Print("Exporting the results in batches: Plain-Inorder");
			result = exporter.export_initial_batch(
			    export_data, found_paths, pf::BatchType::PLAIN_INORDER);
		} else if (query_options.getExportType() ==
		           pf::ExportType::BATCHED_PLAIN_LEVELORDER) {
			Print("Exporting the results in batches: Plain-Levelorder");
			result = exporter.export_initial_batch(
			    export_data, found_paths, pf::BatchType::PLAIN_LEVELORDER);
		} else if (query_options.getExportType() ==
		           pf::ExportType::BATCHED_EDGEWISE) {
			Print("Exporting the results in batches: Edge-Wise");
			result = exporter.export_initial_batch(
			    export_data, found_paths, pf::BatchType::EDGE_WISE);
		} else if (query_options.getExportType() == pf::ExportType::PRUNNING) {
			Print("Exporting the results in prunned view");
			result = exporter.export_prunned_segments(
			    export_data, query_options, p_ds, grid_stack, level_extractor);
		} else if (query_options.getExportType() ==
		           pf::ExportType::EDGE_BASED_PATHFINDER) {
			bool use_bidirectional_edges =
			    query_options.get_ebpf_use_bidirectional_edges();
			bool local_search = query_options.get_ebpf_perform_local_search();
			bool use_node_count = query_options.get_ebpf_use_nodecount();

			if (query_options.getReturnWeightedEdges()) {
				Print("Exporting the results as weighted edgeset");
				result = exporter.export_weighted_edgeset(export_data,
				    query_options, p_ds, level_extractor, local_search,
				    use_bidirectional_edges, use_node_count);
			} else {
				Print("Exporting the results as edgeset");
				result =
				    exporter.export_edgeset(export_data, query_options, p_ds,
				        level_extractor, local_search, use_bidirectional_edges);
			}
		} else if (query_options.getExportType() ==
		           pf::ExportType::GRAPH_SECTION) {
			Print("Exporting graph-view section only (no trajectories)");
			result = exporter.export_full_graph_view(
			    export_data, query_options, p_ds, level_extractor);
		} else if (query_options.getExportType() ==
		           pf::ExportType::GRAPH_SECTION_TOPOLOGY) {
			Print(
			    "Exporting graph-view section topology only (no trajectories)");
			result = exporter.export_graph_topology(
			    export_data, query_options, p_ds, level_extractor);
		} else {
			Print("ERROR: Unknown Export Type: ");
		}

		time_tracker.stop();
		Print("Exporting finished. Took " << time_tracker.getLastMeasurment()
		                                  << " seconds");

		/* Send response */

		response.send(Http::Code::Ok, result);
		Print("----------- Response send ------------");
	}

	void _parse_options_from_query(const Rest::Request& request)
	{
		rapidjson::Document d = parse_request_body(request.body());

		/* Retrieve the request shape information */
		rapidjson::Value& shape = *get_attribute(d, "shape");

		// Retrieve the shape type and coordinates
		std::string type = shape["type"].GetString();
		rapidjson::Value& coords = shape["coords"];

		if (type == "rectangle") {
			// Extract the min and max node
			pf::Coordinate min = pf::Coordinate(
			    coords[0][0].GetDouble(), coords[0][1].GetDouble());
			pf::Coordinate max = pf::Coordinate(
			    coords[1][0].GetDouble(), coords[1][1].GetDouble());
			query_box = pf::BoundingBox(min, max);
		} else {
			// Throw type error
			throw std::invalid_argument("Unsupported type '" + type + "'.");
		}

		/* Retrieve the request map bounds information */

		// Check if the map bounds were set
		if (d.HasMember("bounds")) {
			rapidjson::Value& bounds = d["bounds"];

			// Check if options are empty, if not, parse them
			if (!bounds.ObjectEmpty()) {
				if (bounds.HasMember("coords")) {
					// TODO save map bound coords
					rapidjson::Value& coords = bounds["coords"];
					pf::Coordinate view_min = pf::Coordinate(
					    coords[0][0].GetDouble(), coords[0][1].GetDouble());
					pf::Coordinate view_max = pf::Coordinate(
					    coords[1][0].GetDouble(), coords[1][1].GetDouble());
					view_box = pf::BoundingBox(view_min, view_max);
				}
				if (bounds.HasMember("zoom")) {
					query_options.setZoom(bounds["zoom"].GetUint64());
				}
			}
		}

		/* Retrieve the time interval information */

		// Check if time interval were set
		if (d.HasMember("timeInterval")) {
			rapidjson::Value& timeInterval = d["timeInterval"];

			// Check if time intervals are empty, if not, parse them
			if (!timeInterval.ObjectEmpty()) {

				// Parse the start time, end time and weekday selection (time
				// slots)
				pf::TimeType start_time = timeInterval["start"].GetUint64();
				pf::TimeType end_time = timeInterval["end"].GetUint64();

				// Save the interval selection
				query_options.setParseIntervals(true);
				query_options.setInterval(
				    pf::TimeIntervall(start_time, end_time));
			} else {
				query_options.setParseIntervals(false);
				query_options.setInterval(pf::c::FULL_INTERVALL);
			}
		}

		if (d.HasMember("timeSlots")) {
			rapidjson::Value& timeSlots = d["timeSlots"];

			if (timeSlots.IsString() && !(timeSlots == "")) {
				std::string bit_vector_string = d["timeSlots"].GetString();

				// Bitshift for the days, so Thursday is weekstart. (Unix time
				// starts at thursday)
				std::string days = bit_vector_string.substr(3, 6).append(
				    bit_vector_string.substr(0, 3));

				// Save the interval selection
				query_options.setParseIntervals(true);
				query_options.setSlots(pf::TimeSlots(days));

			} else {
				query_options.setParseIntervals(false);
				query_options.setSlots(pf::TimeSlots(true));
			}
		}

		/* Retrieve request options */

		// Check if any options were set
		if (d.HasMember("options")) {
			rapidjson::Value& options = d["options"];

			// Check if options are empty, if not, parse them
			if (!options.ObjectEmpty()) {
				if (options.HasMember("packing")) {
					query_options.setPacking(options["packing"].GetBool());
				}
				if (options.HasMember("level")) {
					query_options.setLevel(options["level"].GetInt64());
				}
				if (options.HasMember("prunning")) {
					query_options.setPrunning(options["prunning"].GetBool());
				}
				if (options.HasMember("cutoff_level")) {
					query_options.setCutoffLevel(
					    options["cutoff_level"].GetInt64());
				}
				if (options.HasMember("exportType")) {
					query_options.setExportType(
					    options["exportType"].GetString());
				}
				if (options.HasMember("weightedEdges")) {
					query_options.setReturnWeightedEdges(
					    options["weightedEdges"].GetBool());
				}
				if (options.HasMember("weightedEdgesBinning")) {
					query_options.setWeightedEdgeBinning(
					    options["weightedEdgesBinning"].GetInt64());
				}
				if (options.HasMember("min_intersection_filter")) {
					query_options.setMinIntersectionCount(
					    options["min_intersection_filter"].GetInt64());
				}
				if (options.HasMember("useBidirectionalEdges")) {
					query_options.set_ebpf_use_bidirectional_edges(
					    options["useBidirectionalEdges"].GetBool());
				}
				if (options.HasMember("performLocalSearch")) {
					query_options.set_ebpf_perform_local_search(
					    options["performLocalSearch"].GetBool());
				}
				if (options.HasMember("useNodeCounter")) {
					query_options.set_ebpf_use_nodecount(
					    options["useNodeCounter"].GetBool());
				}
				if (options.HasMember("graph_level")) {
					query_options.setGraphLevel(
					    options["graph_level"].GetInt64());
				}
				if (options.HasMember("showAuxLines")) {
					query_options.set_show_aux_lines(
					    options["showAuxLines"].GetBool());
				}
			}
		}

		// Validate the parsed request information
		if (!query_box.isValid()) {
			throw std::invalid_argument("Invalid bounding box.");
		} else if (!query_options.getInterval().isValid()) {
			throw std::invalid_argument("Invalid time interval.");
		} else if (!query_options.getSlots().isValid()) {
			throw std::invalid_argument("Invalid time interval day choice.");
		}
	}

	bool compareCachedRequest(
	    const pf::BoundingBox& query_box, const pf::Options& query_options)
	{
		// Check boxes
		if (!(query_box.min_coordinate == cached_query_box.min_coordinate)) {
			Print("Cache: Min coordinate differs.");
			return false;
		} else if (!(query_box.max_coordinate ==
		               cached_query_box.max_coordinate)) {
			Print("Cache: Max coordinate differs.");
			return false;
		}

		// Check time interval
		if (!(query_options.getInterval() ==
		        cached_query_options.getInterval())) {
			Print("Cache: Time interval differs.");
			return false;
		} else if (!(query_options.getSlots() ==
		               cached_query_options.getSlots())) {
			Print("Cache: Time slots differs.");
			return false;
		}

		// Check options
		if (!(query_options.getPacking() ==
		        cached_query_options.getPacking())) {
			Print("Cache: Packing differs.");
			return false;
		} else if (!(query_options.getLevel() ==
		               cached_query_options.getLevel())) {
			Print("Cache: Level differs.");
			return false;
		} else if (!(query_options.getExportType() ==
		               cached_query_options.getExportType())) {
			Print("Cache: Export Type differs.");
			return false;
		} else if (!(query_options.getReturnWeightedEdges() ==
		               cached_query_options.getReturnWeightedEdges())) {
			Print("Cache: ReturnWeightedEdges Type differs.");
			return false;
		} else if (!(query_options.useEdgeWeightBinning() ==
		               cached_query_options.useEdgeWeightBinning())) {
			Print("Cache: useEdgeWeightBinning number differs.");
			return false;
		} else if (!(query_options.getMinIntersectionCount() ==
		               cached_query_options.getMinIntersectionCount())) {
			Print("Cache: min_intersection_count number differs.");
			return false;
		}

		return true;
	}
};
using namespace pf;

int main()
{
	// Pistache
	int port_nr = 9080;

	// Initialization of the Paths
	std::string graph_filename;
	std::string path_filename;
	std::string path_export_dir;
	std::string path_import_dir;
	std::string path_import_filename;
	std::size_t number_of_random_paths = 0;
	std::int16_t import_type = -1;
	std::int16_t max_path_length = -1;

	// default thread values
	std::int16_t nr_of_pathfinder_threads = 4;
	std::int16_t nr_of_service_threads = 4;

	// default grid size: 256x256, 1 level
	GridIndex p = 8;
	GridIndex q = 8;
	GridIndex level = 1;

	// config :
	std::string stng;
	std::string configPath = "../settings.txt";

	double smoothness = 1;
	int max_zoom;

	std::ifstream fin(configPath);
	std::string line;
	std::istringstream sin;
	ifstream configFile(configPath);

	// Keep track how much time was needed for initializing the datastructures
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);
	std::cout << "[" << std::put_time(&tm, "%d-%m-%Y %H-%M-%S")
	          << "]: Program start" << std::endl;
	TimeTracker startup_time_tracker;
	startup_time_tracker.start();

	// overwrite configuration from settings file.
	cout << "------------Pathfinder-Server------------" << endl;
	if (configFile.good()) {
		cout << "Config file with the name \"settings.txt\" found" << endl;
		while (std::getline(fin, line)) {
			sin.str(line.substr(line.find("=") + 1));
			if (line.rfind("graph_filename", 0) == 0) {
				sin >> graph_filename;
				cout << "| graph_filename:                 " << graph_filename
				     << endl;
			} else if (line.rfind("real_paths_filename", 0) == 0) {
				sin >> path_filename;
				cout << "| real_paths_filename:            " << path_filename
				     << endl;
			} else if (line.rfind("import_type", 0) == 0) {
				sin >> import_type;
				cout << "| import_type:                   " << import_type
				     << endl;
			} else if (line.rfind("generated_paths_export_prefix", 0) == 0) {
				sin >> path_export_dir;
				cout << "| generated_paths_export_prefix: " << path_export_dir
				     << endl;
			} else if (line.rfind("generated_paths_import_name", 0) == 0) {
				sin >> path_import_dir;
				cout << "| generated_paths_import_name:   " << path_import_dir
				     << endl;
			} else if (line.rfind("number_of_random_paths", 0) == 0) {
				sin >> number_of_random_paths;
				cout << "| number_of_random_paths:        "
				     << number_of_random_paths << endl;
			} else if (line.rfind("max_path_length", 0) == 0) {
				sin >> max_path_length;
				cout << "| max_path_length:               " << max_path_length
				     << endl;
			} else if (line.rfind("gen_paths_filename", 0) == 0) {
				sin >> path_import_filename;
				cout << "| gen_paths_filename:            "
				     << path_import_filename << endl;
			} else if (line.rfind("smoothness", 0) == 0) {
				sin >> smoothness;
				cout << "| smoothness:                    " << smoothness
				     << endl;
			} else if (line.rfind("max_zoom", 0) == 0) {
				sin >> max_zoom;
				cout << "| max_zoom:                      " << max_zoom << endl;
			} else if (line.rfind("nr_of_pathfinder_threads", 0) == 0) {
				sin >> nr_of_pathfinder_threads;
				cout << "| nr_of_pathfinder_threads:      "
				     << nr_of_pathfinder_threads << endl;
			} else if (line.rfind("nr_of_service_threads", 0) == 0) {
				sin >> nr_of_service_threads;
				cout << "| nr_of_service_threads:         "
				     << nr_of_service_threads << endl;
			} else if (line.rfind("grid:p", 0) == 0) {
				sin >> p;
				cout << "| Grid size p:                   " << p << endl;
			} else if (line.rfind("grid:q", 0) == 0) {
				sin >> q;
				cout << "| Grid size q:                   " << q << endl;
			} else if (line.rfind("grid:layers", 0) == 0) {
				sin >> level;
				cout << "| Grid #layers:                  " << level << endl;
			} else if (line.rfind("port", 0) == 0) {
				sin >> port_nr;
				cout << "| PATHFINDER port:               " << port_nr << endl;
			}
			sin.clear();
		}
	} else {
		cout << "No config file with the name \"settings.txt\" found." << endl;
		cout << "Please enter in the \"./pathfinder_server/code\" directory "
		        "the \"settings.txt\"-file!"
		     << endl;
		cout << "Server will interrupt, please retry!" << endl;
		return EXIT_SUCCESS;
	}

	cout << endl;

	cout << "------------Calculation will start------------" << endl;

	level_extractor = LevelExtractor(smoothness, max_zoom);
	TimeTracker time_tracker;
	Random random;

	// loading graph
	Print("Loading graph from file..");
	time_tracker.start();
	graph.init(graph_filename);
	time_tracker.stop();
	Print("↳ Took " << time_tracker.getLastMeasurment() << " seconds");

	// load or generate paths based on the import type
	Print("");
	time_tracker.start();
	if (import_type == 0 or import_type == 1) {
		// Parsing .json path files
		Print("Parsing paths...");
		PathParser path_parser(graph, raw_paths_ds);
		bool success = path_parser.parse(path_filename);
		if (!success) {
			throw Exception(
			    "Parsing of the following path file failed:\n" + path_filename);
		}
		paths_ds = partitioner_util::partitionPaths(graph, raw_paths_ds);

		// export parsed paths as binary
		if (import_type == 1) {
			// Exporting binary paths
			std::string export_path = path_export_dir + "_json.bin";
			Print("Exporting the generated paths to '" + export_path +
			      "' [binary]");
			PathExporter().exportPathsBin(paths_ds, export_path);
		}

	} else if (import_type == 2 or import_type == 3 or import_type == 4) {
		// Generating paths
		Print("Generate " << number_of_random_paths << " random paths...");
		PathGenerator path_generator(graph, random, nr_of_pathfinder_threads);
		// the paths are already compressed:
		paths_ds = path_generator.getRandomShortestPaths(
		    number_of_random_paths, max_path_length);
		Print("| Generating paths finished.");

		if (import_type == 3) {
			// Exporting plain paths
			std::string export_path = path_export_dir +
			                          std::to_string(number_of_random_paths) +
			                          "_gen.plain";
			Print("| Exporting the generated paths to '" + export_path +
			      "' [plain]");
			PathExporter().exportPaths(paths_ds, export_path);
		} else if (import_type == 4) {
			// Exporting binary paths
			std::string export_path = path_export_dir +
			                          std::to_string(number_of_random_paths) +
			                          "_gen.bin";
			Print("| Exporting the generated paths to '" + export_path +
			      "' [binary]");
			PathExporter().exportPathsBin(paths_ds, export_path);
		} else {
			Print("| Only-Generate Mode, don't export");
		}

	} else if (import_type == 5) {
		// importing generated paths
		Print("Importing generated paths [plain]:" + path_import_dir);
		bool success =
		    PathImporter(graph, paths_ds).importPaths(path_import_dir);
		assert(success);
	} else if (import_type == 6) {
		// importing generated paths
		Print("Importing generated paths [binary]:" + path_import_dir);
		bool success =
		    PathImporter(graph, paths_ds).importPathsBin(path_import_dir);
		assert(success);
	} else if (import_type == -1) {
		throw Exception("Import Type is not decalred in settings file. Choose "
		                "from 0,1,2,3,4,5,6");
	} else {
		throw Exception("Invalid Import Type! Choose from 0,1,2,3,4,5,6");
	}
	time_tracker.stop();
	Print("| Paths read/genereated successfully.");
	Print("↳ Took " << time_tracker.getLastMeasurment() << " seconds\n");

	Print("Graph-Statistics");
	Print("  #edges:  " << graph.getAllEdges().size());
	Print("  #nodes:  " << graph.getAllNodes().size());

	// --- Creating a grid hierarchy -----------------------------
	Print("Calculating CellCovering Hierarchy...");

	// create grid creator object
	Print("| (1/2) Creating GridHierarchyAssigner...");
	time_tracker.start();

	auto grid_hierarchy_assigner =
	    GridHierarchyAssigner(p, q, level, paths_ds, graph);
	auto level_0_grid = grid_hierarchy_assigner.get_level0_grid();
	grid_stack = GridStack(level_0_grid, level);

	time_tracker.stop();
	Print("| ↳ Took " << time_tracker.getLastMeasurment() << " seconds");

	// create hierarchy unsing the object
	Print("| (2/2) Creating GridHierarchy...");
	time_tracker.start();
	grid_hierarchy_assigner.create_hierarchy();
	time_tracker.stop();
	Print("| ↳ Took " << time_tracker.getLastMeasurment() << " seconds");
	Print("Hierarchy calculated successfully. " << std::endl);

	// --- Start the server -----------------------------
	Print("Execute pathfinder algorithm...");

	std::cout << "------------PathFinder-Server starts------------"
	          << std::endl;

	// Server data
	Pistache::Port port(port_nr);
	Pistache::Address addr(Pistache::Ipv4::any(), port);

	PathFinderService service(
	    addr, nr_of_pathfinder_threads, nr_of_service_threads);

	std::cout << "------------ PathFinder-Server hardware ------------"
	          << std::endl;
	std::cout << "Cores = " << hardware_concurrency() << std::endl;
	std::cout << "Service:    Using " << nr_of_service_threads << " threads"
	          << std::endl;
	std::cout << "PATHFINDER: Using " << nr_of_pathfinder_threads << " threads"
	          << std::endl;

	// show server ready message
	t = std::time(nullptr);
	tm = *std::localtime(&t);
	startup_time_tracker.stop();
	std::cout << "[" << std::put_time(&tm, "%d-%m-%Y %H-%M-%S")
	          << "]: The server is now ready to serve." << std::endl;

	// summarize startup time
	int time_needed = (int)startup_time_tracker.getLastMeasurment();
	auto t_sec = time_needed % 60;
	auto t_min = ((time_needed - t_sec) % 3600) / 60;
	auto t_h = (time_needed - t_sec - 60 * t_min) / 3600;
	Print("[Total startup time:   " << t_h << "h " << t_min << "m " << t_sec
	                                << "s]");

	// start server request handler
	service.init();
	service.start();

	return EXIT_SUCCESS;
}
