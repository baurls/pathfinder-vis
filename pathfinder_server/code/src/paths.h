#pragma once

#include "basic_types.h"
#include "edge_id_range.h"
#include "graph.h"
#include "timeIntervall.h"

#include <stack>
#include <stxxl/vector>
#include <unordered_map>

namespace pf
{

namespace unit_tests
{
void testPaths();
} // namespace unit_tests

class NodeRange;

// The EdgeIndex is the position of the edge in the edges vector
using EdgeIndex = uint64_t;

using PathEdgeIDs = EdgeIDs;
using PathTimes = std::vector<TimeType>;

class Path
{
public:
	// The EdgeIndex is the position of the edge in the edges vector
	using EdgeIndex = uint64_t;
	// The NodeIndex is the position of the edge which has the respective node
	// as source. Note that the last node in the path doesn't have such an edge.
	// Thus, its NodeIndex is edges.size().
	using NodeIndex = int64_t;

	Path();
	Path(Graph const& graph, EdgeIDs const& edges);
	Path(Graph const& graph, GeoMeasurements& geomeasurements, Factors& factors,
	    std::unordered_map<OsmNodeId, NodeID> const& osmIdsToGraphIds);
	Path& operator=(Path const& path);
	Path(const Path&) = default;

	virtual ~Path()
	{
		edges.clear();
		timeAtNodes.clear();
	}

	void init(Graph const& graph, EdgeIDs& edges);

	void push(EdgeID edge_id);
	// This function throws iff the new edge is not consistent with the already
	// existing path
	void push(Graph const& graph, EdgeID edge_id);
	void push(Graph const& graph, EdgeID edge_id,
	    TimeIntervall const& time_intervall);

	// This function throws if there is no edge to pop
	void pop();

	// This function throws if the target of the current path doesn't match the
	// source of the path argument
	void append(Graph const& graph, Path const& path);

	EdgeID back() const;
	void clear();
	// Length is defined to be the number of (shortcut) edges in the path
	std::size_t getNoOfRootEdges() const;
	bool empty() const;

	// Returns the same path using only non-shortcut edges (i.e., the one which
	// results from unpacking every single edge)
	Path unpack(Graph const& graph) const;

	// Returns the same path unpacked to a certain level
	Path unpack(Graph const& graph, Level const& level) const;

	// Returns the same path unpacked to a certain level
	Path unpack(
	    Graph const& graph, Level const& level, std::ofstream& outfile) const;

	// Use those to iterate over the edges e.g. like:
	// for (auto edge_id : path) { ... }
	PathEdgeIDs::const_iterator begin() const;
	PathEdgeIDs::const_iterator end() const;

	// Use those to iterate over the nodes e.g. like:
	// for (auto node_id : path.getNodeRange(0, 4)) { ... }
	//
	// The first function iterates over all nodes of the path while the second
	// function iterates from the source of the edge at index 'begin' until the
	// target of the edge at index 'end - 1'.
	NodeRange getNodeRange(Graph const& graph) const;
	NodeRange getNodeRange(
	    Graph const& graph, NodeIndex begin, NodeIndex end) const;

	EdgeID operator[](EdgeIndex index) const;

	NodeID getSource(Graph const& graph) const;
	NodeID getTarget(Graph const& graph) const;
	PathEdgeIDs const& getEdges() const;
	PathTimes const& getTimeAtNodes() const;

	// Those function check if the index is valid and throw an exception in case
	// of an invalid index.
	NodeID getNode(Graph const& graph, NodeIndex node_index) const;
	EdgeID getEdge(EdgeIndex edge_index) const;

	// this doesn't work because there are some matched paths which contain
	// shortcut edges
	void setTimeIntervallsForCompressed(Graph const& graph, Path const& path);

	void setTimeIntervallsForMoreCompressed(
	    Graph const& graph, Path const& lessCompressed_path);

	bool operator==(const Path& rhs) const;
	bool isUncompressed(Graph const& graph) const;
	Length getLength(Graph const& graph) const;

	bool isCompressedOf(Graph const& graph, Path const& lessCompressed) const;

	// construct a timeintervall for the edge at the position in the path
	const TimeIntervall getTimeIntervallForEdge(EdgeIndex index) const;

	void setTimeIntervallForEdge(EdgeIndex index, TimeIntervall time_intervall);
	void setTimeAtNode(NodeIndex index, TimeType time);

	TimeType getStartTime() const;
	TimeType getEndTime() const;

	Path getSubPathCopy(int start, int end) const;

private:
	PathEdgeIDs edges;

	// is always exactly one more than edges
	PathTimes timeAtNodes;

	bool is_consistent(Graph const& graph) const;
	void checkPushConsistency(Graph const& graph, EdgeID edge_id) const;

	Path _unpack(Graph const& graph, PathEdgeIDs const& edge_ids) const;
	Path _unpack(Graph const& graph, PathEdgeIDs const& edge_ids,
	    std::ofstream& outfile) const;
	Path _unpack(Graph const& graph, PathEdgeIDs const& edge_ids,
	    Level const& level) const;
	Path unpackEdgeToPath(Graph const& graph, EdgeID edge_id) const;

	friend void unit_tests::testPaths();

	EdgeIDRange getPathEdgeRangeForFactor(
	    Graph const& graph, EdgeIDs& edge_ids, Factor factor) const;

	EdgeID osmEdgeToEdge(Graph const& graph, OsmEdge const& osmEdge,
	    std::unordered_map<OsmNodeId, NodeID> const& osmIdsToGraphIds) const;

	// the beginning of the path will be at the top of the stack
	std::stack<EdgeID> getEdgeStack() const;
};

using PathID = int64_t;
using PathIDs = std::vector<PathID>;

namespace unit_tests
{
void testAmbiguousShopas();
} // namespace unit_tests

} // namespace pf

// This include assures that the getNodeRange functions can be used by files
// including this file. If this include wouldn't be here, NodeRange would be an
// incomplete type if node_range.cpp is not included.
#include "node_range.h"
