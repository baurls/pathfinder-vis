#pragma once

#include "defs.h"
#include "timeSlots.h"

namespace pf
{

enum class ExportType {
	TRAJECORIES,
	SEGMENT_GRAPH,
	HEATMAP,
	BATCHED_PLAIN_INORDER,
	BATCHED_PLAIN_LEVELORDER,
	BATCHED_EDGEWISE,
	PRUNNING,
	EDGE_BASED_PATHFINDER,
	GRAPH_SECTION,
	GRAPH_SECTION_TOPOLOGY,
	INVALID
};
enum class BatchType { PLAIN_INORDER, EDGE_WISE, PLAIN_LEVELORDER, INVALID };

class Options
{
private:
	// Flag for parsing intervals. If true, the timestamps will
	// be exported for each path.
	bool parse_intervals;

	// The time interval for trajectory calculation. Only trajectories that have
	// time stamps in this interval will be counted.
	TimeIntervall time_interval;

	// The time slots for time interval calculation. This bitstring sets the
	// weekdays that should be included.
	TimeSlots time_slots;

	// Flag for packing paths. If true, paths will stay packed, else they will
	// be unpacked completly.
	bool packing;

	// Flag for packing paths to a certain level. If packing is set to true and
	// level >= 0, the paths will be (un-)packed to this level.
	Level ch_level;

	// The map zoom level. Will be used to calculate the path levels
	// corresponding to this zoom value.
	int map_zoom;

	// The export type of returned request types
	ExportType export_type;

	// Flag for prunning paths. If true, only high level edges (according to
	// cutoff_level) are kept others will be ignored
	bool prunning;

	// Flag if edge counts shoud be returned, too.
	bool weighted_edge_return;

	// If larger than zero: Gropus the weights into gropus of this size
	int weighted_edge_binning;

	// When EBPF, only edgs with count >= are returned.
	int min_intersection_count;

	// Flag if edges are flipped in case of an EBPF request
	bool ebpf_use_bidirectional_edges;

	// Flag if node counts chould be used case of an EBPF request
	bool ebpf_use_nodecount;

	// Flag if EBPF-sweep should be avoided.
	bool ebpf_perform_local_search;

	// The cutoff level For edges to retrieve.
	// if prunning is activated, only edges larger than the cutoff-level are
	// kept.
	int cutoff_level;

	// Used for exoprting a graph-section. Serves as an min-importance level
	Level graph_level;

	// Bool if Aux lines should be included or not
	bool show_auxiliary_lines;

public:
	// Creates an Option instance and sets its values to the default values.
	Options()
	{
		time_interval = c::FULL_INTERVALL;
		time_slots = TimeSlots(true);
		parse_intervals = false;
		packing = true;
		prunning = true;
		weighted_edge_return = false;
		weighted_edge_binning = -1;
		ch_level = c::NO_LEVEL;
		cutoff_level = c::NO_LEVEL;
		map_zoom = c::NO_ZOOM;
		export_type = ExportType::TRAJECORIES;
		min_intersection_count = 1;
		ebpf_use_bidirectional_edges = false;
		ebpf_perform_local_search = false;
		ebpf_use_nodecount = false;
		show_auxiliary_lines = false;
	}

	void setParseIntervals(bool const& flag)
	{
		parse_intervals = flag;
	}

	bool getParseIntervals() const
	{
		return parse_intervals;
	}

	void setInterval(TimeIntervall const& interval)
	{
		time_interval = interval;
	}

	TimeIntervall getInterval() const
	{
		return time_interval;
	}

	void setSlots(TimeSlots const& slots)
	{
		time_slots = slots;
	}

	TimeSlots getSlots() const
	{
		return time_slots;
	}

	void setLevel(Level const& level)
	{
		ch_level = level >= 0 ? level : c::NO_LEVEL;
	}

	Level getLevel() const
	{
		return ch_level;
	}

	void setCutoffLevel(Level const& level)
	{
		cutoff_level = level >= 0 ? level : c::NO_LEVEL;
	}

	Level getCutoffLevel() const
	{
		return cutoff_level;
	}

	void setPrunning(bool const& flag)
	{
		prunning = flag;
	}

	bool getPrunning() const
	{
		return prunning;
	}

	void setPacking(bool const& flag)
	{
		packing = flag;
	}

	bool getPacking() const
	{
		return packing;
	}

	void setReturnWeightedEdges(bool const& flag)
	{
		weighted_edge_return = flag;
	}

	bool getReturnWeightedEdges() const
	{
		return weighted_edge_return;
	}

	void setWeightedEdgeBinning(int const& binning_value)
	{
		weighted_edge_binning = binning_value > 0 ? binning_value : -1;
	}

	int getWeightedEdgeBinning() const
	{
		return weighted_edge_binning;
	}

	void setMinIntersectionCount(int const& intersection_count)
	{
		min_intersection_count =
		    intersection_count > 0 ? intersection_count : 1;
	}

	int getMinIntersectionCount() const
	{
		return min_intersection_count;
	}

	bool useEdgeWeightBinning() const
	{
		return weighted_edge_binning > 0;
	}

	void setZoom(int zoom)
	{
		map_zoom = zoom >= 0 ? zoom : c::NO_ZOOM;
	}

	int getZoom() const
	{
		return map_zoom;
	}

	void setExportType(std::string type)
	{
		if (type == "trajectories") {
			export_type = ExportType::TRAJECORIES;
		} else if (type == "segment_graph") {
			export_type = ExportType::SEGMENT_GRAPH;
		} else if (type == "heatmap") {
			export_type = ExportType::HEATMAP;
		} else if (type == "batch_plain_inorder") {
			export_type = ExportType::BATCHED_PLAIN_INORDER;
		} else if (type == "batch_plain_levelorder") {
			export_type = ExportType::BATCHED_PLAIN_LEVELORDER;
		} else if (type == "batch_edgewise_queue") {
			export_type = ExportType::BATCHED_EDGEWISE;
		} else if (type == "prunning") {
			export_type = ExportType::PRUNNING;
		} else if (type == "edge-based-pathfinder") {
			export_type = ExportType::EDGE_BASED_PATHFINDER;
		} else if (type == "graph_section") {
			export_type = ExportType::GRAPH_SECTION;
		} else if (type == "graph_section_topology") {
			export_type = ExportType::GRAPH_SECTION_TOPOLOGY;
		} else {
			export_type = ExportType::INVALID;
		}
	}

	ExportType getExportType() const
	{
		return export_type;
	}

	void set_ebpf_use_bidirectional_edges(bool const& flag)
	{
		ebpf_use_bidirectional_edges = flag;
	}

	bool get_ebpf_use_bidirectional_edges() const
	{
		return ebpf_use_bidirectional_edges;
	}

	void set_ebpf_perform_local_search(bool const& flag)
	{
		ebpf_perform_local_search = flag;
	}

	bool get_ebpf_perform_local_search() const
	{
		return ebpf_perform_local_search;
	}

	void set_ebpf_use_nodecount(bool const& flag)
	{
		ebpf_use_nodecount = flag;
	}

	bool get_ebpf_use_nodecount() const
	{
		return ebpf_use_nodecount;
	}

	void setGraphLevel(Level level)
	{
		graph_level = level >= 0 ? level : c::NO_LEVEL;
	}

	Level getGraphLevel() const
	{
		return graph_level;
	}

	void set_show_aux_lines(bool show_aux_lines)
	{
		show_auxiliary_lines =
		    show_aux_lines >= 0 ? show_aux_lines : c::NO_LEVEL;
	}

	bool show_aux_lines() const
	{
		return show_auxiliary_lines;
	}
};

} // namespace pf
