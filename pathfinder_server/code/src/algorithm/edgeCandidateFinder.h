#pragma once

#include "../basic_types.h"
#include "../flags.h"
#include "../pathfinder_ds.h"
#include "../query.h"

#include <algorithm>

namespace pf
{

namespace unit_tests
{
void testEdgeCandidateFinder();
void testEdgeCandidateFinderGenerated(bool sortWRTEdgeStatus);
void testEdgeCandidateFinderReal(bool sortWRTEdgeStatus);
void testEdgeCandidateFinder(Graph const& graph, PathfinderDS& pathfinder_ds);
} // namespace unit_tests

class EdgeCandidateFinder
{
private:
	using Result = CHTraversal::Result;
	using ExitValue = CHTraversal::ExitValue;

	PathfinderDS& ds;
	Graph const& graph;

	NodeIDs node_stack;

	// Reusable vectors
	NodeIDs nodes;
	std::vector<bool> is_contained;
	Flags<bool> seen;

	struct ThreadData {
		EdgeIDs edge_candidates;
		NodeIDs nodes_next_layer;
	};
	std::vector<ThreadData> _thread_data;

public:
	EdgeCandidateFinder(PathfinderDS& ds, int nr_of_threads)
	    : ds(ds)
	    , graph(ds.getGraph())
	    , seen(graph.getNumberOfNodes(), false)
	    , _thread_data(nr_of_threads)
	{
		is_contained.assign(graph.getNumberOfNodes(), false);
	}

	void findEdgeCandidates(Query const& query, NodeIDs const& top_nodes,
	    EdgeIDs& edge_candidates_total)
	{
		edge_candidates_total.clear();

		if (graph.isSortedWRTEdgesStatus()) {
			find_edgesCandidatesBFS(query, top_nodes, edge_candidates_total);
		} else {
			find_edgesStackDFS(top_nodes, query, edge_candidates_total);
		}
	}

private:
	ExitValue find_edgesCandidatesBFS(Query const& query,
	    NodeIDs const& top_nodes, EdgeIDs& edge_candidates_total)
	{
		nodes.clear();
		nodes.insert(nodes.end(), top_nodes.begin(), top_nodes.end());

		is_contained.assign(is_contained.size(), false);

#pragma omp parallel num_threads(_thread_data.size())
		{
			_myThreadData().edge_candidates.clear();
		}

		while (!nodes.empty()) {
#pragma omp parallel num_threads(_thread_data.size())
			{
				_myThreadData().nodes_next_layer.clear();

#pragma omp for schedule(guided)
				for (uint i = 0; i < nodes.size(); i++) {
					_find_edgesCandidatesBFS(query, nodes[i]);
				}
			}

			// merge nodes_next_layer for next round
			nodes.clear();
			for (ThreadData const& thread_data : _thread_data) {
				nodes.insert(nodes.end(), thread_data.nodes_next_layer.begin(),
				    thread_data.nodes_next_layer.end());
			}
		}

		// merge final edgeCandidates
		edge_candidates_total.clear();
		for (ThreadData const& thread_data : _thread_data) {
			edge_candidates_total.insert(edge_candidates_total.end(),
			    thread_data.edge_candidates.begin(),
			    thread_data.edge_candidates.end());
		}

		return ExitValue::NotStopped;
	}
	void _find_edgesCandidatesBFS(Query const& query, NodeID node_id)
	{
		for (auto direction : {Direction::Forward, Direction::Backward}) {
			for (auto const& edge :
			    graph.getNonObsoleteNonTreeDownEdgesOf(node_id, direction)) {
				add_edge(edge, query, _myThreadData().edge_candidates,
				    is_contained[node_id]);
			}

			for (auto const& edge :
			    graph.getTreeDownEdgesOf(node_id, direction)) {
				auto head_id = edge.getHead(direction);

				add_edge(edge, query, _myThreadData().edge_candidates,
				    is_contained[node_id]);
				auto result =
				    check_node_expand(head_id, node_id, query, is_contained);

				if (result == Result::Expand) {
					_myThreadData().nodes_next_layer.emplace_back(head_id);
				}
			}
		}
	}
	Result check_node_expand(NodeID head_id, NodeID parent_id,
	    Query const& query, std::vector<bool>& isContained)
	{
		// Check if we want to continue searching at the head node. Only
		// continue search if the r_node_box intersects the query_box
		// and the r_edge intervall intersects the query_intervall.

		auto r_edge_intervall = ds.getREdgeIntervall(head_id);
		r_edge_intervall.intersect(query.time_intervall);
		if (!r_edge_intervall.isValid()) {
			return Result::Ignore;
		}

		auto r_edge_slots = ds.getREdgeSlots(head_id);
		r_edge_slots.intersect(query.time_slots);
		if (!r_edge_slots.isValid()) {
			return Result::Ignore;
		}

		if (isContained[parent_id]) {
			isContained[head_id] = true;
			return Result::Expand;
		} else {
			auto r_node_box = ds.getRNodeBox(head_id);
			// if everything below is contained, just traverse it without
			// geometry checks
			if (query.bounding_box.contains(r_node_box)) {
				isContained[head_id] = true;
				return Result::Expand;
			} else {
				r_node_box.intersect(query.bounding_box);
				return r_node_box.isValid() ? Result::Expand : Result::Ignore;
			}
		}
	}

	bool intersects_spatially(Edge const& edge, BoundingBox const& bounding_box)
	{
		auto edge_box = ds.getEdgeBox(edge.id);
		edge_box.intersect(bounding_box);
		return edge_box.isValid();
	}
	void add_edge(Edge const& edge, Query const& query,
	    EdgeIDs& edge_candidates, bool contained)
	{
		// Check if this edge is a candidate
		if (!contained && !intersects_spatially(edge, query.bounding_box)) {
			return;
		}

		auto edge_intervall = ds.getEdgeIntervall(edge.id);
		edge_intervall.intersect(query.time_intervall);
		if (!edge_intervall.isValid()) {
			return;
		}

		auto edge_slots = ds.getEdgeSlots(edge.id);
		edge_slots.intersect(query.time_slots);
		if (edge_slots.isValid()) {
			edge_candidates.push_back(edge.id);
		}
	}

	auto find_edgesStackDFS(NodeIDs const& top_nodes, Query const& query,
	    EdgeIDs& edge_candidates_total) -> ExitValue
	{
		seen.reset();

		// reusable stack used in the down search
		NodeIDs node_stack;
		node_stack.clear();

		for (auto start_node_id : top_nodes) {
			node_stack.emplace_back(start_node_id);
			seen.set(start_node_id, true);
		}

		while (!node_stack.empty()) {
			auto node_id = node_stack.back();
			node_stack.pop_back();

			for (auto direction : {Direction::Forward, Direction::Backward}) {
				for (auto const& edge :
				    graph.getDownEdgesOf(node_id, direction)) {
					auto head_id = edge.getHead(direction);

					auto result = find_edgesDFS(
					    edge, direction, query, edge_candidates_total);

					if (result == Result::Expand && !seen[head_id]) {
						node_stack.emplace_back(head_id);
						seen.set(head_id, true);
					}
				}
			}
		}
		return ExitValue::NotStopped;
	}

	auto find_edgesDFS(Edge const& edge, Direction direction,
	    Query const& query, EdgeIDs& edge_candidates) -> Result
	{
		if (ds.isObsolete(edge.id)) {
			return Result::Ignore;
		}

		auto edge_box = ds.getEdgeBox(edge.id);
		edge_box.intersect(query.bounding_box);
		auto edge_intervall = ds.getEdgeIntervall(edge.id);
		edge_intervall.intersect(query.time_intervall);
		auto edge_slots = ds.getEdgeSlots(edge.id);
		edge_slots.intersect(query.time_slots);

		if (edge_box.isValid() && edge_intervall.isValid() &&
		    edge_slots.isValid()) {

			edge_candidates.push_back(edge.id);
		}

		// Check if we want to continue searching at the head node. Only
		// continue search if the r_node_box intersects the query_box
		// and the r_edge intervall intersects the query_intervall.
		auto head_id = edge.getHead(direction);

		auto r_edge_intervall = ds.getREdgeIntervall(head_id);
		r_edge_intervall.intersect(query.time_intervall);
		if (!r_edge_intervall.isValid()) {
			return Result::Ignore;
		}

		auto r_edge_slots = ds.getREdgeSlots(head_id);
		r_edge_slots.intersect(query.time_slots);
		if (!r_edge_slots.isValid()) {
			return Result::Ignore;
		}

		auto r_node_box = ds.getRNodeBox(head_id);
		// if everything below is contained, just traverse it without geometry
		// checks
		if (query.bounding_box.contains(r_node_box)) {
			if (!seen[head_id]) {
				add_edgesStackDFS(head_id, query, edge_candidates);
			}
			return Result::Ignore;
		} else {
			r_node_box.intersect(query.bounding_box);
			return r_node_box.isValid() ? Result::Expand : Result::Ignore;
		}
	}

	auto add_edgesStackDFS(NodeID start_node_id, Query const& query,
	    EdgeIDs& edge_candidates) -> ExitValue
	{
		node_stack.clear();

		node_stack.emplace_back(start_node_id);
		seen.set(start_node_id, true);

		while (!node_stack.empty()) {
			auto node_id = node_stack.back();
			node_stack.pop_back();

			for (auto direction : {Direction::Forward, Direction::Backward}) {
				for (auto const& edge :
				    graph.getDownEdgesOf(node_id, direction)) {
					auto head_id = edge.getHead(direction);

					auto result =
					    add_edgesDFS(edge, direction, query, edge_candidates);

					if (result == Result::Expand && !seen[head_id]) {
						node_stack.emplace_back(head_id);
						seen.set(head_id, true);
					}
				}
			}
		}
		return ExitValue::NotStopped;
	}

	auto add_edgesDFS(Edge const& edge, Direction direction, Query const& query,
	    EdgeIDs& edge_candidates) -> Result
	{
		if (ds.isObsolete(edge.id)) {
			return Result::Ignore;
		}

		auto edge_intervall = ds.getEdgeIntervall(edge.id);
		edge_intervall.intersect(query.time_intervall);
		auto edge_slots = ds.getEdgeSlots(edge.id);
		edge_slots.intersect(query.time_slots);

		if (edge_intervall.isValid() && edge_slots.isValid()) {
			edge_candidates.push_back(edge.id);
		}

		// Check if we want to continue searching at the head node. Only
		// continue search if the r_node_box intersects the query_box
		// and the r_edge intervall intersects the query_intervall.
		auto head_id = edge.getHead(direction);

		auto r_edge_intervall = ds.getREdgeIntervall(head_id);
		r_edge_intervall.intersect(query.time_intervall);
		if (!r_edge_intervall.isValid()) {
			return Result::Ignore;
		}

		auto r_edge_slots = ds.getREdgeSlots(head_id);
		r_edge_slots.intersect(query.time_slots);
		if (!r_edge_slots.isValid()) {
			return Result::Ignore;
		}

		return Result::Expand;
	}

	auto _myThreadData() -> ThreadData&
	{
		return _thread_data[omp_get_thread_num()];
	}
};

} // namespace pf
