#pragma once

#include "basic_types.h"
#include "defs.h"
#include "options.h"
#include "paths.h"
#include "query.h"
#include "time_tracker.h"

#include <map>

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, Master Thesis 2021.    #
    #                                                 #
    #   Supervisor: T. Rupp, Prof. S. Funke           #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

/*
 * Util code split up an array in up to k pieces,
 */

std::vector<size_t> fixed_range_nn_grouping(
    const int& bins, const std::vector<int>& sorted_values)
{
	assert(bins > 0); // Can't split up into 0 groups.

	// init result vector
	std::vector<size_t> start_indices;
	int bin = 0;
	start_indices.push_back(bin);

	// special case with only one bins
	if (bins == 1) {
		return start_indices;
	}

	// insert (#bins - 1) thresholds inbetween and assign classes.
	double distance =
	    static_cast<double>(sorted_values.back() - sorted_values.front()) /
	    static_cast<double>(bins - 1);
	double current_threshold = sorted_values.front() + 0.5 * distance;

	for (size_t i = 1; i < sorted_values.size(); i++) {
		if (sorted_values[i] > current_threshold) {
			bin = bin + 1;
			start_indices.push_back(i);
			current_threshold = sorted_values.front() + (0.5 + bin) * distance;
		}
	}
	return start_indices;
}

} // namespace pf
