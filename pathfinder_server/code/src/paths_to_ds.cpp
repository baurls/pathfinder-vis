#include "paths_to_ds.h"

namespace pf
{

void UnpackingUtil::unpack_and_copy_paths(PathsDS& export_data_paths_ds,
    PathIDs& found_paths, Options const& query_options,
    const PathfinderDS& p_ds, const LevelExtractor& level_extractor)
{
	Graph const& graphM = p_ds.getGraph();
	if (query_options.getPacking()) {
		auto level = level_extractor.extract_level(query_options, graphM);
		// Unpack the paths to the set level
		Print("| Unpacking paths to CH-Level: " + std::to_string(level));

		for (PathID path_id : found_paths) {
			Path unpackedPath;
			unpackedPath =
			    p_ds.getPathsDS().getPath(path_id).unpack(graphM, level);
			export_data_paths_ds.addPath(unpackedPath);
		}

	} else {
		// Unpack the paths completely
		Print("| Unpacking paths completly.");

		for (PathID path_id : found_paths) {
			Path unpackedPath =
			    p_ds.getPathsDS().getPath(path_id).unpack(graphM);
			export_data_paths_ds.addPath(unpackedPath);
		}
	}

	Print("| Unpacking paths finished.");
}

} // namespace pf