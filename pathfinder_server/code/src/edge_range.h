#pragma once

#include "basic_types.h"

namespace pf
{

namespace unit_tests
{
void testEdgeRange();
} // namespace unit_tests

template <typename T>
class ReverseEdgeRange;

template <typename T = Edges>
class EdgeRange
{
public:
	using iterator = typename T::const_iterator;

	EdgeRange(iterator const& begin, iterator const& end)
	    : begin_it(begin), end_it(end)
	{
	}

	iterator begin() const
	{
		return begin_it;
	};

	iterator end() const
	{
		return end_it;
	};

	ReverseEdgeRange<T> reverse() const;

private:
	iterator begin_it;
	iterator end_it;

	friend void unit_tests::testEdgeRange();
};

template <typename T = Edges>
class ReverseEdgeRange
{
public:
	using iterator = typename T::const_iterator;
	using reverse_iterator = typename T::const_reverse_iterator;

	ReverseEdgeRange(iterator const& begin, iterator const& end)
	    : begin_it(end), end_it(begin)
	{
	}

	reverse_iterator begin() const
	{
		return begin_it;
	};

	reverse_iterator end() const
	{
		return end_it;
	};

	EdgeRange<T> reverse() const;

private:
	reverse_iterator begin_it;
	reverse_iterator end_it;
};

template <typename T>
ReverseEdgeRange<T> EdgeRange<T>::reverse() const
{
	return ReverseEdgeRange(begin_it, end_it);
}

template <typename T>
EdgeRange<T> ReverseEdgeRange<T>::reverse() const
{
	return EdgeRange(end_it.base(), begin_it.base());
}

} // namespace pf
