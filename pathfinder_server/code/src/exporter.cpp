#include "exporter.h"

#include "defs.h"
#include "edge_colormaps.h"
#include "graph_exporting/full.h"
#include "graph_exporting/naive.h"
#include "grid_heatmap.h"
#include "grouping.h"
#include "heatmap_builder.h"
#include "paths_ds.h"
#include "segment_splitter.h"
#include "sensitive_unpacking/data_structures.h"
#include "sensitive_unpacking/global_su.h"
#include "sensitive_unpacking/local_su.h"
#include "time_tracker.h"

namespace pf
{

Exporter::Exporter(Graph const& graph) : graph(graph), geometry(graph)
{
}

void append_edgelist_as_json(
    std::string* result, const EdgeIDs& edges, const Graph& graph)
{
	unsigned int j = 1;
	for (EdgeID edge_id : edges) {
		auto const& edge = graph.getEdge(edge_id);

		auto coordinate_lat = graph.getNode(edge.source).coordinate.lat;
		auto coordinate_lng = graph.getNode(edge.source).coordinate.lon;
		*result += "[" + std::to_string(coordinate_lat) + "," +
		           std::to_string(coordinate_lng) + "], ";

		// if last element
		if (edges.size() == j) {
			auto target_coordinate_lat =
			    graph.getNode(edge.target).coordinate.lat;
			auto target_coordinate_lng =
			    graph.getNode(edge.target).coordinate.lon;
			*result += "[" + std::to_string(target_coordinate_lat) + "," +
			           std::to_string(target_coordinate_lng) + "]";
		}
		j++;
	}
}

void append_edge_id_list_as_json(std::string* result, const EdgeIDs& edges)
{
	unsigned int j = 0;
	for (EdgeID edge_id : edges) {
		if (j > 0) {
			*result += ",";
		}
		*result += std::to_string(edge_id);
		j++;
	}
}
void append_unpacked_edgelist_as_json(std::string* result, const EdgeIDs& edges,
    const Graph& graph, Level const& unpacking_level, size_t const& start_index,
    size_t const& excl_end_index)
{
	*result += "[";
	// handle first edge
	if (excl_end_index - start_index > 0) {
		auto const& root_edge_id = edges[start_index];

		EdgeIDs unpacked_root_edge =
		    graph.unpack(root_edge_id, unpacking_level);
		*result += "[";
		append_edgelist_as_json(result, unpacked_root_edge, graph);
		*result += "]";
	}
	// handle all n-1 other edges
	for (uint i = start_index + 1; i < excl_end_index; i++) {
		auto const& root_edge_id = edges[i];

		EdgeIDs unpacked_root_edge =
		    graph.unpack(root_edge_id, unpacking_level);
		*result += ",[";
		append_edgelist_as_json(result, unpacked_root_edge, graph);
		*result += "]";
	}
	*result += "]";
}

void append_unpacked_edgelist_as_json(std::string* result, const EdgeIDs& edges,
    const Graph& graph, Level const& unpacking_level)
{
	const size_t start_index = 0;
	const size_t excl_end_index = edges.size();
	append_unpacked_edgelist_as_json(
	    result, edges, graph, unpacking_level, start_index, excl_end_index);
}

void append_weight_vector_as_json(std::string& result, const Counter& vector,
    size_t const& start_index, size_t const& excl_end_index)
{
	result += "[";
	if (excl_end_index - start_index > 0) {
		// handle first edge
		result += std::to_string(vector[start_index]);
	}
	// handle all n-1 other edges
	for (uint i = start_index + 1; i < excl_end_index; i++) {
		result += "," + std::to_string(vector[i]);
	}
	result += "]";
}

void append_weight_vector_as_json(std::string& result, const Counter& vector)
{
	size_t start_index = 0;
	size_t excl_end_index = vector.size();
	append_weight_vector_as_json(result, vector, start_index, excl_end_index);
}

std::string export_edgeset_color_coded(ColorCodedEdgeset const& export_edges,
    PathfinderDS& p_ds, Level unpack_level)
{
	// ----------- export to json-string ---------------------
	std::string result = "{\"colorcoded_edges\":";

	// -- export groupwise
	TimeTracker time_tracker;
	time_tracker.start();

	const Graph graph = p_ds.getGraph();

	result += "{";
	result += "\"shortcuts\":";
	append_unpacked_edgelist_as_json(
	    &result, export_edges.shortcuts, graph, unpack_level);
	result += ", \"increase_plain\":";
	append_unpacked_edgelist_as_json(
	    &result, export_edges.increase_plain, graph, unpack_level);
	result += ", \"decrease_plain\":";
	append_unpacked_edgelist_as_json(
	    &result, export_edges.decrease_plain, graph, unpack_level);
	result += ", \"aux_shortcuts\":";
	append_unpacked_edgelist_as_json(
	    &result, export_edges.auxiliary_shortcuts, graph, unpack_level);
	result += "}";

	result += ", \"found\":" + std::to_string(-1) +
	          ", \"total\":" + std::to_string(export_edges.size()) +
	          ", \"length\": -1}";

	time_tracker.stop();
	std::cout << "↳ Took " << time_tracker.getLastMeasurment() << " seconds"
	          << std::endl;

	return result;
}

std::string Exporter::exportTrajectories(
    ExportData const& data, Options const& options) const
{
	// The result string
	std::string result = "{\"trajectories\":[";

	auto found = data.intersection_paths;
	auto total = data.total_paths;
	auto totalLength = 0;

	// Write edges of paths
	for (PathID path_id = 0; path_id < data.paths_ds.getNrOfPaths();
	     ++path_id) {

		Path const& path = data.paths_ds.getPath(path_id);

		auto length = path.getLength(graph);
		totalLength += length;

		result += "{\"length\": " + std::to_string(path.getLength(graph));

		if (options.getParseIntervals()) {
			result += ", \"timestamp\":{\"start\":" +
			          std::to_string(path.getStartTime()) +
			          ",\"end\":" + std::to_string(path.getEndTime()) + "}";
		}

		result += ", \"coords\":[";
		// Convert the trajectory objects to coordinate Arrays
		append_edgelist_as_json(&result, path.getEdges(), graph);
		result += "]}";

		// Append comma if the path isn't the last one
		if (path_id < data.paths_ds.getNrOfPaths() - 1) {
			result += ",";
		}
	}

	result += "], \"found\":" + std::to_string(found) +
	          ", \"total\":" + std::to_string(total) +
	          ", \"length\":" + std::to_string(totalLength) + "}";

	return result;
}

EdgeID get_local_index(
    const EdgeID edge_id, EdgeMap& map, EdgeIDs& out_all_edges)
{
	if (map.find(edge_id) == map.end()) {
		map[edge_id] = map.size();
		out_all_edges.push_back(edge_id);
	}
	return map[edge_id];
}

void Exporter::convert_to_indexed_edgelist(const PathsDS& paths_ds,
    EdgeIDs& out_all_edges, std::vector<EdgeIDs>& out_new_edgelists) const
{
	EdgeMap map;

	for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); path_id++) {
		EdgeIDs new_indexed_path;
		// go through all edges and replace by local ids
		Path path = paths_ds.getPath(path_id);
		for (EdgeID edge_id : path.getEdges()) {
			EdgeID new_index = get_local_index(edge_id, map, out_all_edges);
			new_indexed_path.push_back(new_index);
		}
		out_new_edgelists.push_back(new_indexed_path);
	}
}

std::string Exporter::exportTrajectoriesEdgeIndexed(
    ExportData const& data, Options const& options) const
{
	// The result string
	std::string result = "{\"trajectories\":[";

	auto found = data.intersection_paths;
	auto total = data.total_paths;
	auto totalLength = 0;

	EdgeIDs all_edges;
	std::vector<EdgeIDs> new_edgelists;

	convert_to_indexed_edgelist(data.paths_ds, all_edges, new_edgelists);

	// Write edges of paths
	for (PathID path_id = 0; path_id < data.paths_ds.getNrOfPaths();
	     ++path_id) {

		Path const& property_path = data.paths_ds.getPath(path_id);
		EdgeIDs const& path = new_edgelists[path_id];

		auto length = property_path.getLength(graph);
		totalLength += length;

		result +=
		    "{\"length\": " + std::to_string(property_path.getLength(graph));

		if (options.getParseIntervals()) {
			result += ", \"timestamp\":{\"start\":" +
			          std::to_string(property_path.getStartTime()) +
			          ",\"end\":" + std::to_string(property_path.getEndTime()) +
			          "}";
		}

		result += ", \"coords\":[";
		// Convert the trajectory objects to coordinate Arrays
		append_edge_id_list_as_json(&result, path);
		result += "]}";

		// Append comma if the path isn't the last one
		if (path_id < data.paths_ds.getNrOfPaths() - 1) {
			result += ",";
		}
	}

	result += "], edge_coords:[";
	append_edgelist_as_json(&result, all_edges, graph);
	result += "], \"found\":" + std::to_string(found) +
	          ", \"total\":" + std::to_string(total) +
	          ", \"length\":" + std::to_string(totalLength) + "}";

	return result;
}

inline void add_comma_if_not_first(std::string* json_string, int* counter)
{
	if (*counter > 0) {
		*json_string += ",";
	}
	*counter = *counter + 1;
}

std::string Exporter::edgelist_to_str(TGEdgeIDs& edges) const
{
	std::string json_string = "[";
	int output_counter = 0;
	for (auto edge : edges) {
		add_comma_if_not_first(&json_string, &output_counter);
		json_string += std::to_string(edge);
	}
	json_string += "]";
	return json_string;
}

void Exporter::append_node_str(std::string* json_string, TGNode& node) const
{
	*json_string += "\"" + std::to_string(node.id) +
	                "\":" + edgelist_to_str(node.adjacent_edges);
}

std::string Exporter::trajlist_to_str(TrajList& trajecory_list) const
{
	auto last_pointer =
	    std::unique(trajecory_list.begin(), trajecory_list.end());
	trajecory_list.resize(std::distance(trajecory_list.begin(), last_pointer));

	std::string list = "[";
	int output_counter = 0;
	for (auto trajectory : trajecory_list) {
		add_comma_if_not_first(&list, &output_counter);
		list += std::to_string(trajectory);
	}
	list += "]";
	return list;
}

std::string Exporter::node_to_latlon(const NodeID node) const
{
	auto lat = graph.getNode(node).coordinate.lat;
	auto lon = graph.getNode(node).coordinate.lon;
	return std::to_string(lat) + "," + std::to_string(lon);
}

std::string Exporter::segment_to_nodelist(const TGEdge& segment_edge,
    const TGSegments& segments, const TGNodes& nodes) const
{
	std::string nodelist = "[";
	int output_counter = 1;
	nodelist += node_to_latlon(nodes[segment_edge.u].pathnode_id);
	if (segment_edge.segment_link != TrajectoryGraph::EMPTY_SEGMENT_ID) {
		for (auto inner_node : segments[segment_edge.segment_link]) {
			add_comma_if_not_first(&nodelist, &output_counter);
			auto osm_node = nodes[inner_node].pathnode_id;
			nodelist += node_to_latlon(osm_node);
		}
	}
	add_comma_if_not_first(&nodelist, &output_counter);
	nodelist += node_to_latlon(nodes[segment_edge.v].pathnode_id);
	nodelist += "]";
	return nodelist;
}

void Exporter::append_segment_str(std::string* json_string,
    const TGEdge& segment_edge, const TGSegments& segments,
    const TGNodes& nodes, TrajectoryDFA& dfa) const
{
	auto usage_count = dfa.get_usage_for_superimposition_id(
	    segment_edge.trajectory_imposition);
	auto used_trajectories =
	    dfa.get_trajectories_for_state_id(segment_edge.trajectory_imposition);
	auto color_string = AbsolutePathColorPalette::get_edge_color(usage_count);
	auto nodelist_string = segment_to_nodelist(segment_edge, segments, nodes);

	*json_string +=
	    "\"" + std::to_string(segment_edge.id) +
	    "\":{\"sid\":" + std::to_string(segment_edge.trajectory_imposition) +
	    ", " + "\"c\":\"" + color_string + "\"," + "\"n\":" + nodelist_string +
	    "," + "\"u\":" + std::to_string(segment_edge.u) + "," +
	    "\"v\":" + std::to_string(segment_edge.v) + "," +
	    "\"t\":" + trajlist_to_str(used_trajectories) + "}";
}

void Exporter::append_graph_str(
    std::string* json_string, const TrajectoryGraph& traj_graph) const
{
	// load graph components
	auto segments_connections = traj_graph.get_segment_connections();
	auto segments = traj_graph.get_segments();
	auto dfa = traj_graph.trajectory_dfa;
	auto nodes = traj_graph.get_nodes();

	// convert to string
	*json_string += "{";
	// add all 'edges' (=segments)
	*json_string += "\"segments\":";
	*json_string += "{";
	int output_counter = 0;
	for (auto segment_connection : segments_connections) {
		add_comma_if_not_first(json_string, &output_counter);
		append_segment_str(
		    json_string, segment_connection, segments, nodes, dfa);
	}
	*json_string += "},";
	// add segment connections
	*json_string += "\"connections\":";
	*json_string += "{";
	output_counter = 0;
	for (auto v : traj_graph.get_active_nodes()) {
		add_comma_if_not_first(json_string, &output_counter);
		append_node_str(json_string, v);
	}
	*json_string += "}";
	*json_string += "}";
}

std::string Exporter::exportSegmentGraph(ExportData const& data) const
{

	// The result string

	auto found = data.intersection_paths;
	auto total = data.total_paths;
	auto totalLength = 0;

	TimeTracker time_tracker;
	time_tracker.start();
	// get segment graph
	std::cout << "  Creating Graph.." << std::endl;
	const TrajectoryGraph traj_graph(graph, data.paths_ds);
	time_tracker.stop();
	std::cout << "  ╙ Graph Creation finished. Took "
	          << time_tracker.getLastMeasurment() << " seconds" << std::endl;

	// write all segements
	time_tracker.start();
	std::cout << "  Exporting to String.." << std::endl;
	std::string result = "{\"trajectorySegmentGraph\": ";
	append_graph_str(&result, traj_graph);
	result += ", \"found\":" + std::to_string(found) +
	          ", \"total\": " + std::to_string(total) +
	          ", \"length\":" + std::to_string(totalLength) + "}";
	time_tracker.stop();
	std::cout << "  String exporting finished. Took "
	          << time_tracker.getLastMeasurment() << " seconds" << std::endl;

	return result;
}

std::size_t Exporter::CoordinateHash::operator()(
    Coordinate const& coordinate) const
{
	auto lat_hash = std::hash<Coordinate::FloatType>{}(coordinate.lat);
	auto lon_hash = std::hash<Coordinate::FloatType>{}(coordinate.lon);
	return lat_hash + lon_hash;
}

std::string Exporter::exportHeatmap(ExportData const& data) const
{

	// The result string

	auto found = data.intersection_paths;
	auto total = data.total_paths;
	auto totalLength = 0;

	TimeTracker time_tracker;
	time_tracker.start();
	std::cout << "  Collecting Nodes.." << std::endl;
	const HeatmapBuilder heatmap_builder(graph, data.paths_ds);
	auto absolute_counter_vec = heatmap_builder.get_absolute_nodes_counter();
	time_tracker.stop();
	std::cout << "  ╙ finished. Took " << time_tracker.getLastMeasurment()
	          << " seconds" << std::endl;

	// write all segements
	time_tracker.start();
	std::cout << "  Exporting to String..";
	std::string result = "{\"trajectoryHeatmap\": ";
	append_heatmap_str(&result, absolute_counter_vec);
	result += ", \"found\":" + std::to_string(found) +
	          ", \"total\": " + std::to_string(total) +
	          ", \"length\":" + std::to_string(totalLength) + "}";
	time_tracker.stop();
	std::cout << "  String exporting finished. Took"
	          << time_tracker.getLastMeasurment() << " seconds" << std::endl;

	return result;
}

void Exporter::append_heatmap_str(
    std::string* json_string, const CounterVector& absolute_counter_vec) const
{
	// load graph components
	auto max_counter = *std::max_element(
	    absolute_counter_vec.begin(), absolute_counter_vec.end());
	float normalization = 1.0f / static_cast<float>(max_counter);

	// contvert to string
	*json_string += "[";

	// find first non-zero entry
	auto first_non_zero = absolute_counter_vec.size();
	int vertex_counter = 0;
	for (size_t i = 0; i < absolute_counter_vec.size(); i++) {
		if (absolute_counter_vec[i] > 0) {
			first_non_zero = i;
			vertex_counter++;
			*json_string += counter_to_string_representation(
			    i, normalization, absolute_counter_vec);
			break;
		}
	}
	for (size_t i = first_non_zero + 1; i < absolute_counter_vec.size(); i++) {
		if (absolute_counter_vec[i] > 0) {
			vertex_counter++;
			*json_string += "," + counter_to_string_representation(
			                          i, normalization, absolute_counter_vec);
		}
	}
	*json_string += "]";
	std::cout << " " << vertex_counter << " vertices written." << std::endl;
}

inline std::string Exporter::counter_to_string_representation(
    const size_t node_index, const float normalization,
    const CounterVector& absolute_counter_vec) const
{
	auto const& node = graph.getNode(node_index);
	// [lat, lon, intensity]
	return "[" + std::to_string(node.coordinate.lat) + "," +
	       std::to_string(node.coordinate.lon) + "," +
	       std::to_string(absolute_counter_vec[node_index] * normalization) +
	       "]";
}

std::string Exporter::export_initial_batch(ExportData const& export_data,
    PathIDs const& found_paths, BatchType const& batch_type) const
{
	if (batch_type == pf::BatchType::PLAIN_INORDER) {
		return get_initial_plain_inorder_batch(export_data, found_paths);
	} else if (batch_type == pf::BatchType::PLAIN_LEVELORDER) {
		return get_initial_plain_levelorder_batch(export_data, found_paths);
	} else if (batch_type == pf::BatchType::EDGE_WISE) {
		return get_initial_edgewise_level_batch(export_data, found_paths);
	}
	std::cout << "ERROR: Unknown Export Type.";
	return "ERROR: Unknown Export Type.";
}

std::string Exporter::get_initial_plain_inorder_batch(
    ExportData const& export_data, PathIDs const& found_paths) const
{
	std::string name = "InitPlainInorderBatch";
	return get_initial_uv_batch(name, export_data, found_paths);
}

std::string Exporter::get_initial_plain_levelorder_batch(
    ExportData const& export_data, PathIDs const& found_paths) const
{
	std::string name = "InitPlainLevelorderBatch";
	return get_initial_uv_batch(name, export_data, found_paths);
}

std::string Exporter::get_initial_edgewise_level_batch(
    ExportData const& export_data, PathIDs const& found_paths) const
{
	std::string name = "InitEdgeWiseBatch";
	return get_initial_uv_batch(name, export_data, found_paths);
}

std::string Exporter::get_initial_uv_batch(std::string batch_name,
    ExportData const& export_data, PathIDs const& found_paths) const
{

	// The result string
	std::string result = "{\"" + batch_name + "\": {";

	auto found = export_data.intersection_paths;
	auto total = export_data.total_paths;
	auto totalLength = 0;

	auto is_first_element = true;
	// Write first and last node of all edges
	for (PathID path_id : found_paths) {
		if (!is_first_element) {
			result += ",";
		}
		is_first_element = false;

		Path const& path = export_data.paths_ds.getPath(path_id);
		// increment lengths
		auto length = path.getLength(graph);
		totalLength += length;

		// extract endpoints
		auto u_id = path.getSource(graph);
		auto v_id = path.getTarget(graph);

		result += "\"" + std::to_string(path_id) + "\":{";
		result += "\"length\":" + std::to_string(path.getLength(graph));
		result += ",\"n\":" + std::to_string(path.getNoOfRootEdges() + 1);
		result += ",\"uv\":[[" + node_to_latlon(u_id) + "],[" +
		          node_to_latlon(v_id) + "]]";
		result += "}";
	}
	result += "}";
	result += ", \"found\":" + std::to_string(found) +
	          ", \"total\": " + std::to_string(total) +
	          ", \"length\":" + std::to_string(totalLength);
	result += "}";
	return result;
}

// add all root edges which are visible and have a certain level
void filter_root_edges_wrt_view_and_level(PathIDs const& found_paths,
    Graph const& graph, Level const cutoff_level, BoundingBox const& view_box,
    PathfinderDS const& p_ds, EdgeIDs& matching_edges, EdgeIDs& removed_edges)
{
	TimeTracker time_tracker;
	time_tracker.start();
	std::cout << "| | Filter wrt view and level" << std::endl;

	// path (-bounding box) is at least partially visible, go over all
	// root-edges and check visibility
	PathsDS paths_ds = p_ds.getPathsDS();

	// iterate over all root paths and copy if visible and above cutoff level
	for (PathID path_id : found_paths) {

		// check path for high-level properties:
		if (not view_box.overlaps_with(p_ds.getPathBox(path_id))) {
			// the hole path is not visible.
			// -> Skip it.
			continue;
		}

		const Path& path = paths_ds.getPath(path_id);
		for (const EdgeID edge_id : path.getEdges()) {
			const Edge edge = graph.getEdge(edge_id);
			auto const source = graph.getNode(edge.source);
			auto const target = graph.getNode(edge.target);

			bool is_visible = view_box.overlaps_with(p_ds.getEdgeBox(edge_id));

			if (not is_visible) {
				// skip nodes which are not visible
				continue;
			}

			bool is_not_below_level =
			    source.level >= cutoff_level or target.level >= cutoff_level;
			if (is_not_below_level) {
				matching_edges.push_back(edge_id);
			} else {
				removed_edges.push_back(edge_id);
			}
		}
	}
	time_tracker.stop();
	std::cout << "| | ↳ Took " << time_tracker.getLastMeasurment() << " seconds"
	          << std::endl;

	time_tracker.start();
	std::cout << "| | Removing duplicates" << std::endl;
	// remove duplicates
	matching_edges.erase(
	    std::unique(matching_edges.begin(), matching_edges.end()),
	    matching_edges.end());
	time_tracker.stop();
	std::cout << "| | ↳ Took " << time_tracker.getLastMeasurment() << " seconds"
	          << std::endl;
}

std::string Exporter::export_prunned_segments(ExportData const& data,
    Options const& options, PathfinderDS const& p_ds,
    GridStack const& grid_stack, LevelExtractor const& level_extractor) const
{
	auto found = data.found_paths.size();
	auto total = data.total_paths;

	TimeTracker time_tracker;

	Level const root_edge_unpack_level =
	    level_extractor.extract_level(options, graph);
	Level const cutoff_level = options.getCutoffLevel();
	BoundingBox const view_box = data.boxes[1];

	std::cout << "| Filter edges" << std::endl;
	time_tracker.start();

	EdgeIDs filtered_root_edges;
	EdgeIDs kept_root_edges;
	filter_root_edges_wrt_view_and_level(data.found_paths, graph, cutoff_level,
	    view_box, p_ds, kept_root_edges, filtered_root_edges);
	time_tracker.stop();
	std::cout << "| ↳ Took " << time_tracker.getLastMeasurment() << " seconds"
	          << std::endl;

	std::cout << "| Convert resultlist to json" << std::endl;
	std::cout << "| | writing high level edges: " << kept_root_edges.size()
	          << std::endl;
	time_tracker.start();
	// ----------- export to json-string ---------------------
	// (1) write high-level edges
	std::string result = "{\"edges\":";
	append_unpacked_edgelist_as_json(
	    &result, kept_root_edges, graph, root_edge_unpack_level);
	time_tracker.stop();
	std::cout << "| | ↳ Took " << time_tracker.getLastMeasurment() << " seconds"
	          << std::endl;

	const GridIndex level = 0;
	Grid grid = grid_stack.get_grid(level);
	CountGrid count_grid = CountGrid(grid);

	std::cout << "| | writing low level edges (heatmap): "
	          << filtered_root_edges.size() << std::endl;
	time_tracker.start();
	// (2) write low-level heatmap
	// work on first edge
	if (filtered_root_edges.size() > 0) {
		auto const& edge_id = filtered_root_edges[0];
		Edge const& edge = graph.getEdge(edge_id);
		count_grid.process_covering(edge.cell_coverings[level]);
	}
	// handle all n-1 other edges
	for (uint i = 1; i < filtered_root_edges.size(); i++) {
		auto const& edge_id = filtered_root_edges[i];
		auto const& edge = graph.getEdge(edge_id);
		count_grid.process_covering(edge.cell_coverings[level]);
	}
	// write heatmap_data:
	const float cutoff_value = 20.0f;
	result += ",";
	GridHeatmapExporter::append_as_json(&result, count_grid, cutoff_value);
	// (3) wirte statistics
	result += ", \"found\":" + std::to_string(found) +
	          ", \"total\":" + std::to_string(total) + ", \"length\": -1}";

	time_tracker.stop();
	std::cout << "| | ↳ Took " << time_tracker.getLastMeasurment() << " seconds"
	          << std::endl;

	return result;
}

std::string Exporter::export_edgeset(ExportData const& data,
    Options const& options, PathfinderDS& p_ds,
    LevelExtractor const& level_extractor, bool local_search,
    bool use_bidirectional_edges, bool use_node_count) const
{
	auto found = data.found_paths.size();
	auto total = data.total_paths;
	TimeTracker time_tracker;

	time_tracker.start();
	// perform unpacking (remove child edges if below threshold, unpack roots if
	// children are below)
	const size_t min_intersections = options.getMinIntersectionCount();
	EdgeIDs export_edges;
	if (local_search) {
		export_edges = sensitive_unpacking::local::unpack_edgeset(
		    data.found_edges, min_intersections, p_ds, use_bidirectional_edges,
		    use_node_count);
	} else {
		export_edges = sensitive_unpacking::global::unpack_edgeset(
		    data.found_edges, min_intersections, p_ds, use_bidirectional_edges);
	}
	time_tracker.stop();
	std::cout << "| unpack sensitively: Took "
	          << time_tracker.getLastMeasurment() << " seconds" << std::endl;

	Level const unpack_level = level_extractor.extract_level(options, graph);
	// ----------- export to json-string ---------------------
	std::cout << "| writing edges to json: " << export_edges.size()
	          << " (unpacking to level " << unpack_level << ")" << std::endl;
	std::string result = "{\"edges\":";

	time_tracker.start();
	result += "[";
	append_unpacked_edgelist_as_json(
	    &result, export_edges, p_ds.getGraph(), unpack_level);
	result += "]";
	time_tracker.stop();

	std::cout << "↳ Took " << time_tracker.getLastMeasurment() << " seconds"
	          << std::endl;

	result += ", \"found\":" + std::to_string(found) +
	          ", \"total\":" + std::to_string(total) + ", \"length\": -1}";

	return result;
}

std::string Exporter::export_weighted_edgeset(ExportData const& data,
    Options const& options, PathfinderDS& p_ds,
    LevelExtractor const& level_extractor, bool local_search,
    bool use_bidirectional_edges, bool use_node_count) const
{
	if (options.useEdgeWeightBinning() & (data.found_edges.size() > 0)) {
		// map each count to one of k bins, group results set
		return _export_binned_weighted_edgeset(data, options, p_ds,
		    level_extractor, local_search, use_bidirectional_edges,
		    use_node_count);
	} else {
		// return each count as it is.
		return _export_weighted_edgeset(data, options, p_ds, level_extractor,
		    local_search, use_bidirectional_edges, use_node_count);
	}
}

std::string Exporter::_export_weighted_edgeset(ExportData const& data,
    Options const& options, PathfinderDS& p_ds,
    LevelExtractor const& level_extractor, bool local_search,
    bool use_bidirectional_edges, bool use_node_count) const
{
	auto found = data.found_edges.size();

	// unpack edges sensitively
	const size_t min_intersections = options.getMinIntersectionCount();

	const sensitive_unpacking::WeightedEdgeset E_in(
	    data.found_edges, data.edge_weights);
	sensitive_unpacking::WeightedEdgeset E_out;

	if (local_search) {
		sensitive_unpacking::local::unpack_weighted_edgeset(E_in, E_out,
		    min_intersections, p_ds, use_bidirectional_edges, use_node_count);
	} else {
		sensitive_unpacking::global::unpack_weighted_edgeset(E_in, E_out,
		    min_intersections, p_ds, use_bidirectional_edges, use_node_count);
	}

	Level const unpack_level = level_extractor.extract_level(options, graph);
	// ----------- export to json-string ---------------------
	std::cout << "| writing edges to json: " << E_out.size()
	          << " (unpacking to level " << unpack_level << ", no binning)"
	          << std::endl;
	std::string result = "{\"weighted_edges\":";

	TimeTracker time_tracker;
	time_tracker.start();
	result += "[";
	append_unpacked_edgelist_as_json(
	    &result, E_out.edges, p_ds.getGraph(), unpack_level);
	result += "]";
	time_tracker.stop();

	result += ", \"weights\" : ";
	append_weight_vector_as_json(result, E_out.weights);

	std::cout << "↳ Took " << time_tracker.getLastMeasurment() << " seconds"
	          << std::endl;

	result += ", \"found\":" + std::to_string(found) + ", \"total\": \"-\"" +
	          ", \"length\": \"-\" }";

	return result;
}

std::string Exporter::_export_binned_weighted_edgeset(ExportData const& data,
    Options const& options, PathfinderDS& p_ds,
    LevelExtractor const& level_extractor, bool local_search,
    bool use_bidirectional_edges, bool use_node_count) const
{
	auto found = data.found_edges.size();

	const size_t min_intersections = options.getMinIntersectionCount();

	const sensitive_unpacking::WeightedEdgeset E_in(
	    data.found_edges, data.edge_weights);
	sensitive_unpacking::WeightedEdgeset E_out;

	if (local_search) {
		sensitive_unpacking::local::unpack_weighted_edgeset(E_in, E_out,
		    min_intersections, p_ds, use_bidirectional_edges, use_node_count);
	} else {
		sensitive_unpacking::global::unpack_weighted_edgeset(
		    E_in, E_out, min_intersections, p_ds, use_bidirectional_edges);
	}

	// sort for binning
	E_out.sortByWeights();

	// find the k start positions for each group
	Level const unpack_level = level_extractor.extract_level(options, graph);
	int bins = options.getWeightedEdgeBinning();
	std::vector<size_t> group_start_indices =
	    fixed_range_nn_grouping(bins, E_out.weights);

	// ----------- export to json-string ---------------------
	std::cout << "| writing edges to json: " << E_out.size()
	          << " (unpacking to level " << unpack_level
	          << ", binning activated: " << bins << ")" << std::endl;
	std::string result = "{\"weighted_binned_edges\":";

	// -- export groupwise
	TimeTracker time_tracker;
	time_tracker.start();
	result += "[";
	// export first k-1 groups
	for (size_t group_nr = 0; group_nr < group_start_indices.size() - 1;
	     group_nr++) {
		auto start_index = group_start_indices[group_nr];
		auto excl_end_index = group_start_indices[group_nr + 1];
		result += "{\"edges\": ";
		append_unpacked_edgelist_as_json(&result, E_out.edges, p_ds.getGraph(),
		    unpack_level, start_index, excl_end_index);
		result += ", \"weight\" : \"" +
		          std::to_string(E_out.weights[start_index]) + "\"";
		result += "},";
	}
	// export last group
	auto start_index = group_start_indices.back();
	auto excl_end_index = E_out.size();
	std::string weight;
	if (start_index > 0) {
		weight = std::to_string(E_out.weights[start_index]);
	} else {
		weight = "[]";
	}
	result += "{\"edges\": ";
	append_unpacked_edgelist_as_json(&result, E_out.edges, p_ds.getGraph(),
	    unpack_level, start_index, excl_end_index);
	result += ", \"weight\" : \"" + weight + "\"";
	result += "}";
	result += "]";

	time_tracker.stop();
	std::cout << "↳ Took " << time_tracker.getLastMeasurment() << " seconds"
	          << std::endl;

	result += ", \"found\":" + std::to_string(found) + ", \"total\": \"-\"" +
	          ", \"length\": \"-\" }";

	return result;
}

std::string Exporter::export_graph_topology(ExportData const& data,
    Options const& options, PathfinderDS& p_ds,
    const pf::LevelExtractor& level_extractor) const
{
	// ----------- export to json-string ---------------------
	std::string result = "{\"edges\":";

	BoundingBox request_bb = data.boxes[0];
	ColorCodedEdgeset color_coded_edgeset;
	EdgeIDs removed_edges;

	EdgeIDs export_edges =
	    pf::graph_segment_exporting::topology::naive::export_graph_section(
	        p_ds, request_bb, options.getGraphLevel(), removed_edges);
	if (options.show_aux_lines()) {
		color_coded_edgeset.auxiliary_shortcuts = removed_edges;
	}

	for (auto edge_id : export_edges) {
		auto& edge = graph.getEdge(edge_id);
		if (edge.isShortcut()) {
			color_coded_edgeset.shortcuts.push_back(edge_id);
		} else {
			if (edge.source < edge.target) {
				color_coded_edgeset.increase_plain.push_back(edge_id);
			} else {
				color_coded_edgeset.decrease_plain.push_back(edge_id);
			}
		}
	}
	/*	size_t no_start_edges = export_edges.size();
	    // -- export groupwise
	    TimeTracker time_tracker;
	    time_tracker.start();
	    result += "[";
	    append_unpacked_edgelist_as_json(
	        &result, export_edges, p_ds.getGraph(), options.getLevel());
	    result += "]";
	    time_tracker.stop();

	    std::cout << "↳ Took " << time_tracker.getLastMeasurment() << " seconds"
	              << std::endl;

	    result += ", \"found\":" + std::to_string(no_start_edges) +
	              ", \"total\":" + std::to_string(export_edges.size()) + ",
	   \"length\": -1}";

	    return result;*/

	return export_edgeset_color_coded(color_coded_edgeset, p_ds,
	    level_extractor.extract_level(options, graph));
}

std::string Exporter::export_full_graph_view(ExportData const& data,
    Options const& options, PathfinderDS& p_ds,
    const pf::LevelExtractor& level_extractor) const
{
	// ----------- export to json-string ---------------------
	std::string result = "{\"edges\":";

	BoundingBox request_bb = data.boxes[0];
	ColorCodedEdgeset color_coded_edgeset;

	EdgeIDs removed_edges;
	EdgeIDs export_edges =
	    pf::graph_segment_exporting::full::export_graph_section(
	        p_ds, request_bb, options.getGraphLevel(), removed_edges);

	if (options.show_aux_lines()) {
		color_coded_edgeset.auxiliary_shortcuts = removed_edges;
	}

	// sort edges w.r.t. type
	for (auto& e_id : export_edges) {
		const Edge& edge = graph.getEdge(e_id);
		if (edge.source < edge.target) {
			color_coded_edgeset.increase_plain.push_back(e_id);
		} else {
			color_coded_edgeset.decrease_plain.push_back(e_id);
		}
	}

	return export_edgeset_color_coded(color_coded_edgeset, p_ds,
	    level_extractor.extract_level(options, graph));
}

} // namespace pf