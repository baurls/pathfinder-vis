#pragma once

#include <vector>

namespace pf
{

namespace unit_tests
{
void testFlags();
void testSimpleFlags();
} // namespace unit_tests

// TODO: make abstract flag type class

template <typename FlagType>
class Flags
{
private:
	using Index = std::size_t;

public:
	Flags(std::size_t size, FlagType default_value);

	void set(Index index, FlagType value);
	void reset();
	std::size_t size() const;

	FlagType operator[](Index index) const;

private:
	FlagType default_value;

	std::vector<FlagType> flags;
	std::vector<Index> changed_flags;

	friend void unit_tests::testFlags();
};

template <typename FlagType>
Flags<FlagType>::Flags(std::size_t size, FlagType default_value)
    : default_value(default_value), flags(size, default_value)
{
}

template <typename FlagType>
void Flags<FlagType>::set(Index index, FlagType value)
{
	if (flags[index] == default_value && value != default_value) {
		changed_flags.push_back(index);
	}

	flags[index] = value;
}

template <typename FlagType>
void Flags<FlagType>::reset()
{
	for (auto index : changed_flags) {
		flags[index] = default_value;
	}

	changed_flags.clear();
}

template <typename FlagType>
std::size_t Flags<FlagType>::size() const
{
	return flags.size();
}

template <typename FlagType>
FlagType Flags<FlagType>::operator[](Index index) const
{
	return flags[index];
}

template <typename FlagType>
class SimpleFlags
{
private:
	using Index = std::size_t;

public:
	SimpleFlags(std::size_t size, FlagType default_value);

	void set(Index index, FlagType value, std::vector<Index>& changed_flags);
	void reset(std::vector<Index>& changed_flags);
	std::size_t size() const;

	FlagType operator[](Index index) const;

private:
	FlagType default_value;

	std::vector<FlagType> flags;

	friend void unit_tests::testSimpleFlags();
};

template <typename FlagType>
SimpleFlags<FlagType>::SimpleFlags(std::size_t size, FlagType default_value)
    : default_value(default_value), flags(size, default_value)
{
}

template <typename FlagType>
void SimpleFlags<FlagType>::set(
    Index index, FlagType value, std::vector<Index>& changed_flags)
{
	if (flags[index] == default_value && value != default_value) {
		changed_flags.push_back(index);
	}

	flags[index] = value;
}

template <typename FlagType>
void SimpleFlags<FlagType>::reset(std::vector<Index>& changed_flags)
{
	for (auto index : changed_flags) {
		flags[index] = default_value;
	}

	changed_flags.clear();
}

template <typename FlagType>
std::size_t SimpleFlags<FlagType>::size() const
{
	return flags.size();
}

template <typename FlagType>
FlagType SimpleFlags<FlagType>::operator[](Index index) const
{
	return flags[index];
}

} // namespace pf
