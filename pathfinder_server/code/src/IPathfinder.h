#pragma once

#include "basic_types.h"
#include "defs.h"
#include "paths.h"
#include "query.h"
#include "stxxl_tracker.h"
#include "time_tracker.h"

#include <map>

namespace pf
{

/*
 * Interface of a Pathfinder.
 */
class IPathfinder
{
public:
	/*
	 * Retrieves all path ids for the given query
	 */
	virtual PathIDs run(Query const& query) = 0;

	virtual void printTimes()
	{
		// Do nothing;
	}

	virtual std::map<Substep, TimeTracker> getSubstepTimeTrackers()
	{
		// return empty map
		std::map<Substep, TimeTracker> substepTimeTrackers;
		return substepTimeTrackers;
	}

	virtual StxxlTracker* getStxxlTracker()
	{
		return nullptr;
	}

	virtual ~IPathfinder()
	{
	}

	virtual std::size_t getNofEdgesConsidered()
	{
		return 0;
	}
};
} // namespace pf
