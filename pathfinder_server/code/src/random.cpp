#include "random.h"

#include "defs.h"
#include "timeSlots.h"

#include <chrono>

namespace pf
{

Random::Random()
{
	seed_value = std::chrono::system_clock::now().time_since_epoch().count();
	generator.seed(seed_value);
}

Random::Random(unsigned int seed)
{
	seed_value = seed;
	generator.seed(seed_value);
}

void Random::seed(unsigned int seed)
{
	seed_value = seed;
	generator.seed(seed_value);
}

unsigned int Random::getSeed() const
{
	return seed_value;
}

NodeID Random::getNode(Graph const& graph)
{
	if (graph.getNumberOfNodes() == 0) {
		return c::NO_NID;
	}

	std::uniform_int_distribution<NodeID> distribution(
	    0, graph.getNumberOfNodes() - 1);

	return distribution(generator);
}

EdgeID Random::getEdge(Graph const& graph)
{
	if (graph.getNumberOfEdges() == 0) {
		return c::NO_EID;
	}

	std::uniform_int_distribution<EdgeID> distribution(
	    0, graph.getNumberOfEdges() - 1);

	return distribution(generator);
}

EdgeID Random::getNonShortcutOutgoingEdge(Graph const& graph, NodeID node_id)
{
	EdgeIDs nonShortcutOutEdges = graph.getNonShortcutOutEdgesOf(node_id);
	if (nonShortcutOutEdges.size() == 0) {
		return c::NO_EID;
	}

	std::uniform_int_distribution<std::size_t> distribution(
	    0, nonShortcutOutEdges.size() - 1);

	auto index = distribution(generator);

	return nonShortcutOutEdges.at(index);
}

BoundingBox Random::getBox(Graph const& graph)
{
	auto node_id1 = getNode(graph);
	auto node_id2 = getNode(graph);
	auto const node1 = graph.getNode(node_id1);
	auto const node2 = graph.getNode(node_id2);

	return BoundingBox(node1, node2);
}

BoundingBox Random::getBox(Graph const& graph, double width, double height)
{
	auto node_id = getNode(graph);
	auto const node = graph.getNode(node_id);

	Coordinate coordinate1 = Coordinate(
	    node.coordinate.lat - height / 2, node.coordinate.lon - width / 2);
	Coordinate coordinate2 =
	    Coordinate(coordinate1.lat + height, coordinate1.lon + width);

	return BoundingBox(coordinate1, coordinate2);
}

TimeIntervall Random::getIntervall()
{
	TimeType start = getInt(pam::reasonable_min_start_time, pam::max_end_time);
	TimeType end = start + getInt(0, 60 * 60 * 24 * 7); // up to one week
	//	TimeType end = start + getInt(0, 2629743); // up to one month

	return TimeIntervall(start, end);
}

TimeSlots Random::getSingleTimeSlot()
{
	TimeSlots timeSlots(false);

	int single_slot = getInt(0, TimeSlots::nofslots);
	timeSlots.slots[single_slot] = 1;

	return timeSlots;
}

TimeSlots Random::getTimeSlots()
{
	TimeSlots timeSlots(false);

	// we do not cover all slots with this approach so we call it "rough"
	int rough_nof_slots_for_a_weekday = TimeSlots::nofslots / 7;
	int week_day = getInt(0, 7);
	int slot_begin = week_day * rough_nof_slots_for_a_weekday;
	int slot_end = slot_begin + rough_nof_slots_for_a_weekday;

	for (int i = slot_begin; i < slot_end; i++) {
		timeSlots.slots[i] = 1;
	}
	return timeSlots;
}

Course Random::getCourse()
{
	auto lat = getFloat(0, 1);
	auto lon = getFloat(0, 1);

	return Course(lat, lon);
}

int Random::getInt(int lower_bound, int upper_bound)
{
	assert(upper_bound >= lower_bound);

	std::uniform_int_distribution<int> distribution(
	    lower_bound, upper_bound - 1);

	return distribution(generator);
}

float Random::getFloat(float lower_bound, float upper_bound)
{
	assert(upper_bound >= lower_bound);

	std::uniform_real_distribution<float> distribution(
	    lower_bound, upper_bound);

	return distribution(generator);
}

} // namespace pf
