#include "grid.h"
#include "defs.h"
#include <math.h>
#include "basic_types.h"
#include "grid_heatmap.h"

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{
	// --- GridHeatmapExporter ----------

	void GridHeatmapExporter::append_as_json(std::string* json, const CountGrid& count_grid, const float cutoff_value){
		*json += "\"heatmap\":[";
		// handle first match
		GlobalGridIndex stop_index;
		for(GlobalGridIndex i = 0; i < count_grid.count_vector.size(); i++){
			if(count_grid.count_vector[i] > 0){
				const float count = std::min(static_cast<float>(count_grid.count_vector[i]) / cutoff_value, 1.0f);
				const Coordinate center_coord = count_grid.get_cell_center(i);
				*json += "[" + std::to_string(center_coord.lat) + "," + std::to_string(center_coord.lon) + "," + std::to_string(count) + "]";

				// break and store last match's index.
				stop_index = i;
				break;
			}
		}
		// handle all other matches (continue in vector)
		for(GlobalGridIndex i = stop_index +1; i < count_grid.count_vector.size(); i++){
			if(count_grid.count_vector[i] > 0){
				const float count = std::min(static_cast<float>(count_grid.count_vector[i]) / cutoff_value, 1.0f);
				const Coordinate center_coord = count_grid.get_cell_center(i);
				*json += ",[" + std::to_string(center_coord.lat) + "," + std::to_string(center_coord.lon) + "," + std::to_string(count) + "]";
			}
		}
		

		*json += "]";
	}

} // namespace pf
