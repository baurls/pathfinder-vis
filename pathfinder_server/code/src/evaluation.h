#pragma once
#include "basic_types.h"
#include "exporter.h"
#include "pathfinder.h"
#include "pathfinder_ds.h"
#include "random.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, Master Thesis 2021.    #
    #                                                 #
    #   Supervisor: T. Rupp, Prof. S. Funke           #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

static Random random;

class Evaluator
{
public:
	Evaluator(PathfinderDS& p_ds, Pathfinder& p);
	std::string evaluate_experiment(int number) const;

private:
	PathfinderDS& p_ds;
	Exporter exporter;
	Pathfinder& pathfinder;

	std::string experiment_no_1() const;
	std::string experiment_export_segment_graph() const;

	// util functions
	PathIDs _get_random_path_result_on_level(int power) const;
};

} // namespace pf
