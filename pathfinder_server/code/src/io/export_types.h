#pragma once

#include "../bounding_box.h"
#include "../paths.h"
#include "../paths_ds.h"

#include <cstdint>
#include <limits>
#include <ostream>
#include <vector>

namespace pf
{

// The colors vectors have to have the same size as the vectors for the objects.
struct ExportData {
	std::string filename;

	BoundingBoxes boxes;

	// contains a copy of the (unpacked) paths (local ids)
	PathsDS paths_ds;

	// used global ids
	PathIDs found_paths;
	EdgeIDs found_edges;
	Counter edge_weights;

	long total_paths;
	long intersection_paths;

	ExportData() = default;
	ExportData(const ExportData&) = default;
	ExportData& operator=(const ExportData&) = default;

	virtual ~ExportData()
	{
	}
};

} // namespace pf
