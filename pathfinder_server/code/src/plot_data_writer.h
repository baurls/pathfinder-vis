#pragma once

#include "basic_types.h"
#include "time_tracker.h"
#include <fstream>
#include <ios>

namespace pf
{

class PlotDataWriter
{
public:
	PlotDataWriter();

	void writePathSizes(
	    const PathsSizes& paths_sizes, std::string paths_filename) const;

private:
	void writePathSizes(PathSizes const& path_sizes, std::ofstream& f) const;
};

PlotDataWriter::PlotDataWriter()
{
}

void PlotDataWriter::writePathSizes(
    const PathsSizes& paths_sizes, std::string paths_filename) const
{
	std::string out_filename = paths_filename + "_pathSizes";
	std::ofstream f(out_filename);
	if (!f.is_open()) {
		throw Exception("The file " + out_filename + " could not be opened.");
	}

	TimeTracker time_tracker;

	Print("Write paths data sizes ...");
	time_tracker.start();

	for (PathSizes const& path_sizes : paths_sizes) {
		writePathSizes(path_sizes, f);
	}

	time_tracker.stop();
	Print("Took " << time_tracker.getLastMeasurment() << " seconds");
}

void PlotDataWriter::writePathSizes(
    PathSizes const& path_sizes, std::ofstream& f) const
{
	f << path_sizes.original_size;
	f << " ";
	f << path_sizes.compressed_size;
	f << "\n";
}

} // namespace pf
