#pragma once

#include "basic_types.h"
#include "defs.h"
#include "options.h"
#include "paths.h"
#include "query.h"
#include "time_tracker.h"

#include <map>

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, Master Thesis 2021.    #
    #                                                 #
    #   Supervisor: T. Rupp, Prof. S. Funke           #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

/*
 * Util function to get the dynamic level based on the zoom.
 */
class LevelExtractor
{
public:
	/*
	 * Retrieves all path ids for the given query
	 */
	LevelExtractor();
	LevelExtractor(const double smoothness, const double max_zoom);
	pf::Level extract_level(
	    pf::Options const& query_options, pf::Graph const& graph) const;

	double smoothness;
	double max_zoom;
};
} // namespace pf
