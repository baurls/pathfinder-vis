#include "level_extractor.h"
#include <string>

// Most of the code extracted from main.cpp file, created by the Project-Inf
// team members.

namespace pf
{

LevelExtractor::LevelExtractor()
{
}
LevelExtractor::LevelExtractor(
    const double in_smoothness, const double in_max_zoom)
{
	smoothness = in_smoothness;
	max_zoom = in_max_zoom;
}

pf::Level LevelExtractor::extract_level(
    pf::Options const& query_options, pf::Graph const& graph) const
{
	// Retrieve the graph max_level
	pf::Level level = 0;

	if (query_options.getLevel() != -1) {
		// Unpack the paths to a set level
		level = query_options.getLevel();
		// Check if the set level is greater than the ch max level
		if (level > graph.getMaxLevel()) {
			level = graph.getMaxLevel();
		}
		return level;
	}
	// Unpack the paths to a level corresponding to the map zoom level.
	pf::Level max_level = graph.getMaxLevel();
	int zoom = query_options.getZoom();

	if (zoom < 0) {
		// Zoomlevel is invalid, don't unpack the paths at all.
		return level;
	}

	if (zoom == 0) {
		level = max_level;
	} else {
		double a = 1 / (double)max_level;
		double b = log(max_level / smoothness) / log(max_zoom);

		double level_double = 1 / (a * pow(zoom, b));
		level = floor(level_double);
	}
	return level;
}

} // namespace pf
