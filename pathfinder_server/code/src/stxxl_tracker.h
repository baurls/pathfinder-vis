#pragma once

#include "basic_types.h"
#include "defs.h"

#include <map>
#include <stxxl/vector>
#include <vector>

namespace pf
{

using Measurement = std::map<StxxlStatValue, double>;

class StxxlTracker
{
private:
	std::vector<Measurement> measurements;

	stxxl::stats_data stats_begin;

public:
	StxxlTracker()
	{
	}

	void start()
	{
		stxxl::stats* Stats = stxxl::stats::get_instance();
		stats_begin = stxxl::stats_data(*Stats);
	}

	void stop()
	{
		stxxl::stats* Stats = stxxl::stats::get_instance();
		stxxl::stats_data now = stxxl::stats_data(*Stats);
		stxxl::stats_data diff = now - stats_begin;
		Measurement measurement;
		measurement.insert(
		    std::make_pair(StxxlStatValue::Nof_bytes_read_from_disk,
		        (double)diff.get_read_volume() / (1E6)));
		measurement.insert(std::make_pair(
		    StxxlStatValue::Read_time, (double)diff.get_read_time()));

		measurements.emplace_back(measurement);
	}

	Measurement getLastMeasurment()
	{
		assert(!measurements.empty());
		return measurements.back();
	}

	void clear()
	{
		measurements.clear();
	}

	Measurement calculateMean() const
	{
		Measurement sum;
		for (StxxlStatValue stxxlStatValue : allStxxlStatValues) {
			sum.insert(std::make_pair(stxxlStatValue, 0));
		}

		for (Measurement const& measurement : measurements) {
			for (StxxlStatValue stxxlStatValue : allStxxlStatValues) {
				double new_value =
				    sum.at(stxxlStatValue) + measurement.at(stxxlStatValue);
				sum.erase(stxxlStatValue);
				sum.insert(std::make_pair(stxxlStatValue, new_value));
			}
		}

		Measurement mean;
		for (StxxlStatValue stxxlStatValue : allStxxlStatValues) {
			double mean_value = sum.at(stxxlStatValue) / measurements.size();
			mean.insert(std::make_pair(stxxlStatValue, mean_value));
		}

		return mean;
	}

	void printSummary()
	{

		Measurement mean;
		Measurement median;
		Measurement stddev;

		if (measurements.empty()) {
			Print("No measurements taken, and therefore no summary necessary.");
			return;
		}

		if (measurements.size() == 1) {
			mean = measurements.front();
			Print("Mean of megabytes read from disk of experiment: " << mean.at(
			          StxxlStatValue::Nof_bytes_read_from_disk));
			Print("Time spent reading from disk of experiment: "
			      << mean.at(StxxlStatValue::Read_time) << " s");
			return;
		}

		mean = calculateMean();

		Print("Mean number of megabytes read from disk of experiment: "
		      << mean.at(StxxlStatValue::Nof_bytes_read_from_disk));
		Print("Time spent reading from disk of experiment: "
		      << mean.at(StxxlStatValue::Read_time) << " s");

		// Avoid unused warnings for non-verbose compilation.
		Unused(mean);
		Unused(median);
		Unused(stddev);
	}
};

} // namespace pf
