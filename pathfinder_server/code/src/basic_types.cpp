#include "basic_types.h"
#include <iomanip>

namespace pf
{

auto COORDINATE_PRECISION = 20;
auto DEFAULT_PRECISION = 6;

std::string getTextForSubstep(Substep substep)
{
	switch (substep) {
	case Substep::Get_top_nodes:
		return "a) get_top_nodes";
	case Substep::Find_edge_candidates:
		return "b) find_edge_candidates";
	case Substep::Refine_candidates:
		return "c) Refine_candidates";
	case Substep::Sort_edges:
		return "d) Sort_edges";
	case Substep::Get_path_ids:
		return "e) Get_path_ids";
	case Substep::Remove_duplicate_path_ids:
		return "f) Remove_duplicate_path_ids";
	case Substep::Reset_Collected:
		return "g) Reset_Collected";
	default:
		return "Not recognized..";
	}
}

std::string getTextForStxxlStatValue(StxxlStatValue stxxlStatValue)
{
	switch (stxxlStatValue) {
	case StxxlStatValue::Nof_bytes_read_from_disk:
		return "nof read megabytes from disk";
	case StxxlStatValue::Read_time:
		return "read time";
	default:
		return "Not recognized..";
	}
}

Direction operator!(Direction direction)
{
	return Direction(!(bool)direction);
}

std::ostream& operator<<(std::ostream& stream, Coordinate const& coordinate)
{
	stream << std::fixed << std::setprecision(COORDINATE_PRECISION);
	stream << "lat: " << coordinate.lat << ", lon: " << coordinate.lon;
	stream << std::setprecision(DEFAULT_PRECISION) << std::defaultfloat;
	return stream;
}

std::ostream& operator<<(std::ostream& stream, Edge const& edge)
{
	stream << "id: " << edge.id << ", source: " << edge.source
	       << ", target: " << edge.target << ", length: " << edge.length
	       << ", type: " << edge.type << ", speed: " << edge.speed
	       << ", child1: " << edge.child_edge1
	       << ", child2: " << edge.child_edge2;

	return stream;
}

std::ostream& operator<<(std::ostream& stream, Node const& node)
{
	stream << "id: " << node.id << ", osm_id: " << node.osm_id
	       << ", coordinate: " << node.coordinate
	       << ", elevation: " << node.elevation << ", level: " << node.level;

	return stream;
}

std::ostream& operator<<(std::ostream& stream, PathSizes const& path_sizes)
{
	stream << "original: " << path_sizes.original_size
	       << ", compressed: " << path_sizes.compressed_size;

	return stream;
}

std::ostream& operator<<(
    std::ostream& stream, CellCovering const& cell_covering)
{
	if (cell_covering.size() == 0) {
		stream << "CellCovering=[]";
		return stream;
	}
	stream << "CellCovering=[" << cell_covering[0];
	for (uint32_t i = 1; i < cell_covering.size(); i++) {
		stream << ", " << cell_covering[i];
	}

	stream << "]";
	return stream;
}

} // namespace pf
