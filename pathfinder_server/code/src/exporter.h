#pragma once

#include "flags.h"
#include "geometry.h"
#include "graph.h"
#include "grid_stack.h"
#include "heatmap_builder.h"
#include "io/export_types.h"
#include "level_extractor.h"
#include "options.h"
#include "pathfinder_ds.h"
#include "segment_splitter.h"

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

namespace pf
{

using EdgeMap = std::unordered_map<EdgeID, EdgeID>;

namespace unit_tests
{
void testExporter();
} // namespace unit_tests

class Exporter
{
public:
	Exporter(Graph const& graph);

	// original exporting
	std::string exportTrajectories(
	    ExportData const& data, Options const& options) const;
	// added exporting functionality
	std::string exportTrajectoriesEdgeIndexed(
	    ExportData const& data, Options const& options) const;
	std::string exportSegmentGraph(ExportData const& data) const;
	std::string exportHeatmap(ExportData const& data) const;
	std::string export_initial_batch(ExportData const& data,
	    PathIDs const& found_paths, BatchType const& batch_type) const;
	std::string export_prunned_segments(ExportData const& data,
	    Options const& options, PathfinderDS const& p_ds,
	    GridStack const& grid_stack,
	    LevelExtractor const& level_extractor) const;
	std::string export_edgeset(ExportData const& data, Options const& options,
	    PathfinderDS& p_ds, LevelExtractor const& level_extractor,
	    bool local_search = false, bool use_bidirectional_edges = true,
	    bool use_node_count = false) const;
	std::string export_weighted_edgeset(ExportData const& data,
	    Options const& options, PathfinderDS& p_ds,
	    LevelExtractor const& level_extractor, bool local_search = false,
	    bool use_bidirectional_edges = false,
	    bool use_node_count = false) const;
	std::string export_full_graph_view(ExportData const& data,
	    Options const& options, PathfinderDS& p_ds,
	    const pf::LevelExtractor& level_extractor) const;
	std::string export_graph_topology(ExportData const& data,
	    Options const& options, PathfinderDS& p_ds,
	    const pf::LevelExtractor& level_extractor) const;

private:
	Graph const& graph;
	Geometry geometry;
	struct CoordinateHash {
		std::size_t operator()(Coordinate const& coordinate) const;
	};

	// "to string" helpers
	std::string node_to_latlon(const NodeID) const;
	std::string segment_to_nodelist(
	    const TGEdge&, const TGSegments&, const TGNodes&) const;
	std::string counter_to_string_representation(const size_t node_index,
	    const float normalization,
	    const CounterVector& absolute_counter_vec) const;
	std::string trajlist_to_str(TrajList& trajecory_list) const;
	std::string edgelist_to_str(TGEdgeIDs& edges) const;
	void append_segment_str(std::string*, const TGEdge&, const TGSegments&,
	    const TGNodes&, TrajectoryDFA&) const;
	void append_graph_str(
	    std::string* json_string, const TrajectoryGraph&) const;
	void append_node_str(std::string* json_string, TGNode& node) const;
	void append_heatmap_str(std::string* json_string,
	    const CounterVector& absolute_counter_vec) const;

	// batches
	std::string get_initial_plain_inorder_batch(
	    ExportData const& data, PathIDs const& found_paths) const;
	std::string get_initial_plain_levelorder_batch(
	    ExportData const& data, PathIDs const& found_paths) const;
	std::string get_initial_edgewise_level_batch(
	    ExportData const& data, PathIDs const& found_paths) const;
	std::string get_initial_uv_batch(std::string name, ExportData const& data,
	    PathIDs const& found_paths) const;

	// weigted edges
	std::string _export_weighted_edgeset(ExportData const& data,
	    Options const& options, PathfinderDS& p_ds,
	    LevelExtractor const& level_extractor, bool local_search,
	    bool use_bidirectional_edges, bool use_node_count) const;
	std::string _export_binned_weighted_edgeset(ExportData const& data,
	    Options const& options, PathfinderDS& p_ds,
	    LevelExtractor const& level_extractor, bool local_search,
	    bool use_bidirectional_edges, bool use_node_count) const;

	// util methods
	void convert_to_indexed_edgelist(const PathsDS& paths_ds,
	    EdgeIDs& all_edges, std::vector<EdgeIDs>& new_edgelists) const;
};

struct ColorCodedEdgeset {
	EdgeIDs shortcuts;
	EdgeIDs increase_plain; // edges with source < target
	EdgeIDs decrease_plain; // edges with source > target
	EdgeIDs auxiliary_shortcuts;

public:
	size_t size() const
	{
		return shortcuts.size() + increase_plain.size() +
		       decrease_plain.size() + auxiliary_shortcuts.size();
	}
};

} // namespace pf
