#include "graph.h"

#include "defs.h"
#include "parser.h"
#include "paths.h"
#include "r_tree.h"
#include "time_tracker.h"

#include <algorithm>
#include <iterator>
#include <numeric>
#include <queue>

namespace pf
{

void Graph::init(
    std::string const& filename, bool resort_IDs_for_spatial_locality)
{
	// those vector will be filled by the parser
	std::vector<Node> nodes_from_parser;
	std::vector<Edge> edges_from_parser;

	TimeTracker time_tracker;
	time_tracker.start();

	Parser parser(nodes_from_parser, edges_from_parser);
	bool success = parser.parse(filename);

	if (!success) {
		throw Exception(
		    "Parsing of the following graph file failed:\n" + filename);
	}

	Print("| Load file..");

	// The max level for this graph
	max_level = c::NO_LEVEL;
	for (Node const& node : nodes_from_parser) {
		if (node.level > max_level) {
			max_level = node.level;
		}
	}
	time_tracker.stop();
	Print("| ↳ Parsing finished: max_level: " << max_level);
	Print("| ↳ Took " << time_tracker.getLastMeasurment() << " seconds");

	time_tracker.start();
	Print("| Executing 'build()' function...");
	build(std::move(nodes_from_parser), std::move(edges_from_parser),
	    resort_IDs_for_spatial_locality);
	time_tracker.stop();
	Print("| ↳ Took " << time_tracker.getLastMeasurment() << " seconds");

	time_tracker.start();
	Print("| Checking graph..");
	check();
	time_tracker.stop();
	Print("| ↳ Took " << time_tracker.getLastMeasurment() << " seconds");
}

std::size_t Graph::getNumberOfNodes() const
{
	return nodes.size();
}

std::size_t Graph::getNumberOfEdges() const
{
	return out_edges.size();
}

Nodes const& Graph::getAllNodes() const
{
	return nodes;
}

Edges const& Graph::getAllEdges() const
{
	return out_edges;
}

Edges const& Graph::getLevelorderedEdges() const
{
	return level_ordered_edges;
}

Level const& Graph::getMaxLevel() const
{
	return max_level;
}

Node const& Graph::getNode(NodeID node_id) const
{
	return nodes[node_id];
}

Edge const& Graph::getEdge(EdgeID edge_id) const
{
	return out_edges[id_to_index[edge_id]];
}

void Graph::set_edge_cell_covering(EdgeID edge_id, CellCoverings cell_coverings)
{
	out_edges[id_to_index[edge_id]].cell_coverings = cell_coverings;
}

std::vector<std::size_t> const& Graph::getLevelsortedEdgeIndexMap() const
{
	return id_to_levelordered_index;
}

EdgeID Graph::getUniqueUnidirectionalEdge(EdgeID edge_id) const
{
	return uniqueUnidirectionalMap[edge_id];
}

EdgeRange<> Graph::getOutEdgesOf(NodeID node_id) const
{
	auto begin = out_edges.begin() + out_offsets[node_id];
	auto end = out_edges.begin() + out_offsets[node_id + 1];

	return EdgeRange(begin, end);
}

EdgeIDs Graph::getNonShortcutOutEdgesOf(NodeID node_id) const
{
	std::vector<EdgeID> non_shortcut_out_edges;

	for (Edge edge : getOutEdgesOf(node_id)) {
		assert(getEdge(edge.id).id == edge.id);

		if (!edge.isShortcut()) {
			non_shortcut_out_edges.push_back(edge.id);
		}
	}
	return non_shortcut_out_edges;
}

EdgeRange<> Graph::getInEdgesOf(NodeID node_id) const
{
	auto begin = in_edges.begin() + in_offsets[node_id];
	auto end = in_edges.begin() + in_offsets[node_id + 1];

	return EdgeRange(begin, end);
}

EdgeRange<> Graph::getEdgesOf(NodeID node_id, Direction direction) const
{
	return direction == Direction::Forward ? getOutEdgesOf(node_id)
	                                       : getInEdgesOf(node_id);
}

EdgeRange<> Graph::getUpEdgesOf(NodeID node_id, Direction direction) const
{
	EdgeRange<>::iterator begin;
	EdgeRange<>::iterator end;

	if (direction == Direction::Forward) {
		begin = out_edges.begin() + out_offsets[node_id];
		end = out_edges.begin() + out_up_edges_end[node_id];
	} else {
		begin = in_edges.begin() + in_offsets[node_id];
		end = in_edges.begin() + in_up_edges_end[node_id];
	}

	return EdgeRange(begin, end);
}

EdgeRange<> Graph::getDownEdgesOf(NodeID node_id, Direction direction) const
{
	EdgeRange<>::iterator begin;
	EdgeRange<>::iterator end;

	if (direction == Direction::Forward) {
		begin = out_edges.begin() + out_up_edges_end[node_id];
		end = out_edges.begin() + out_offsets[node_id + 1];
	} else {
		begin = in_edges.begin() + in_up_edges_end[node_id];
		end = in_edges.begin() + in_offsets[node_id + 1];
	}

	return EdgeRange(begin, end);
}

EdgeRange<> Graph::getNonObsoleteDownEdgesOf(
    NodeID node_id, Direction direction) const
{
	assert(isSortedWRTEdgesStatus());

	EdgeRange<>::iterator begin;
	EdgeRange<>::iterator end;

	if (direction == Direction::Forward) {
		begin = out_edges.begin() + out_up_edges_end[node_id];
		end = out_edges.begin() + out_non_obsolete_down_edges_end[node_id];
	} else {
		begin = in_edges.begin() + in_up_edges_end[node_id];
		end = in_edges.begin() + in_non_obsolete_down_edges_end[node_id];
	}

	return EdgeRange(begin, end);
}

EdgeRange<> Graph::getTreeDownEdgesOf(NodeID node_id, Direction direction) const
{
	assert(isSortedWRTEdgesStatus());

	EdgeRange<>::iterator begin;
	EdgeRange<>::iterator end;

	if (direction == Direction::Forward) {
		begin = out_edges.begin() + out_up_edges_end[node_id];
		end = out_edges.begin() + out_tree_down_edges_end[node_id];
	} else {
		begin = in_edges.begin() + in_up_edges_end[node_id];
		end = in_edges.begin() + in_tree_down_edges_end[node_id];
	}

	return EdgeRange(begin, end);
}

EdgeRange<> Graph::getNonObsoleteNonTreeDownEdgesOf(
    NodeID node_id, Direction direction) const
{
	assert(isSortedWRTEdgesStatus());

	EdgeRange<>::iterator begin;
	EdgeRange<>::iterator end;

	if (direction == Direction::Forward) {
		begin = out_edges.begin() + out_tree_down_edges_end[node_id];
		end = out_edges.begin() + out_non_obsolete_down_edges_end[node_id];
	} else {
		begin = in_edges.begin() + in_tree_down_edges_end[node_id];
		end = in_edges.begin() + in_non_obsolete_down_edges_end[node_id];
	}

	return EdgeRange(begin, end);
}

std::size_t Graph::getNumberOfOutEdges(NodeID node_id) const
{
	return out_offsets[node_id + 1] - out_offsets[node_id];
}

std::size_t Graph::getNumberOfInEdges(NodeID node_id) const
{
	return in_offsets[node_id + 1] - in_offsets[node_id];
}

std::size_t Graph::getNumberOfEdges(NodeID node_id, Direction direction) const
{
	return direction == Direction::Forward ? getNumberOfOutEdges(node_id)
	                                       : getNumberOfInEdges(node_id);
}

std::size_t Graph::getNumberOfEdges(NodeID node_id) const
{
	return getNumberOfOutEdges(node_id) + getNumberOfInEdges(node_id);
}

bool Graph::isUpEdge(EdgeID edge_id, Direction direction) const
{
	auto const& edge = getEdge(edge_id);
	return isUpEdge(edge, direction);
}

bool Graph::isUpEdge(Edge const& edge, Direction direction) const
{
	auto source_level = nodes[edge.source].level;
	auto target_level = nodes[edge.target].level;

	if (direction == Direction::Forward) {
		return source_level < target_level;
	} else {
		return source_level > target_level;
	}
}

bool Graph::isDownEdge(EdgeID edge_id, Direction direction) const
{
	return !isUpEdge(edge_id, direction);
}

NodeID Graph::getLowerNode(EdgeID edge_id) const
{
	auto const& edge = getEdge(edge_id);
	auto source_level = getNode(edge.source).level;
	auto target_level = getNode(edge.target).level;

	assert(source_level != target_level);
	return source_level < target_level ? edge.source : edge.target;
}

EdgeIDs Graph::getParents(EdgeID edge_id) const
{
	EdgeIDs parents;

	auto const& edge = getEdge(edge_id);

	for (auto const& out_edge : getOutEdgesOf(edge.source)) {
		if (out_edge.child_edge1 == edge_id) {
			parents.push_back(out_edge.id);
		}
	}

	for (auto const& in_edge : getInEdgesOf(edge.target)) {
		if (in_edge.child_edge2 == edge_id) {
			parents.push_back(in_edge.id);
		}
	}

	return parents;
}

NodeID Graph::getShortcutNode(const EdgeID edge_id) const
{
	auto const& edge = getEdge(edge_id);
	return getShortcutNode(edge);
}

NodeID Graph::getShortcutNode(const Edge& edge) const
{
	if (!edge.isShortcut()) {
		return c::NO_NID;
	}
	auto const& child_edge = getEdge(edge.child_edge1);
	return child_edge.target;
}

Level Graph::getShortcutLevel(EdgeID edge_id) const
{
	auto const& edge = getEdge(edge_id);
	return getShortcutLevel(edge);
}

Level Graph::getShortcutLevel(const Edge& edge) const
{
	auto shortcut_node_id = getShortcutNode(edge);

	if (shortcut_node_id == c::NO_NID) {
		return c::NO_LEVEL;
	} else {
		auto const& shortcut_node = getNode(shortcut_node_id);
		return shortcut_node.level;
	}
}

Path Graph::unpack(EdgeID edge_id) const
{
	Path path;
	EdgeIDs todo = {edge_id};

	while (!todo.empty()) {
		auto const& current_edge = getEdge(todo.back());
		todo.pop_back();

		if (current_edge.isShortcut()) {
			todo.push_back(current_edge.child_edge2);
			todo.push_back(current_edge.child_edge1);
		} else {
			path.push(*this, current_edge.id);
		}
	}

	return path;
}

EdgeIDs Graph::unpack(EdgeID edge_id, Level const& level) const
{
	EdgeIDs edges;
	EdgeIDs todo = {edge_id};

	while (!todo.empty()) {
		auto const& current_edge = getEdge(todo.back());
		todo.pop_back();

		// Check target level
		if (current_edge.isShortcut()) {
			if (getShortcutLevel(current_edge) >= level) {
				todo.push_back(current_edge.child_edge2);
				todo.push_back(current_edge.child_edge1);
			} else {
				// finished: level
				edges.push_back(current_edge.id);
			}
		} else {
			// finished: edge is no shortcut
			edges.push_back(current_edge.id);
		}
	}
	return edges;
}

EdgeIDs Graph::getEdgeCreationOrder() const
{
	EdgeIDs creation_order(getNumberOfEdges());
	std::iota(creation_order.begin(), creation_order.end(), 0);

	auto is_lower = [&](EdgeID edge_id1, EdgeID edge_id2) {
		auto shortcut_node_id1 = getShortcutNode(edge_id1);
		auto shortcut_node_id2 = getShortcutNode(edge_id2);

		// edge_id1 isn't a shortcut
		if (shortcut_node_id1 == c::NO_NID) {
			if (shortcut_node_id2 == c::NO_NID) {
				return false;
			}

			return true;
		}
		// edge_id2 isn't a shortcut
		if (shortcut_node_id2 == c::NO_NID) {
			return false;
		}

		auto const& shortcut_node1 = getNode(shortcut_node_id1);
		auto const& shortcut_node2 = getNode(shortcut_node_id2);

		return shortcut_node1.level < shortcut_node2.level;
	};
	std::sort(creation_order.begin(), creation_order.end(), is_lower);

	return creation_order;
}

NodeIDs Graph::getContractionOrder() const
{
	NodeIDs contraction_order(getNumberOfNodes());
	std::iota(contraction_order.begin(), contraction_order.end(), 0);

	auto lower_level = [&](NodeID node_id1, NodeID node_id2) {
		return getNode(node_id1).level < getNode(node_id2).level;
	};
	std::sort(contraction_order.begin(), contraction_order.end(), lower_level);

	return contraction_order;
}

//
// private member functions
//

void Graph::build(std::vector<Node>&& nodes_from_parser,
    std::vector<Edge>&& edges_from_parser, bool resort_IDs_for_spatial_locality)
{
	if (resort_IDs_for_spatial_locality) {
		resortIDsForSpatialLocality(nodes_from_parser, edges_from_parser);
	}

	// setup edges and nodes vectors
	nodes.swap(nodes_from_parser);
	out_edges.swap(edges_from_parser);
	in_edges = out_edges;
	level_ordered_edges = out_edges;

	// sort nodes
	auto node_comp = [this](Node const& node1, Node const& node2) {
		return node1.id < node2.id; // sort by ids
	};

	std::sort(nodes.begin(), nodes.end(), node_comp);

	// sort edges
	auto out_edge_comp = [this](Edge const& edge1, Edge const& edge2) {
		auto target_level_1 = nodes[edge1.target].level;
		auto target_level_2 = nodes[edge2.target].level;

		return (edge1.source < edge2.source) ||
		       (edge1.source == edge2.source &&
		           target_level_1 > target_level_2);
	};
	auto in_edge_comp = [this](Edge const& edge1, Edge const& edge2) {
		auto source_level_1 = nodes[edge1.source].level;
		auto source_level_2 = nodes[edge2.source].level;

		return (edge1.target < edge2.target) ||
		       (edge1.target == edge2.target &&
		           source_level_1 > source_level_2);
	};
	auto level_edge_comp = [this](Edge const& edge1, Edge const& edge2) {
		auto shortcut_level_1 = getShortcutLevel(edge1);
		auto shortcut_level_2 = getShortcutLevel(edge2);

		NodeID shortcut_node1 = getShortcutNode(edge1);
		NodeID shortcut_node2 = getShortcutNode(edge2);

		return (shortcut_level_1 < shortcut_level_2) ||
		       (shortcut_level_1 == shortcut_level_2 &&
		           shortcut_node1 < shortcut_node2);
	};

	std::sort(out_edges.begin(), out_edges.end(), out_edge_comp);
	std::sort(in_edges.begin(), in_edges.end(), in_edge_comp);

	uint nr_of_nodes(nodes.size());

	// initialize offsets vectors

	out_offsets.assign(nr_of_nodes + 1, 0);
	in_offsets.assign(nr_of_nodes + 1, 0);

	/* assume "valid" edges are in _out_edges and _in_edges */
	for (auto const& edge : out_edges) {
		out_offsets[edge.source]++;
		in_offsets[edge.target]++;
	}

	uint out_sum(0);
	uint in_sum(0);
	for (uint i = 0; i < nr_of_nodes; i++) {
		auto old_out_sum = out_sum, old_in_sum = in_sum;
		out_sum += out_offsets[i];
		in_sum += in_offsets[i];
		out_offsets[i] = old_out_sum;
		in_offsets[i] = old_in_sum;
	}
	assert(out_sum == out_edges.size());
	assert(in_sum == in_edges.size());
	out_offsets[nr_of_nodes] = out_sum;
	in_offsets[nr_of_nodes] = in_sum;

	setIdToIndexVector();

	// sort w.r.t. shortcut level. Note that this step has to be done here,
	// since the index has to be created in advane.
	std::sort(level_ordered_edges.begin(), level_ordered_edges.end(),
	    level_edge_comp);
	setIdToIndexVectorLevelWiseSorted();

	// initialize out_up_edges_end
	out_up_edges_end.resize(nodes.size());
	for (NodeID node_id = 0; node_id < (NodeID)nodes.size(); ++node_id) {
		auto begin = out_offsets[node_id];
		auto end = out_offsets[node_id + 1];

		out_up_edges_end[node_id] = end;
		for (std::size_t edge_index = begin; edge_index < end; ++edge_index) {

			auto const& edge = out_edges[edge_index];
			if (isDownEdge(edge.id, Direction::Forward)) {
				out_up_edges_end[node_id] = edge_index;
				break;
			}
		}
	}

	// initialize in_up_edges_end
	in_up_edges_end.resize(nodes.size());
	for (NodeID node_id = 0; node_id < (NodeID)nodes.size(); ++node_id) {
		auto begin = in_offsets[node_id];
		auto end = in_offsets[node_id + 1];

		in_up_edges_end[node_id] = end;
		for (std::size_t edge_index = begin; edge_index < end; ++edge_index) {

			auto const& edge = in_edges[edge_index];
			if (isDownEdge(edge.id, Direction::Backward)) {
				in_up_edges_end[node_id] = edge_index;
				break;
			}
		}
	}

	// to speed up other processes, since non-shortcuts can be skipped in
	// advance.
	for (size_t i = 0; i < level_ordered_edges.size(); i++) {
		if (level_ordered_edges[i].isShortcut()) {
			first_shortcut_edge_index = i;
			break;
		}
	}

	initUniqueUnidirectionalMap();
}

void Graph::resortIDsForSpatialLocality(
    std::vector<Node> const& nodes, std::vector<Edge>& edges) const
{
	RTree<EdgeID> r_tree;

	for (Edge const& edge : edges) {
		r_tree.emplace(
		    BoundingBox(nodes[edge.source], nodes[edge.target]), edge.id);
	}
	r_tree.build();

	// edges which are spatially close to each other are close too each other in
	// this vector too. This vector gives the new edge ids
	EdgeIDs inorder = r_tree.getInorder();

	// build the mapping from old to new ids
	assert(edges.size() == inorder.size());
	EdgeIDs oldIDtoNewID(edges.size());
	for (uint i = 0; i < edges.size(); i++) {
		assert(edges.at(i).id == (int)i);
	}
	for (uint new_edge_id = 0; new_edge_id < inorder.size(); new_edge_id++) {
		EdgeID old_edge_id = inorder[new_edge_id];
		oldIDtoNewID[old_edge_id] = new_edge_id;
	}

	// replace old with new
	for (Edge& edge : edges) {
		edge.id = oldIDtoNewID[edge.id];
		if (edge.isShortcut()) {
			edge.child_edge1 = oldIDtoNewID[edge.child_edge1];
			edge.child_edge2 = oldIDtoNewID[edge.child_edge2];
		}
	}
}

void Graph::setIdToIndexVector()
{
	id_to_index.resize(out_edges.size());
	for (std::size_t i = 0; i < out_edges.size(); ++i) {
		id_to_index[out_edges[i].id] = i;
	}
}

void Graph::setIdToIndexVectorLevelWiseSorted()
{
	id_to_levelordered_index.resize(level_ordered_edges.size());
	for (std::size_t i = 0; i < level_ordered_edges.size(); ++i) {
		id_to_levelordered_index[level_ordered_edges[i].id] = i;
	}
}

void Graph::initUniqueUnidirectionalMap()
{
	for (EdgeID edge_id = 0; edge_id < (EdgeID)getAllEdges().size();
	     edge_id++) {
		EdgeID unique_edge_id = computeUniqueUnidirectionalEdge(edge_id);
		uniqueUnidirectionalMap.push_back(unique_edge_id);
	}
}

void Graph::check()
{
	// Check if source and target ids are in the right range
	for (auto const& edge : out_edges) {
		auto const source = edge.source;
		auto const target = edge.target;

		if (source < 0 || source >= (NodeID)nodes.size()) {
			Exception("The source id of the edge " + std::to_string(edge.id) +
			          " is out of bounds.");
		}

		if (target < 0 || target >= (NodeID)nodes.size()) {
			Exception("The target id of the edge " + std::to_string(edge.id) +
			          " is out of bounds.");
		}
	}

	// Check if the child edges are in the right order, i.e., child_edge1 is
	// really the first edge and child_edge2 is really the second edge. Also
	// check if child edges are consistent, i.e., iff one child edge is a valid
	// id then the other one is too.
	for (auto const& edge : out_edges) {
		if ((edge.child_edge1 == c::NO_EID && edge.child_edge2 != c::NO_EID) ||
		    (edge.child_edge1 != c::NO_EID && edge.child_edge2 == c::NO_EID)) {

			throw Exception("The child edges of edge " +
			                std::to_string(edge.id) + " are inconsistent.");
		}

		if (edge.child_edge1 != c::NO_EID) {
			auto const& child_edge = getEdge(edge.child_edge1);

			if (child_edge.source != edge.source) {
				throw Exception(
				    "The sources of the edge (id = " + std::to_string(edge.id) +
				    ") and its first child edge (id = " +
				    std::to_string(child_edge.id) + ") don't match.");
			}
		}

		if (edge.child_edge2 != c::NO_EID) {
			auto const& child_edge = getEdge(edge.child_edge2);

			if (child_edge.target != edge.target) {
				throw Exception(
				    "The targets of the edge (id = " + std::to_string(edge.id) +
				    ") and its second child edge (id = " +
				    std::to_string(child_edge.id) + ") don't match.");
			}
		}
	}

	// Check if offsets make sense
	for (auto offset : out_offsets) {
		assert(offset <= out_edges.size());
		Unused(offset);
	}
	for (auto offset : in_offsets) {
		assert(offset <= out_edges.size());
		Unused(offset);
	}
	for (auto offset : out_up_edges_end) {
		assert(offset <= out_edges.size());
		Unused(offset);
	}
	for (auto offset : in_up_edges_end) {
		assert(offset <= out_edges.size());
		Unused(offset);
	}

	for (std::size_t i = 0; i < out_offsets.size() - 1; ++i) {
		assert(out_offsets[i] <= out_offsets[i + 1]);
	}
	for (std::size_t i = 0; i < in_offsets.size() - 1; ++i) {
		assert(in_offsets[i] <= in_offsets[i + 1]);
	}
	for (std::size_t i = 0; i < out_up_edges_end.size() - 1; ++i) {
		assert(out_up_edges_end[i] <= out_up_edges_end[i + 1]);
	}
	for (std::size_t i = 0; i < in_up_edges_end.size() - 1; ++i) {
		assert(in_up_edges_end[i] <= in_up_edges_end[i + 1]);
	}
}

bool Graph::isSortedWRTEdgesStatus() const
{
	return sortedWRTEdgeStatus;
}

void Graph::sortWRTEdgeStatus(
    std::vector<bool>& is_obsolete, std::vector<bool>& is_tree_edge)
{
	// resort edges such that there are blocks of non obsolete edges
	auto out_edge_comp = [this, &is_obsolete, &is_tree_edge](
	                         Edge const& edge1, Edge const& edge2) {
		if (edge1.source != edge2.source) {
			return edge1.source < edge2.source;
		} else {
			bool up1 = isUpEdge(edge1, Direction::Forward);
			bool up2 = isUpEdge(edge2, Direction::Forward);
			if (up1 != up2) {
				return up1; // no need to look at up2
			} else {
				bool obsolete1 = is_obsolete[edge1.id];
				bool obsolete2 = is_obsolete[edge2.id];
				if (obsolete1 != obsolete2) {
					return !obsolete1; // no need to look at obsolete2
				} else {
					bool tree_edge1 = is_tree_edge[edge1.id];
					bool tree_edge2 = is_tree_edge[edge2.id];
					if (tree_edge1 != tree_edge2) {
						return tree_edge1; // no need to look at tree_edge2
					} else {
						return nodes[edge1.target].level >
						       nodes[edge2.target].level;
					}
				}
			}
		}
	};
	auto in_edge_comp = [this, &is_obsolete, &is_tree_edge](
	                        Edge const& edge1, Edge const& edge2) {
		if (edge1.target != edge2.target) {
			return edge1.target < edge2.target;
		} else {
			bool up1 = isUpEdge(edge1, Direction::Backward);
			bool up2 = isUpEdge(edge2, Direction::Backward);
			if (up1 != up2) {
				return up1; // no need to look at up2
			} else {
				bool obsolete1 = is_obsolete[edge1.id];
				bool obsolete2 = is_obsolete[edge2.id];
				if (obsolete1 != obsolete2) {
					return !obsolete1; // no need to look at obsolete2
				} else {
					bool tree_edge1 = is_tree_edge[edge1.id];
					bool tree_edge2 = is_tree_edge[edge2.id];
					if (tree_edge1 != tree_edge2) {
						return tree_edge1; // no need to look at tree_edge2
					} else {
						return nodes[edge1.source].level >
						       nodes[edge2.source].level;
					}
				}
			}
		}
	};

	std::sort(out_edges.begin(), out_edges.end(), out_edge_comp);
	std::sort(in_edges.begin(), in_edges.end(), in_edge_comp);

	setIdToIndexVector(); // rebuild

	// following data structures are still valid and don't need to be updated:
	// out_offsets
	// in_offsets
	// out_up_edges_end
	// in_up_edges_end

	// initialize out_non_obsolete_down_edges_end
	out_non_obsolete_down_edges_end.resize(nodes.size());
	for (NodeID node_id = 0; node_id < (NodeID)nodes.size(); ++node_id) {
		auto begin = out_up_edges_end[node_id];
		auto end = out_offsets[node_id + 1];

		out_non_obsolete_down_edges_end[node_id] = end;
		for (std::size_t edge_index = begin; edge_index < end; ++edge_index) {

			auto const& edge = out_edges[edge_index];
			if (is_obsolete[edge.id]) {
				out_non_obsolete_down_edges_end[node_id] = edge_index;
				break;
			}
		}
	}

	// initialize in_non_obsolete_down_edges_end
	in_non_obsolete_down_edges_end.resize(nodes.size());
	for (NodeID node_id = 0; node_id < (NodeID)nodes.size(); ++node_id) {
		auto begin = in_up_edges_end[node_id];
		auto end = in_offsets[node_id + 1];

		in_non_obsolete_down_edges_end[node_id] = end;
		for (std::size_t edge_index = begin; edge_index < end; ++edge_index) {

			auto const& edge = in_edges[edge_index];
			if (is_obsolete[edge.id]) {
				in_non_obsolete_down_edges_end[node_id] = edge_index;
				break;
			}
		}
	}

	// initialize out_tree_down_edges_end
	out_tree_down_edges_end.resize(nodes.size());
	for (NodeID node_id = 0; node_id < (NodeID)nodes.size(); ++node_id) {
		auto begin = out_up_edges_end[node_id];
		auto end = out_non_obsolete_down_edges_end[node_id];

		out_tree_down_edges_end[node_id] = end;
		for (std::size_t edge_index = begin; edge_index < end; ++edge_index) {

			auto const& edge = out_edges[edge_index];
			if (!is_tree_edge[edge.id]) {
				out_tree_down_edges_end[node_id] = edge_index;
				break;
			}
		}
	}

	// initialize in_tree_down_edges_end
	in_tree_down_edges_end.resize(nodes.size());
	for (NodeID node_id = 0; node_id < (NodeID)nodes.size(); ++node_id) {
		auto begin = in_up_edges_end[node_id];
		auto end = in_non_obsolete_down_edges_end[node_id];

		in_tree_down_edges_end[node_id] = end;
		for (std::size_t edge_index = begin; edge_index < end; ++edge_index) {

			auto const& edge = in_edges[edge_index];
			if (!is_tree_edge[edge.id]) {
				in_tree_down_edges_end[node_id] = edge_index;
				break;
			}
		}
	}

	check();
	checkAfterSort();
	sortedWRTEdgeStatus = true;
}

void Graph::checkAfterSort() const
{
	// old and new vectors should be the same
	std::vector<std::size_t> new_out_offsets;
	std::vector<std::size_t> new_in_offsets;
	std::vector<std::size_t> new_out_up_edges_end;
	std::vector<std::size_t> new_in_up_edges_end;

	uint nr_of_nodes(nodes.size());
	new_out_offsets.assign(nr_of_nodes + 1, 0);
	new_in_offsets.assign(nr_of_nodes + 1, 0);

	/* assume "valid" edges are in _out_edges and _in_edges */
	for (auto const& edge : out_edges) {
		new_out_offsets[edge.source]++;
		new_in_offsets[edge.target]++;
	}

	uint out_sum(0);
	uint in_sum(0);
	for (uint i = 0; i < nr_of_nodes; i++) {
		auto old_out_sum = out_sum, old_in_sum = in_sum;
		out_sum += new_out_offsets[i];
		in_sum += new_in_offsets[i];
		new_out_offsets[i] = old_out_sum;
		new_in_offsets[i] = old_in_sum;
	}
	assert(out_sum == out_edges.size());
	assert(in_sum == in_edges.size());
	new_out_offsets[nr_of_nodes] = out_sum;
	new_in_offsets[nr_of_nodes] = in_sum;

	for (uint i = 0; i < nr_of_nodes + 1; i++) {
		assert(out_offsets[i] == new_out_offsets[i]);
		assert(in_offsets[i] == new_in_offsets[i]);
	}

	// initialize out_up_edges_end
	new_out_up_edges_end.resize(nodes.size());
	for (NodeID node_id = 0; node_id < (NodeID)nodes.size(); ++node_id) {
		auto begin = new_out_offsets[node_id];
		auto end = new_out_offsets[node_id + 1];

		new_out_up_edges_end[node_id] = end;
		for (std::size_t edge_index = begin; edge_index < end; ++edge_index) {

			auto const& edge = out_edges[edge_index];
			if (isDownEdge(edge.id, Direction::Forward)) {
				new_out_up_edges_end[node_id] = edge_index;
				break;
			}
		}
	}

	// initialize in_up_edges_end
	new_in_up_edges_end.resize(nodes.size());
	for (NodeID node_id = 0; node_id < (NodeID)nodes.size(); ++node_id) {
		auto begin = new_in_offsets[node_id];
		auto end = new_in_offsets[node_id + 1];

		new_in_up_edges_end[node_id] = end;
		for (std::size_t edge_index = begin; edge_index < end; ++edge_index) {

			auto const& edge = in_edges[edge_index];
			if (isDownEdge(edge.id, Direction::Backward)) {
				new_in_up_edges_end[node_id] = edge_index;
				break;
			}
		}
	}

	for (uint i = 0; i < nr_of_nodes; i++) {
		assert(out_up_edges_end[i] == new_out_up_edges_end[i]);
		assert(in_up_edges_end[i] == new_in_up_edges_end[i]);
	}

	// just another check
	for (uint i = 0; i < out_edges.size(); i++) {
		auto edge = out_edges.at(i);
		assert(nodes[edge.target].level != nodes[edge.source].level);
		Unused(edge);
	}
}

bool Graph::is_edge_below_level(const Level level, const Edge& edge) const
{
	auto level1 = getNode(edge.source).level;
	auto level2 = getNode(edge.target).level;
	return std::max(level1, level2) < level;
}

bool Graph::edgeHasIncreasingOrder(const EdgeID edge_id) const
{
	const auto& edge = getEdge(edge_id);
	return edgeHasIncreasingOrder(edge);
}

bool Graph::edgeHasIncreasingOrder(const Edge& edge) const
{
	return edge.source < edge.target;
}

EdgeID Graph::getFlippedEdge(const EdgeID edge_id) const
{
	const auto& edge = getEdge(edge_id);
	return getFlippedEdge(edge);
}

EdgeID Graph::getFlippedEdge(const Edge& edge) const
{
	const auto source_node = edge.source;
	auto target_out_edges = getOutEdgesOf(edge.target);
	for (auto& edge_candidate : target_out_edges) {
		if (edge_candidate.target == source_node) {
			return edge_candidate.id;
		}
	}

	// return invalid edge if no back-edge is available
	return c::NO_EID;
}

EdgeID Graph::computeUniqueUnidirectionalEdge(const EdgeID edge_id) const
{
	const auto& edge = getEdge(edge_id);
	return computeUniqueUnidirectionalEdge(edge);
}

EdgeID Graph::computeUniqueUnidirectionalEdge(const Edge& edge) const
{
	EdgeID smaller = std::min(edge.source, edge.target);
	EdgeID bigger = std::max(edge.source, edge.target);

	auto smaller_out_edges = getOutEdgesOf(smaller);
	for (const Edge& edge_candidate : smaller_out_edges) {
		if (edge_candidate.target == bigger) {
			return edge_candidate.id;
		}
	}

	// no forward edge available, then search in the backward edges
	auto bigger_out_edges = getOutEdgesOf(bigger);
	for (const Edge& edge_candidate : bigger_out_edges) {
		if (edge_candidate.target == smaller) {
			return edge_candidate.id;
		}
	}

	// can't happen because we must found at least our input edge by now
	assert(false);
	return c::NO_EID;
}

} // namespace pf
