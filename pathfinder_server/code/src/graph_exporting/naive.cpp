
#include "naive.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Baur, Funke, Rupp; 2022			  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace graph_segment_exporting
{
    
namespace topology
{

namespace naive
{



// check if a node has at most degree 2 (in- and out-edges are only counted once) 
bool is_link_node(const NodeID node_id, const Graph& graph, const std::vector<bool>& active_edges){
    auto out_edges  = graph.getOutEdgesOf(node_id);
    auto in_edges  = graph.getInEdgesOf(node_id);
    std::set<EdgeID> edges;
    for(auto& e : out_edges){
        auto e_dir = graph.getLevelsortedEdgeIndexMap().at(graph.getUniqueUnidirectionalEdge(e.id));
        if (active_edges.at(e_dir)){
            edges.insert(e_dir);
        }
    }
    for(auto& e : in_edges){
        auto e_dir = graph.getLevelsortedEdgeIndexMap().at(graph.getUniqueUnidirectionalEdge(e.id));
        if (active_edges.at(e_dir)){
            edges.insert(e_dir);
        }
    }
    return edges.size() <= 2;
}


    EdgeIDs export_graph_section(PathfinderDS& p_ds, const BoundingBox& box, Level min_level, EdgeIDs& removed_edges){
       
        SweepEdges& sweep_edges = p_ds.getSweepEdges();
	    const Graph& graph = p_ds.getGraph();

        // initially serach for relevant nodes
        std::vector<bool> active_nodes (graph.getAllNodes().size(), false); 
        mark_active_nodes(active_nodes, graph, box, min_level, p_ds);
        
        // 2n loop (edges): extract edge-connections & unpack edges
        std::vector<bool> active_edges(sweep_edges.size(), false); 
        push_markings_downwards(sweep_edges, active_edges, active_nodes, graph, removed_edges);

        // Sweep all shortcuts: If a node shares exactly two (active) adjacent edges by an shortcut -> merge 
        int removed_edges_count = 0;
        for (EdgeID i = graph.get_first_shortcut_edge_index(); i < (EdgeID) sweep_edges.size(); i++)
        {
            // check if shortcut-node is degree 2.
            auto& edge = sweep_edges.at(i);

            EdgeID responsible_edge_id =  graph.getUniqueUnidirectionalEdge(edge.id);

            EdgeID responsible_child1 = graph.getUniqueUnidirectionalEdge(sweep_edges.at(edge.child_edge1).id);
            EdgeID responsible_child2 = graph.getUniqueUnidirectionalEdge(sweep_edges.at(edge.child_edge2).id);

            EdgeID local_responsible_child1 = graph.getLevelsortedEdgeIndexMap().at(responsible_child1);
            EdgeID local_responsible_child2 = graph.getLevelsortedEdgeIndexMap().at(responsible_child2);

            if(active_edges.at(local_responsible_child1) & 
                active_edges.at(local_responsible_child2) & 
                is_link_node(p_ds.getSweepNodes().at(edge.shortcut_node).id, graph, active_edges) &
                (edge.id == responsible_edge_id)
                ){
                // remove this edge and add shortcut instead
                active_edges.at(local_responsible_child1) = false;
                active_edges.at(local_responsible_child2) = false;
                active_edges.at(i) = true;
                removed_edges_count += 1;
            }
        }
        Print("Edges removed: " << removed_edges_count);
    

        // export results
        EdgeIDs export_edges;
        copy_active_edges(active_edges, sweep_edges, export_edges);
	
        return export_edges;
    }
}



} // namespace naive

} // namespace topology

} // namespace pf
