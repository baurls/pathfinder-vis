#pragma once

#include "../basic_types.h"
#include "../defs.h"

#include "../pathfinder_ds.h"
#include "../bounding_box.h"
#include "shared.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Baur, Funke, Rupp; 2022			  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace graph_segment_exporting
{
    void mark_active_nodes(std::vector<bool>& active_nodes, const Graph& graph, const BoundingBox& box, Level min_level, const PathfinderDS& p_ds);
    void push_markings_downwards(SweepEdges& sweep_edges, std::vector<bool>& active_edges, const std::vector<bool>& active_nodes, const Graph& graph, EdgeIDs& removed_Edges);
    void copy_active_edges(const std::vector<bool>& active_edges, const SweepEdges& sweep_edges, EdgeIDs& export_edges);    
} // namespace graph_segment_exporting

} // namespace pf
