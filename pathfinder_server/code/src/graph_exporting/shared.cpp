
#include "shared.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Baur, Funke, Rupp; 2022			  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace graph_segment_exporting
{

    
    // extract all nodes within the request-box having a minimum importance 
    void mark_active_nodes(std::vector<bool>& active_nodes, const Graph& graph, const BoundingBox& box, Level min_level, const PathfinderDS& p_ds){

        for (NodeID i = 0; i < (NodeID) graph.getAllNodes().size(); i++)
        {
            auto& node = graph.getAllNodes().at(i);
            assert(node.id == i);
            if((node.level > min_level) & (box.contains(node.coordinate))){
                NodeID sweep_node_id = p_ds.nodeIDToSweepNodeID(i);
                active_nodes.at(sweep_node_id) = true;
            }
        }

    }

    // fully push edges downwards to plain edges
    void push_markings_downwards(SweepEdges& sweep_edges, std::vector<bool>& active_edges, const std::vector<bool>& active_nodes, const Graph& graph, EdgeIDs& removed_edges){
        for (EdgeID i = sweep_edges.size(); i > 0; i--)
        {
            auto& edge = sweep_edges.at(i-1);
            if((active_nodes.at(edge.source) &  active_nodes.at(edge.target)) | active_edges.at(i-1)){
                active_edges.at(i-1) = false;
                if(edge.isShortcut()){
                    removed_edges.push_back(edge.id);
                    // push marking down
                    assert(edge.child_edge1 < (EdgeID) i-1);
                    assert(edge.child_edge2 < (EdgeID) i-1);
                    active_edges.at(edge.child_edge1) = true;
                    active_edges.at(edge.child_edge2) = true;
                }
                else{
                    // mark this (plain) edge
                    auto unique_edge = graph.getUniqueUnidirectionalEdge(edge.id); 
                    auto unique_edge_index = graph.getLevelsortedEdgeIndexMap().at(unique_edge);
                    active_edges.at(unique_edge_index) = true;
                }
            }
        }
        
    }

    // loops through all edges and copies the entriy if it is marked active
    void copy_active_edges(const std::vector<bool>& active_edges, const SweepEdges& sweep_edges, EdgeIDs& export_edges){
        assert(active_edges.size() == sweep_edges.size());
        
        for (EdgeID i = 0; i < (EdgeID) sweep_edges.size(); i++)
        {
            if(active_edges.at(i)){
                export_edges.push_back(sweep_edges.at(i).id);
            }
        }

    }
} // namespace graph_segment_exporting

} // namespace pf
