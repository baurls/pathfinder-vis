#pragma once

#include "../basic_types.h"
#include "../defs.h"
#include "shared.h"

#include "../pathfinder_ds.h"
#include "../bounding_box.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Baur, Funke, Rupp; 2022			  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace graph_segment_exporting
{

namespace full
{

    // plot all edges in a certain region connecting nodes having a minimum importance 
    EdgeIDs export_graph_section(PathfinderDS& p_ds, const BoundingBox& box, Level min_level, EdgeIDs& removed_edges);

}  // namespace full

} // namespace graph_segment_exporting

} // namespace pf
