
#include "full.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Baur, Funke, Rupp; 2022			  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace graph_segment_exporting
{

namespace full
{

    EdgeIDs export_graph_section(PathfinderDS& p_ds, const BoundingBox& box, Level min_level, EdgeIDs& removed_edges){
        
        SweepEdges& sweep_edges = p_ds.getSweepEdges();
	    const Graph& graph = p_ds.getGraph();

        std::vector<bool> active_nodes(graph.getAllNodes().size(), false); 
        
        // 1st loop (nodes): mark all active nodes
        mark_active_nodes(active_nodes, graph, box, min_level, p_ds);

        // 2n loop (edges): extract edge-connections & unpack edges
        std::vector<bool> active_edges(sweep_edges.size(), false); 

        // initially mark edges which are included
        for (EdgeID i = sweep_edges.size(); i > 0; i--)
        {
            auto& edge = sweep_edges.at(i-1);
            if((active_nodes.at(edge.source) &  active_nodes.at(edge.target))){
                active_edges.at(i-1) = true;
            }
        }             
        
        // 2n loop (edges): extract edge-connections & unpack edges
       push_markings_downwards(sweep_edges, active_edges, active_nodes, graph, removed_edges);

    
        // export results
        EdgeIDs export_edges;
        copy_active_edges(active_edges, sweep_edges, export_edges);
        Print("Matches : " << export_edges.size());
	
        return export_edges;
    }



} // namespace full

} // namespace graph_segment_exporting

} // namespace pf
