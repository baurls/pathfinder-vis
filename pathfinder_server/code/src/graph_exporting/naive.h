#pragma once

#include "../basic_types.h"
#include "../defs.h"
#include "shared.h"

#include "../pathfinder_ds.h"
#include "../bounding_box.h"

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Baur, Funke, Rupp; 2022			  #
    #   (Vis-Paper 2022)                              #
    #                                                 #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

namespace unit_tests{
    void testTopologicalGraphExporting();
} // namespace unit_tests

namespace graph_segment_exporting
{

namespace topology
{

namespace naive
{

    EdgeIDs export_graph_section(PathfinderDS& p_ds, const BoundingBox& box, Level min_level, EdgeIDs& removed_edges);

}  // namespace naive

}  // namespace topology

} // namespace graph_segment_exporting

} // namespace pf
