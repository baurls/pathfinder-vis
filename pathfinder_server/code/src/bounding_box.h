#pragma once

#include "basic_types.h"
#include "paths.h"

#include <ostream>
#include <vector>

namespace pf
{

// This follows the OSM convention defined here:
// http://wiki.openstreetmap.org/wiki/Bounding_Box
struct BoundingBox {
	Coordinate min_coordinate;
	Coordinate max_coordinate;

	// the coordinates are set invalid by their default constructors
	BoundingBox();
	BoundingBox(Coordinate coordinate);
	BoundingBox(Coordinate min_coordinate, Coordinate max_coordinate);
	BoundingBox(Node const& node1, Node const& node2);
	BoundingBox(Graph const& graph, Path const& path);
	BoundingBox(Graph const& graph);

	bool operator==(BoundingBox const& box) const;

	bool isValid() const;
	void expand(BoundingBox const& box);
	void intersect(BoundingBox const& box);
	Coordinate calcCenter() const;
	bool contains(BoundingBox const& box) const;
	bool contains(Coordinate const& point) const;
	bool overlaps_with(BoundingBox const& box) const;

	double getWidth() const;
	double getHeight() const;
};

std::ostream& operator<<(std::ostream& stream, BoundingBox const& box);

using BoundingBoxes = std::vector<BoundingBox>;

} // namespace pf
