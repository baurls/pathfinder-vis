#pragma once

#include "paths_ds.h"
#include "shortest_path_partitioner.h"
#include "time_tracker.h"

#include <memory>

namespace pf
{

enum class PartitionerType { INCREMENTAL };

namespace partitioner_util
{

std::unique_ptr<ShortestPathPartitioner> static createPartitioner(
    Graph const& graph, PartitionerType type)
{
	switch (type) {
	case PartitionerType::INCREMENTAL: {
		return std::unique_ptr<ShortestPathPartitioner>(
		    new IncrementalPartitioner(graph));
	}
	}
	throw Exception(
	    "Unknown PartitionerType passed to ShortestPathPartitioner::run.");
}

PathsDS static partitionPaths(Graph const& graph, PathsDS const& paths_ds)
{
	TimeTracker time_tracker;
	Print("Partition paths...");
	time_tracker.start();

	PathsDS compressed_paths_ds;

	std::unique_ptr<ShortestPathPartitioner> partitioner =
	    createPartitioner(graph, PartitionerType::INCREMENTAL);
	for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); path_id++) {
		Path path = paths_ds.getPath(path_id);
		auto compressed_path = partitioner->run(path);
		compressed_paths_ds.addPath(compressed_path);
	}

	time_tracker.stop();
	Print("Took " << time_tracker.getLastMeasurment() << " seconds");

	// FIXME: why does the compiler says not used without it??
	// and why is compiler tricked by this and does not optimize it out?
	if (false) {
		partitionPaths(graph, paths_ds);
	}

	return compressed_paths_ds;
}

} // namespace partitioner_util

} // namespace pf
