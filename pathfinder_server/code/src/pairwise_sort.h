#pragma once

#include "basic_types.h"
#include "defs.h"
#include "options.h"
#include "paths.h"
#include "query.h"
#include "time_tracker.h"

#include <map>

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, Master Thesis 2021.    #
    #                                                 #
    #   Supervisor: T. Rupp, Prof. S. Funke           #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{

/*
 * Util code to get sone array sorted wrt. another,
 * code adapted from
 * https://stackoverflow.com/questions/17074324/how-can-i-sort-two-vectors-in-the-same-way-with-criteria-that-uses-only-one-of
 */

struct less_than_int {
	inline bool operator()(
	    const std::pair<EdgeID, int>& a, const std::pair<EdgeID, int>& b)
	{
		return (a.second < b.second);
	}
};

// make sure vectorA and vectorB are of the same size, before calling function
template <typename T, typename R>
void sort_pairwise(std::vector<T>& dependent_vec, std::vector<R>& primary_vec)
{
	std::vector<std::pair<T, R>> vecC;
	vecC.reserve(dependent_vec.size());
	for (size_t i = 0; i < dependent_vec.size(); i++) {
		vecC.push_back(std::make_pair(dependent_vec[i], primary_vec[i]));
	}

	std::sort(vecC.begin(), vecC.end(), less_than_int());

	dependent_vec.clear();
	primary_vec.clear();
	dependent_vec.reserve(vecC.size());
	primary_vec.reserve(vecC.size());
	for (size_t i = 0; i < vecC.size(); i++) {
		dependent_vec.push_back(vecC[i].first);
		primary_vec.push_back(vecC[i].second);
	}
}

} // namespace pf
