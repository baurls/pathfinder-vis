#include "paths_as_list.h"
#include "defs.h"

namespace pf
{

PathAsList::PathAsList(Path const& path) : iterator(*this)
{
	edges.insert(edges.end(), path.getEdges().begin(), path.getEdges().end());
	timeAtNodes.insert(timeAtNodes.end(), path.getTimeAtNodes().begin(),
	    path.getTimeAtNodes().end());
	iterator.edge_it = edges.begin();
	iterator.time_it = timeAtNodes.begin();
}

Path PathAsList::makeVectorPath(Graph const& graph) const
{
	auto edgeIt = edges.begin();
	auto timeAtNodeIt = timeAtNodes.begin();

	Path path;
	path.setTimeAtNode(0, *timeAtNodeIt);
	timeAtNodeIt++;

	for (EdgeIndex i = 0; i < edges.size(); i++) {
		path.push(graph, *edgeIt);
		path.setTimeAtNode(i + 1, *timeAtNodeIt);

		edgeIt++;
		timeAtNodeIt++;
	}

	return path;
}

bool PathAsList::SpecialIterator::operator==(
    const PathAsList::SpecialIterator& rhs) const
{
	return this->edge_it == rhs.edge_it;
}

bool PathAsList::iteratorIsAtEnd()
{
	return iterator.edge_it == edges.end();
}

bool PathAsList::iteratorIsAtBegin()
{
	return iterator.edge_it == edges.begin();
}

PathAsList::SpecialIterator& PathAsList::SpecialIterator::operator++()
{
	edge_it++;
	time_it++;
	return *this;
}

PathAsList::SpecialIterator& PathAsList::SpecialIterator::operator--()
{
	edge_it--;
	time_it--;
	return *this;
}

void PathAsList::lastTwoEdgesToShortcut(Graph const& graph, EdgeID shortcut)
{
	iterator.edge_it--;
	assert(graph.getEdge(shortcut).child_edge1 == *(iterator.edge_it));
	iterator.edge_it++;
	assert(graph.getEdge(shortcut).child_edge2 == *(iterator.edge_it));

	iterator.edge_it--;
	// erase child edge 1
	iterator.edge_it = edges.erase(iterator.edge_it);
	// substitute child edge 2 with shortcut
	*iterator.edge_it = shortcut;

	iterator.time_it = timeAtNodes.erase(iterator.time_it);
	iterator.time_it--;
}

EdgeID PathAsList::getPreviosEdgeID()
{
	iterator.edge_it--;
	EdgeID edge_id = *(iterator.edge_it);
	iterator.edge_it++;
	return edge_id;
}

EdgeID PathAsList::getCurrentEdgeID()
{
	return *(iterator.edge_it);
}

PathAsList::SpecialIterator::SpecialIterator(PathAsList& path_as_list)
{
	edge_it = path_as_list.edges.begin();
	time_it = path_as_list.timeAtNodes.begin();
}

} // namespace pf
