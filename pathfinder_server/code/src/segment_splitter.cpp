#include "segment_splitter.h"

#include "assert.h"
#include "defs.h"
#include "paths_ds.h"
#include "time_tracker.h"
#include <fstream>

/*
    # # # # # # # # # # # # # # # # # # # # # # # # # #
    #                                                 #
    #   Created by Lukas Baur, Master Thesis 2021.    #
    #                                                 #
    #   Supervisor: T. Rupp, Prof. S. Funke           #
    #   Institute:  FMI (University of Stuttgart)     #
    #                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # #
*/

namespace pf
{
std::ostream& operator<<(std::ostream& o, const TGEdgeIDs& path_ids)
{
	if (path_ids.size() > 0) {
		o << path_ids[0];
	}
	for (long unsigned int i = 1; i < path_ids.size(); i++) {
		o << ", " << path_ids[i];
	}

	return o;
}

std::ostream& operator<<(std::ostream& o, const NodePair& p)
{
	if (p.first < p.second) {
		o << "(" << p.first << ", " << p.second << ")";
	} else {
		o << "(" << p.second << ", " << p.first << ")";
	}
	return o;
}

std::ostream& operator<<(std::ostream& o, const TGNode& v)
{
	o << "Node(external-id=" << v.pathnode_id << " virtual=" << v.is_virtual
	  << " edges=[" << v.adjacent_edges << "])";
	return o;
}

std::ostream& operator<<(std::ostream& o, const TGEdge& e)
{
	o << "Edge(virtual=" << e.is_virtual
	  << " superimposition_id=" << e.trajectory_imposition << " u=" << e.u
	  << " v=" << e.v << " segments_id=" << e.segment_link << ")";
	return o;
}

TrajectoryGraph::TrajectoryGraph(Graph const& graph, PathsDS const& paths_ds)
    : graph(graph)
    , geometry(graph)
    , paths_ds(paths_ds)
    , trajectory_dfa(TrajectoryDFA(0))
{
	create_graph();
}

PathnodeNodeMap TrajectoryGraph::create_nodes()
{

	PathnodeNodeMap pathnode_to_node;

	// iteravte over all found paths and extract used nodes
	int no_of_vertices = 0;
	for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); path_id++) {
		Path path = paths_ds.getPath(path_id);
		for (NodeID node_id : path.getNodeRange(graph)) {
			no_of_vertices += 1;
			if (pathnode_to_node.find(node_id) == pathnode_to_node.end()) {
				pathnode_to_node[node_id] = create_node(node_id);
			}
		}
	}

	std::cout << "  ║║|T| (input nodes): " << no_of_vertices << std::endl;
	std::cout << "  ║║|V|(output nodes): " << pathnode_to_node.size()
	          << std::endl;
	return pathnode_to_node;
}

void TrajectoryGraph::create_edges(PathnodeNodeMap& pathnode_to_node)
{
	NodepairEdgeMap nodepair_to_edge;

	// Timing code
	int number_of_creation = 0;
	int number_of_reuseage = 0;

	TrajectoryDFA emptydfa(paths_ds.getNrOfPaths());
	trajectory_dfa = emptydfa;

	// PHASE 2: Create Edges: Link Nodes
	for (PathID trajectory_id = 0; trajectory_id < paths_ds.getNrOfPaths();
	     trajectory_id++) {
		Path path = paths_ds.getPath(trajectory_id);
		TGNodeID last_id = -1;
		TGNodeID current_id = -1;
		for (NodeID node_id : path.getNodeRange(graph)) {
			last_id = current_id;
			current_id = pathnode_to_node[node_id];
			if (last_id == -1) {
				continue;
			}

			NodePair pair =
			    std::make_pair<int, int>(int(last_id), int(current_id));

			if (nodepair_to_edge.find(pair) == nodepair_to_edge.end()) {
				// create new edge
				// CASE 1: transision does not exist yet, create state and edge
				auto trajectory_imposition =
				    trajectory_dfa.make_epsilon_transition(trajectory_id);

				auto edge_id =
				    create_edge(last_id, current_id, trajectory_imposition);
				// link to nodes and map
				nodepair_to_edge[pair] = edge_id;
				V[last_id].adjacent_edges.push_back(edge_id);
				V[current_id].adjacent_edges.push_back(edge_id);
				number_of_creation++;
			} else {
				// CASE 2: Transition does exist. Use it.
				auto edge_id = nodepair_to_edge[pair];

				E[edge_id].trajectory_imposition =
				    trajectory_dfa.make_transition(
				        E[edge_id].trajectory_imposition, trajectory_id);
				number_of_reuseage++;
			}
		}
	}
	auto total_states = number_of_reuseage + number_of_creation;
	std::cout << "  ║║#States:         " << total_states << std::endl;
	if (total_states > 0) {
		std::cout << "  ║║#States created: " << number_of_creation << "("
		          << (number_of_creation * 100) / total_states << "%)"
		          << std::endl;
		std::cout << "  ║║#States reused:  " << number_of_reuseage << "("
		          << (number_of_reuseage * 100) / total_states << "%)"
		          << std::endl;
	}
}

TGEdgeID TrajectoryGraph::create_edge(const TGNodeID node1,
    const TGNodeID node2, const TrajSuperimpositionID traj_superimposition_id)
{
	return create_edge(node1, node2, traj_superimposition_id, EMPTY_SEGMENT_ID);
}

TGEdgeID TrajectoryGraph::create_edge(const TGNodeID node1,
    const TGNodeID node2, const TrajSuperimpositionID traj_superimposition_id,
    const TGSegmentID segment_id)
{
	TGEdge tg_edge;
	tg_edge.u = std::min(node1, node2);
	tg_edge.v = std::max(node1, node2);
	tg_edge.trajectory_imposition = traj_superimposition_id;
	tg_edge.segment_link = segment_id;
	tg_edge.id = E.size();
	E.push_back(tg_edge);
	return tg_edge.id;
}

TGNodeID TrajectoryGraph::create_node(const NodeID node_id)
{
	TGNode node;
	node.pathnode_id = node_id;
	node.id = V.size();
	V.push_back(node);
	return node.id;
}

TGSegmentID TrajectoryGraph::create_segment(TGSegment& segment)
{
	S.push_back(segment);
	return S.size() - 1;
}

void TrajectoryGraph::print() const
{
	int i = 0;
	for (auto node : V) {
		std::cout << "#" << i << node << std::endl;
		i++;
	}
	int j = 0;
	for (auto edge : E) {
		std::cout << "#" << j << " " << edge << std::endl;
		j++;
	}
	int k = 0;
	for (auto segment : S) {
		std::cout << "#" << k << " Segment[" << segment << "]" << std::endl;
		k++;
	}
}

inline bool TrajectoryGraph::have_same_color(
    const TGEdgeID& e1_id, const TGEdgeID& e2_id)
{
	return E[e1_id].trajectory_imposition == E[e2_id].trajectory_imposition;
}
inline bool TrajectoryGraph::has_a_virtual_edge(
    const TGEdgeID& e1_id, const TGEdgeID& e2_id)
{
	return E[e1_id].is_virtual or E[e2_id].is_virtual;
}

inline bool TrajectoryGraph::can_merge(const TGNode& v)
{
	if (v.is_virtual or (v.adjacent_edges.size() != 2)) {
		return false;
	}
	if (has_a_virtual_edge(v.adjacent_edges[0], v.adjacent_edges[1])) {
		return false;
	}
	return have_same_color(v.adjacent_edges[0], v.adjacent_edges[1]);
}

inline TGNodeID TrajectoryGraph::get_vertex_on_other_end(
    const TGNodeID& node_id, const TGEdgeID& edge_id)
{
	// u      v
	// ◉——————◉
	if (E[edge_id].u == node_id) {
		return E[edge_id].v;
	} else {
		return E[edge_id].u;
	}
}

void TrajectoryGraph::merge_in_both_directions(TGNodeID v_id)
{
	// merge in w and u direction:
	//
	//       s                 u      v      w                t
	// ...~~~◉———e_s——— ... ———◉——————◉——————◉—— ... ———e_t———◉~~~...
	//
	// with:
	// —— = same TrajSuperimpositionID
	// ~~ = different TrajSuperimpositionID, dead end or intersection

	// set virtual
	V[v_id].is_virtual = true;
	auto u_id = get_vertex_on_other_end(v_id, V[v_id].adjacent_edges[0]);
	auto w_id = get_vertex_on_other_end(v_id, V[v_id].adjacent_edges[1]);

	TGNodeID s_id;
	TGEdgeID s_edge_id;
	TGSegment s_subsegment;
	mark_invalid_and_return_path_end(
	    v_id, u_id, &s_id, &s_edge_id, &s_subsegment);
	TGNodeID t_id;
	TGEdgeID t_edge_id;
	TGSegment t_subsegment;
	mark_invalid_and_return_path_end(
	    v_id, w_id, &t_id, &t_edge_id, &t_subsegment);

	// get common color of v and neighbors
	auto trajectory_imposition_id =
	    E[V[v_id].adjacent_edges[0]].trajectory_imposition;

	// create new segment for skipped enntries
	TGSegmentID segment_id;
	bool sort_from_t_to_s = s_id > t_id;
	if (sort_from_t_to_s) {
		segment_id =
		    create_segment_from_parts(t_subsegment, v_id, s_subsegment);
	} else {
		segment_id =
		    create_segment_from_parts(s_subsegment, v_id, t_subsegment);
	}

	// create new edge
	auto edge_id =
	    create_edge(s_id, t_id, trajectory_imposition_id, segment_id);

	// update edge references in s and t
	update_adjacent_edges(V[s_id].adjacent_edges, s_edge_id, edge_id);
	update_adjacent_edges(V[t_id].adjacent_edges, t_edge_id, edge_id);
}
TGSegmentID TrajectoryGraph::create_segment_from_parts(
    const TGSegment& s_subsegment, TGNodeID v_id, const TGSegment& t_subsegment)
{
	//
	//        s          v           t
	// ... ~~~◉—— ... ———◉—— ... ————◉~~~...

	// merge sub-parts
	TGSegment segment;
	for (size_t i = s_subsegment.size(); i > 0; i--) {
		segment.push_back(s_subsegment[i - 1]);
	}
	segment.push_back(v_id);
	for (size_t i = 0; i < t_subsegment.size(); i++) {
		segment.push_back(t_subsegment[i]);
	}
	// create segment
	auto segment_id = create_segment(segment);
	return segment_id;
}

void TrajectoryGraph::update_adjacent_edges(
    TGEdgeIDs& adjacent_edges, TGEdgeID old_edge_id, TGEdgeID new_ege_id)
{
	auto it =
	    std::find(adjacent_edges.begin(), adjacent_edges.end(), old_edge_id);
	assert(it !=
	       adjacent_edges.end()); // element to be replaced must be in the list.
	auto old_edge_index = it - adjacent_edges.begin();

	adjacent_edges[old_edge_index] = new_ege_id;
}

inline TGEdgeID TrajectoryGraph::get_edge_between(
    const TGNodeID& u_id, const TGNodeID& v_id)
{
	// u has only 2 neighbors, v can have arbitrary many.
	// r       u       v
	// ◉———————◉———e———◉

	// get either r or v.
	auto vertex_cantidate =
	    get_vertex_on_other_end(u_id, V[u_id].adjacent_edges[0]);

	if (vertex_cantidate == v_id) {
		return V[u_id].adjacent_edges[0];
	} else {
		return V[u_id].adjacent_edges[1];
	}
}

inline TGNodeID TrajectoryGraph::get_next_node_along_path(
    const TGNodeID& v_id, const TGNodeID& w_id)
{
	// returns x
	//
	// v      w      x
	// ◉——————◉——————◉...
	//
	auto candidate1_id =
	    get_vertex_on_other_end(w_id, V[w_id].adjacent_edges[0]);
	auto candidate2_id =
	    get_vertex_on_other_end(w_id, V[w_id].adjacent_edges[1]);
	if (candidate1_id == v_id) {
		return candidate2_id;
	}
	return candidate1_id;
}

void TrajectoryGraph::mark_invalid_and_return_path_end(TGNodeID v_id,
    TGNodeID w_id, TGNodeID* out_t, TGEdgeID* out_e, TGSegment* out_subsegment)
{
	// search along vw direction for non-mergeable t
	//
	// v       w           t
	// ◉———e———◉——— ... ———◉~~~...
	//
	auto e_id = get_edge_between(v_id, w_id);

	while (can_merge(V[w_id])) {
		(*out_subsegment).push_back(w_id);
		V[w_id].is_virtual = true;
		E[e_id].is_virtual = true;

		// shift by one vertex
		auto next_node_id = get_next_node_along_path(v_id, w_id);
		v_id = w_id;
		w_id = next_node_id;
		e_id = get_edge_between(v_id, w_id);
	}

	E[e_id].is_virtual = true;

	// 'return' last edge and last vertex
	*out_t = w_id;
	*out_e = e_id;
}

void TrajectoryGraph::merge_graph_segments()
{
	for (size_t v_id = 0; v_id < V.size(); v_id++) {
		if (can_merge(V[v_id])) {
			merge_in_both_directions(v_id);
		}
	}
}

void TrajectoryGraph::create_graph()
{

	// PHASE 1: Extract Nodes
	TimeTracker time_tracker;
	time_tracker.start();
	std::cout << "  ╙Nodes  " << std::endl;
	PathnodeNodeMap pathnode_to_node = create_nodes();
	time_tracker.stop();
	std::cout << "  ║╙total:  " << time_tracker.getLastMeasurment()
	          << " seconds" << std::endl;

	// PHASE 2: Add edges among nodes
	time_tracker.start();
	std::cout << "  ╙Edges  " << std::endl;
	create_edges(pathnode_to_node);
	time_tracker.stop();
	std::cout << "  ║╙total:  " << time_tracker.getLastMeasurment()
	          << " seconds" << std::endl;

	// PHASE 3: Merge segments
	time_tracker.start();
	merge_graph_segments();
	time_tracker.stop();
	std::cout << "  ║╙Segmnets:     Took " << time_tracker.getLastMeasurment()
	          << " seconds" << std::endl;
}

TGEdges TrajectoryGraph::get_segment_connections() const
{
	TGEdges edges;
	for (auto e : E) {
		if (!e.is_virtual) {
			edges.push_back(e);
		}
	}
	return edges;
}

TGSegments TrajectoryGraph::get_segments() const
{
	return S;
}

TGNodes TrajectoryGraph::get_nodes() const
{
	return V;
}

TGNodes TrajectoryGraph::get_active_nodes() const
{
	TGNodes nodes;
	for (auto v : V) {
		if (!v.is_virtual) {
			nodes.push_back(v);
		}
	}
	return nodes;
}

} // namespace pf
