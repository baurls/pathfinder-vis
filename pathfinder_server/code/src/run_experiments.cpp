#include "experiments.h"

#include "defs.h"

#include <string>

namespace
{

void printUsage()
{
	Print("USAGE: ./run_experiments");
	Print("       ./run_experiments <exp_number>");
}

} // anonymous namespace

int main(int argc, char* argv[])
{
	if (argc >= 3) {
		Print("ERROR: Too many arguments provided.");
		printUsage();
		return EXIT_FAILURE;
	}

	try {
		// without manual seed
		if (argc == 1) {
			pf::experiments::runAll();
		}
		// with single choice
		if (argc == 2) {
			unsigned int exp_number;
			try {
				exp_number = std::stoul(argv[1]);
			} catch (std::invalid_argument const& e) {
				Print("ERROR: The seed has to be an unsigned int.");
				printUsage();
				return EXIT_FAILURE;
			}
			pf::experiments::run(exp_number);
		}
		// with manual seed
		if (argc == 3) {
			unsigned int exp_number;
			unsigned int seed;
			try {
				exp_number = std::stoul(argv[1]);
				seed = std::stoul(argv[2]);
			} catch (std::invalid_argument const& e) {
				Print("ERROR: The seed has to be an unsigned int.");
				printUsage();
				return EXIT_FAILURE;
			}
			pf::experiments::run(exp_number, seed);
		}
	} catch (Exception const& e) {
		std::cerr << e.what() << std::endl;
	}

	return EXIT_SUCCESS;
}
