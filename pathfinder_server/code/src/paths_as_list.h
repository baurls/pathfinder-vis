#pragma once

#include "basic_types.h"
#include "edge_id_range.h"
#include "graph.h"
#include "timeIntervall.h"

#include <list>

namespace pf
{

class NodeRange;

class PathAsList
{
public:
	// The EdgeIndex is the position of the edge in the edges list
	using EdgeIndex = uint64_t;
	// The NodeIndex is the position of the edge which has the respective node
	// as source. Note that the last node in the path doesn't have such an edge.
	// Thus, its NodeIndex is edges.size().
	using NodeIndex = int64_t;

	PathAsList(Path const& path);

	Path makeVectorPath(Graph const& graph) const;

	// TODO: make a better iterator
	class SpecialIterator
	{

	public:
		bool operator==(const SpecialIterator& rhs) const;
		SpecialIterator& operator++();
		SpecialIterator& operator--();

		SpecialIterator(PathAsList& path_as_list);

		std::list<EdgeID>::iterator edge_it;
		std::list<TimeType>::iterator time_it;
	};

	void lastTwoEdgesToShortcut(Graph const& graph, EdgeID shortcut);
	EdgeID getPreviosEdgeID();
	EdgeID getCurrentEdgeID();

	bool iteratorIsAtEnd();
	bool iteratorIsAtBegin();

	std::list<EdgeID> edges;

	// is always exactly one more than edges
	std::list<TimeType> timeAtNodes;

	SpecialIterator iterator;
};

} // namespace pf

// This include assures that the getNodeRange functions can be used by files
// including this file. If this include wouldn't be here, NodeRange would be an
// incomplete type if node_range.cpp is not included.
#include "node_range.h"
