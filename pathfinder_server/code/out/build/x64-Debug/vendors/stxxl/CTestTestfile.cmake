# CMake generated Testfile for 
# Source directory: C:/Users/Patrick/Documents/workspace/pathfinder_server/code/vendors/stxxl
# Build directory: C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("lib")
subdirs("tools")
subdirs("examples")
subdirs("tests")
subdirs("local")
