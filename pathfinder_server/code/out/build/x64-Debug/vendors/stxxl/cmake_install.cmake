# Install script for directory: C:/Users/Patrick/Documents/workspace/pathfinder_server/code/vendors/stxxl

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "c:/users/patrick/documents/workspace/pathfinder_server/code/out/install/x64-Debug")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES "C:/Users/Patrick/Documents/workspace/pathfinder_server/code/vendors/stxxl/include/stxxl.h")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "C:/Users/Patrick/Documents/workspace/pathfinder_server/code/vendors/stxxl/include/stxxl")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/stxxl/bits" TYPE FILE FILES "C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/include/stxxl/bits/config.h")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/stxxl_debug.pc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/CMake" TYPE FILE FILES
    "C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/stxxl-version.cmake"
    "C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/CMakeFiles/stxxl-config.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/CMake/stxxl-targets.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/CMake/stxxl-targets.cmake"
         "C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/CMakeFiles/Export/CMake/stxxl-targets.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/CMake/stxxl-targets-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/CMake/stxxl-targets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/CMake" TYPE FILE FILES "C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/CMakeFiles/Export/CMake/stxxl-targets.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/CMake" TYPE FILE FILES "C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/CMakeFiles/Export/CMake/stxxl-targets-debug.cmake")
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/lib/cmake_install.cmake")
  include("C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/tools/cmake_install.cmake")
  include("C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/examples/cmake_install.cmake")
  include("C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/tests/cmake_install.cmake")
  include("C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/local/cmake_install.cmake")

endif()

