# Data description

This folder contains real world data from Saarland Trajectory data and Saarland graph.

The filtered version removed paths with invalid timestamps.

## Renamings

We renamed the files to get a consistet naming sheme. The original names were:

#### original graph name

saarland-180101-maxspeed-distance-dm-all-ch.ftxt

#### original paths name:
paths.0_2147483647.json