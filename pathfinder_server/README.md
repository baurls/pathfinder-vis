# PATHFINDER-Server

This code implements a REST server, using the 'Pistache' REST framework for the PATHFINDER application and can use all the PATHFINDER-functions to calculate trajectories via a remotely web client.

In addition, several transmission method have been implemented to allow for different used cases to be accomplished.

#### Authors

**Original work** (ProjectInf):

Shoma Kaiser | st142241@stud.uni-stuttgart.de
Patrick Lindemann | st155665@stud.uni-stuttgart.de
Enis Spahiu | st141173@stud.uni-stuttgart.de



**Extention** (Master thesis):

Lukas Baur | st141998@stud.uni-stuttgart.de



#### Version

30.11.2021



## How to use

An visual example how to use the PathfinderVis application is shown in the *docs* folder



##  Essential apt's 

```
sudo apt install git

sudo apt install cmake

sudo apt install make

sudo apt-get install build-essential

sudo apt-get install libssl-dev

sudo apt-get install libcurl3-dev

```

### Dependencies

 * A compiler supporting C++11
 * CMake
 * stxxl (as submodule)


## How to build Pistache-lib

### Build Commands

To *build the easy way*, enter the `code/libs/pistache` directory and execute:
```
cd ./pathfinder_server/code/libs/pistache

sh build.sh
```

To *manually build*, enter the `code/libs/pistache` directory and execute the following commands:

```
cd ./pathfinder_server/code/libs/pistache


mkdir build
cd build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..
make
sudo make install
```

## How to build Pathfinder-Server


### Build Commands

To *build the easy way*, enter the `code` directory and execute:

```
./build.sh
```

To *manually build*, enter the `code` directory and execute the following commands:

```
mkdir build
cd build
cmake ..
make -j
```

## If the error: 'cannot open shared object file' occurs while running ./pathfinder_server : 

```
sudo /sbin/ldconfig -v
```

## Optional libs

```
sudo apt-get update -y
sudo apt-get install -y rapidjson-dev
```

## How to use

### Configfile 

Set the Configfile: 'settings.txt' in the '/pathfinder_server/code' with the necessary Paramaeters (absolute path): 


```
graph_filename = ./*.ftxt

path_filename = ./*.json

(Optional)generatePaths = <Integer>    // (n = 0 || n = 1)  <0> = off (default) / <1> = on

(Optional)number_of_random_paths = <Integer>   //(n => 1)

// *if* (generatePaths == 1) *then* random paths with the number_of_random_paths will be generated.
   *else*  the path_filename will be utilized.

smoothness = <Double>		// (0.0 < smoothness <= 10.0) // from the value 1 is no longer unpacked to the lowest CH level
max_zoom = <Integer>		// (0 <= max_zoom <= 17) // default value is 17

```


## How to run the server

To run, enter the `code` directory and execute:


```
./build/pathfinder_server
```

## How to stop the server


```
press `crtl`-key + `c`-key
```

## Unit tests of the 'local' PATHFINDER

### Run Test

The local unit test will be executed via the command:

```
./run_tests
```

